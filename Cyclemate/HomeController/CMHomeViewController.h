//
//  CMHomeViewController.h
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Accelerator.h"
#import "CMAppDelegate.h"
#import "CMSpecialDetailController.h"
@interface CMHomeViewController : UIViewController<CLLocationManagerDelegate,SHXAcceleratorDelegate>
{
    NSString          *bikeColor;
    NSDictionary      *bikeNumberDic;

    NSDateFormatter   *df;
    CLLocationManager *locationManager;
    
    BOOL              isUpdateCurrLocation;
    
    NSMutableArray *fivedaysArr;
    CLGeocoder     *geocoder;
    NSInteger       locationRequestID;
    NSString          *venueID;
    NSString          *campaignName;
    NSString          *payLoad;
    NSString      *skyhookResponse;
}
-(IBAction)disPlayNotificationClick:(id)sender;
-(void)showLanguageSelection;
-(void)automaticallyEnterviewPOPupMobile;
@property (assign, nonatomic) NSInteger locationRequestID;

@end
