 //
//  CMHomeViewController.m
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import  "CMWeatherAlertVC.h"
#import  "CMHomeViewController.h"
#import   "CMBikeInfoController.h"
#import  "CMBuddyNotificationVC.h"
#import "CMParkBikeController.h"
#import "CMSaveRideController.h"
#import "CMDirectDealerMailVC.h"

#import "CMMessageViewController.h"
#import "CMBuddyContactsController.h"
#import "CMNewBikeInfoController.h"

#import "CMEventsController.h"
#import "CMSpecialController.h"

#import "NFLanguageVC.h"
#import "Mixpanel/Mixpanel.h"

@interface CMHomeViewController ()<NFLanguagePopupDelegate,UITextFieldDelegate>
{
    IBOutlet UILabel *daysLbl;
    IBOutlet UILabel *tempLbl;
    NSArray *itemsArry;
    NSMutableDictionary *currDayWeatherDic;
    
    
    IBOutlet UILabel *maxMinTempLbl;
    IBOutlet UILabel *dayTempLbl;
    IBOutlet UILabel *descriptionLbl;
    IBOutlet UILabel *currDayNameLbl;
    
    IBOutlet UILabel *cityNameLbl;
    
    IBOutlet UIImageView *weatherIcon;
    
    IBOutlet UIView *messageNotyView;
    IBOutlet UILabel *messageNotyLbl;
    
    NSString *messageNotyText;
    
    //pressure""humidity
    
    IBOutlet UICollectionView *bikeCollectionView;
    
    IBOutlet UIImageView *baseImageView;
    IBOutlet UIImageView *logoImageView;
    
    UIAlertView *mobileAlertView;
    
}
@property (weak, nonatomic) SHXAccelerator *accelerator;
@end

@implementation CMHomeViewController
@synthesize locationRequestID;





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)getShareParkingListData{
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    NSString * ownerId    = [dic stringValueForKey:@"OwnerId"];
    
    
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetShareLocation/OwnerId=%@",Service_URL,ownerId];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            
            NSArray *arr = [resultDict objectForKey:@"GetShareLocationResult"];
            if ([arr count]) {
                
                NSDictionary *onj = [arr objectAtIndex:0];
                NSLog(@"*** GetShareLocationResult %@ **",onj);
                
                
                
                NSNumber *lat = [NSNumber numberWithFloat:[[onj objectForKey:@"PostionLet"] floatValue]];
                NSNumber *lon = [NSNumber numberWithFloat:[[onj objectForKey:@"PostionLon"] floatValue]];
                NSDictionary *userLocation=@{@"lat":lat,@"long":lon};
                
                [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:BIKE_LOCATION_KEY];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [UIAppDelegate.userDefault setBool:YES forKey:IsBikeParkKey];
                [bikeCollectionView reloadData];
            }
        }
        
    }];
    
    
}



-(void)hideMessagePopUp{
    
    messageNotyView.hidden = YES;
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
}
-(IBAction)disPlayNotificationClick:(id)sender{
    CMDirectDealerMailVC *secondDetailViewController = [[CMDirectDealerMailVC alloc] initWithNibName:@"CMDirectDealerMailVC" bundle:nil];
    secondDetailViewController.isSendBuddy = NO;
    secondDetailViewController.controller = self;
    secondDetailViewController.massegeText = messageNotyText;
    
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
}
-(NSString *)URLEncodeString:(NSString *)string {
    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)string, NULL, CFSTR("% '\"?=&+<>;:-"), kCFStringEncodingUTF8));
    
    return result;
}

-(void)postData{
    
    NSDictionary *tmp = [[NSDictionary alloc] initWithObjectsAndKeys:
                         @"gfgf", @"Description",
                         @"1", @"DealerId",
                         @"1", @"CycleId",
                         @"1", @"Flag",
                         nil];
    NSError *error;
    NSData *postdata = nil;//[NSJSONSerialization dataWithJSONObject:tmp options:0 error:&error];
    
    
    
    // NSString *str = @"{\"Description\": \"MY vehicle Test\", \"DealerId\": \"1\",\"Flag\": \"1\", \"CycleId\": \"1\"}";
    
    //  NSString  *postStr = [NSString stringWithFormat:@"objDealerEventBO=%@",str];
    
    NSURL *url = [NSURL URLWithString:@"http://14.141.92.86/TroubleShootingData/GetData.aspx"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //  NSData *postData = [postStr dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    [request setValue:[NSString stringWithFormat:@"%d", [postdata length]] forHTTPHeaderField:@"Content-Length"];
    
    
    [request setHTTPBody: postdata];
    
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    //    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
    NSLog(@"requestReply: %@", requestReply);
}


-(void)markAsRead:(NSString *)notifyType{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             ownerId, @"OwnerId",
                             @"0", @"CycleId",
                             notifyType, @"NotificationType",nil];
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/MarkIsReadNotification",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** MarkIsReadNotification %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}
-(void)checkNumberForGride{
    
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    NSString *stringURL =[NSString stringWithFormat:@"%@/GetAllNotification/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            if ([[resultDict objectForKey:@"GetAllNotificationResult"] count]){
                bikeNumberDic = [[resultDict objectForKey:@"GetAllNotificationResult"] objectAtIndex:0];
                
                NSString *counter = [bikeNumberDic stringValueForKey:@"TotalCount"];
                if ([counter length]) {
                    [UIApplication sharedApplication].applicationIconBadgeNumber = [counter integerValue];
                }else
                    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                
                
                
                
                [bikeCollectionView reloadData];
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:bikeNumberDic];
                [UIAppDelegate.userDefault setObject:data forKey:CycleNotificationKey];
            }
            
            
            
        }
        
    }];
    
    
    
    
}


-(IBAction)postOwnerInfo{
    
    
    
    NSString *tempUrl = @"http://mroiapi.mobileroi.com/mobileroiapi/v1/device/updateprofile";
    
    
    
    NSDictionary *dict1 = @{@"key":@"name",@"value":@"Rajesh"};
    
    NSDictionary *dict2 = @{@"key":@"age",@"value":@"23"};
    
    NSArray *array = [NSArray arrayWithObjects:dict1,dict2, nil];
    
    
    
    NSDictionary *dict3 = @{@"device_id":@"abcd",@"app_id":@"abcdes",@"listVars":array};
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:tempUrl param:dict3 requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
        }}];
    
}

- (void)myButtonHandlerAction {
    /*
     // Create the item to share (in this example, a url)
     NSURL *url = [NSURL URLWithString:@"http://getsharekit.com"];
     SHKItem *item = [SHKItem URL:url title:@"ShareKit is Awesome!" contentType:SHKURLContentTypeUndefined];
     
     // Get the ShareKit action sheet
     SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
     
     // ShareKit detects top view controller (the one intended to present ShareKit UI) automatically,
     // but sometimes it may not FIND one. To be safe, set it explicitly
     [SHK setRootViewController:self];
     
     // Display the action sheet
     if (NSClassFromString(@"UIAlertController")) {
     
     //iOS 8+
     SHKAlertController *alertController = [SHKAlertController actionSheetForItem:item];
     [alertController setModalPresentationStyle:UIModalPresentationPopover];
     UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
     popPresenter.barButtonItem = self.toolbarItems[1];
     [self presentViewController:alertController animated:YES completion:nil];
     
     } else {
     
     //deprecated
     //  SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
     [actionSheet showInView:self.navigationController.view];
     }
     */
}


-(IBAction)weatherBtnClicked:(id)sender
{
    
    //fetch dealer info
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleDealerInfoKey]];
    NSLog(@"dealer info..%@",dic);
    CMWeatherAlertVC *UPC = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMWeatherAlertVC"];
    if (dic!=nil && [dic objectForKey:@"OrganizationName"])
    {
        UPC.selectedDealer=[dic objectForKey:@"OrganizationName"];
    }
    else
        UPC.selectedDealer=@"";
    [self.navigationController pushViewController:UPC animated:YES];
    
    
    
}


-(IBAction)topMenuBtnClicked:(id)sender
{
    
}

-(IBAction)backBtnClicked:(id)sender
{
    [UIAppDelegate setSelectedController:[(UIButton *)sender tag]];
    
    
}
-(IBAction)topleftBtnClicked:(id)sender{
    
    
}

-(IBAction)bottomViewBtnClicked:(id)sender
{
    [UIAppDelegate setSelectedController:[(UIButton *)sender tag]];
    
}

-(void)checkForBikeColor{
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    //OwnerCycleListKey
    [[CMNetworkManager sharedInstance] getResponse:[NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=%@",Service_URL,@"0",ownerId] requestHandler:^(NSDictionary *resultDict, NSError *error) {
        NSString *cycleID = @"";
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[resultDict objectForKey:@"GetCycleResult"]];
            [UIAppDelegate.userDefault setObject:data forKey:OwnerCycleListKey];
            
            if ([[resultDict objectForKey:@"GetCycleResult"] count]) {
                cycleID =  [[[resultDict objectForKey:@"GetCycleResult"] objectAtIndex:0] stringValueForKey:@"CycleId"];
                
                //OwnerCycleListKey
                
                
                
                
                NSString *stringURL =[NSString stringWithFormat:@"%@%@",GetCyclePreference,cycleID];
                
                [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                        NSLog(@"***  %@ **",resultDict);
                        
                        NSDictionary *midDic = nil;
                        if ([[resultDict objectForKey:@"GetCyclePreferenceResult"] count]){
                            midDic = [[resultDict objectForKey:@"GetCyclePreferenceResult"] objectAtIndex:0];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:midDic];
                            [UIAppDelegate.userDefault setObject:data forKey:CyclePreferenceKey];
                        }
                        else
                            [UIAppDelegate.userDefault removeObjectForKey:CyclePreferenceKey];
                        
                        
                        NSString *cycleId = [midDic stringValueForKey:@"PrimaryBikeId"] ;
                        if (![cycleId length]) {
                            cycleId = [midDic stringValueForKey:@"CycleId"] ;
                        }
                        //CycleId
                        
                        NSString *stringURL =[NSString stringWithFormat:@"%@%@/DealerId=%@",GetCycleHealthColor,cycleId,DealerID];
                        
                        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                NSLog(@"***  %@ **",resultDict);
                                
                                NSDictionary *dic;
                                
                                if ([[resultDict objectForKey:@"GetCycleHealthColorResult"] count]) dic = [[resultDict objectForKey:@"GetCycleHealthColorResult"] objectAtIndex:0];
                                
                                bikeColor = [dic stringValueForKey:@"Color"];
                                
                                
                                [bikeCollectionView reloadData];
                                
                                
                            }
                        }];
                        
                        
                    }
                    
                }];
            }
        }
        
        
        
        
    }];
    
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    //  [self callSkyhookAPI];
    NSLog(@"DealerID-- >%@",DealerID);
    
    [self.navigationController setNavigationBarHidden:YES];
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    NSLog(@"%@",dic);
    
    NSString *ownerId    = [dic stringValueForKey:@"OwnerId"];
    NSString *cycleCount = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:ValidationUUIDKey]] stringValueForKey:@"CycleCount"];
    
    
    if ([ownerId intValue] && [cycleCount intValue]){
        [self getShareParkingListData];
        
        [self checkForBikeColor];
        [self checkNumberForGride];
    }
    
    isUpdateCurrLocation = YES;
    [self startLocationRequest];
    
    [bikeCollectionView reloadData];
    
    [self loadDealerInfo];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    
}

- (void)cancelButtonClicked:(NFLanguageVC*)secondDetailViewController{
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleDealerInfoKey]];
    
    
    baseImageView.image = nil;
    logoImageView.image = nil;
    
    NSString *baseImagePath = [dic stringValueForKey:@"BackgroundImage"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
    [baseImageView setImageWithURL:[NSURL URLWithString:baseImagePath] placeholderImage:nil];
    
    NSString *logoImagePath = [dic stringValueForKey:@"LogoImagePath"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
    [logoImageView setImageWithURL:[NSURL URLWithString:logoImagePath] placeholderImage:nil];
    
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNo = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    NSLog(@"ayyooooo...");
    if (!mobileNo) {
        [self addYourMobileNumber];
    }
    else
    {
        [self checkForOwnerWithOwneServer];
    }
    
    //  [self addYourMobileNumber];
    
    [UIAppDelegate insertUUID];
    
    [self checkForBikeColor];
    [self checkNumberForGride];
    UIAppDelegate.rearVC.selectedIndex = 4;
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

-(void)languageBtnClicked
{
    UIStoryboard* storyBoard2 = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CMVINEntryViewController * viewController1 = [storyBoard2 instantiateViewControllerWithIdentifier:@"CMVINEntryViewController"];
    
    viewController1._CMHomeViewController=self;
    [self presentPopupViewController:viewController1 animationType:MJPopupViewAnimationFade];
    //    CMAppDelegate *_CMAppDelegate=(CMAppDelegate *)
    //    [[UIApplication sharedApplication] delegate];
    //    _CMAppDelegate.rearVC.selectedIndex=0;
}
-(void)showLanguageSelection
{
    UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NFLanguageVC * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"NFLanguageVC"];
    viewController.delegate= self;
    
    [self presentPopupViewController:viewController animationType:MJPopupViewAnimationFade];
}

-(void)automaticallyEnterviewPOPupMobile
{
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleDealerInfoKey]];
    
    
    baseImageView.image = nil;
    logoImageView.image = nil;
    
    NSString *baseImagePath = [dic stringValueForKey:@"BackgroundImage"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
    [baseImageView setImageWithURL:[NSURL URLWithString:baseImagePath] placeholderImage:nil];
    
    NSString *logoImagePath = [dic stringValueForKey:@"LogoImagePath"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
    [logoImageView setImageWithURL:[NSURL URLWithString:logoImagePath] placeholderImage:nil];
    
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNo = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    NSLog(@"ayyooooo...");
    //    if (!mobileNo) {
    //        [self addYourMobileNumber];
    //    }
    //    else
    //    {
    //        [self checkForOwnerWithOwneServer];
    //    }
    
    [self addYourMobileNumber];
    
    [UIAppDelegate insertUUID];
    
    [self checkForBikeColor];
    [self checkNumberForGride];
    UIAppDelegate.rearVC.selectedIndex = 4;
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)addYourMobileNumber{
    
    
    
    //    if (!mobileAlertView) {
    //        mobileAlertView = [[UIAlertView alloc]
    //                           initWithTitle:@"MOBILE NUMBER"
    //                           message:@"Please enter your mobile number for SMS alerts."
    //                           delegate:self
    //                           cancelButtonTitle:@"Cancel"
    //                           otherButtonTitles:@"Add", nil];
    //        [mobileAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    //        /* Display a numerical keypad for this text field */
    //        UITextField *textField = [mobileAlertView textFieldAtIndex:0];
    //        textField.keyboardType = UIKeyboardTypePhonePad;
    //    }
    
    
    if (!mobileAlertView) {
        //        mobileAlertView = [[UIAlertView alloc]
        //                           initWithTitle:@"MOBILE NUMBER"
        //                           message:@"Please enter your mobile number for SMS alerts. \n By Cancelling you will not receive notifications, such as promotions and buddy requests"
        //                           delegate:self
        //                           cancelButtonTitle:@"Cancel"
        //                           otherButtonTitles:@"Add", nil];
        
        mobileAlertView = [[UIAlertView alloc]
                           initWithTitle:@"MOBILE NUMBER"
                           message:@"Please enter your mobile number for SMS alerts. \n By cancelling you will not receive notifications; such as, promotions and buddy requests"
                           delegate:self
                           cancelButtonTitle:@"Cancel"
                           otherButtonTitles:@"Add", nil];
        [mobileAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        mobileAlertView.tag = 101;
        /* Display a numerical keypad for this text field */
        UITextField *textField = [mobileAlertView textFieldAtIndex:0];
        textField.delegate = self;
        textField.text = [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
        
        textField.keyboardType = UIKeyboardTypePhonePad;
    }
    
    
    
    
    [mobileAlertView show];
    
    
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    int length = (int)[self getLength:textField.text];
    //NSLog(@"Length  =  %d ",length);
    
    if(length == 10)
    {
        if(range.length == 0)
            return NO;
    }
    
    if(length == 3)
    {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"%@-",num];
        
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 6)
    {
        NSString *num = [self formatNumber:textField.text];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);
        textField.text = [NSString stringWithFormat:@"%@-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
    return YES;
    
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}


- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}
#pragma mark - UIAlertViewDelegate

//Now below code will check if uitextfield value.
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    
    
    /*if (![inputText length]) {
     [CMHelper alertMessageWithText:@"Please fill in Mobile Number."];
     return NO;
     
     }else if (![Utilities isNumeric:inputText]) {
     
     [CMHelper alertMessageWithText:@"Please enter vaild Mobile Number."];
     return NO;
     
     
     }else  if (inputText.length<10 || inputText.length>10){
     
     [CMHelper alertMessageWithText:@"Mobile Number should be 10 digit."];
     return NO;
     
     }*/
    if( [inputText length] > 0)
    {
        //text field value is greater than zero ,then Done button will appear as blue,or else it will be blurred
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    // [self.view endEditing:YES];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        NSString *strMobile = textField.text;
        NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        textField.text = [[textField.text componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
        
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            
            if (![textField.text length]) {
                
                [[UIAlertView alertViewWithTitle:@""
                                         message:@"Please fill in Mobile Number." cancelButtonTitle:nil
                               otherButtonTitles:@[@"OK"]
                                       onDismiss:^(int buttonIndex)
                  {
                      [self addYourMobileNumber];
                  } onCancel:^{   }] show];
                
                
                
            }else if (![Utilities isNumeric:textField.text]) {
                [[UIAlertView alertViewWithTitle:@""
                                         message:@"Please enter vaild Mobile Number." cancelButtonTitle:nil otherButtonTitles:@[@"OK"]
                                       onDismiss:^(int buttonIndex) {
                                           
                                           [self addYourMobileNumber];  } onCancel:^{   }] show];
                
                
            }else  if (textField.text.length<10 || textField.text.length>10){
                [[UIAlertView alertViewWithTitle:@""
                                         message:@"Mobile Number should be 10 digit." cancelButtonTitle:nil otherButtonTitles:@[@"OK"]
                                       onDismiss:^(int buttonIndex) {
                                           [self addYourMobileNumber];  } onCancel:^{   }] show];
                
            }else
            {
                [UIAppDelegate.userDefault setObject:strMobile forKey:@"MOBILENO"];
                [self checkForExistingOwnerWithOwneServer];
                
                
                //                if ([[UIAppDelegate.userDefault objectForKey:@"VIN_Record"] isKindOfClass:[NSMutableDictionary class]] && !([UIAppDelegate.userDefault objectForKey:@"VIN_Record"]==NULL))
                //                {
                //                    [self prePopulateTextFieldsWithVINDetails:[UIAppDelegate.userDefault objectForKey:@"VIN_Record"]];
                //                }
                //                else
                //                {
                //                   [self checkForOwnerWithOwneServer];
                //                }
            }
        }
    }
    
}
-(void)prePopulateTextFieldsWithVINDetails:(NSDictionary *)VINDict
{
    //    mcOwnernameField.text=[VINDict objectForKey:@"OwnerFirstName"];
    //    mcOwnerLastNameField.text=[VINDict objectForKey:@"OwnerLastName"];
    //    addressField.text=[VINDict objectForKey:@"OwnerAddressLine1"];
    //    cityField.text=[VINDict objectForKey:@"OwnerCity"];
    //    stateField.text=[VINDict objectForKey:@"OwnerState"];
    //    zipField.text=[VINDict objectForKey:@"OwnerZipCode"];
    //    //mobileField.text=[VINDict objectForKey:@"OwnerPhone"];
    //    mobileField.text=[UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    //    altMobileField.text=@"";
    //    emailField.text=[VINDict objectForKey:@"OwnerEmail"];
    
    
    
    NSString *firstName =[[VINDict objectForKey:@"OwnerFirstName"] uppercaseString];
    NSString *lastName =[[VINDict objectForKey:@"OwnerLastName"] uppercaseString];
    NSString *address =[VINDict objectForKey:@"OwnerAddressLine1"];
    NSString *city =[VINDict objectForKey:@"OwnerCity"];
    NSString *state =[VINDict objectForKey:@"OwnerState"];
    NSString *zip =[VINDict objectForKey:@"OwnerZipCode"];
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobile = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    NSString *email=[VINDict objectForKey:@"OwnerEmail"];
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys: firstName, @"FName",lastName, @"LName", address, @"Address",
                             // [cityTypeBtn currentTitle] , @"City", //cityField.text
                             city, @"City",
                             state, @"State", //   stateField.text
                             zip, @"Zip",
                             mobile, @"Mobile",
                             @"", @"Phone",
                             email, @"Email",
                             @"1", @"StyleId",
                             @"", @"DLPhoto",
                             @"", @"DLPhotoPath",
                             @"", @"PhotoPath",
                             @"", @"Photo",
                             @"1343hfdk28krk", @"DLNumber",
                             @"11/10/14", @"DLExpiration",
                             @"1", @"IsActive",
                             @"0", @"OwnerId",[FCUUID uuidForDevice],@"UUid",DealerID,@"DealerId",
                             nil];
    
    NSString *clientServiceURL = @"http://7mg.biglistof.com/7mg_r2r_update.php?";//[NSString stringWithFormat:@"http://7mg.biglistof.com/7mg_r2r_update.php?mn=%@&did=%@&first=%@&last=%@&address=%@&city=%@&state=%@&zip=%@&alt_phone=%@&email=%@",DealerID, mobileField.text,firstName,lastName,addressField.text,[cityTypeBtn currentTitle], [stateTypeBtn selectedObjectID],zipField.text,altMobileField.text,emailField.text];
    
    
    NSDictionary *clientDic = [NSDictionary dictionaryWithObjectsAndKeys:DealerID,@"did", mobile,@"mn",firstName,@"first",lastName,@"last",address,@"address",city,@"city", state,@"state",zip,@"zip",mobile,@"alt_phone",email,@"email", nil];
    
    [[CMNetworkManager sharedInstance] postUserDataOnClientServer:clientServiceURL withPostParameters:clientDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        NSLog(@"%@",resultDict);
    }];//postUserDataOnClientServer
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateOwner",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        [SVProgressHUD dismiss];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1)
            {
                
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                if (userDefaults) {
                    [userDefaults setObject:[resultDict stringValueForKey:@"response"] forKey:@"userID"];
                    [userDefaults synchronize];
                }
                
                [self checkForOwnerWithOwneServer];
            }
        }
    }];
    
    
    
}

-(void)checkForExistingOwnerWithOwneServer{
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNo = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    NSString *stringURL =[NSString stringWithFormat:@"%@/GetOwnerByMobile/Mobile=%@",Service_URL,mobileNo];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic;
            
            if ([[resultDict objectForKey:@"GetOwnerByMobileResult"] count]){
                dic = [[resultDict objectForKey:@"GetOwnerByMobileResult"] objectAtIndex:0];
                
            }
            NSLog(@"dic count  =>%lu",(unsigned long)[dic count]);
            if ([dic count] > 0)
            {
                [[UIAlertView alertViewWithTitle:@""
                                         message:@"This mobile number already used. Please try with another mobile number." cancelButtonTitle:nil
                               otherButtonTitles:@[@"OK"]
                                       onDismiss:^(int buttonIndex)
                  {
                      [self addYourMobileNumber];
                  } onCancel:^{   }] show];
                
                
            }else{
                
                if ([[UIAppDelegate.userDefault objectForKey:@"VIN_Record"] isKindOfClass:[NSMutableDictionary class]] && !([UIAppDelegate.userDefault objectForKey:@"VIN_Record"]==NULL))
                {
                    [self prePopulateTextFieldsWithVINDetails:[UIAppDelegate.userDefault objectForKey:@"VIN_Record"]];
                }
                else
                {
                    [self checkForOwnerWithOwneServer];
                }
            }
            
        }
    }];
    
    
    
}

-(void)checkForOwnerWithOwneServer{
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNo = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    NSString *stringURL =[NSString stringWithFormat:@"%@/GetOwnerByMobile/Mobile=%@",Service_URL,mobileNo];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic;
            
            if ([[resultDict objectForKey:@"GetOwnerByMobileResult"] count]){
                dic = [[resultDict objectForKey:@"GetOwnerByMobileResult"] objectAtIndex:0];
            }
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
            [UIAppDelegate.userDefault setObject:data forKey:CycleValidationUUIDKey];
            
            NSArray *dataDisplay = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
            NSLog(@"dataDisplay  %lu **",(unsigned long)dataDisplay.count);
            if(dataDisplay.count>0)
            {
            NSString *fName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"FName"];
            
            NSString *lName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"LName"];
            
            NSString *email    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"Email"];
            
            NSString *dealerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"DealerId"];
            
            NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel identify:ownerId];
            
            [mixpanel.people set:@{
                                   @"FirstName": fName,
                                   @"LastName": lName,
                                   @"Email": email,
                                   @"DealerId": dealerId
                                   }];
           }
            
            if ([[UIAppDelegate.userDefault objectForKey:@"VIN_Record"] isKindOfClass:[NSMutableDictionary class]] && !([UIAppDelegate.userDefault objectForKey:@"VIN_Record"]==NULL))
            {
                CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                [self.navigationController pushViewController:viewController animated:YES];
            }
            else
            {
                if (![dic count]) {
                    [self checkForOwnerWithClientServer];
                }else{
                    UIAppDelegate.rearVC.selectedIndex = 3;
                    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                    [notificationCenter postNotificationName:CM_OWNER_REGISTER object:self];
                    
                    
                }
            }
        }
    }];
    
    
    
}
-(void)checkForOwnerWithClientServer{
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNumber = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    
    [[CMNetworkManager sharedInstance] getResponseWithPhone:mobileNumber requestHandler:^(NSDictionary *dic, NSError *error) {
        NSLog(@"*** getbrands %@ **",dic);
        
        if ([[[dic stringValueForKey:@"status"] lowercaseString] isEqualToString:@"ok"]) {
            
            NSDictionary *newDic = [NSDictionary  dictionaryWithObjectsAndKeys:[dic stringValueForKey:@"first"],@"FName",[dic stringValueForKey:@"last"],@"LName",[dic stringValueForKey:@"address"],@"Address",[dic stringValueForKey:@"mobile"],@"Mobile",[dic stringValueForKey:@"city"],@"City",[dic stringValueForKey:@"state"],@"State",[dic stringValueForKey:@"zip"],@"Zip",[dic stringValueForKey:@"email"],@"Email",[dic stringValueForKey:@"alt_phone"],@"Phone", nil];
            
            
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:newDic];
            [UIAppDelegate.userDefault setObject:data forKey:CycleValidationUUIDKey];
            [UIAppDelegate postBlankOwnerInfo];
            
            NSArray *dataDisplay = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
            NSLog(@"dataDisplay  %lu **",(unsigned long)dataDisplay.count);
            if(dataDisplay.count>0)
            {
                NSString *fName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"FName"];
                
                NSString *lName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"LName"];
                
                NSString *email    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"Email"];
                
                NSString *dealerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"DealerId"];
                
                NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel identify:ownerId];
                
                [mixpanel.people set:@{
                                       @"FirstName": fName,
                                       @"LastName": lName,
                                       @"Email": email,
                                       @"DealerId": dealerId
                                       }];
            }

            
        }else{
            
            UIAppDelegate.rearVC.selectedIndex = 3;
            
        }
        
    }];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.accelerator = ((CMAppDelegate*)[UIApplication sharedApplication].delegate).accelerator;
    
    if (self.accelerator != nil)
    {
        self.accelerator.delegate = self;
    }
    
    if (!IS_IPHONE_5) {
        bikeCollectionView.scrollEnabled = YES;
    }
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleDealerInfoKey]];
    NSLog(@"dealer info..%@",dic);
    
    if (!dic) [self languageBtnClicked];
    else{
        
        
        baseImageView.image = nil;
        logoImageView.image = nil;
        NSString *baseImagePath = [dic stringValueForKey:@"BackgroundImage"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
        [baseImageView setImageWithURL:[NSURL URLWithString:baseImagePath] placeholderImage:nil];
        
        NSString *logoImagePath = [dic stringValueForKey:@"LogoImagePath"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
        [logoImageView setImageWithURL:[NSURL URLWithString:logoImagePath] placeholderImage:nil];
        
    }
    
#if TARGET_IPHONE_SIMULATOR
    // if (dic) [self languageBtnClicked];
#endif
    messageNotyView.hidden = YES;
    
    messageNotyLbl.layer.borderColor = [UIColor colorWithRed:31.0f/255.0f green:157.0/255.0f blue:193.0/255.0f alpha:1].CGColor;;
    messageNotyLbl.layer.borderWidth = 1.0;
    messageNotyLbl.layer.cornerRadius = 7;
    
    // Do any additional setup after loading the view.
    
    // itemsArry = [NSArray arrayWithObjects:@"mail",@"service",@"health",@"weather",@"events",@"specials",@"buddy",@"my rides",@"park", nil];
    
    
    itemsArry = [NSArray arrayWithObjects:@"weather",@"mail",@"buddy",@"service",@"health",@"my rides",@"park",@"specials",@"events", nil];
    
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:HomeBootomTabBar]];
    
    // NSDateFormatter *
    df=[[NSDateFormatter alloc]init];
    [df setDateFormat:@"E dd/MM/yy"];
    
    [self setUpWeatherData];
    
    
    
}

-(void)loadDealerInfo{
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleDealerInfoKey]];
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetDealer/DealerId=%@",Service_URL,[dic stringValueForKey:@"DealerId"]];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSArray *dealerArr = [resultDict objectForKey:@"GetDealerResult"];
            
            if ([dealerArr count]) {
                
                NSDictionary *dic = [dealerArr objectAtIndex:0];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
                [UIAppDelegate.userDefault setObject:data forKey:CycleDealerInfoKey];
                
                
                baseImageView.image = nil;
                logoImageView.image = nil;
                NSString *baseImagePath = [dic stringValueForKey:@"BackgroundImage"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
                [baseImageView setImageWithURL:[NSURL URLWithString:baseImagePath] placeholderImage:nil];
                
                NSString *logoImagePath = [dic stringValueForKey:@"LogoImagePath"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
                [logoImageView setImageWithURL:[NSURL URLWithString:logoImagePath] placeholderImage:nil];
            }
            
        }
    }];
    
}
-(void)getWeatherData:(NSString *)withCity{
    
    
    //NSString *weatherApi = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?lat=%@&lon=%@&cnt=7&mode=json&units=imperial",[UIAppDelegate.userDefault objectForKey:@"Lat"],[UIAppDelegate.userDefault objectForKey:@"Lon"]];
    NSString *weatherApi = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=%@&cnt=7&mode=json&units=imperial&APPID=5a3c099d390d1f4f986412264d3da193",withCity];
    
    
    //  q=%@
    
    
    
    [[CMNetworkManager sharedInstance] getResponse:weatherApi requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            //  NSLog(@"*** getbrands %@ **",resultDict);
            
            NSData *wholeleData = [NSKeyedArchiver archivedDataWithRootObject:resultDict];
            [UIAppDelegate.userDefault setObject:wholeleData forKey:Cycle5DaysWeatherKey];
            
            [self setUpWeatherData];
            
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_GET_WEATHERDATA object:self];
            
        }
    }];
    
}
-(void)setUpWeatherData{
    __block NSString *daysStr =@"";
    __block NSString *tempStr =@"";
    
    
    NSDictionary * resultDict = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:Cycle5DaysWeatherKey]];;
    NSArray *listData = [resultDict objectForKey:@"list"];
    
    [listData enumerateObjectsUsingBlock:^(NSDictionary *dic,NSUInteger idx,BOOL *stop) {
        
        //for (NSDictionary *dic in listData ) {
        
        
        NSTimeInterval interval = [[dic stringValueForKey:@"dt"] doubleValue];
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        [df setTimeZone:sourceTimeZone];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
        NSLog(@"%@",[df stringFromDate:date]);
        
        if (idx!=0) {
            [df setDateFormat:@"E"];
            daysStr = [daysStr stringByAppendingFormat:@"%@    ",[[[df stringFromDate:date] substringWithRange:NSMakeRange(0,2)]uppercaseString]];
            
            NSString *digitStr =[NSString stringWithFormat:@"%.0f",([[[dic objectForKey:@"temp"] objectForKey:@"day"] floatValue])];
            
            if ([digitStr length]==1)
                tempStr = [tempStr stringByAppendingFormat:@" %@      ",digitStr];
            else  if ([digitStr length]==2)
                tempStr = [tempStr stringByAppendingFormat:@"%@     ",digitStr];
            else  if ([digitStr length]==3)
                tempStr = [tempStr stringByAppendingFormat:@"%@    ",digitStr];
            else  if ([digitStr length]==4)
                tempStr = [tempStr stringByAppendingFormat:@"%@   ",digitStr];
            
        }
        else{
            currDayWeatherDic = [dic mutableCopy];
            
            [currDayWeatherDic setObject:[resultDict objectForKey:@"city"] forKey:@"City"];
            
            [df setDateFormat:@"EEEE"];
            currDayNameLbl.text = [[df stringFromDate:date] uppercaseString];
            
            maxMinTempLbl.text = [NSString stringWithFormat:@"H%.0fº  L%.0fº",([[[dic objectForKey:@"temp"] objectForKey:@"max"] floatValue]),([[[dic objectForKey:@"temp"] objectForKey:@"min"] floatValue])];
            //max
            dayTempLbl.text = [NSString stringWithFormat:@"%.0fº",([[[dic objectForKey:@"temp"] objectForKey:@"day"] floatValue])];;
            descriptionLbl.text = [[[[dic  objectForKey:@"weather"] objectAtIndex:0]objectForKey:@"description"] uppercaseString];
            
            [weatherIcon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[[[dic  objectForKey:@"weather"] objectAtIndex:0]objectForKey:@"icon"]]]];
            
            
            
        }
        
        
        
    }];
    
    NSLog(@"*** getbrands %@ **%@",daysStr ,tempStr);
    daysLbl.text =daysStr;
    tempLbl.text =tempStr;
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currDayWeatherDic];
    [UIAppDelegate.userDefault setObject:data forKey:CycleWeatherKey];
    
    NSString *address = [UIAppDelegate.userDefault objectForKey:CycleLocationNameKey];
    
    if ([address length])
        cityNameLbl.text = address;
    else
        cityNameLbl.text = [NSString stringWithFormat:@"%@, %@",[[currDayWeatherDic objectForKey:@"City"] stringValueForKey:@"name"],[[currDayWeatherDic objectForKey:@"City"]stringValueForKey:@"country"]];
    
    cityNameLbl.text = [cityNameLbl.text stringByReplacingOccurrencesOfString:@"(null), (null)" withString:@"Searching.."];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return itemsArry.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *identifier = @"ItemCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.layer.backgroundColor=[[UIColor whiteColor] CGColor];
    //    cell.layer.cornerRadius = 1;
    //    cell.layer.borderWidth = 1;
    //    cell.layer.masksToBounds = YES;
    //    cell.layer.borderColor=[[UIColor colorWithRed:201/255.0f green:208.0/255.0f blue:214.0/255.0f alpha:1] CGColor];
    //
    NSString *title =[itemsArry objectAtIndex:indexPath.row];
    
    UILabel *recipeLblName = (UILabel *)[cell viewWithTag:101];
    recipeLblName.text =  [title uppercaseString] ;
    
    
    UILabel *notificationLbl = (UILabel *)[cell viewWithTag:102];
    notificationLbl.text = @"";
    
    
    // notificationLbl.text = [NSString stringWithFormat:@"%ld",(1113%(indexPath.row+1))+1];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    
    
    
    
    if (indexPath.row==4) {
        
        
        if ([bikeColor isEqualToString:@"Red"])
            recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_home_red.png",title]];
        else if ([bikeColor isEqualToString:@"Amber"])
            recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_home_yellow.png",title]];
        else
            recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_home.png",title]];
        
    }
    else
        recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_home.png",title]];
    
    
    //[NSArray arrayWithObjects:@"weather",@"mail",@"buddy",@"service",@"health",@"my rides",@"park",@"specials",@"events", nil]
    
    switch (indexPath.row) {
        case 0:{
            notificationLbl.text = @"";// [bikeNumberDic stringValueForKey:@"WeatherNotification"];
            int value = 0;
            if ([[bikeNumberDic stringValueForKey:@"WeatherNotification"] length])
                value = [[bikeNumberDic stringValueForKey:@"WeatherNotification"] intValue];
            
            
        }
            break;
        case 1:
            notificationLbl.text = [bikeNumberDic stringValueForKey:@"MailNotification"];
            recipeLblName.text = @"MESSAGES";
            
            break;
        case 2:
            notificationLbl.text = [bikeNumberDic stringValueForKey:@"BuddyNotification"];
            recipeLblName.text = @"BUDDIES";//[bikeNumberDic stringValueForKey:@""];
            
            break;
        case 3:
            //notificationLbl.text = [bikeNumberDic stringValueForKey:@"ServiceNotification"];
            recipeLblName.text = @"MY VEHICLE";
            recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"10.png"]];
            
            break;
        case 4:
            notificationLbl.text = [bikeNumberDic stringValueForKey:@"ServiceNotification"];
            
            //  notificationLbl.text = [bikeNumberDic stringValueForKey:@"HealthNotification"];
            recipeLblName.text = @"VEHICLE HEALTH";
            break;
            
        case 5:{
            notificationLbl.text = [bikeNumberDic stringValueForKey:@"RideNotification"];
            NSString *status = [bikeNumberDic stringValueForKey:@"ActiveRide"];
            int number = [status length]?[status intValue]:0;
            recipeImageView.image = [UIImage imageNamed:(number?@"my rides_home_sel.png":@"my rides_home.png")];
            
            //RideNotification
        }
            break;
        case 6:
            
            recipeLblName.text =[UIAppDelegate.userDefault boolForKey:IsBikeParkKey]?@"FIND MY VEHICLE": @"PARK MY VEHICLE";//[bikeNumberDic stringValueForKey:@""];
            
            break;
            
        case 7:
            notificationLbl.text = [bikeNumberDic stringValueForKey:@"SpecialNotification"];
            
            break;
            
        case 8:
            notificationLbl.text = [bikeNumberDic stringValueForKey:@"EventNotification"];
            
            break;
            
            
            
        default:
            notificationLbl.text = @"";
            break;
    }
    
    if (![notificationLbl.text length] || [ notificationLbl.text isEqualToString:@"0"]) {
        notificationLbl.hidden = YES;
    }
    else  notificationLbl.hidden = NO;
    
    
    if (indexPath.row) {
        notificationLbl.layer.borderColor = [UIColor colorWithRed:31.0f/255.0f green:157.0/255.0f blue:193.0/255.0f alpha:1].CGColor;;
    }
    else
        notificationLbl.layer.borderColor = [UIColor colorWithRed:255.0f/255.0f green:186.0/255.0f blue:0.0/255.0f alpha:1].CGColor;
    
    notificationLbl.layer.borderWidth = 1.0;
    notificationLbl.layer.cornerRadius = 10;
    
    
    
    
    
    //    if (indexPath.row == 4 || indexPath.row == 3 || indexPath.row == 5 ) {
    //        cell.backgroundColor = [UIColor colorWithRed:223/255.0f green:241.0/255.0f blue:244.0/255.0f alpha:1];
    //    }
    //
    
    
    return cell;
    
    
    
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    //  NSLog(@"%@",dic);
    
    NSString *ownerId    = [dic stringValueForKey:@"FName"];// [dic stringValueForKey:@"OwnerId"];
    NSString *cycleCount = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:ValidationUUIDKey]] stringValueForKey:@"CycleCount"];
    
    //ValidationUUIDKey
    //   //[NSArray arrayWithObjects:@"weather",@"mail",@"buddy",@"service",@"health",@"my rides",@"park",@"specials",@"events", nil]
    
    
    if ([ownerId isEqualToString:@"0"] || ![ownerId length]) {
        switch (indexPath.row) {
            case 0:{
                
                [self weatherBtnClicked:0];
                break;
                
            }
            case 7:{
                CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
                [self.navigationController pushViewController:viewController animated:YES];
                
                
                break;
                
            }
            case 8:{
                CMEventsController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMEventsController"];
                [self.navigationController pushViewController:viewController animated:YES];
                
                break;
            }
            case 6:
            {
                CMParkBikeController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
                controller.isFromHome = YES;
                [self.navigationController pushViewController:controller animated:YES];
                
            }
                break;
            default:
                [UIAppDelegate goToSettingScreen];
                break;
        }
        
    }
    else if ([cycleCount isEqualToString:@"0"] || ![cycleCount length]) {
        
        switch (indexPath.row) {
            case 1:{
                //  [self markAsRead:@"Mail"];
                
                CMMessageViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMMessageViewController"];
                
                
                // "MailNotificationCycleId""ServiceNotificationCycleId
                NSString *cycleID  = [bikeNumberDic stringValueForKey:@"MailNotificationCycleId"];
                
                if (![cycleID length] || ([cycleID intValue]==0)) {
                    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
                    
                    
                    cycleID = [dic stringValueForKey:@"PrimaryBikeId"];
                    if (![cycleID length]) {
                        cycleID  = [dic stringValueForKey:@"CycleId"];
                    }
                }
                
                viewController.messCycleId =cycleID;
                [self.navigationController pushViewController:viewController animated:YES];
                
                break;
                
            }
                
            case 0:{
                
                [self weatherBtnClicked:0];
                break;
                
            }
                
            case 7:{
                CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
                [self.navigationController pushViewController:viewController animated:YES];
                break;
            }
            case 8:{
                CMEventsController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMEventsController"];
                [self.navigationController pushViewController:viewController animated:YES];
                break;
            }
            case 5:
            {
                CMSaveRideController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMSaveRideController"];
                controller.isFromHome = YES;
                
                [self.navigationController pushViewController:controller animated:YES];
                
            }
                break;
            case 2:
            {
                
                CMBuddyContactsController *buddyProfile = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBuddyContactsController"];
                buddyProfile.isFromHome = YES;
                [self.navigationController pushViewController:buddyProfile animated:YES];
                
            }
                break;
            case 6:
            {
                CMParkBikeController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
                controller.isFromHome = YES;
                [self.navigationController pushViewController:controller animated:YES];
                
            }
                break;
            case 4:
            {
                
                //   NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
                
                NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
                
                NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
                if (![primaryBikeId length]) {
                    primaryBikeId  = [dic stringValueForKey:@"CycleId"];
                }
                NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
                
                [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    
                    
                    if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                        NSLog(@"***  %@ **",resultDict);
                        NSMutableArray *itemArr = [[NSMutableArray alloc]init];
                        if ([itemArr count]) {
                            [itemArr removeAllObjects];
                        }
                        
                        for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                            [itemArr addObject:obj];
                        }
                        
                        NSLog(@"count --> %lu",(unsigned long)itemArr.count);
                        if (itemArr.count>0)
                        {
                            
                            CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                            [viewController performSelector:@selector(openHealthTab) withObject:nil afterDelay:.5];
                            [self.navigationController pushViewController:viewController animated:YES];
                        }
                        else
                        {
                            [UIAppDelegate goToAddBikeScreen];
                        }
                        
                    }
                }];
                
            }
                break;
            default:
                [UIAppDelegate goToAddBikeScreen];
                break;
        }
        
        
        
    }
    else{
        
        
        switch (indexPath.row) {
            case 1:{
                //[self markAsRead:@"Mail"];
                
                CMMessageViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMMessageViewController"];
                
                
                // "MailNotificationCycleId""ServiceNotificationCycleId
                NSString *cycleID  = [bikeNumberDic stringValueForKey:@"MailNotificationCycleId"];
                
                if (![cycleID length] || ([cycleID intValue]==0)) {
                    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
                    
                    
                    cycleID = [dic stringValueForKey:@"PrimaryBikeId"];
                    if (![cycleID length]) {
                        cycleID  = [dic stringValueForKey:@"CycleId"];
                    }
                }
                
                viewController.messCycleId =cycleID;
                [self.navigationController pushViewController:viewController animated:YES];
                
                break;
                
            }
            case 0:{
                
                [self weatherBtnClicked:0];
                break;
                
            }
            case 7:{
                CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
                [self.navigationController pushViewController:viewController animated:YES];
                
                
                break;
                
            }
            case 8:{
                CMEventsController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMEventsController"];
                [self.navigationController pushViewController:viewController animated:YES];
                
                break;
            }
            case 3:{
                
                [self markAsRead:@"Service"];
                
            }
            case 4:
            {
                CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                
                
                if (indexPath.row==3){
                    NSString *notification = [bikeNumberDic stringValueForKey:@"ServiceNotification"];
                    if (![notification length] || [ notification isEqualToString:@"0"])
                        [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                    else
                        [viewController performSelector:@selector(openHealthTab)  withObject:nil afterDelay:.5];
                }
                else
                    [viewController performSelector:@selector(openHealthTab)  withObject:nil afterDelay:.5];
                
                
                viewController.isFromHome = YES;
                [self.navigationController pushViewController:viewController animated:YES];
                
                
            }
                break;
            case 5:
            {
                CMSaveRideController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMSaveRideController"];
                controller.isFromHome = YES;
                
                [self.navigationController pushViewController:controller animated:YES];
                
            }
                break;
            case 2:
            {
                CMBuddyContactsController *buddyProfile = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBuddyContactsController"];
                
                
                buddyProfile.isFromHome = YES;
                [self.navigationController pushViewController:buddyProfile animated:YES];
                
            }
                break;
            case 6:
            {
                CMParkBikeController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
                
                
                controller.isFromHome = YES;
                
                [self.navigationController pushViewController:controller animated:YES];
                
            }
                break;
            default:
                break;
        }
        
    }
    
    
    
}


#pragma mark Collection view layout things
// Layout: Set cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // NSLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(106, 70);
    return mElementSize;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}
#pragma mark - UICollectionViewDelegateFlowLayout
- (BOOL)collectionView:(UICollectionView *)collectionView
shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return YES;
}

#pragma mark - CLLocationManagerDelegate

- (void)startLocationRequest
{
    __weak __typeof(self) weakSelf = self;
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                                                timeout:10
                                                   delayUntilAuthorized:YES
                                                                  block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                      __typeof(weakSelf) strongSelf = weakSelf;
                                                                      
                                                                      if (status == INTULocationStatusSuccess) {
                                                                          // achievedAccuracy is at least the desired accuracy (potentially better)
                                                                          NSLog(@"Location request successful! Current Location:\n%@", currentLocation);
                                                                          [self setCurrentCityAndState:currentLocation];
                                                                      }
                                                                      else if (status == INTULocationStatusTimedOut) {
                                                                          // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                                                                          NSLog(@"Location request timed out. Current Location:\n%@", currentLocation);
                                                                          [self setCurrentCityAndState:currentLocation];
                                                                          
                                                                      }
                                                                      else {
                                                                          // An error occurred
                                                                          if (status == INTULocationStatusServicesNotDetermined) {
                                                                              NSLog(@"Error: User has not responded to the permissions alert.");
                                                                          } else if (status == INTULocationStatusServicesDenied) {
                                                                              NSLog(@"Error: User has denied this app permissions to access device location.");
                                                                          } else if (status == INTULocationStatusServicesRestricted) {
                                                                              NSLog(@"Error: User is restricted from using location services by a usage policy.");
                                                                          } else if (status == INTULocationStatusServicesDisabled) {
                                                                              NSLog( @"Error: Location services are turned off for all apps on this device.");
                                                                          } else {
                                                                              NSLog( @"An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)");
                                                                          }
                                                                      }
                                                                      strongSelf.locationRequestID = NSNotFound;
                                                                  }];
}

-(void)setCurrentCityAndState:( CLLocation *)newLocation{
    
    //    [UIAppDelegate.userDefault setObject:[NSString stringWithFormat:@"%.4f", newLocation.coordinate.latitude] forKey:@"Lat"];
    //    [UIAppDelegate.userDefault  setObject:[NSString stringWithFormat:@"%.4f", newLocation.coordinate.longitude] forKey:@"Lon"];
    
    
    [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:newLocation.coordinate.latitude] forKey:@"Lat"];
    [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:newLocation.coordinate.longitude] forKey:@"Lon"];
    
    
    
    [UIAppDelegate.userDefault  synchronize];
    
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        //        [UIAppDelegate.userDefault setObject:[NSString stringWithFormat:@"%.4f", currentLocation.coordinate.latitude] forKey:@"Lat"];
        //        [UIAppDelegate.userDefault  setObject:[NSString stringWithFormat:@"%.4f", currentLocation.coordinate.longitude] forKey:@"Lon"];
        
        
        [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:newLocation.coordinate.latitude] forKey:@"Lat"];
        [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:newLocation.coordinate.longitude] forKey:@"Lon"];
        
        if (isUpdateCurrLocation){
            
            if (!geocoder) geocoder = [[CLGeocoder alloc] init] ;
            [geocoder reverseGeocodeLocation:currentLocation
                           completionHandler:^(NSArray *placemarks, NSError *error) {
                               NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                               
                               if (error){
                                   NSLog(@"Geocode failed with error: %@", error);
                                   return;
                                   
                               }
                               
                               
                               CLPlacemark *placemark = [placemarks objectAtIndex:0];
                               
                               NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
                               NSLog(@"placemark.country %@",placemark.country);
                               NSLog(@"placemark.postalCode %@",placemark.postalCode);
                               NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
                               NSLog(@"placemark.locality %@",placemark.locality);
                               NSLog(@"placemark.subLocality %@",placemark.subLocality);
                               NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
                               
                               
                               [UIAppDelegate.userDefault setObject:[NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.administrativeArea] forKey:CycleLocationNameKey];
                               
                               
                               [self getWeatherData:placemark.locality];
                               
                               
                           }];
        }
        
        isUpdateCurrLocation = NO;
        
        
        
    }
    
    
    
}
#pragma mark - SHXAcceleratorDelegate

-(void)accelerator:(SHXAccelerator *)accelerator didFailWithError:(NSError *)error
{
    //    NSLog(@"accelerator didFailWithError %@", error);
    //
    //    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
    //                              @"Title" message:@"accelerator didFailWithError" delegate:self
    //                                             cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    //    [alertView show];
    
}

-(void)accelerator:(SHXAccelerator *)accelerator didEnterVenue:(SHXCampaignVenue *)venue
{
    // SHXAcceleratorDelegate methods are always executed on main thread. Depending on
    // how much work your code is going to do you might want to run it off main thread.
    // NSLog(@"accelerator venue entry: %@ %@", venuevenue.campaignName, venue.venueIdent);
    
    
    //    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
    //                              @"Title" message:StrID delegate:self
    //                                             cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    //    [alertView show];
    
    
    venueID = [NSString stringWithFormat:@"%@", venue.venueIdent];
    campaignName = [NSString stringWithFormat:@"%@", venue.campaignName];
    payLoad = [NSString stringWithFormat:@"%@", venue.customData];
    skyhookResponse = [NSString stringWithFormat:@"%@",venue];
    
    [accelerator fetchInfoForVenues:@[venue.venueIdent] completion:^(NSArray *venueInfoList, NSError *error)
     {
         if (venueInfoList)
         {
             SHXVenueInfo *venueInfo = venueInfoList[0];
             
             UILocalNotification *notification = [UILocalNotification new];
             notification.fireDate = nil;
             // notification.alertBody = [NSString stringWithFormat:@"Approaching %@", venueInfo.venueIdent];
             notification.alertBody = [NSString stringWithFormat:@"%@", venue.customData];
             notification.soundName = UILocalNotificationDefaultSoundName;
             [[UIApplication sharedApplication] scheduleLocalNotification:notification];
         }
         else
         {
             NSLog(@"Error: %@", error);
         }
     }];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults synchronize];
    NSString *myString = [userDefaults stringForKey:@"userID"];
    NSLog(@"myString-->%@",myString);
    if (([venueID isEqualToString:@""]) || ([myString isEqualToString:@""]) ||  (myString == (id)[NSNull null]) || (myString.length == 0)  ||  (venueID == (id)[NSNull null]) || (venueID.length == 0)  || ([payLoad isEqualToString:@""]) || (payLoad == (id)[NSNull null]) || (payLoad.length == 0))
    {
        
    }
    else
    {
        [self callSkyhookAPI];
    }
}
- (void) callSkyhookAPI
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults synchronize];
    NSString *myString = [userDefaults stringForKey:@"userID"];
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             myString, @"UserId",
                             venueID,@"VenueId",
                             campaignName,@"CampaignName",
                             DealerID,@"DealerId",
                             payLoad,@"PayLoad",
                             skyhookResponse,@"JsonResponse",
                             nil];
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/SendPayLoadToUser",Service_URL];
    
    
    
    //    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"http://198.12.154.24/7MediaService/CycleMate.svc/SendPayLoadToUser"] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
    
    [[CMNetworkManager sharedInstance] postDataResponse:stringURL param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** SkyhookAPI %@ **",resultDict);
            NSString *strMsg = [NSString stringWithFormat:@"%@",resultDict];
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                                  message:strMsg
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
            
            
        }}];
    
    
}

@end
