//
//  CMWeatherAlertVC.h
//  Cyclemate
//
//  Created by Rajesh on 9/28/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMWeatherAlertVC : UIViewController

{
#pragma mark temperatureView:-

    
    IBOutlet UILabel *_hiloLbl;
    IBOutlet UILabel *mainTempLbl;
    IBOutlet UILabel *_feelLbl;
    IBOutlet UILabel *_windLbl;
    IBOutlet UILabel *_humadityLbl;
    IBOutlet UILabel *_precdiptitationLbl;
    IBOutlet UILabel *_visibilityLbl;
    
    IBOutlet UILabel     *_weatherDesLbl;
    IBOutlet UITextField *_cityLbl;
    IBOutlet UILabel     *_dateLbl;

    
    IBOutlet UIImageView *weatherIcon;
    IBOutlet UITableView *weatherTableView;

    NSDateFormatter *df;
    NSArray *listData;

    
    IBOutlet UIView *currentView;
    IBOutlet UIView *forcastView;
    IBOutlet UIView *innerforcastView;


//
    BOOL isHouly;
    
    IBOutlet UILabel *statusLineLble;

    NSMutableArray *threehourData;

    IBOutlet UILabel *extremeAlertLbl;
    IBOutlet UILabel *tipOfDayLbl;
    
    IBOutlet UIView *tipOfDayView;

    IBOutlet UIImageView *tipOfDayImage;
    
    
    
    IBOutlet UIButton *currentlyBtn;
    IBOutlet UIButton *houlyBtn;
    IBOutlet UIButton *fiveDayBtn;
    NSString *imageURL;
    
    NSDictionary *weatherDic;

    IBOutlet UIView *baseTipOfDayView;

}

@property (nonatomic,retain)    NSMutableDictionary *currDayWeatherDic;
@property (nonatomic,retain)    NSString *selectedDealer;

-(IBAction)topSegementClicked:(id)sender;
-(IBAction)tipOfDayBtnClicked:(id)sender;


-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)topMenuBtnClicked:(id)sender;
-(IBAction)backBtnClicked:(id)sender;
@end
