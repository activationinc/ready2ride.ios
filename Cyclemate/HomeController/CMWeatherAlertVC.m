//
//  CMWeatherAlertVC.m
//  Cyclemate
//
//  Created by Rajesh on 9/28/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMWeatherAlertVC.h"
#import "CMSpecialDetailController.h"

@interface CMWeatherAlertVC ()
{
    NSArray *hourlyTimeArr;
    IBOutlet UIImageView *wAlertImage;
    
}
@end

@implementation CMWeatherAlertVC
@synthesize currDayWeatherDic;
@synthesize selectedDealer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)searchBtnClicke:(id)sender{
    
    if (![_cityLbl.text length]) {
        return;
    }

    [self getthreHourseWeatherCurrentSearchData];
    [self getWeaklyWeatherForCurrentSearchData];
    [self.view endEditing:YES];
}

-(IBAction)crossBtnClicked:(id)sender{
    
    CGRect fraem  = tipOfDayView.frame;
    fraem.origin.y = self.navigationController.view.frame.size.height;
    [UIView animateWithDuration:1
                     animations:^{
                         tipOfDayView.frame = fraem;
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"completion block");
                         [tipOfDayView removeFromSuperview];

                     }];
    
}
-(IBAction)tipOfDayBtnClicked:(id)sender{
    
    CMSpecialDetailController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialDetailController"];
   viewController.specialDic = weatherDic;
    viewController.isFromWeather = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];

    return;
    if ([imageURL length]) {
        [self.navigationController.view addSubview:tipOfDayView];
        
        CGRect fraem  = tipOfDayView.frame;
        fraem.origin.y = self.navigationController.view.frame.size.height-20;
        fraem.size.height = self.navigationController.view.frame.size.height-20;
        tipOfDayView.frame = fraem;
        fraem.origin.y = 20;
        
        
        [UIView animateWithDuration:1
                         animations:^{
                             tipOfDayView.frame = fraem;
                             
                         }
                         completion:^(BOOL finished){
                             NSLog(@"completion block");
                         }];

    }
    
    
}

-(void)getWeatherTipsData{
    
    
  NSString *condition = @"0";
    NSString *temp = @"0";
    
    @try {
        condition = [[[currDayWeatherDic objectForKey:@"weather"] objectAtIndex:0] stringValueForKey:@"description"];
        temp = [[currDayWeatherDic objectForKey:@"temp"] stringValueForKey:@"max"];
    }
    @catch (NSException *exception) {
        condition = @"0";
        temp = @"0";
    }
    @finally {
        
    }
    
    baseTipOfDayView.hidden = YES;

    
    NSString *weatherTipsApi = [NSString stringWithFormat:@"%@/GetWeather/Temp=%d/Condition=%@/ExtreamAlerts=0/DealerId=%@",Service_URL,[temp intValue],condition,DealerID];
    [[CMNetworkManager sharedInstance] getResponse:weatherTipsApi requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** weatherTipsApi %@ **",resultDict);
            
            NSArray *dataArr = [resultDict objectForKey:@"GetWeatherResult"];
            
            for ( NSDictionary  *infoDic in dataArr ) {
                NSUInteger type = [[infoDic stringValueForKey:@"Type"] integerValue];
                if (type==2) {
                    extremeAlertLbl.text = [infoDic stringValueForKey:@"MessageText"];

                }else  if (type==1) {
                    
                    weatherDic = infoDic;
                    NSString *desc = [infoDic stringValueForKey:@"MessageText"];
                    
//                    if (![desc length]) {
//                        desc =[infoDic stringValueForKey:@"ExtremeAlertName"];
//                    }
                   
                      tipOfDayLbl.text  = desc;
                    
                    
                    
                    
                    if ( [tipOfDayLbl.text length]) {
                        baseTipOfDayView.hidden = NO;
                    }
                    else
                        baseTipOfDayView.hidden = YES;
                    
                }
            }
            
            
            
        }
        
        
        if (![extremeAlertLbl.text length])
            wAlertImage.hidden = YES;
        else
            wAlertImage.hidden = NO;

    }];
    
}
/*
-(void)getWeatherTipsData{
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    
    NSString *stringURL =  [NSString stringWithFormat:@"%@/GetPromotion/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];

    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        //        [self.view.window hideActivityViewWithAfterDelay:0];
        [SVProgressHUD dismiss];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getmodel %@ **",resultDict);
            //  dealerSpecialArry = [resultDict objectForKey:@"GetDealerSpeacialByOwnerIdResult"];
            NSArray *midArr = [resultDict objectForKey:@"GetPromotionResult"];
            
            if ([midArr count]) {
                int number = arc4random()%[midArr count];
                weatherDic = [midArr objectAtIndex:number];
                
               // extremeAlertLbl.text = [weatherDic stringValueForKey:@"Title"];
                NSString *desc = [weatherDic stringValueForKey:@"FooterDescription"];
                
                if (![desc length]) {
                    desc =[weatherDic stringValueForKey:@"Description"];
                }
                if (![desc length]) {
                    desc = [weatherDic stringValueForKey:@"Title"];
                }
               tipOfDayLbl.text  = desc;

            }
            
            if ( [tipOfDayLbl.text length]) {
                baseTipOfDayView.hidden = NO;
            }
            else
                baseTipOfDayView.hidden = YES;

            //
//            extremeAlertLbl.text = [infoDic stringValueForKey:@"Title"];
//            tipOfDayLbl.text  = [infoDic stringValueForKey:@"Description"];
            
            
        }
    }];
    
    NSString *condition = @"0";
    NSString *temp = @"0";

    @try {
        condition = [[[currDayWeatherDic objectForKey:@"weather"] objectAtIndex:0] stringValueForKey:@"main"];
      temp = [[currDayWeatherDic objectForKey:@"temp"] stringValueForKey:@"description"];
    }
    @catch (NSException *exception) {
        condition = @"0";
        temp = @"0";
    }
    @finally {
       
    }
  
    
    NSString *weatherTipsApi = [NSString stringWithFormat:@"%@/GetWeather/Temp=%d/Condition=%@/ExtreamAlerts=0/DealerId=%@",Service_URL,[temp intValue],condition,DealerID];
      [[CMNetworkManager sharedInstance] getResponse:weatherTipsApi requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** weatherTipsApi %@ **",resultDict);
            
            NSArray *dataArr = [resultDict objectForKey:@"GetWeatherResult"];
            if ([dataArr count]) {
                NSDictionary  *infoDic = [dataArr objectAtIndex:0];
                
                extremeAlertLbl.text = [infoDic stringValueForKey:@"Title"];
            
            }
            
            
        }
    }];
    
}*/
/**
 *  For Search Data
 */


-(void)getWeaklyWeatherForCurrentSearchData{

    
    NSString *weatherApi = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=%@&mode=json&units=imperial&APPID=5a3c099d390d1f4f986412264d3da193",_cityLbl.text];

    
    [[CMNetworkManager sharedInstance] getResponse:weatherApi requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
              NSLog(@"*** getbrands %@ **",resultDict);
            listData = [resultDict objectForKey:@"list"];
            if ([listData count])  currDayWeatherDic = [[listData objectAtIndex:0] mutableCopy];

            [self setUpWeatherDetails:currDayWeatherDic];
            
            NSDictionary *cityDic = [resultDict objectForKey:@"city"];
            
             _cityLbl.text = [NSString stringWithFormat:@"%@, %@",[cityDic stringValueForKey:@"name"],[cityDic stringValueForKey:@"country"]];

            [weatherTableView reloadData];

            
            
        }
    }];
    
}
-(void)getthreHourseWeatherCurrentSearchData{
   
    
    NSString *weatherApi = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast?q=%@&mode=json&units=imperial&cnt=10&APPID=5a3c099d390d1f4f986412264d3da193",_cityLbl.text];
    
    
    [[CMNetworkManager sharedInstance] getResponse:weatherApi requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            //  NSLog(@"*** getbrands %@ **",resultDict);
            
            int k =0;
            
            if ([threehourData count]) [threehourData removeAllObjects];
            
            for (id obj  in [resultDict objectForKey:@"list"]) {
                
                if (k!=0)[threehourData addObject:obj];
                k++;
                if (k==10) {
                    break;
                }
            
        }
        }
    }];
}
/**
 *  For Search Data
 */

-(void)getthreHourseWeatherData{
    
    
   // NSTimeInterval interval = [[midDic stringValueForKey:@"dt"] doubleValue];
    /*
    NSString *dateStr = [MCFDate dateStringFromDate:[NSDate date] format:@"yyyy-MM-dd"];
    NSDate *startDate =  [MCFDate dateFromString:[dateStr stringByAppendingFormat:@" 00:00:00"] ];
    NSDate *endDate = [MCFDate dateFromString:[dateStr stringByAppendingFormat:@" 23:59:59"] ];*/
//    
//    NSDate *startDate =  [NSDate date];
//    NSDate *endDate   = [startDate dateByAddingTimeInterval:-24*60*60];
//    
    //NSTimeInterval interval = [startDate timeIntervalSince1970];
  //23:59:59
    
    NSString *weatherApi = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast?lat=%@&lon=%@&cnt=10&mode=json&units=imperial&type=hour&APPID=5a3c099d390d1f4f986412264d3da193",[UIAppDelegate.userDefault objectForKey:@"Lat"],[UIAppDelegate.userDefault objectForKey:@"Lon"]];
  //  weatherApi = @"http://api.openweathermap.org/data/2.5/history/city?lat=37.33233141&lon=-122.0312186&type=hour&start=1436553000&end=1436639399";
    //@"http://api.openweathermap.org/data/2.5/history/city?lat=37.33233141&lon=-122.0312186&type=day&cnt=24&mode=json&units=imperial";
    
    
 // weatherApi = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/history/city?lat=%@&lon=%@&type=hour&start=%f&end=%f",[UIAppDelegate.userDefault objectForKey:@"Lat"],[UIAppDelegate.userDefault objectForKey:@"Lon"],[startDate timeIntervalSince1970],[endDate timeIntervalSince1970]];

    [[CMNetworkManager sharedInstance] getResponse:weatherApi requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            //  NSLog(@"*** getbrands %@ **",resultDict);
            int k =0;
            
            if ([threehourData count]) [threehourData removeAllObjects];
            
            for (id obj  in [resultDict objectForKey:@"list"]) {
                
                if (k!=0)[threehourData addObject:obj];
                
                k++;
                if (k==10) {
                    break;
                }
                
            }
            
            
                       
            [weatherTableView reloadData];

            
        }
    }];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    [self getthreHourseWeatherData];
    [self getWeatherTipsData];
    
    
    if (!currDayWeatherDic) [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpWeatherDetails) name:kNOTIFICATION_GET_WEATHERDATA object:nil];

}
- (void)viewWillDisappear:(BOOL)animated{

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNOTIFICATION_GET_WEATHERDATA object:nil];

}

-(IBAction)topMenuBtnClicked:(id)sender
{
    
}

-(IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)topleftBtnClicked:(id)sender{
    
    
}
-(IBAction)bottomViewBtnClicked:(id)sender
{
    [UIAppDelegate setSelectedController:[(UIButton *)sender tag]];
    
}
-(IBAction)topSegementClicked:(id)sender;
{
    
    currentlyBtn.backgroundColor =   fiveDayBtn.backgroundColor  =    houlyBtn.backgroundColor  = DarkBlurColor;
  
    UIButton *currBtn = (UIButton *)sender;
    CGRect lblFrame = statusLineLble.frame;
    lblFrame.origin.x = currBtn.frame.origin.x;
    [statusLineLble setFrame:lblFrame];
    
    currBtn.backgroundColor =  LightBlurColor;
    
  
    
    switch ([sender tag]) {
        case 0:
            currentView.hidden = NO;
            forcastView.hidden = YES;
            break;
        case 1:{
            isHouly = YES;
            currentView.hidden = YES;
            forcastView.hidden = NO;
            
            CGRect frame =     innerforcastView.frame;
            frame.size.height =  MIN(40*threehourData.count, IS_IPHONE_5?280:200);
            innerforcastView.frame = frame;
            
            frame =     weatherTableView.frame;
            frame.size.height = MIN(40*threehourData.count, IS_IPHONE_5?280:200);
            weatherTableView.frame = frame;

        }
            break;
        case 2:{
            isHouly = NO;
            currentView.hidden = YES;
            forcastView.hidden = NO;
            CGRect frame =     innerforcastView.frame;
            frame.size.height =  MIN(40*6, IS_IPHONE_5?280:200);
            innerforcastView.frame = frame;
            
            frame =     weatherTableView.frame;
            frame.size.height = MIN(40*6, IS_IPHONE_5?280:200);
            weatherTableView.frame = frame;

            
        }
            break;
        default:
        
            break;
    }
    [weatherTableView reloadData];


}

-(void)setUpWeatherDetails:(NSMutableDictionary *)withWeatherDic{
    
    NSTimeInterval interval = [[withWeatherDic stringValueForKey:@"dt"] doubleValue];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [df setTimeZone:sourceTimeZone];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSLog(@"%@",[df stringFromDate:date]);
    [df setDateFormat:@"EEEE MMMM dd, yyyy"];
    
    _dateLbl.text = [df stringFromDate:date];
    _humadityLbl.text = [NSString stringWithFormat:@"%@%%",[withWeatherDic stringValueForKey:@"humidity"]];
    //_windLbl.text = [NSString stringWithFormat:@"%.2f mph (%@º)",[[withWeatherDic stringValueForKey:@"speed"] floatValue] ,[withWeatherDic stringValueForKey:@"deg"] ];
    _windLbl.text = [NSString stringWithFormat:@"%.2f mph",[[withWeatherDic stringValueForKey:@"speed"] floatValue] ];
    
    
    
    _feelLbl.text = [NSString stringWithFormat:@"%@%%",[withWeatherDic stringValueForKey:@"clouds"]];
    
    mainTempLbl.text = [NSString stringWithFormat:@"%.0fº",([[[withWeatherDic objectForKey:@"temp"] stringValueForKey:@"day"] floatValue])];;;
    
    _hiloLbl.text = [NSString stringWithFormat:@"%.0fº/%.0fº",([[[withWeatherDic objectForKey:@"temp"] stringValueForKey:@"max"] floatValue]),([[[withWeatherDic objectForKey:@"temp"] stringValueForKey:@"min"] floatValue])];
    
    _weatherDesLbl.text = [[[[withWeatherDic  objectForKey:@"weather"] objectAtIndex:0]objectForKey:@"description"] uppercaseString];
    
    
    _precdiptitationLbl.text = [NSString stringWithFormat:@"%.0f hPa",[[withWeatherDic stringValueForKey:@"pressure"] floatValue] ];
    
    if ([[withWeatherDic stringValueForKey:@"rain"] length])
        _visibilityLbl.text = [NSString stringWithFormat:@"%.2f inches",[[withWeatherDic stringValueForKey:@"rain"] floatValue]*0.0393 ];
    else
        _visibilityLbl.text =  @"0 inches";
    
    // _cityLbl.text = [NSString stringWithFormat:@"%@, %@",[[withWeatherDic objectForKey:@"City"]stringValueForKey:@"name"],[[withWeatherDic objectForKey:@"City"]stringValueForKey:@"country"] ];
    
    
   
    
    
    [weatherIcon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_small.png",[[[withWeatherDic  objectForKey:@"weather"] objectAtIndex:0]objectForKey:@"icon"]]]];
    
    
    [df setDateFormat:@"EEEE"];
    
   
    if (!isHouly) {
        CGRect frame =     innerforcastView.frame;
        frame.size.height =  MIN(40*6, 320);
        innerforcastView.frame = frame;
        
        frame =     weatherTableView.frame;
        frame.size.height = MIN(40*6, 320);
        weatherTableView.frame = frame;
    }
   
    
    [weatherTableView reloadData];
}
-(void)setUpWeatherDetails{
    
    _cityLbl.backgroundColor = [UIColor clearColor];
    NSDictionary * resultDict = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:Cycle5DaysWeatherKey]];;
    listData = [resultDict objectForKey:@"list"];
    
    currDayWeatherDic =  [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleWeatherKey]];
    
    NSString *address = [UIAppDelegate.userDefault objectForKey:CycleLocationNameKey];
    
    if ([address length])
        _cityLbl.text = address;
    else
        _cityLbl.text = [NSString stringWithFormat:@"%@, %@",[[currDayWeatherDic objectForKey:@"City"]stringValueForKey:@"name"],[[currDayWeatherDic objectForKey:@"City"]stringValueForKey:@"country"]];
    
    [self setUpWeatherDetails:currDayWeatherDic];
    
    //listData = [resultDict objectForKey:@"list"];
    
   
}

-(void)markAsReadWeather{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             ownerId, @"OwnerId",nil];
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/IsReadWeather",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"Weather" navigationItem:self.navigationItem];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    
    NSLog(@"WEATHER DICT.. %@",currDayWeatherDic);
    df = [[NSDateFormatter alloc] init];
    threehourData = [NSMutableArray new];

    //set BG - checked by name(we can do this with dealer id also)
    if ([selectedDealer containsString:@"Smoky Mountain Harley Davidson"])
    {
        [tipOfDayImage setImage:[UIImage imageNamed:@"background2.jpg"]];
    }
    else
        [tipOfDayImage setImage:[UIImage imageNamed:@"background.jpg"]];
    
    [self setUpWeatherDetails];
    [self markAsReadWeather];
    
 hourlyTimeArr = [NSArray arrayWithObjects:@"6 AM",@"9 AM",@"12 PM",@"3 PM",@"6 PM",@"9 PM",@"12 AM",@"3 AM",nil];
    
    if (!IS_IPHONE_5) {
     CGRect frame =  wAlertImage.frame;
        frame.origin.y-=39;
        wAlertImage.frame = frame;
        
        frame =  extremeAlertLbl.frame;
        frame.origin.y-=20;
        frame.size.height = 25;
        extremeAlertLbl.numberOfLines = 1;
        extremeAlertLbl.frame = frame;
        
        frame =  baseTipOfDayView.frame;
        frame.origin.y-=5;
        baseTipOfDayView.frame = frame;
        
    }
}


#pragma mark -
#pragma mark UITableView Method Implementation Call



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.1;
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
//{
//    return 0.1;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   // return  isHouly?hourlyTimeArr.count:(listData.count?(listData.count):listData.count);//5;
    return  isHouly?threehourData.count:(listData.count?(listData.count):listData.count);//5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary *midDic = [isHouly?threehourData:listData objectAtIndex:isHouly?indexPath.row:(indexPath.row)];//+1

    if (isHouly) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HourlyForcast"];
        
        UILabel *dayTextLbl = (UILabel *)[cell viewWithTag:1];
        UILabel *tempTextLbl = (UILabel *)[cell viewWithTag:3];
        UILabel *cloudeLbl = (UILabel *)[cell viewWithTag:4];
        
        UILabel *windLbl  = (UILabel *)[cell viewWithTag:5];

        UIImageView *alertImg = (UIImageView *)[cell viewWithTag:2];
        
        
        NSTimeInterval interval = [[midDic stringValueForKey:@"dt"] doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
        [df setDateFormat:@"hh a"];
        
        NSArray *strValArr = [[df stringFromDate:date] componentsSeparatedByString:@" "];
        if ([strValArr count]>1)  dayTextLbl.text = [NSString stringWithFormat:@"%1ld %@",(long)[[strValArr objectAtIndex:0] integerValue],[strValArr objectAtIndex:1]];
        
       // NSLog(@"%@",dayTextLbl.text );
        
        tempTextLbl.text =  [NSString stringWithFormat:@"%.fº",[[[midDic objectForKey:@"main"] stringValueForKey:@"temp"] floatValue]];
        
        windLbl.text =  [NSString stringWithFormat:@"%.2f mph",[[[midDic objectForKey:@"wind"] stringValueForKey:@"speed"] floatValue]];
        cloudeLbl.text =  [NSString stringWithFormat:@"%@ %%",[[midDic objectForKey:@"clouds"] stringValueForKey:@"all"]];


        [alertImg setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_small.png",[[[midDic  objectForKey:@"weather"] objectAtIndex:0]objectForKey:@"icon"]]]];
        
        
        //for Dummy Date
       // dayTextLbl.text = [hourlyTimeArr objectAtIndex:indexPath.row];
        
        return  cell;

    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"5dayForcast"];
    
    UILabel *dayTextLbl     = (UILabel *)[cell viewWithTag:1];
    UILabel *hiTempTextLbl  = (UILabel *)[cell viewWithTag:3];
    UILabel *lowTempTextLbl = (UILabel *)[cell viewWithTag:4];
    UIImageView *alertImg   = (UIImageView *)[cell viewWithTag:2];
    
    
    NSTimeInterval interval = [[midDic stringValueForKey:@"dt"] doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    [df setDateFormat:@"EEEE"];

    
    dayTextLbl.text = [df stringFromDate:date];
    
    if (!indexPath.row) dayTextLbl.text= @"Today";
    hiTempTextLbl.text =  [NSString stringWithFormat:@"%.fº",[[[midDic objectForKey:@"temp"] stringValueForKey:@"max"] floatValue]];
    lowTempTextLbl.text =  [NSString stringWithFormat:@"%.fº",[[[midDic objectForKey:@"temp"] stringValueForKey:@"min"] floatValue]];
    [alertImg setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_small.png",[[[midDic  objectForKey:@"weather"] objectAtIndex:0]objectForKey:@"icon"]]]];
    

    
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor clearColor]];
  //  [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
    
}


- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
 
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 
*/

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    //Tim
    _cityLbl.text = @"";
    
    return YES;
}
- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
    
    if (![_cityLbl.text length]) {
        return;
    }
    
    [self getthreHourseWeatherCurrentSearchData];
    [self getWeaklyWeatherForCurrentSearchData];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.view endEditing:YES];
    
    
}

@end
