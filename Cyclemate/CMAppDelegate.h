//
//  CMAppDelegate.h
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//com.7media.ondemand
//com.ondemand.R2R

typedef enum BikeInfoType{
    CMMotorcycleViewType = 1,
    CMOwnerViewType = 2,
    CMDLViewType = 3,
    CMInsuranceViewType = 4,
    CMPreferencesViewType = 5,
    
    
}CMBikeInfoType;



#define IS_IOS7  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)



#define UIAppDelegate \
((CMAppDelegate *)[UIApplication sharedApplication].delegate)


#import <UIKit/UIKit.h>
#import "GAI.h"
#import "SHXAccelerator.h"
#import "CMVINEntryViewController.h"
@interface CMAppDelegate : UIResponder <UIApplicationDelegate>
{

    UITabBarController *rearVC;
    
    NSUserDefaults *userDefault;
    NSString *statusVale;
    
    BOOL isRidePermission;
    NSMutableArray *itemArr;
 
}
@property (nonatomic) BOOL isRidePermission;


@property (strong,nonatomic) NSUserDefaults *userDefault;
@property (strong,nonatomic) NSDateFormatter * dateFormatter;

@property (retain, nonatomic)UITabBarController *rearVC;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *isAddBikeFlg;

@property(nonatomic, strong) id<GAITracker> tracker;

@property(strong, nonatomic, readonly) SHXAccelerator* accelerator;

-(IBAction)setSelectedController:(int)withIndex;

-(IBAction)bottomTabBarClicked:(id)sender;


+ (NSArray *)loadNibNamed:(NSString *)name owner:(id)owner options:(NSDictionary *)options;
+(UIColor*)navigationBarColor;
-(UIView *)loadBottomView:(int)withIndex;
- (NSArray*)rightBarButtons;
+(void)setNavigationBarTitle:(NSString*)title navigationItem:(UINavigationItem*)navigationItem;
- (UIBarButtonItem*)backBarButtonItem:(id)controller;
- (void) hideTabBar:(UITabBarController *) tabbarcontroller;

-(void)goToSettingScreen;
-(void)goToAddBikeScreen;
-(void)validationForUUID;

- (NSArray*)rightBarButtonsWithResetBtn;

-(void)openPopUpMenu;

-(void)topNavMessageClicked:(id)sender;
- (void)saveImage:(UIImage *)image WithName:(NSString *)imageName;

-(UIImage *)readImageFromPlistByKey:(NSString *)keyName;
@property (assign, nonatomic) NSInteger locationRequestID;



+(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width;
-(NSString *)getDealerID;
-(NSString *)getDealerInfo:(NSString *)withKey;
-(NSString *)getOwnerID;

-(void)insertUUID;
-(void)postBlankOwnerInfo;

@end
