//
//  CMHelper.m
//  Cyclemate
//
//  Created by Sourav on 27/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMHelper.h"

@implementation CMHelper

@synthesize flagTrackingLocationStarts;

+(NSString *)removeSpaceCharactersFromStart:(NSString *)string{
    
    NSString *untrimedString = string;
    NSString *trimedString = [untrimedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return trimedString;
}

+(void)alertMessageWithText:(NSString *)messageText {

    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                          message:messageText
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    
    [myAlertView show];

}
+(void)alertMessageWithText:(NSString *)messageText delegate:(id)sender tag:(int)alerttag{
    
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                          message:messageText
                                                         delegate:sender
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    [myAlertView setTag:alerttag];
    
    [myAlertView show];
    
}
#pragma mark -
#pragma mark Location Tracking
-(IBAction)resumeLocationServiceIfNeeded:(id)sender {
    if(!self.flagTrackingLocationStarts)
        [self startFindingLocation:nil];
}

-(void)startFindingLocation:(id)sender {
    if(!self.flagTrackingLocationStarts) {
            DebugLog(@"startFindingLocation");
        
        
        if (!locationManager) {
            locationManager = [[CLLocationManager alloc] init];
        }
        
        if ([CLLocationManager locationServicesEnabled]) {
            
            locationManager.delegate = self;
            locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [locationManager requestWhenInUseAuthorization];
            [locationManager startUpdatingLocation];
           // [locationManager startUpdatingHeading];
            self->flagTrackingLocationStarts = YES;
            isSetLocationToCurrentLocation = YES;
            
        }
        else{
            self->locationStatus = IRLocationStatusDenied;
            NSError *error = [NSError errorWithDomain:@"Location Services switch is Off in General." code:kCLErrorDenied userInfo:nil];
            [self locationManager:locationManager didFailWithError:error];
            
        }
        if (!locationStatus) {
            locationStatus = IRLocationStatusUpdating;
        }
        
    }
}

-(void)stopLocationTracking{
    
    locationManager.delegate = nil;
    self->flagTrackingLocationStarts = NO;
    [locationManager stopUpdatingLocation];
}


#pragma mark -
#pragma mark CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorized) {
        self->locationStatus = IRLocationStatusAuthorizedAndUpdating;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_LOCATION_STATUS_CHANGED object:self];
    }
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    
    double lat1 = newLocation.coordinate.latitude;
    double lon1 = newLocation.coordinate.longitude;
    
 
   
    NSLog(@"Current Location:Lat:%f Lon:%f",lat1,lon1);

    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:lat1] forKey:@"Lat"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:lon1] forKey:@"Lon"];



    locationStatus = IRLocationStatusUpdated;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_LOCATION_STATUS_CHANGED object:self];
    
 
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading{
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error


{
    
    //    if(locationStatus != IRLocationStatusDenied || locationStatus != IRLocationStatusUnknown){
    //        [IRAnalytics localyticsEvent:@"Location_Disallow"];
    //        [IRAnalytics localyticsDimentionLocationAccess:@"Location_Disallow"];
    //    }
    
    self->flagTrackingLocationStarts = NO;
    //manager.delegate = nil;
    //[manager stopUpdatingLocation];
    
    NSMutableString *errorString = [[NSMutableString alloc] init];
    
    if ([error domain] == kCLErrorDomain) {
        
        // CoreLocation-related errors are handled here
        
        switch ([error code]) {
                // This error code is usually returned whenever user taps "Don't Allow" in response to
                // being told your app wants to access the current location. Once this happens, you cannot
                // attempt to get the location again until the app has quit and relaunched.
                //
                // "Don't Allow" on two successive app launches is the same as saying "never allow". The user
                // can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
                //
            case kCLErrorDenied:
            {
                [errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationDenied", nil)];
                
                if (!isAlertShown && !self->isLocationAlertDisplaying) {
                    isAlertShown = YES;
                    self->isLocationAlertDisplaying = YES;
                    if (isAlertShown) {
                        UIAlertView *amsg=[[UIAlertView alloc]initWithTitle:kAPPLICATION_NAME message:[NSString stringWithFormat:@"Please enable the location services settings for %@, to enjoy the best of our services.",kAPPLICATION_NAME] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        amsg.tag = 3001;
                        [amsg show];
                    }
                    
                }
                
                locationStatus = IRLocationStatusDenied;
                //isSetLocationToCurrentLocation = NO;
//                IRLocation *defaultLocation = [PLAccessor defaultLocation];
//                [IRLocation storeLocation:defaultLocation forKey:kLOCATION_CURRENT_LOCATION];
//                [IRLocation storeLocation:nil forKey:kLOCATION_SETTED_LOCATION];
            }
                break;
                
                // This error code is usually returned whenever the device has no data or WiFi connectivity,
                // or when the location cannot be determined for some other reason.
                //
                // CoreLocation will keep trying, so you can keep waiting, or prompt the user.
                //
            case kCLErrorLocationUnknown:
            {
                locationStatus = IRLocationStatusUnknown;
                //[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
                [errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
//                IRLocation *defaultLocation1 = [PLAccessor defaultLocation];
//                [IRLocation storeLocation:defaultLocation1 forKey:kLOCATION_CURRENT_LOCATION];
//                [IRLocation storeLocation:nil forKey:kLOCATION_SETTED_LOCATION];
            }
                break;
                
                // We shouldn't ever get an unknown error code, but just in case...
                //
            default:
            {
                [errorString appendFormat:@"%@ %d\n", NSLocalizedString(@"GenericLocationError", nil), [error code]];
            }
                
                break;
        }
    } else {
        // We handle all non-CoreLocation errors here
        // (we depend on localizedDescription for localization)
        locationStatus = IRLocationStatusUnknown;
        [errorString appendFormat:@"Error domain: \"%@\"  Error code: %d\n", [error domain], [error code]];
        [errorString appendFormat:@"Description: \"%@\"\n", [error localizedDescription]];
    }
//    if (IROBO_DEBUG) {
//        DebugLog(@"Location manager error:%@",errorString);
//    }
//    
    
    //DebugLog(@"Location Error: %@", errorString);
    [[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_LOCATION_STATUS_CHANGED object:self];
    
}

@end
