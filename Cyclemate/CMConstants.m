//
//  CMConstants.m
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

//NSString * const CMSetting = @"Setting_small";
NSString * const CMSetting = @"Setting";
NSString * const CMMain = @"Main";
//NSString * const CMMain = @"Main_small";
NSString * const CMBuddy = @"Buddy";
//NSString * const CMBuddy = @"Buddy_small";
//NSString * const CMMapride = @"MapRide_small";
NSString * const CMMapride = @"MapRide";
NSString * const CMNewBike = @"NewBike";





#import "CMConstants.h"

@implementation CMConstants : NSObject 


+(CGSize)screenSize
{
    
    CGSize screenSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    screenSize.height = screenSize.height-(IS_IOS7?0:20);
    return screenSize;
}

+(CGSize)screenSizeWithNavbar
{
    CGSize screenSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    screenSize.height = screenSize.height;
    return screenSize;
}


@end
