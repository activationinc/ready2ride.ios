//
//  CMConstants.h
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//
//com.7 Media.ondemand
//com.activationmobile.cyclemate

#import <Foundation/Foundation.h>

//https://github.com/ShareKit/ShareKit/wiki/Installing-ShareKit

#define CM_OWNER_REGISTER  @"CMOWNERREGISTER"


#define OWNER_ID       [UIAppDelegate getOwnerID]


#define BIKE_LOCATION_KEY  @"BIKELOCATION"


#if TARGET_IPHONE_SIMULATOR


#define  IMAGESOURSETYPE      UIImagePickerControllerSourceTypePhotoLibrary// Rajesh

#else

#define  IMAGESOURSETYPE  UIImagePickerControllerSourceTypeCamera

#endif


#define    ZoomlatitudeDelta       0.01
#define    ZoomlongitudeDelta      0.01

#define DarkBlurColor     [UIColor colorWithRed:9.0f/255.0f green:54.0f/255.0f blue:71.0f/255.0f alpha:1.0]

#define LightBlurColor      [UIColor colorWithRed:65.0f/255.0f green:65.0f/255.0f blue:66.0f/255.0f alpha:1.0]

#define DebugLog(args...) NSLog(args); //_DebugLog(__FILE__,__LINE__,__PRETTY_FUNCTION__,args);
//#define DebugLog(x...)
#define DEBUG_LOCATION NO
#define DEBUG_MEMORY_WARNING NO
#define DEBUG_FLImageView NO


typedef enum IRLocationStatus {
    IRLocationStatusUpdating = 1,
    IRLocationStatusUpdated,
    IRLocationStatusUnknown,
    IRLocationStatusDenied,
    IRLocationNotUpdated,
    IRLocationStatusAuthorizedAndUpdating
}IRLocationStatus;


#define kNOTIFICATION_LOCATION_STATUS_CHANGED  @"NOTIFICATION_LOCATION_STATUS_CHANGED"
#define kNOTIFICATION_LOCATION_DOWNLOAD_CANCELLED  @"NOTIFICATION_LOCATION_DOWNLOAD_CANCELLED"
#define kNOTIFICATION_PUBLISH_TRIP_OBSERVER_NAME  @"NOTIFICATION_PUBLISH_TRIP_OBSERVER_NAME"
#define kNOTIFICATION_CHECK_FOR_PUBLISH_TRIP_OBSERVER_NAME  @"NOTIFICATION_CHECK_FOR_PUBLISH_TRIP_OBSERVER_NAME"
#define kNOTIFICATION_LOCATION_CHANGED  @"NOTIFICATION_LOCATION_CHANGED"
#define kkNOTIFICATION_PUBLISH_TRIP_SHOW_DESC_POP_OBSERVER_NAME @"NOTIFICATION_PUBLISH_TRIP_SHOW_DESC_POP_OBSERVER_NAME"
#define kNOTIFICATION_PUBLISH_TRIP_SHOW_DESC_POP_OBSERVER_NAME  @"NOTIFICATION_PUBLISH_TRIP_SHOW_DESC_POP_OBSERVER_NAME"
#define kLOCATION_SETTED_LOCATION_DETAIL @"LOCATION_SETTED_LOCATION_DETAIL"
#define kNOTIFICATION_UPDATE_DASHBOARD @"NOTIFICATION_UPDATE_DASHBOARD"
#define kNOTIFICATION_UPDATE_ME_SECTION @"kNOTIFICATION_UPDATE_ME_SECTION"
#define kNOTIFICATION_UPDATE_MENU @"kNOTIFICATION_UPDATE_MENU"
#define kNOTIFICATION_CITY_CHANGED @"NOTIFICATION_CITY_CHANGED"


#define kNOTIFICATION_GET_WEATHERDATA  @"NOTIFICATION_GET_WEATHERDATA"



#define kAPPLICATION_NAME  @"Ready2Ride"
#define kCHOOSEPICTURE_From_Library 2
#define kCHOOSEPICTURE_Take_Photo   1




#define IsBikeParkKey             @"IsBikePark"

#define BikeLocationNameKey       @"BikeLocationName"

#define CycleDealerInfoKey      @"CycleDealerInfo"


#define CycleLocationNameKey      @"CycleLocationName"
#define CyclePreferenceKey        @"CyclePreferenceResult"
#define CycleNotificationKey      @"CycleNotificationKey"


#define CycleWeatherKey           @"CycleWeatherKey"
#define Cycle5DaysWeatherKey      @"Cycle5DaysWeatherKey"


#define ValidationUUIDKey         @"ValidationUUIDKey"

#define CycleValidationUUIDKey    @"CycleValidationUUID"

#define OwnerBuddyGroupListKey    @"OwnerBuddyGroupList"

#define OwnerCycleListKey         @"OwnerCycleList"


#define DealerID   [UIAppDelegate getDealerID]

#define OrgnizationName   [UIAppDelegate getDealerInfo:@"OrganizationName"]



#define redioInActiveImage   [UIImage imageNamed:@"radio.png"]
#define redioActiveImage  [UIImage imageNamed:@"radio_active.png"]

#define  NormalImage  [UIImage imageNamed:@"unchecked.png"]
#define  SelectedImage  [UIImage imageNamed:@"checked.png"]

//#define IS_IPHONE_5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


#define PopUpBaseBk [UIColor colorWithRed:201.0f/255.0f green:217.0f/255.0f blue:221.0f/255.0f alpha:1.0]


#define  segmentSelectedColor  [UIColor whiteColor]
#define  segmentUnSelectedColor  [UIColor colorWithRed:243.0f/255.0f green:243.0f/255.0f blue:243.0f/255.0f alpha:1.0]

//System Version Checking
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//OS version comparision
#define _IR_IOS_VERSION_7 @"7.0"
#define _IR_IOS_VERSION_6 @"6.0"
#define _IR_IOS_VERSION_5 @"5.0"

//#define kLatitudeDelta 0.008
//#define kLongitudeDelta 0.008

#define kLatitudeDelta 0.0
#define kLongitudeDelta 0.0


#define IR_IS_SYSTEM_VERSION_LESS_THAN_5_0 SYSTEM_VERSION_LESS_THAN(_IR_IOS_VERSION_5)
#define IR_IS_SYSTEM_VERSION_LESS_THAN_6_0 SYSTEM_VERSION_LESS_THAN(_IR_IOS_VERSION_6)
#define IR_IS_SYSTEM_VERSION_LESS_THAN_7_0 SYSTEM_VERSION_LESS_THAN(_IR_IOS_VERSION_7)

typedef NS_ENUM(NSInteger, BootomTabBar)  {
    BikerBootomTabBar=0,
    MapBootomTabBar=1,
    BuddyBootomTabBar=2,
    SettingBootomTabBar=3,
    HomeBootomTabBar=4,
} ;


#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)


#define  BASEURL         @"http://198.12.154.24"  // Prod
//
//
//#define Service_URL   [NSString stringWithFormat:@"%@/7MediaService/CycleMate.svc",BASEURL]  //New



#define Service_URL   [NSString stringWithFormat:@"http://apiv1.ready2ride.bike/CycleMate.svc"]


//CycleMate.svc/GetOwner/

#define ImageServerPath       [NSString stringWithFormat:@"%@:8080/cyclemate",BASEURL]
#define ImageUploadURL        [NSString stringWithFormat:@"%@/image_uploading.php",ImageServerPath]





#define GetCycleServiceLog [NSString stringWithFormat:@"%@/GetCycleServiceLog/CycleId=",Service_URL]

#define GetOwner           [NSString stringWithFormat:@"%@/GetOwner/OwnerId=",Service_URL]

#define GetCycleInsurance  [NSString stringWithFormat:@"%@/GetCycleInsurance/CycleInsuranceId=",Service_URL]

#define GetCyclePreference [NSString stringWithFormat:@"%@/GetCyclePreference/CycleId=",Service_URL]

#define GetDealerByCycleId [NSString stringWithFormat:@"%@/GetDealerByCycleId/CycleId=",Service_URL]

#define GetDealerEvent     [NSString stringWithFormat:@"%@/GetEvent/EventId=",Service_URL]

#define GetDealerSpeacialByOwnerId   [NSString stringWithFormat:@"%@/GetPromotion/PromotionId=",Service_URL]

#define GetCycleService     [NSString stringWithFormat:@"%@/GetCycleService/CycleId=",Service_URL]

#define GetCycleHealthColor [NSString stringWithFormat:@"%@/GetCycleHealthColor/CycleId=",Service_URL]

#define GetCycleServiceList [NSString stringWithFormat:@"%@/GetCycleServiceList/CycleId=",Service_URL]

#define GetCycleHealth      [NSString stringWithFormat:@"%@/GetCycleHealth/CycleId=",Service_URL]

#define GetDealer           [NSString stringWithFormat:@"%@/GetDealer/DealerId=",Service_URL]

#define GetDealerLocation   [NSString stringWithFormat:@"%@/GetDealerLocation/DealerId=",Service_URL]











extern NSString * const CMSetting;
extern NSString * const CMBuddy;
extern NSString * const CMMapride;
extern NSString * const CMNewBike;


extern NSString * const CMMain;

@interface CMConstants : NSObject
+(CGSize)screenSize;
+(CGSize)screenSizeWithNavbar;
@end
