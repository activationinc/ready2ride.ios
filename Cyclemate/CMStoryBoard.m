//
//  CMStoryBoard.m
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMStoryBoard.h"

@implementation CMStoryBoard

+ (UIStoryboard*)storyboardWithName:(NSString*)name
{
    
    return [UIStoryboard storyboardWithName:name bundle:nil];

    if (IS_IPHONE_5) {
        return [UIStoryboard storyboardWithName:name bundle:nil];
    }else{
        NSString *storyboardName = [NSString stringWithFormat:@"%@_small",name];
        return [UIStoryboard storyboardWithName:storyboardName bundle:nil];

    }
    
}

@end
