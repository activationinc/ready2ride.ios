//
//  NFLanguageVC.m
//  NewsApp
//
//  Created by Rajesh on 3/31/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "NFLanguageVC.h"


@interface NFLanguageVC ()
{
    IBOutlet UISegmentedControl *segmentCnt;
    
    UIRefreshControl *refreshControl;

    IBOutlet UITableView *dealerTableView;
    
    NSArray *dealerArr;
}

@end

@implementation NFLanguageVC
@synthesize delegate;


-(IBAction)closePopu:(id)sender{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }

}






- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
  
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(loadDealerInfo) forControlEvents:UIControlEventValueChanged];
    [dealerTableView addSubview:refreshControl];
    [self loadDealerInfo];
}

-(void)loadDealerInfo{
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetDealer/DealerId=0",Service_URL];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            dealerArr = [resultDict objectForKey:@"GetDealerResult"];
            
            [dealerTableView reloadData];
            
        }
    }];

}
#pragma mark -
#pragma mark UITableView Method Implementation Call



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return .1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return   UITableViewCellEditingStyleNone;
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
      
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.1;
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
//{
//    return 0.1;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  dealerArr.count;//5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DealerCell"];
    
    UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    
    UILabel *labeName = (UILabel *)[cell viewWithTag:2];
   // UILabel *labeOrg = (UILabel *)[cell viewWithTag:3];

    

    
    NSDictionary *dic = [dealerArr objectAtIndex:indexPath.row];
    
    labeName.text = [dic stringValueForKey:@"OrganizationName"];//[dic stringValueForKey:@"Name"];
    //labeOrg.text  = [dic stringValueForKey:@"Name"];
    
   NSString *imagePath = [dic stringValueForKey:@"LogoImagePath"];//[ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
   [placeholderImage setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil];
    
    
    
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor clearColor]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[dealerArr objectAtIndex:indexPath.row]];
    NSDictionary *userDic = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSLog(@"dealer info..%@",userDic);
    //NSString *dealerID = [userDic stringValueForKey:withKey];
    [UIAppDelegate.userDefault setObject:data forKey:CycleDealerInfoKey];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
    
    
    

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
