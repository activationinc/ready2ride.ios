//
//  CMSaveRideController.h
//  Cyclemate
//
//  Created by Sourav on 02/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMSaveRide.h"
#import "GAITrackedViewController.h"


@interface CMSaveRideController : GAITrackedViewController

@property (nonatomic, strong) NSMutableArray *rideData;
@property(nonatomic)BOOL isFromHome;


@end
