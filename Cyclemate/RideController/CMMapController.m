//
//  CMMapController.m
//  Cyclemate
//
//  Created by Sourav on 03/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMMapController.h"

@interface CMMapController ()

-(IBAction)backBtnClicked:(id)sender;

@end

@implementation CMMapController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MAP & RIDE Track" navigationItem:self.navigationItem];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:MapBootomTabBar]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark IBAction delegate methods

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}





@end
