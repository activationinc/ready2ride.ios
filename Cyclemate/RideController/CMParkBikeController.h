//
//  CMParkBikeController.h
//  Cyclemate
//
//  Created by Sourav on 03/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "GAITrackedViewController.h"
@interface CMParkBikeController : GAITrackedViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>{

    int zoomlavel;
    
    IBOutlet UIView *currentView;
    
    IBOutlet UIImageView *bikeImage;
    IBOutlet UIView *parkBikeView;

}
@property(nonatomic)BOOL isFromHome;
@property (assign, nonatomic) NSInteger locationRequestID;
-(void)resetBtnClicked;
@end
