//
//  CMAddPhotoController.m
//  Cyclemate
//
//  Created by Sourav on 02/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMAddPhotoController.h"

@interface CMAddPhotoController (){
    IBOutlet UIImageView *imageView;
    IBOutlet UIButton *retakeBtn;
    IBOutlet UIButton *chooseLibraryBtn;
}

-(IBAction)clickPhotoBtnClicked:(id)sender;
-(IBAction)selectPhotoBtnClicked:(id)sender;


-(IBAction)applyBtnClicked:(id)sender;


@end

@implementation CMAddPhotoController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:[@"Park My Vehicle" uppercaseString] navigationItem:self.navigationItem];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:MapBootomTabBar]];
    
    retakeBtn.layer.borderWidth = 1.0;
    //retakeBtn.layer.borderColor = [UIColor colorWithRed:245/255 green:67/255 blue:11/255 alpha:1.0].CGColor;
    retakeBtn.layer.borderColor = [UIColor orangeColor].CGColor;
    
    chooseLibraryBtn.layer.borderWidth = 1.0;
    //chooseLibraryBtn.layer.borderColor = [UIColor colorWithRed:245/255 green:67/255 blue:11/255 alpha:1.0].CGColor;
    chooseLibraryBtn.layer.borderColor = [UIColor orangeColor].CGColor;

    //chooseLibraryBtn.layer.borderColor = [UIColor blackColor].CGColor;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark IBAction delegate methods

-(IBAction)applyBtnClicked:(id)sender;
{

    [UIAppDelegate.userDefault setBool:YES forKey:IsBikeParkKey];

    [UIAppDelegate saveImage: imageView.image WithName:@"Bike.png"];
    [self.navigationController popViewControllerAnimated:YES];


}

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(IBAction)clickPhotoBtnClicked:(id)sender{
    
    
    if (![UIImagePickerController isSourceTypeAvailable:IMAGESOURSETYPE]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error !!"
                                                              message:@"Current Device does not support the  camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        return;
        
    }
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    picker.sourceType = IMAGESOURSETYPE;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

-(IBAction)selectPhotoBtnClicked:(id)sender{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

#pragma mark UIImagePickerControllerDelegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    imageView.image = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



@end
