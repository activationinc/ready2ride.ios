//
//  CMNewRideSetUpVC.m
//  Cyclemate
//
//  Created by Rajesh on 3/9/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

typedef NS_ENUM(NSUInteger, RideActionType) {
    TopSegmentAction,
    BackButtonAction,
    RightPopUpAction,
    NoAction=0,
};

#define CLCOORDINATE_EPSILON 0.005f
#define CLCOORDINATES_EQUAL2( coord1, coord2 ) (fabs(coord1.latitude - coord2.latitude) < CLCOORDINATE_EPSILON && fabs(coord1.longitude - coord2.longitude) < CLCOORDINATE_EPSILON)

#import "CMNewRideSetUpVC.h"
#import <MapKit/MapKit.h>
#import "DropDownListView.h"
#import "CMHelper.h"
#import "CMCustomAnnotation.h"
#import "CMRideNotesDetailVC.h"

#import "CMDirectDealerMailVC.h"

#import "ASBlockTimer.h"

@interface CMNewRideSetUpVC ()<CLLocationManagerDelegate,UITextFieldDelegate,MKMapViewDelegate,kDropDownListViewDelegate,DirectDealerMailDelegate>
{
    NSMutableArray *popUpDataArr;
    IBOutlet MKMapView *mapView;
    IBOutlet UIView *baseMapView;
    
    IBOutlet UIButton *upDownArrow;

    
    UIImagePickerController *imagePicker;
    DropDownListView * Dropobj;
    
    IBOutlet CMButton_DropDown *rideTypeBtn;

    IBOutlet UIButton *rideStartBtn;
    IBOutlet UIButton *ridePauseBtn;
    IBOutlet UIButton *rideEndBtn;

    IBOutlet UIView *rideCameraView;
    IBOutlet UIView *rideNoteView;
    IBOutlet UIView *rideShareView;
    
    IBOutlet UIButton *rideCameraBtn;
    IBOutlet UIButton *rideNoteBtn;
    IBOutlet UIButton *rideShareBtn;
    
    IBOutlet UITextField *rideNameTextField;
    IBOutlet UITextField *startDateTextField;
    
    IBOutlet UITextView *descriptionText;
    
    IBOutlet UIButton *rideSaveBtn;
    
    IBOutlet UIView *sideRideView;
    IBOutlet UIView *topSelectView;
    
    CLLocationCoordinate2D notesCord;
    
    NSMutableArray *notesArr;
    
    
    UIButton *topActionBtn;
    
    RideActionType actiontype;

    
}
@property (nonatomic,strong)NSArray *rideTypeArrays;

@end

@implementation CMNewRideSetUpVC
@synthesize locationRequestID;

@synthesize isNewRide;
@synthesize rideDic;






- (UIBarButtonItem*)backBarButtonItem
{
    static UIBarButtonItem *backButton;
    
    if (IR_IS_SYSTEM_VERSION_LESS_THAN_7_0){
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [button setImage:[UIImage imageNamed:@"Back_Button.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    else{
        backButton =  [[UIBarButtonItem alloc]
                       initWithImage:[[UIImage imageNamed:@"Back_Button.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
                       target:self
                       action:@selector(backBtnClicked:)];
    }

    
    
    return backButton;
}
-(void)backBtnClicked:(id)sender{
    
    actiontype = BackButtonAction;


    if (!UIAppDelegate.isRidePermission && isNewRide){
        [self openRideSetupView];
        return;
    }else
        [self.navigationController popViewControllerAnimated:YES];

}

-(void)openRideSetupView{
    
    if (!UIAppDelegate.isRidePermission && isNewRide) {
        NSString *message =   @"Do you want to save this ride?";
        [[UIAlertView alertViewWithTitle:@"Alert"
                                 message:message cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Ok"]
                               onDismiss:^(int buttonIndex) {
                                   if (rideScrollView.hidden) {
                                       [self expandOrShrinkView:upDownArrow];

                                   }
                                   
                                   
                               } onCancel:^{
                                   
                                   
                                   
                                   NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:rideID,@"RideId",OWNER_ID,@"OwnerId", nil];
                                   
                                   
                                   
                                   [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteRide",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                       if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                           NSLog(@"*** getbrands %@ **",resultDict);
                                           
                                           UIAppDelegate.isRidePermission = YES;
                                           [self.navigationController popViewControllerAnimated:YES];
                                           
                                       }
                                   }];
                                   
                                   
                                   
                                   
                                   
                               }] show];
        

    }
    
    
    
    
    
    
    
    
    
}



- (MKOverlayView *)mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay{
    
    
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineView* aView = [[MKPolylineView alloc]initWithPolyline:(MKPolyline*)overlay] ;
        aView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        aView.lineWidth = 10;
        return aView;
    }
    return nil;
}


-(void)drawRoute1:(CLLocationCoordinate2D )currentChord withDes:(CLLocationCoordinate2D)desChord{
    
  
    
    MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:currentChord addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    [srcMapItem setName:@""];
    
    MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:desChord addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    [distMapItem setName:@""];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeAny];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        NSLog(@"response = %@",response);
        
        if (error == nil || response) {
            
            NSArray *arrRoutes = [response routes];
            [arrRoutes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                MKRoute *rout = obj;
                
                MKPolyline *line = [rout polyline];
                [mapView addOverlay:line];
                NSLog(@"Rout Name : %@",rout.name);
                NSLog(@"Total Distance (in Meters) :%f",rout.distance);
                
                
                NSArray *steps = [rout steps];
                
                NSLog(@"Total Steps : %d",[steps count]);
                
                [steps enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSLog(@"Rout Instruction : %@",[obj instructions]);
                    NSLog(@"Rout Distance : %f",[obj distance]);
                }];
            }];
            
        }else{
            
            if ( currentChord.latitude != 0 && currentChord.longitude != 0 ){
                NSLog(@"Coordinate valid");
                mapView.region = MKCoordinateRegionMake(currentChord, MKCoordinateSpanMake(0.5,0.5));
            }
            

            /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry !" message:error.description delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"Ok action") otherButtonTitles:nil];
             [alertView show];*/
        }
    }];
}



-(void)getPathDetailsForRide{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    
    
    [[CMNetworkManager sharedInstance] getResponse:[NSString stringWithFormat:@"%@/GetRideState/OwnerId=%@/RideId=%@",Service_URL,ownerId,rideID]  requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            
            int k = 0;
            NSArray *dataArr = [resultDict objectForKey:@"GetRideStateResult"];
            for (NSDictionary *dic in  dataArr) {
                
                if ([dataArr count]>(k+1)) {
                    
                   // [self drawRoute1];
                    
                    NSDictionary *currDic = dic;//[dataArr objectAtIndex:k];
                    NSDictionary *destDic = [dataArr objectAtIndex:k+1];
                    
                    
                    CLLocationCoordinate2D currChor;
                    currChor.latitude = [[currDic objectForKey:@"Lattitude"] floatValue];
                    currChor.longitude = [[currDic objectForKey:@"Longititude"] floatValue];
                    
                    
                    CLLocationCoordinate2D destChor;
                    destChor.latitude = [[destDic objectForKey:@"Lattitude"] floatValue];
                    destChor.longitude = [[destDic objectForKey:@"Longititude"] floatValue];

                    [self drawRoute1:currChor withDes:destChor];
                    
                    
                    mapView.region = MKCoordinateRegionMake(currChor, MKCoordinateSpanMake(0.5,0.5));
                
                }
//                    break;
                k++;
            }
            
            
        }
    }];
    
    
}



-(void)cancleTimeCallBack{
    [[ASBlockTimer sharedTimers] cancelAllBlockTimers];


}
/**
 *  Updateing Path Details
 *
 *  @param locationChord current Lat long
 */

-(void)insertPathDetails:(CLLocationCoordinate2D)locationChord{
    
    if (!rideID) {
        return;
    }
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    lastChord = locationChord;
    
    /**
     *  Getting Dicnory
     */
    NSNumber *lat = [NSNumber numberWithDouble:locationChord.latitude];
    NSNumber *lon = [NSNumber numberWithDouble:locationChord.longitude];
    NSDictionary *lastChordDic =@{@"lat":lat,@"long":lon};
    [[NSUserDefaults standardUserDefaults] setObject:lastChordDic forKey:@"lastChord"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:rideID, @"RideId",  ownerId, @"OwnerId",
                             @"0", @"RideStateId",
                             lat, @"Lattitude",
                             lon, @"Longititude",
                             nil];
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateRideState",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            
            
            
            
            
        }
    }];


}

-(void)keepUpdatingLoaction{

    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
  int MidLavelId   =  [locMgr subscribeToLocationUpdatesWithBlock:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        if (status == INTULocationStatusSuccess) {
            // A new updated location is available in currentLocation, and achievedAccuracy indicates how accurate this particular location is.
            
            currChord = currentLocation.coordinate;
            
            
            if (!CLCOORDINATES_EQUAL2( currChord, forMapChord)) {
                forMapChord = currChord;
                
                mapView.region = MKCoordinateRegionMake(currChord,MKCoordinateSpanMake(0.2,0.1) );
                
                
//                [UIAppDelegate.userDefault setObject:[NSString stringWithFormat:@"%.4f", currChord.latitude] forKey:@"Lat"];
//                [UIAppDelegate.userDefault  setObject:[NSString stringWithFormat:@"%.4f",currChord.longitude] forKey:@"Lon"];
                [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:currChord.latitude] forKey:@"Lat"];
                [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:currChord.longitude] forKey:@"Lon"];
                
                
                [UIAppDelegate.userDefault  synchronize];
            }
            
        }
        else {
            // An error occurred, more info is available by looking at the specific status returned. The subscription has been automatically canceled.
        }
    }];

    if (MidLavelId!=locationUpdateID  && locationUpdateID>0) {
        [locMgr cancelLocationRequest:locationUpdateID];
    }
    locationUpdateID = MidLavelId;

}

-(void)startTimerUpdate{
    
    //CLLocationCoordinate2D currChord;

    
    
    
    
    [self cancleTimeCallBack];
    NSTimer *myTimer = [[ASBlockTimer sharedTimers] scheduleBlockTimerWithTimeInterval:300 userInfo:nil repeats:YES completion:^(NSTimer *timer) {
    
        NSLog(@"Repeats...");
    
    if (!CLCOORDINATES_EQUAL2( currChord, lastChord)) {
        lastChord = currChord;
        [self insertPathDetails:currChord];
    }

 
    
    
   
}];
}

-(void)getDropDownDataForMessage:(BOOL)showActivity{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetContactListWithGroupName/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];
    
    if (showActivity)
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetContactListWithGroupName %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetContactListWithGroupNameResult"];
            [popUpDataArr removeAllObjects];
            
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
            [UIAppDelegate.userDefault setObject:data forKey:OwnerBuddyGroupListKey];
            
            for (NSDictionary *dic in arr) {
                if (![[dic  stringValueForKey:@"Status"] isEqualToString:@"Dealer"]) {
                    [popUpDataArr addObject:dic];
                }
            }
            
            
            
        }
        
        if ([popUpDataArr count]) {
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Ride Share With" withOption:[popUpDataArr valueForKey:@"Name"] xy:CGPointMake(10, 100) size:CGSizeMake(300, MIN(200, [popUpDataArr count]*50+50)) isMultiple:NO withTagValue:99];
            
        }
        [SVProgressHUD dismiss];
    }];


}
-(void)getDropDownDataForMessage{
    [self getDropDownDataForMessage:YES];
  
    
}

-(IBAction)rideShareBtnClicked:(id)sender{
    
    if (!UIAppDelegate.isRidePermission && isNewRide){
        [self openRideSetupView];
        return;
    }
    
    if ([popUpDataArr count]) {
        [Dropobj fadeOut];
        [self showPopUpWithTitle:@"Select Ride Share With" withOption:[popUpDataArr valueForKey:@"Name"] xy:CGPointMake(10, 100) size:CGSizeMake(300, MIN(200, [popUpDataArr count]*50+50)) isMultiple:NO withTagValue:99];
        [self getDropDownDataForMessage:NO];

    }
    else
        [self getDropDownDataForMessage];
    
    
    
}

-(void)hideRideSetUpView{

    
    float yMargin = 210;
    
    CGRect baseMapFrame = baseMapView.frame;
    CGRect mapFrame = mapView.frame;
    [upDownArrow setTag:1];
    baseMapFrame.origin.y-=yMargin;
    baseMapFrame.size.height+=yMargin;
    mapFrame.size.height+=yMargin;
    [upDownArrow setImage:[UIImage imageNamed:@"downarrow.png"] forState:UIControlStateNormal];
    baseMapView.frame = baseMapFrame;
    mapView.frame = mapFrame;
    rideScrollView.hidden = YES;
    
}

- (IBAction)expandOrShrinkView:(id)sender {
    
    float yMargin = 210;
    
    CGRect baseMapFrame = baseMapView.frame;
    CGRect mapFrame = mapView.frame;
    UIButton *expandBtn = ( UIButton *)sender;
    
    
   
    if ([expandBtn tag]) {

        [expandBtn setTag:0];
        baseMapFrame.origin.y+=yMargin;
        baseMapFrame.size.height-=yMargin;
        mapFrame.size.height-=yMargin;
       [upDownArrow setImage:[UIImage imageNamed:@"uparrow.png"] forState:UIControlStateNormal];
        
    }else{
        [expandBtn setTag:1];
        baseMapFrame.origin.y-=yMargin;
        baseMapFrame.size.height+=yMargin;
        mapFrame.size.height+=yMargin;
        [upDownArrow setImage:[UIImage imageNamed:@"downarrow.png"] forState:UIControlStateNormal];
    }
 
    [UIView animateWithDuration:0.8f delay:0.0f options:UIViewAnimationOptionTransitionNone animations: ^{
        baseMapView.frame = baseMapFrame;
        mapView.frame = mapFrame;
        if ( [expandBtn tag]){
            rideScrollView.hidden = YES;
        }

    } completion:^ (BOOL completed) {
        
        
        if (completed && ![expandBtn tag]) {
            rideScrollView.hidden = NO;
        }
        else{
            rideScrollView.hidden = YES;

        }
    
        

    }];
}



-(IBAction)rideCameraBtnClicked:(id)sender{
    [self showUIImagePicker];
}

-(IBAction)rideNotesBtnClicked:(id)sender{
    CMDirectDealerMailVC *secondDetailViewController = [[CMDirectDealerMailVC alloc] initWithNibName:@"CMDirectDealerMailVC" bundle:nil];
    secondDetailViewController.delegate = self;
    secondDetailViewController.isSendBuddy = NO;
    secondDetailViewController.titleText = @"Ride Notes";
    
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
    
}


-(void)rideIntilizationPopUp{
    
    /*
     CreatedDate = "1/1/1900 ";
     CycleId = 113;
     Description = "";
     EndDate = "<null>";
     EndGeoLat = "";
     EndGeoLong = "";
     EndLocation = "";
     GroupId = "";
     IsBuddyInvite = 1;
     IsMileageShare = 0;
     IsRide = S;
     IsWeatherAlert = 0;
     Name = testtext;
     OwnerId = "";
     RideId = 74;
     RideShareId = "";
     RideStatus = "<null>";
     RideTypeId = "";
     SenderOwnerId = "";
     StartDate = "<null>";
     StartGeoLat = "";
     StartGeoLon = "";
     StartLocation = "";
     */
    NSString *message =   [NSString stringWithFormat:@"Already, there is a active ride. If you start this ride that will close your active ride '%@'.",[activeRideDic stringValueForKey:@"Name"]];
    [[UIAlertView alertViewWithTitle:@"Alert"
                             message:message cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Continue"]
                           onDismiss:^(int buttonIndex) {
                             
                               
                               NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                               
                               NSString *ridetypeId  = [activeRideDic stringValueForKey:@"RideTypeId"];
                               if (![ridetypeId length])
                                   ridetypeId = @"0";
                               
                               NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[activeRideDic stringValueForKey:@"CycleId"], @"CycleId",     ownerId, @"OwnerId",
                                                        [activeRideDic stringValueForKey:@"Name"], @"Name",
                                                        [activeRideDic stringValueForKey:@"Description"], @"Description",
                                                        [activeRideDic stringValueForKey:@"CreatedDate"], @"CreatedDate",
                                                        [activeRideDic stringValueForKey:@"IsBuddyInvite"], @"IsBuddyInvite",
                                                        [activeRideDic stringValueForKey:@"RideId"], @"RideId",
                                                        @"E",@"IsRide",
                                                        ridetypeId,@"RideTypeId",
                                                        nil];
                               
                               
                               
                               [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateRide",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                   NSLog(@"*** save set up ride %@ **",resultDict);
                                   
                                 
                               }
                                ];
                               
                               activeRideDic = nil;
                               
                               [self topSegmentBtnClicked:rideStartBtn];
                               
                           } onCancel:^{
                               
                               
                               
                           
                               
                               
                               
                               
                               
                           }] show];



}


-(IBAction)topSegmentBtnClicked:(id)sender{
    
    
    actiontype = TopSegmentAction;
    
    topActionBtn = (UIButton *)sender;
    
    if ([isRide isEqualToString:@"E"]) {
        return;
    }
    
    if ([activeRideDic count]&&([sender tag]==1)) {
        [self rideIntilizationPopUp];
        return;
    }

    
     if (!UIAppDelegate.isRidePermission && isNewRide){
         [self openRideSetupView];
        return;
     }
    
    
    
    
    
    [rideStartBtn setImage:[UIImage imageNamed:@"start_icon_not_selected.png"] forState:UIControlStateNormal];
    [ridePauseBtn setImage:[UIImage imageNamed:@"pause_icons_not_selected.png"] forState:UIControlStateNormal];
    [rideEndBtn setImage:[UIImage imageNamed:@"end_icon_not_selected.png"] forState:UIControlStateNormal];

    rideStartBtn.backgroundColor = ridePauseBtn.backgroundColor = rideEndBtn.backgroundColor = DarkBlurColor;
    
    UIButton *selectesBtn = topActionBtn;
    selectesBtn.backgroundColor = LightBlurColor;
    
    NSString *rideStatus;
    
    if ([sender tag]==3) {
        rideCameraBtn.enabled = NO;
        rideNoteBtn.enabled = NO;
        rideShareBtn.enabled = NO;
    }else{
        rideCameraBtn.enabled = YES;
        rideNoteBtn.enabled = YES;
        rideShareBtn.enabled = YES;
    }
    [self cancleTimeCallBack];

    switch ([sender tag]) {
        case 1:{
            
            
            [rideStartBtn setImage:[UIImage imageNamed:@"start_icon_selected.png"] forState:UIControlStateNormal];
            rideStatus = @"S";
            [self startTimerUpdate];
            [self keepUpdatingLoaction];
        }
            break;
        case 2:{
            [ridePauseBtn setImage:[UIImage imageNamed:@"pause_icon_selected.png"] forState:UIControlStateNormal];
            rideStatus = @"P";
            [self keepUpdatingLoaction];

        }
            break;
        case 3:{
            [self getPathDetailsForRide];

            [rideEndBtn setImage:[UIImage imageNamed:@"end_icons_selected.png"] forState:UIControlStateNormal];
            rideStatus = @"E";
            rideShareBtn.enabled = YES;



        }
            break;
            
        default:
            break;
    }
  //  if (!isNewRide) {
        isRide = rideStatus;
 //   }
    [self updateStatus];

    
    if (rideID) {
        [self insertPathDetails:currChord];
    }

    
    NSDictionary *currzDic = [[NSUserDefaults standardUserDefaults] objectForKey:@"currLocation"];
    
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:[[currzDic objectForKey:@"lat"] floatValue] longitude:[[currzDic objectForKey:@"long"] floatValue]];
    [self setCurrentCityAndState:loc];
    
    
}
-(void)updateStatus{
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    NSString *rideTypeId =     [rideTypeBtn selectedObjectID];

    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys: @"113", @"CycleId",     ownerId, @"OwnerId",
                             rideNameTextField.text, @"Name",
                             descriptionText.text, @"Description",
                             rideDate, @"CreatedDate",
                             @"1", @"IsBuddyInvite",
                             rideID, @"RideId",
                             isRide,@"IsRide",
                             rideTypeId,@"RideTypeId",
                             nil];
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateRide",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
               NSLog(@"*** save set up ride %@ **",resultDict);
        
        if (!rideID  || ![rideID length]) {
            rideID = [resultDict stringValueForKey:@"response"];
            [self insertPathDetails:currChord];
        }
        
        if (isNewRide) {
            rideID = [resultDict stringValueForKey:@"response"];
            UIAppDelegate.isRidePermission = NO;
        }
    }
     ];
    


}
-(IBAction)saveRiderClicked:(id)sender{
    
    NSLog(@"saveBtnClick");
    
    //Input Parameter: CycleId, OwnerId, Name, RideTypeId, Description, IsBuddyInvite, CreatedDate, RideId
    
    NSString *rideTypeId =     [rideTypeBtn selectedObjectID];

    
    if (![rideNameTextField.text length]) {
        [CMHelper alertMessageWithText:@"Please fill in ride name."];
    }
    /*else     if (![descriptionText.text length]) {
        [CMHelper alertMessageWithText:@"Please fill in ride description."];
    }
    else     if (![startDateTextField.text length]) {
        [CMHelper alertMessageWithText:@"Please fill in start date."];
    }else     if ([rideTypeId isEqualToString:@"0"]) {
        [CMHelper alertMessageWithText:@"Please fill in Ridetype."];
    }*/
  
    else{
        
        if (![isRide length] || !isRide) {
            isRide = @"P";
        }
        
//        S Stands for start
//            P Statnds for Pause
//                E Stands for End
        
        //Note:-In IsRide parameter pass ‘S’ for Start ,pass ‘P’ for Pause or pass ‘E’ for end otherwise pass null value
        NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
        
        NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys: @"113", @"CycleId",     ownerId, @"OwnerId",
                                 rideNameTextField.text, @"Name",
                                 descriptionText.text, @"Description",
                                   rideDate, @"CreatedDate",
                                 @"1", @"IsBuddyInvite",
                                 rideID, @"RideId",
                                 isRide,@"IsRide",
                                 rideTypeId,@"RideTypeId",
                                 nil];
  

        
          [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateRide",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
              [CMHelper alertMessageWithText:[rideID intValue]?@"You have successfully updated your ride.":@"Congratulations you have successfully created your ride."];
              isNewRide = NO;

              
              if (!UIAppDelegate.isRidePermission) {
                  UIAppDelegate.isRidePermission = YES;
                  if (actiontype==TopSegmentAction) {
                      [self topSegmentBtnClicked:topActionBtn];
                  }else if (actiontype==BackButtonAction) {
                      [self.navigationController popViewControllerAnimated:YES];
                  }

              }
              
              if (![rideID integerValue]) {
                  [self.navigationController popViewControllerAnimated:YES];
              }
              
              if (!rideScrollView.hidden) {
                  [self.view endEditing:YES];
                  [upDownArrow setTag:0];
                  [self expandOrShrinkView:upDownArrow];
              }
              /*
              else if (!UIAppDelegate.isRidePermission){
              
                  [self expandOrShrinkView:upDownArrow];
              }*/

              NSLog(@"*** save set up ride %@ **",resultDict);

          }
           ];
        
    }
}
-(void)checkForActiveRide{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    

    
    
    [[CMNetworkManager sharedInstance] getResponse:[NSString stringWithFormat:@"%@/GetActiveRide/OwnerId=%@",Service_URL,ownerId]  requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            NSArray *arr = [resultDict objectForKey:@"GetActiveRideResult"];
            activeRideDic = nil;
            if ([arr count]) {
                activeRideDic = [arr objectAtIndex:0];
            }
            
            
        }
    }];
    
    
}



-(void)showUIImagePicker{
    
    
    @try {
        //        IstoCamera = YES;
        
        // dispatch_async(dispatch_get_current_queue(), ^(void){
       
        imagePicker.delegate = self;
        imagePicker.sourceType = IMAGESOURSETYPE;//UIImagePickerControllerSourceTypePhotoLibrary;//;
        imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
        [self.navigationController presentViewController:imagePicker animated:YES completion:NULL];
        
        //  } ) ;
        
    }
    @catch (NSException * e) {
        UIAlertView *alertmsg=[[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Couldn’t locate the camera, check your settings and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertmsg show];
    }
    @finally {
    }
    
}



#pragma mark -
#pragma mark CMDirectDealerMailVCDelegate Methods
- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController withSend:(BOOL)isSend messageText:(NSString *)messageText{
    
    
 

    
    [self updateRideWithNotesORImage:@"" withNotes:messageText];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
  //  [self getNotesPhotoForRide];
}


- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)getNotesPhotoForRide{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

   
    
//GetRideNote/OwnerId=0/RideId=0
    
    
    [[CMNetworkManager sharedInstance] getResponse:[NSString stringWithFormat:@"%@/GetRideNote/OwnerId=%@/RideId=%@",Service_URL,ownerId,rideID]  requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            
            if ([[mapView annotations] count])             [mapView removeAnnotations:[mapView annotations]];
            
            if ([notesArr count]) {
                [notesArr removeAllObjects];
            }

            
          //  notesArr = [resultDict objectForKey:@"GetRideNoteResult"];
            int k =0;
            for (NSDictionary *dic  in [resultDict objectForKey:@"GetRideNoteResult"]) {
                [notesArr addObject:dic];
                
                CLLocationCoordinate2D coordinate;
                coordinate.latitude = [[dic stringValueForKey:@"Lattitude"] floatValue];
                coordinate.longitude = [[dic stringValueForKey:@"Longititude"] floatValue];
                

                
                CMCustomAnnotation *bikeLocationPoint = [[CMCustomAnnotation  alloc] init];
                bikeLocationPoint.coordinate = coordinate;
                bikeLocationPoint.title = [dic stringValueForKey:@"Note"];
                bikeLocationPoint.imageUrl = [dic stringValueForKey:@"ImageUrl"];
                bikeLocationPoint.pinId = [NSString stringWithFormat:@"%d",k];
                bikeLocationPoint.infoDic = dic;
                
                
                notesCord = coordinate;
                
                if (![bikeLocationPoint.title length]) bikeLocationPoint.title =  @"   ";
                
                [mapView addAnnotation:bikeLocationPoint];
                
                k++;
            }
            
            
            if ( notesCord.latitude != 0 && notesCord.longitude != 0 ){
                NSLog(@"Coordinate valid");
                mapView.region = MKCoordinateRegionMake(notesCord,MKCoordinateSpanMake(0.2,0.1) );
            }
            
            

            
        }
    }];
    

}

#pragma mark - WebService Implementation
-(void)postDataToServer:(NSDictionary *)withInfoDic withPostURL:(NSString *)urlStr
{
   
    [[CMNetworkManager sharedInstance] postDataResponse:urlStr param:withInfoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            
            [CMHelper alertMessageWithText:[rideID intValue]?@"You have successfully updated your ride.":@"Congratulations you have successfully save your ride."];
        }
    }];
    
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    popUpDataArr = [NSMutableArray new];
    notesArr = [NSMutableArray new];
    [self checkForActiveRide];

    actiontype = NoAction;

   //  UIImagePickerController *
    imagePicker = [[UIImagePickerController alloc] init];
    
    self.navigationItem.leftBarButtonItem =[self backBarButtonItem];// [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MAP & RIDE Track" navigationItem:self.navigationItem];
    
    
    [rideTypeBtn setSelectedObjectID:@"0"];

    
    _rideTypeArrays = [NSArray arrayWithObjects:@"The Getaway",@"The Afternoon Delight",@"The Day Tripper",@"The Weekender",@"The Touring",@"Chaptering", nil];

    
    [startDateTextField setTextFieldType:@"Date"];
    // Do any additional setup after loading the view.
    
    
    rideCameraView.clipsToBounds = YES;
    rideCameraView.layer.cornerRadius = CGRectGetWidth(rideCameraView.frame)/2;//half of the width
    rideCameraView.layer.borderColor=[UIColor clearColor].CGColor;
    rideCameraView.layer.borderWidth=2.0f;
    
    
    rideNoteView.clipsToBounds = YES;
    rideNoteView.layer.cornerRadius = CGRectGetWidth(rideNoteView.frame)/2;//half of the width
    rideNoteView.layer.borderColor=[UIColor clearColor].CGColor;
    rideNoteView.layer.borderWidth=2.0f;
    
    
    rideShareView.clipsToBounds = YES;
    rideShareView.layer.cornerRadius = CGRectGetWidth(rideShareView.frame)/2;//half of the width
    rideShareView.layer.borderColor=[UIColor clearColor].CGColor;
    rideShareView.layer.borderWidth=2.0f;
    
    mapView.showsUserLocation = YES;
    
    
    NSDictionary *lastChordDic = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastChord"];
    lastChord.latitude = [[lastChordDic objectForKey:@"lat"] floatValue];
    lastChord.longitude = [[lastChordDic objectForKey:@"long"] floatValue];
    
    [self hideRideSetUpView];
    
    UIAppDelegate.isRidePermission = YES;

    if (isNewRide) {
        
        rideCameraBtn.enabled = NO;
        rideNoteBtn.enabled = NO;
        rideShareBtn.enabled = NO;
        
        [rideSaveBtn setTitle:@"SAVE" forState:UIControlStateNormal];

        rideID = @"0";
        
        rideDate = [MCFDate dateStringFromDate:[NSDate date] format:@"MM/dd/yyyy"];
    }else{
      

        rideCameraBtn.enabled = YES;
        rideNoteBtn.enabled = YES;
        rideShareBtn.enabled = YES;
        
        
        //rideSaveBtn.hidden = YES;
        [rideSaveBtn setTitle:@"UPDATE" forState:UIControlStateNormal];

        rideID = [rideDic stringValueForKey:@"RideId"];
        isRide = [rideDic stringValueForKey:@"RideStatus"];
        
        rideNameTextField.text = [rideDic stringValueForKey:@"Name"];
        descriptionText.text = [rideDic stringValueForKey:@"Description"];
        
        rideDate = [rideDic stringValueForKey:@"CreatedDate"];
        
        //            RideTypeId = 1;
        
        [rideTypeBtn setSelectedObjectID:[rideDic stringValueForKey:@"RideTypeId"]];
        [rideTypeBtn setTitle:[ _rideTypeArrays objectAtIndex:[[rideDic stringValueForKey:@"RideTypeId"] integerValue]] forState:UIControlStateNormal];

        [self getNotesPhotoForRide];
        
    
        
        
        
        [rideStartBtn setImage:[UIImage imageNamed:@"start_icon_not_selected.png"] forState:UIControlStateNormal];
        [ridePauseBtn setImage:[UIImage imageNamed:@"pause_icons_not_selected.png"] forState:UIControlStateNormal];
        [rideEndBtn setImage:[UIImage imageNamed:@"end_icon_not_selected.png"] forState:UIControlStateNormal];
        
        rideStartBtn.backgroundColor = ridePauseBtn.backgroundColor = rideEndBtn.backgroundColor = DarkBlurColor;
        
      
        if([isRide length]){
        switch ([isRide characterAtIndex:0]) {
            case 'S':{
                [rideStartBtn setImage:[UIImage imageNamed:@"start_icon_selected.png"] forState:UIControlStateNormal];
                rideStartBtn.backgroundColor = LightBlurColor;
                [self startTimerUpdate];
                [self keepUpdatingLoaction];

                isRide = @"S";
            }
                break;
            
            case 'E':{
                [rideEndBtn setImage:[UIImage imageNamed:@"end_icons_selected.png"] forState:UIControlStateNormal];
                rideEndBtn.backgroundColor = LightBlurColor;
                isRide = @"E";

                
                rideCameraBtn.enabled = NO;
                rideNoteBtn.enabled = NO;
                rideShareBtn.enabled = YES;
                [self getPathDetailsForRide];

            }
                break;
                
            default:
            case   'P':{
                isRide = @"P";

                [ridePauseBtn setImage:[UIImage imageNamed:@"pause_icon_selected.png"] forState:UIControlStateNormal];
                ridePauseBtn.backgroundColor = LightBlurColor;
                
                    [self keepUpdatingLoaction];


            }
                break;
                break;
        }
        }
        else{
            isRide = @"P";

//            rideCameraBtn.enabled = NO;
//            rideNoteBtn.enabled = NO;
//            rideShareBtn.enabled = NO;
            [ridePauseBtn setImage:[UIImage imageNamed:@"pause_icon_selected.png"] forState:UIControlStateNormal];
            ridePauseBtn.backgroundColor = LightBlurColor;
        }

    }
    

    

}
-(void)viewWillAppear:(BOOL)animated{
    
    
    [self startLocationRequest];
    

    
    if ([popUpDataArr count])
        [popUpDataArr removeAllObjects];
    
    
    NSArray *contactList = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:OwnerBuddyGroupListKey]];
    
    for (NSDictionary *dic in contactList) {
        if (![[dic  stringValueForKey:@"Status"] isEqualToString:@"Dealer"]) {
            [popUpDataArr addObject:dic];
        }
    }

    
    
}

#pragma mark
#pragma mark UITextFieldDelegate Methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Ride Description"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Ride Description";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
       
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    
    if ([[textField textFieldType] isEqualToString:@"Date"]) {
        selectedField =textField;
        [self.view endEditing:YES];
        [self datePickerViewClicked:nil];
        
        return NO;
        
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
}
#pragma mark - IQActionSheet DatePicker


-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    
    [selectedField setText:[titles componentsJoinedByString:@" - "]];
#if 0
    switch (pickerView.tag)
    {
        case 1: [buttonSingle setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 2: [buttonDouble setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 3: [buttonTriple setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 4: [buttonRange setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 5: [buttonTripleSize setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 6: [buttonDate setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
            
        default:
            break;
    }
#endif
}

- (IBAction)datePickerViewClicked:(UIButton *)sender
{
    //IQActionSheetPickerView *
    
    if (!picker) picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    [picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}


#pragma mark - Collapse Click Delegate

- (IBAction)DropDownSingle:(id)sender {
    UIButton *btn = ( UIButton *)sender;
    
    [Dropobj fadeOut];
    // [self showPopUpWithTitle:@"Ride Type" withOption:_rideTypeArrays xy:CGPointMake(16, 150) size:CGSizeMake(287, 280) isMultiple:NO];
    
    CGRect frame = btn.frame;//[self.view convertRect:btn.frame fromView:rideDetailView];
    
    [self showPopUpWithTitle:@"Ride Type" withOption:_rideTypeArrays xy:frame.origin size:CGSizeMake(300, MIN(200, _rideTypeArrays.count*50+50)) isMultiple:NO withTagValue:11];
    
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple withTagValue:(int)tagValue{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    Dropobj.tag = tagValue;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/

    if (dropdownListView.tag==99) {
        
        
        NSDictionary *tempDic = [popUpDataArr objectAtIndex:anIndex];
        
        NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
        NSString *message ;
        NSString *senderOwnerId    = [dic stringValueForKey:@"OwnerId"];
        
         NSString *ownerId    = [[popUpDataArr objectAtIndex:anIndex] stringValueForKey:@"ContactId"];

        
        NSString *groupId    = @"";
        if ([[tempDic stringValueForKey:@"Status"] isEqualToString:@"Owner"]) {
            message  = [NSString stringWithFormat:@"Do you want to share this Ride details with %@?", [[popUpDataArr valueForKey:@"Name"] objectAtIndex:anIndex]];
            
            groupId = @"0";
            
        }
        else{
            
            message  = [NSString stringWithFormat:@"Do you want to share this Ride details with %@ Group?", [[popUpDataArr valueForKey:@"Name"] objectAtIndex:anIndex]];
            
            groupId = [[popUpDataArr  objectAtIndex:anIndex] stringValueForKey:@"ContactId"];
            ownerId = @"0";
            
        }

        
        
        [[UIAlertView alertViewWithTitle:@"Alert"
                                 message:message cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"]
                               onDismiss:^(int buttonIndex) {
                                   
                                   //InsertUpdateShareRide
                                   //Input Parameter: RideId, GroupId, OwnerId, RideShareId(output parameter pass   always zero)

                                   
                                   NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[rideDic stringValueForKey:@"RideId"],@"RideId",groupId,@"GroupId",ownerId,@"OwnerId",senderOwnerId,@"SenderOwnerId",@"0",@"RideShareId", nil];
                                   
                                   
                                   [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateShareRide",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                       if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                           NSLog(@"*** getbrands %@ **",resultDict);
                                           
                                       }
                                   }];
                                   
                                   
                                   
                                   
                                   
                               } onCancel:^{
                                   
                                   
                               }] show];
    }
    else{
    [rideTypeBtn setTitle:[_rideTypeArrays objectAtIndex:anIndex] forState:UIControlStateNormal];
        [rideTypeBtn setSelectedObjectID:[NSString stringWithFormat:@"%d",anIndex+1]];}
    
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
    
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}



#pragma mark -
#pragma mark UIImagePickerControllerDelegate Methods

-(void)updateRideWithNotesORImage:(NSString *)imageUrl withNotes:(NSString *)notesStr{
    
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [[UIAppDelegate.userDefault objectForKey:@"Lat"] floatValue];
    coordinate.longitude = [[UIAppDelegate.userDefault objectForKey:@"Lon"] floatValue];
    
    CMCustomAnnotation *bikeLocationPoint = [[CMCustomAnnotation  alloc] init];
    bikeLocationPoint.coordinate = coordinate;
    bikeLocationPoint.imageUrl = imageUrl;
    bikeLocationPoint.title =  notesStr;
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:rideID, @"RideId",ownerId, @"OwnerId", notesStr, @"Note",
                             [imageUrl length]?[imageUrl lastPathComponent]:@"", @"ImageName",
                             imageUrl, @"ImageUrl",
                             @"0", @"RideNoteId",
                             [UIAppDelegate.userDefault objectForKey:@"Lat"], @"Lattitude",
                             [UIAppDelegate.userDefault objectForKey:@"Lon"], @"Longititude",
                             nil];
    
    
    
    NSMutableDictionary *mutDic = [infoDic mutableCopy];
    [mutDic setObject:[MCFDate dateStringFromDate:[NSDate date] format:@"MM/dd/yyyy"] forKey:@"CreatedDate"];
    [notesArr addObject:mutDic];
    
    
    bikeLocationPoint.infoDic = mutDic;
    [mapView addAnnotation:bikeLocationPoint];
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateRideNote",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            [self insertPathDetails:currChord];
            
            
        }
    }];
}

-(void)postImageWithServiceType:(UIImage * )imageFile{
    
    
    
    if (imageFile) {
        
      NSString  *imageName = [NSString stringWithFormat:@"%@_user_%d.png",[[FCUUID uuidForDevice] substringFromIndex:10],arc4random()%9999];
      NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[NSString stringWithFormat:@"uploads/%@",imageName]];
        [self updateRideWithNotesORImage:[NSString stringWithFormat:@"uploads/%@",imageName] withNotes:@""];
        
        NSLog(@"imagePath with manual  = %@",imagePath);

        
        [[CMNetworkManager sharedInstance] postImage:ImageUploadURL  withImage:imageFile    withImageName:imageName withImageSize:CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT)  param:nil   requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            if ([resultDict count]) {
                NSDictionary *tempDic =  [(NSArray *)resultDict objectAtIndex:0];
                NSLog(@"imagePath with server  = %@",[tempDic stringValueForKey:@"image_url"]);
                }
      
            
            
            
            
        }];
    }
    
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
        [self postImageWithServiceType:image];
        
        
        
        
        [imagePicker dismissViewControllerAnimated:YES completion:nil];
        
        
        
        
    }
}

- (void)startLocationRequest
{
    __weak __typeof(self) weakSelf = self;
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                                                timeout:10
                                                   delayUntilAuthorized:YES
                                                                  block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                      __typeof(weakSelf) strongSelf = weakSelf;
                                                                      
                                                                      if (status == INTULocationStatusSuccess) {
                                                                          // achievedAccuracy is at least the desired accuracy (potentially better)
                                                                          NSLog(@"Location request successful! Current Location:\n%@", currentLocation);
                                                                          [self setCurrentCityAndState:currentLocation];
                                                                      }
                                                                      else if (status == INTULocationStatusTimedOut) {
                                                                          // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                                                                          NSLog(@"Location request timed out. Current Location:\n%@", currentLocation);
                                                                          [self setCurrentCityAndState:currentLocation];
                                                                          
                                                                      }
                                                                      else {
                                                                          // An error occurred
                                                                          if (status == INTULocationStatusServicesNotDetermined) {
                                                                              NSLog(@"Error: User has not responded to the permissions alert.");
                                                                          } else if (status == INTULocationStatusServicesDenied) {
                                                                              NSLog(@"Error: User has denied this app permissions to access device location.");
                                                                          } else if (status == INTULocationStatusServicesRestricted) {
                                                                              NSLog(@"Error: User is restricted from using location services by a usage policy.");
                                                                          } else if (status == INTULocationStatusServicesDisabled) {
                                                                              NSLog( @"Error: Location services are turned off for all apps on this device.");
                                                                          } else {
                                                                              NSLog( @"An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)");
                                                                          }
                                                                      }
                                                                      strongSelf.locationRequestID = NSNotFound;
                                                                  }];
}


-(void)setCurrentCityAndState:( CLLocation *)newLocation{
    
//    [UIAppDelegate.userDefault setObject:[NSString stringWithFormat:@"%.4f", newLocation.coordinate.latitude] forKey:@"Lat"];
//    [UIAppDelegate.userDefault  setObject:[NSString stringWithFormat:@"%.4f", newLocation.coordinate.longitude] forKey:@"Lon"];
    
    [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:newLocation.coordinate.latitude] forKey:@"Lat"];
    [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:newLocation.coordinate.longitude] forKey:@"Lon"];
    
    [UIAppDelegate.userDefault  synchronize];
    
   
    if (!([isRide characterAtIndex:0] == 'E')) {
        mapView.showsUserLocation = YES;

//        if (isNewRide) {
//            [self setLocation:newLocation.coordinate inBottomCenterOfMapView:mapView];
//        }
//        else
        
        if (notesCord.latitude == 0 && notesCord.longitude == 0 ){
            mapView.region = MKCoordinateRegionMake(newLocation.coordinate,MKCoordinateSpanMake(0.2,0.1) );
            
        }else
            mapView.region = MKCoordinateRegionMake(notesCord,MKCoordinateSpanMake(0.2,0.1) );
    }
    else {
    
        mapView.showsUserLocation = NO;
    }
   

    
    
        // mapView.region = MKCoordinateRegionMake(newLocation.coordinate, MKCoordinateSpanMake(ZoomlatitudeDelta,ZoomlongitudeDelta));
    
    
}


-(void)setLocation:(CLLocationCoordinate2D)location inBottomCenterOfMapView:(MKMapView*)mapView
{
    //Get the region (with the location centered) and the center point of that region
    MKCoordinateRegion oldRegion = [mapView regionThatFits:MKCoordinateRegionMakeWithDistance(location, 800, 800)];
    CLLocationCoordinate2D centerPointOfOldRegion = oldRegion.center;
    
    //Create a new center point (I added a quarter of oldRegion's latitudinal span)
    CLLocationCoordinate2D centerPointOfNewRegion = CLLocationCoordinate2DMake(centerPointOfOldRegion.latitude + oldRegion.span.latitudeDelta/4.0, centerPointOfOldRegion.longitude);
    
    //Create a new region with the new center point (same span as oldRegion)
    MKCoordinateRegion newRegion = MKCoordinateRegionMake(centerPointOfNewRegion, oldRegion.span);
    
    //Set the mapView's region
    [mapView setRegion:newRegion animated:YES];
    
    
//mapView.region = MKCoordinateRegionMake(centerPointOfNewRegion,MKCoordinateSpanMake(location.latitude,location.longitude) );

}



#pragma mark -
#pragma mark MKMapViewDelegate
// creates MKAnnotationViews
- (MKAnnotationView *)mapView:(MKMapView *)mView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    
    static NSString *AnnotationViewID = @"annotationViewID";
    
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil)
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
    }
    
    //annotationView.image = [UIImage imageNamed:@"location.png"];
    annotationView.annotation = annotation;
    
    if (![annotation.title isEqualToString: @"   "])
        annotationView.image = [UIImage imageNamed:@"note_event_pin.png"];
    else
        annotationView.image = [UIImage imageNamed:@"camera_event_pin.png"];

    
    return annotationView;
    
    
    UIImageView *dropImage = nil;
    
    MKPinAnnotationView *pin = nil;
    static NSString *reuseId = @"StandardPin";
    pin =  (MKPinAnnotationView*)[mView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if(pin == nil) {
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        pin.canShowCallout = YES;
      
        
    }
    // pin.frame = CGRectMake(0, 0, 100, 60);
    NSLog(@"title = ====%@",annotation.title);
    
    pin.canShowCallout = YES;//[UIAppDelegate.userDefault boolForKey:IsBikeParkKey]?NO:YES;
    
    pin.leftCalloutAccessoryView = nil;
    //CMCustomAnnotation *currLoc = (CMCustomAnnotation *)annotation;
    
    if (![annotation.title isEqualToString: @"   "]) {
        pin.image = [UIImage imageNamed:@"note_event_pin.png"];
        [dropImage removeFromSuperview];

    }
    else{
        pin.image = [UIImage imageNamed:@"camera_event_pin.png"];
      /*
        dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        dropImage.tag = 11;
        //dropImage.image = [UIImage imageNamed:@"staining.png"];
        
        NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[currLoc imageUrl]];
        [dropImage setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil];

        pin.leftCalloutAccessoryView =dropImage ;*/

    }
    
    
    //   pin.leftCalloutAccessoryView = currentView;
    pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
     pin.canShowCallout = NO;
    return pin;
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    CMCustomAnnotation *annotation = [view annotation];
    if(![annotation.title isEqualToString:@"Current Location"]) {
        
    }
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
        NSLog(@"annotation touched: %@", view.annotation.title);
    CMCustomAnnotation *annotation = [view annotation];
    [mapView deselectAnnotation:annotation animated:YES];
  
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return ;
    }
    CMRideNotesDetailVC *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideNotesDetailVC"];
    controller.infoDic = annotation.infoDic;
    controller.itemsArr = notesArr;
    controller.currIndex = [annotation.pinId integerValue];
    
    [self.navigationController pushViewController:controller animated:YES];
    
    //CMRideNotesDetailVC
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
