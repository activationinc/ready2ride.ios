//
//  CMMapShowViewController.h
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMMapShowViewController : UIViewController
-(IBAction)bottomViewBtnClicked:(id)sender;

-(IBAction)backBtnClicked:(id)sender;

-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)topMenuBtnClicked:(id)sender;

@end
