//
//  CMRideEditController.h
//  Cyclemate
//
//  Created by Sourav on 07/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMSaveRide.h"

@protocol CMRideEditControllerDelegate;

@interface CMRideEditController : UIViewController

@property(nonatomic,weak)id<CMRideEditControllerDelegate> delagate;

@property (nonatomic, readonly, strong) NSIndexPath *indexPathInTableView;
@property (nonatomic, readonly, strong) CMSaveRide *saveRide;
@property (nonatomic,readonly,strong) IBOutlet UITextView *editebaleTextField;


//@property(nonatomic,strong)NSMutableDictionary *dict;

- (void)initWithPhotoRecord:(CMSaveRide *)saveRide atIndexPath:(NSIndexPath *)indexPath delegate:(id<CMRideEditControllerDelegate>) theDelegate;

@end

@protocol CMRideEditControllerDelegate <NSObject>

-(void)saveInfo:(CMRideEditController *)controller;

@end

