//
//  CMRideSetUpController.h
//  Cyclemate
//
//  Created by Sourav on 02/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQActionSheetPickerView.h"

@interface CMRideSetUpController : UIViewController<IQActionSheetPickerViewDelegate>{
IQActionSheetPickerView *picker;
    UITextField *selectedField;

}

@property(nonatomic)BOOL isSetUp;

@end
