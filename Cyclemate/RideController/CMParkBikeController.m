//
//  CMParkBikeController.m
//  Cyclemate
//
//  Created by Sourav on 03/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//
#import "UIViewController+MJPopupViewController.h"
#import "CMDirectDealerMailVC.h"

#import "CMParkBikeController.h"
#import "CMAddPhotoController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "CMAddPhotoController.h"
#import "CMCustomAnnotation.h"
#import "RegexKitLite.h"

#import "DropDownListView.h"
#import "CMPhotoViewVC.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAILogger.h"
#import "GAIFields.h"
#import "GAITrackedViewController.h"
#import "GAITracker.h"


@interface CMParkBikeController ()<CLLocationManagerDelegate,MKMapViewDelegate,DirectDealerMailDelegate,kDropDownListViewDelegate>{
    IBOutlet UISegmentedControl *upperSegmentControl;
    CLLocationManager *locationManager;
    MKPointAnnotation *bikePoint;
    CMCustomAnnotation *currentLocationPoint;
    CMCustomAnnotation *bikeLocationPoint;
    
    
    CMCustomAnnotation *shareLocationPoint;

    
    
    IBOutlet UIButton *findBtn;
    NSDictionary *bikeLocationDict;
    NSArray *arrRoutePoints;
    MKPolyline *line;
    IBOutlet MKMapView *mapView;
    CLGeocoder *geocoder;
    
    
    IBOutlet UIButton *shareBtn;
    IBOutlet UIButton *notesBtn;
    
    BOOL isShareView;
    
    NSMutableArray *popUpDataArr;

    DropDownListView * Dropobj;
    
    NSMutableArray *shareLocationArr;

}

@property(nonatomic,weak)IBOutlet MKMapView *mapView;
@property(nonatomic,strong)MKPolyline *objPolyline;


-(IBAction)backBtnClicked:(id)sender;
-(IBAction)showPhotoClicked:(id)sender;
-(IBAction)endLocationBtnClicked:(id)sender;
-(IBAction)findBtnClicked:(id)sender;

-(IBAction)zoomINBtnClicked:(id)sender;
-(IBAction)zoomOUTBtnClicked:(id)sender;

@end

@implementation CMParkBikeController
@synthesize isFromHome;
@synthesize locationRequestID;


-(void)markIsReadShareLocation
{
   

    
    // NSString *notificationId =[bikeNumberDic stringValueForKey:@"NotificationId"];//
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             OWNER_ID, @"OwnerId",nil];
    
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/MarkIsReadShareLocation",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}



-(IBAction)openBikePhoto:(id)sender{
    if (bikeImage.image) {
        CMPhotoViewVC *secondDetailViewController = [[CMPhotoViewVC alloc] initWithNibName:@"CMPhotoViewVC" bundle:nil];
        secondDetailViewController.controller = self;

        secondDetailViewController.photoImage = bikeImage.image;
        //bikeImage
        [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
    }
 
}

-(void)resetSharedLoactionData{
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    NSString * ownerId    = [dic stringValueForKey:@"OwnerId"];
    
    
    
    [SVProgressHUD show];
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:     ownerId, @"OwnerId",    nil];
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/ResetShareLocation",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [SVProgressHUD dismiss];

        [self.navigationController popViewControllerAnimated:YES];

        
        NSLog(@"ResetShareLocation=%@=",resultDict);
    }
     ];

}

-(void)getShareParkingListData{
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    NSString * ownerId    = [dic stringValueForKey:@"OwnerId"];
    
    
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetShareLocation/OwnerId=%@",Service_URL,ownerId];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            
            NSArray *arr = [resultDict objectForKey:@"GetShareLocationResult"];
            if ([arr count]) {
                
          
                
                if (![UIAppDelegate.userDefault boolForKey:IsBikeParkKey]){

                    NSDictionary *onj = [arr objectAtIndex:0];
                    NSLog(@"*** GetShareLocationResult %@ **",onj);
                    
                NSNumber *lat = [NSNumber numberWithFloat:[[onj objectForKey:@"PostionLet"] floatValue]];
                NSNumber *lon = [NSNumber numberWithFloat:[[onj objectForKey:@"PostionLon"] floatValue]];
                NSDictionary *userLocation=@{@"lat":lat,@"long":lon};
                
                [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:BIKE_LOCATION_KEY];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [UIAppDelegate.userDefault setBool:YES forKey:IsBikeParkKey];
                }
                
                
                if ([UIAppDelegate.userDefault boolForKey:IsBikeParkKey]){
                    
                    [self.mapView removeAnnotations:self.mapView.annotations];
                    NSDictionary *currzDic = [[NSUserDefaults standardUserDefaults] objectForKey:@"currLocation"];
                    
                    CLLocationCoordinate2D coordinate;
                    coordinate.latitude = [[currzDic objectForKey:@"lat"] floatValue];
                    coordinate.longitude = [[currzDic objectForKey:@"long"] floatValue];
                    
                    
                    
                    if (!bikeLocationPoint) bikeLocationPoint = [[CMCustomAnnotation  alloc] init];
                    
                    bikeLocationPoint.coordinate = coordinate;
                    bikeLocationPoint.title =  @"FIND MY BIKE";
                    
                    
                    [self.mapView addAnnotation:bikeLocationPoint];
                    
                    self.mapView.region = MKCoordinateRegionMake(bikeLocationPoint.coordinate, MKCoordinateSpanMake(ZoomlatitudeDelta,ZoomlongitudeDelta));
                    [self.mapView selectAnnotation:bikeLocationPoint animated:YES];
                    
                }

            }
        }
        
    }];
    
    
}
/*
-(void)getShareParkingListData{
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    NSString * ownerId    = [dic stringValueForKey:@"OwnerId"];

 //   NSString * stringURL =  [NSString stringWithFormat:@"%@/GetParking/ParkingId=0/OwnerId=%@",Service_URL,ownerId];
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetShareLocation/OwnerId=%@",Service_URL,ownerId];
    
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetShareLocationResult %@ **",resultDict);
            
         
            NSArray *arr = [resultDict objectForKey:@"GetShareLocationResult"];//GetBuddyContactsDataResult\\
            
            
            if ([shareLocationArr count]) [shareLocationArr removeAllObjects];
            
            
            
            
           
            
            

            
            for (id onj in arr) {
                [shareLocationArr addObject:onj];
                
                
               
            }
        
            
            
            
         
                
                
            }
            //GetGroupBuddyByOwnerIDResult
        
    }];
    
    
}
*/
-(IBAction)zoomINBtnClicked:(id)sender{

    //NSLog(@"Zoom - IN");
    zoomlavel+=2;
    [mapView setCenterCoordinate:locationManager.location.coordinate zoomLevel:zoomlavel animated:YES];

}


-(IBAction)zoomOUTBtnClicked:(id)sender{
    
    zoomlavel-=2;

    [mapView setCenterCoordinate:locationManager.location.coordinate zoomLevel:zoomlavel animated:YES];


    
}
-(IBAction)bikeShareBtnClicked:(id)sender{

    [self.mapView removeAnnotations:self.mapView.annotations];
    if (line) {
        [self.mapView removeOverlay:line];
    }
    
    [self.mapView addAnnotation:shareLocationPoint];
    [self.mapView selectAnnotation:shareLocationPoint animated:YES];
    
    
    /**
     *  Comment all shared location point
     */
    
#if 0
    CMCustomAnnotation *buddyLocation;
    for (id onj in shareLocationArr) {
        
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [[onj objectForKey:@"PostionLet"] floatValue];
        coordinate.longitude = [[onj objectForKey:@"PostionLon"] floatValue];
        
        
        buddyLocation = [[CMCustomAnnotation  alloc] init];
        buddyLocation.coordinate = coordinate;
        buddyLocation.title = [onj stringValueForKey:@"Name"];
        [self.mapView addAnnotation:buddyLocation];
    }
    @try {
         if (buddyLocation)self.mapView.region = MKCoordinateRegionMake(buddyLocation.coordinate, MKCoordinateSpanMake(ZoomlatitudeDelta,ZoomlongitudeDelta));
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
#endif

}
-(IBAction)myBikeNotesBtnClicked:(id)sender{
    CMDirectDealerMailVC *secondDetailViewController = [[CMDirectDealerMailVC alloc] initWithNibName:@"CMDirectDealerMailVC" bundle:nil];
    secondDetailViewController.delegate = self;
    secondDetailViewController.isSendBuddy = NO;
    secondDetailViewController.isParkMyBike = YES;
    secondDetailViewController.titleText = @"Parking Notes";
    
       secondDetailViewController.massegeText = [UIAppDelegate.userDefault objectForKey:@"ParkingNotes"];

    
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];

}

-(void)resetBtnClicked;
{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

    [UIAppDelegate.userDefault removeObjectForKey:BIKE_LOCATION_KEY];
    [UIAppDelegate.userDefault removeObjectForKey:@"ParkingNotes"];
    [UIAppDelegate.userDefault setBool:NO forKey:IsBikeParkKey];
    
    [self resetSharedLoactionData];

    [CMAppDelegate setNavigationBarTitle:[UIAppDelegate.userDefault boolForKey:IsBikeParkKey]?@"FIND MY VEHICLE": @"PARK MY VEHICLE" navigationItem:self.navigationItem];
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    if (line) {
        [self.mapView removeOverlay:line];
    }
    
    if([[NSFileManager defaultManager] fileExistsAtPath:[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"imageInfo.plist"]])
    {
        [[NSFileManager defaultManager] removeItemAtPath:[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"imageInfo.plist"] error:nil];
        
        bikeImage.image = nil;
    }
    

}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self markIsReadShareLocation];

    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems =  [UIAppDelegate rightBarButtonsWithResetBtn] ;//[UIAppDelegate rightBarButtons];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:isFromHome?HomeBootomTabBar:MapBootomTabBar]];
    
    
    
    mapView.showsUserLocation = YES;
    
    popUpDataArr = [NSMutableArray new];
    
    shareLocationArr= [NSMutableArray new];
    /*
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    self.mapView.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager startUpdatingLocation];
     */
    self.mapView.delegate = self;
    bikeLocationDict = [[NSUserDefaults standardUserDefaults] objectForKey:BIKE_LOCATION_KEY];
   
    [self.mapView removeAnnotations:self.mapView.annotations];

    
    
    NSDictionary *currzDic = [[NSUserDefaults standardUserDefaults] objectForKey:@"currLocation"];
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [[currzDic objectForKey:@"lat"] floatValue];
    coordinate.longitude = [[currzDic objectForKey:@"long"] floatValue];

    if (![UIAppDelegate.userDefault boolForKey:IsBikeParkKey]){
        
        
        
        
        currentLocationPoint = [[CMCustomAnnotation  alloc] init];
        currentLocationPoint.coordinate = coordinate;
        currentLocationPoint.isCurrLocation = YES;
        currentLocationPoint.title = [UIAppDelegate.userDefault objectForKey:CycleLocationNameKey];

        [self.mapView addAnnotation:currentLocationPoint];
        

        self.mapView.region = MKCoordinateRegionMake(currentLocationPoint.coordinate, MKCoordinateSpanMake(ZoomlatitudeDelta,ZoomlongitudeDelta));
        [self.mapView selectAnnotation:currentLocationPoint animated:YES];
        
        

        
    }
    else
    {
     
     
        if (!bikeLocationPoint) bikeLocationPoint = [[CMCustomAnnotation  alloc] init];
        
        bikeLocationPoint.coordinate = coordinate;
        bikeLocationPoint.title =  @"FIND MY BIKE";
        
        
        [self.mapView addAnnotation:bikeLocationPoint];
        
        self.mapView.region = MKCoordinateRegionMake(bikeLocationPoint.coordinate, MKCoordinateSpanMake(ZoomlatitudeDelta,ZoomlongitudeDelta));
        [self.mapView selectAnnotation:bikeLocationPoint animated:YES];

    }
    if (!shareLocationPoint) shareLocationPoint = [[CMCustomAnnotation  alloc] init];
    shareLocationPoint.coordinate = coordinate;
    shareLocationPoint.isShareCurrLocation = YES;
    shareLocationPoint.title =  @"Share my location to buddy";
   

    
    [self getShareParkingListData];
    
    @try
    {
        
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"Park My Vehicle"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }
   // [self drawRoute1];
    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView * alert= [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                            message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}
-(void)viewDidAppear:(BOOL)animated{
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:@"UA-80658429-4"];
    [self.tracker set:kGAIScreenName value:@"Park My Vehicle"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}
-(IBAction)findBikeBtnClicked:(id)sender{
   
    if ([UIAppDelegate.userDefault boolForKey:IsBikeParkKey]){
      //  [self.mapView removeAnnotations:self.mapView.annotations];
        
        
        bikeLocationDict = [[NSUserDefaults standardUserDefaults] objectForKey:BIKE_LOCATION_KEY];
  
        
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [[bikeLocationDict objectForKey:@"lat"] floatValue];
        coordinate.longitude = [[bikeLocationDict objectForKey:@"long"] floatValue];
        
        if (!currentLocationPoint) currentLocationPoint = [[CMCustomAnnotation  alloc] init];
        currentLocationPoint.coordinate = coordinate;
        currentLocationPoint.title  = @"";//@"FIND MY BIKE";
        
        [self.mapView addAnnotation:currentLocationPoint];
        
        
        
    //    self.mapView.region = MKCoordinateRegionMake(currentLocationPoint.coordinate, MKCoordinateSpanMake(1,1));
        
        
    }
    
    // [self.mapView selectAnnotation:currentLocationPoint animated:YES];
    [self drawRoute1];
    
    [self.mapView deselectAnnotation:bikeLocationPoint animated:YES];

   
}
-(void)viewWillAppear:(BOOL)animated{
    
    if ([popUpDataArr count])
        [popUpDataArr removeAllObjects];

  
    NSArray *contactList = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:OwnerBuddyGroupListKey]];
    for (NSDictionary *dic in contactList) {
        if (![[dic  stringValueForKey:@"Status"] isEqualToString:@"Dealer"]) {
            [popUpDataArr addObject:dic];
        }
    }
    
//    if (<#condition#>) {
//        parkBikeView
//    }

     bikeImage.image  = [UIAppDelegate  readImageFromPlistByKey:@"Bike.png"];
    
    [self startLocationRequest];

    [CMAppDelegate setNavigationBarTitle:[UIAppDelegate.userDefault boolForKey:IsBikeParkKey]?@"FIND MY VEHICLE": @"PARK MY VEHICLE" navigationItem:self.navigationItem];
    
    

}

-(void)drawRoute1{
    
    if (line) {
        [self.mapView removeOverlay:line];
    }
    
    MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:currentLocationPoint.coordinate addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    [srcMapItem setName:@""];
    
    MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:bikeLocationPoint.coordinate addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    

    
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    [distMapItem setName:@""];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeAutomobile];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        NSLog(@"response = %@",response);
        
        if (error == nil || response) {
            
            NSArray *arrRoutes = [response routes];
            [arrRoutes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                MKRoute *rout = obj;
                
                line = [rout polyline];
                [self.mapView addOverlay:line];
                NSLog(@"Rout Name : %@",rout.name);
                NSLog(@"Total Distance (in Meters) :%f",rout.distance);
                
                NSArray *steps = [rout steps];
                
                NSLog(@"Total Steps : %d",[steps count]);
                
                [steps enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSLog(@"Rout Instruction : %@",[obj instructions]);
                    NSLog(@"Rout Distance : %f",[obj distance]);
                }];
            }];

        }else{
            if ( bikeLocationPoint.coordinate.latitude != 0 && bikeLocationPoint.coordinate.longitude != 0 ){
                NSLog(@"Coordinate valid");
                _mapView.region = MKCoordinateRegionMake(bikeLocationPoint.coordinate, MKCoordinateSpanMake(0.5,0.5));
            }
            /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry !" message:error.description delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"Ok action") otherButtonTitles:nil];
            [alertView show];*/
            
        }
            }];
}
- (MKAnnotationView *)mapView:(MKMapView *)mView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        //Don't trample the user location annotation (pulsing blue dot).
        return nil;

        if ([UIAppDelegate.userDefault boolForKey:IsBikeParkKey]) {
            MKPinAnnotationView *pin = nil;

            static NSString *reuseId = @"userPin";

            pin =  (MKPinAnnotationView*)[mView dequeueReusableAnnotationViewWithIdentifier:reuseId];
            if(pin == nil) {
                pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
            }
            pin.image = [UIImage imageNamed:@"you-here.png"];
            return pin;
        }
        else
          return nil;
    }
    
    MKPinAnnotationView *pin = nil;
    static NSString *reuseId = @"StandardPin";
    pin =  (MKPinAnnotationView*)[mView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if(pin == nil) {
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        pin.canShowCallout = YES;
        
    }
   // pin.frame = CGRectMake(0, 0, 100, 60);
    NSLog(@"title = ====%@",annotation.title);
    
    pin.canShowCallout = YES;//[UIAppDelegate.userDefault boolForKey:IsBikeParkKey]?NO:YES;

    pin.leftCalloutAccessoryView = nil;
   CMCustomAnnotation *currLoc = (CMCustomAnnotation *)annotation;
    
   if (currLoc.isCurrLocation) {
            pin.image = [UIImage imageNamed:@"parked.png"];
            pin.leftCalloutAccessoryView = currentView;

    }
   else if (currLoc.isShareCurrLocation) {
        //if ([annotation.title  isEqualToString:@"I am here"]) {
        pin.image = [UIImage imageNamed:@"parked.png"];
        pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeContactAdd];
    }
    else if ([annotation.title  isEqualToString:@"FIND MY BIKE"]){
        pin.image = [UIImage imageNamed:@"you-here.png"];
         //   pin.leftCalloutAccessoryView = currentView;
        pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
    }
    else{
    
        pin.image = [UIImage imageNamed:@"parked.png"];
        pin.leftCalloutAccessoryView = nil;
        pin.rightCalloutAccessoryView = nil;

    }
    //    [mapView selectAnnotation:currentLocationPoint animated:YES];


    return pin;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
//- (void)selectAnnotation:(id < MKAnnotation >)annotation animated:(BOOL)animated
    
    
    if (isShareView) {
         if (shareLocationPoint) [self.mapView selectAnnotation:shareLocationPoint animated:YES];
    }
    else{
    if (currentLocationPoint)[self.mapView selectAnnotation:currentLocationPoint animated:YES];
    }
   

}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
   CMCustomAnnotation *currLoc = (CMCustomAnnotation *)view.annotation;
    
    
    if (currLoc.isShareCurrLocation) {
        
        
       
        
        if ([popUpDataArr count]) {
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Share Location With" withOption:[popUpDataArr valueForKey:@"Name"] xy:CGPointMake(10, 100) size:CGSizeMake(300, MIN(200, [popUpDataArr count]*50+50)) isMultiple:NO];
            
        }
        else
            [self getDropDownDataForMessage];
        
        
        
        
    
    }
    else
        [self findBikeBtnClicked:0];
    
}

- (MKOverlayView *)mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay{
    

    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineView* aView = [[MKPolylineView alloc]initWithPolyline:(MKPolyline*)overlay] ;
        aView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        aView.lineWidth = 10;
        return aView;
    }
    return nil;
}


-(void)getDropDownDataForMessage{
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    
    
    NSString *ownerId    = [dic stringValueForKey:@"OwnerId"];
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetContactListWithGroupName/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetContactListWithGroupName %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetContactListWithGroupNameResult"];
            [popUpDataArr removeAllObjects];
            
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
            [UIAppDelegate.userDefault setObject:data forKey:OwnerBuddyGroupListKey];
            
            for (NSDictionary *dic in arr) {
                if (![[dic  stringValueForKey:@"Status"] isEqualToString:@"Dealer"]) {
                    [popUpDataArr addObject:dic];
                }
            }
            
           
            
        }
         [SVProgressHUD dismiss];
        if ([popUpDataArr count]) {
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Message Type" withOption:[popUpDataArr valueForKey:@"Name"] xy:CGPointMake(10, 100) size:CGSizeMake(300, MIN(200, [popUpDataArr count]*50+50)) isMultiple:NO];
            
        }
        else
        {
             [CMHelper alertMessageWithText:@"Currently there is no buddy available."];
        }
       
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark IBAction delegate methods

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)segmentClick:(id)sender{
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 1) {
        
        CMAddPhotoController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMAddPhotoController"];
         [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    else if(selectedSegment == 0){
        
        
    }
    
}

-(IBAction)showPhotoClicked:(id)sender{
    /*
    CMAddPhotoController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMAddPhotoController"];
    [self.navigationController pushViewController:controller animated:YES];*/
    
    [self showUIImagePicker];
}


-(IBAction)showUIImagePicker{
    

        @try {
            //        IstoCamera = YES;
            
           // dispatch_async(dispatch_get_current_queue(), ^(void){
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType = IMAGESOURSETYPE;
                imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
                
                //   imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:IMAGESOURSETYPE];
                // [self.navigationController pushViewController:imagePicker animated:YES];
                
                [self.navigationController presentViewController:imagePicker animated:YES completion:NULL];
                
          //  } ) ;
            
        }
        @catch (NSException * e) {
            UIAlertView *alertmsg=[[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Couldn’t locate the camera, check your settings and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertmsg show];
        }
        @finally {
        }

}
-(IBAction)endLocationBtnClicked:(id)sender{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BIKE_LOCATION_KEY];
    [self.navigationController popViewControllerAnimated:YES];

}


-(IBAction)findBtnClicked:(id)sender{
    
    ((UIButton *)sender).backgroundColor = [UIColor whiteColor];
    
    [self drawRoute1];
}


#pragma mark - CLLocationManagerDelegate

- (void)startLocationRequest
{
    __weak __typeof(self) weakSelf = self;
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                                                timeout:10
                                                   delayUntilAuthorized:YES
                                                                  block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                      __typeof(weakSelf) strongSelf = weakSelf;
                                                                      
                                                                      if (status == INTULocationStatusSuccess) {
                                                                          // achievedAccuracy is at least the desired accuracy (potentially better)
                                                                          NSLog(@"Location request successful! Current Location:\n%@", currentLocation);
                                                                          [self setCurrentCityAndState:currentLocation];
                                                                      }
                                                                      else if (status == INTULocationStatusTimedOut) {
                                                                          // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                                                                          NSLog(@"Location request timed out. Current Location:\n%@", currentLocation);
                                                                          [self setCurrentCityAndState:currentLocation];
                                                                          
                                                                      }
                                                                      else {
                                                                          // An error occurred
                                                                          if (status == INTULocationStatusServicesNotDetermined) {
                                                                              NSLog(@"Error: User has not responded to the permissions alert.");
                                                                          } else if (status == INTULocationStatusServicesDenied) {
                                                                              NSLog(@"Error: User has denied this app permissions to access device location.");
                                                                          } else if (status == INTULocationStatusServicesRestricted) {
                                                                              NSLog(@"Error: User is restricted from using location services by a usage policy.");
                                                                          } else if (status == INTULocationStatusServicesDisabled) {
                                                                              NSLog( @"Error: Location services are turned off for all apps on this device.");
                                                                          } else {
                                                                              NSLog( @"An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)");
                                                                          }
                                                                      }
                                                                      strongSelf.locationRequestID = NSNotFound;
                                                                  }];
}


-(void)setCurrentCityAndState:( CLLocation *)newLocation{
    
//    [UIAppDelegate.userDefault setObject:[NSString stringWithFormat:@"%.4f", newLocation.coordinate.latitude] forKey:@"Lat"];
//    [UIAppDelegate.userDefault  setObject:[NSString stringWithFormat:@"%.4f", newLocation.coordinate.longitude] forKey:@"Lon"];
    
    
    [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:newLocation.coordinate.latitude] forKey:@"Lat"];
    [UIAppDelegate.userDefault setObject:[NSNumber numberWithDouble:newLocation.coordinate.longitude] forKey:@"Lon"];
    
    [UIAppDelegate.userDefault  synchronize];


    if (!bikeLocationDict) {
        if (!currentLocationPoint) currentLocationPoint = [[CMCustomAnnotation  alloc] init];
        currentLocationPoint.coordinate = newLocation.coordinate;
        //currentLocationPoint.title = @"I am here";
        currentLocationPoint.title = [UIAppDelegate.userDefault objectForKey:CycleLocationNameKey];

        currentLocationPoint.isCurrLocation = YES;
        [self.mapView addAnnotation:currentLocationPoint];
        
        self.mapView.region = MKCoordinateRegionMake(newLocation.coordinate, MKCoordinateSpanMake(kLatitudeDelta, kLongitudeDelta));
        
        [self.mapView selectAnnotation:currentLocationPoint animated:YES];
        
        
        if (![UIAppDelegate.userDefault boolForKey:IsBikeParkKey]){
            NSNumber *lat = [NSNumber numberWithDouble:currentLocationPoint.coordinate.latitude];
            NSNumber *lon = [NSNumber numberWithDouble:currentLocationPoint.coordinate.longitude];
                NSDictionary *userLocation=@{@"lat":lat,@"long":lon};
                
                [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:BIKE_LOCATION_KEY];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [UIAppDelegate.userDefault setBool:YES forKey:IsBikeParkKey];

                
            }
        self.mapView.region = MKCoordinateRegionMake(newLocation.coordinate, MKCoordinateSpanMake(ZoomlatitudeDelta,ZoomlongitudeDelta));


    }
    else{
    
        if (!bikeLocationPoint) bikeLocationPoint = [[CMCustomAnnotation  alloc] init];

        bikeLocationPoint.coordinate = newLocation.coordinate;
        bikeLocationPoint.title =  @"FIND MY BIKE";
        
        
        [self.mapView addAnnotation:bikeLocationPoint];
        
        self.mapView.region = MKCoordinateRegionMake(bikeLocationPoint.coordinate, MKCoordinateSpanMake(ZoomlatitudeDelta,ZoomlongitudeDelta));
        [self.mapView selectAnnotation:bikeLocationPoint animated:YES];

        
    }
    
    
    
    if (!shareLocationPoint) shareLocationPoint = [[CMCustomAnnotation  alloc] init];
    shareLocationPoint.coordinate = newLocation.coordinate;
    shareLocationPoint.isShareCurrLocation = YES;
    shareLocationPoint.title =  @"Share my location to buddy";

    
    /*
    
    if (!geocoder) geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                       
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                           
                       }
                       
                       
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       
                       NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
                       NSLog(@"placemark.country %@",placemark.country);
                       NSLog(@"placemark.postalCode %@",placemark.postalCode);
                       NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
                       NSLog(@"placemark.locality %@",placemark.locality);
                       NSLog(@"placemark.subLocality %@",placemark.subLocality);
                       NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
                       
                       
                     //  [UIAppDelegate.userDefault setObject:[NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.administrativeArea] forKey:BikeLocationNameKey];
                       
                       //CycleLocationNameKey
                       
                       
                   }];*/


}


/* MKMapViewDelegate Meth0d -- for viewForOverlay*/
- (MKOverlayView*)mapView:(MKMapView*)theMapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineView *view = [[MKPolylineView alloc] initWithPolyline:self.objPolyline];
    view.fillColor = [UIColor blackColor];
    view.strokeColor = [UIColor blackColor];
    view.lineWidth = 4;
    return view;
}

/* This will get the route coordinates from the google api. */
- (NSArray*)getRoutePointFrom:(MKPointAnnotation *)origin to:(MKPointAnnotation *)destination
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError *error;
    NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSUTF8StringEncoding error:&error];
    NSString* encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
    
   /* NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"points:\\\"([^\\\"]*)\\\"" options:0 error:NULL];
    NSTextCheckingResult *match = [regex firstMatchInString:apiResponse options:0 range:NSMakeRange(0, [apiResponse length])];
    NSString *encodedPoints = [apiResponse substringWithRange:[match rangeAtIndex:1]];*/
    
    return [self decodePolyLine:[encodedPoints mutableCopy]];
}

- (NSMutableArray *)decodePolyLine:(NSMutableString *)encodedString
{
    [encodedString replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                      options:NSLiteralSearch
                                        range:NSMakeRange(0, [encodedString length])];
    NSInteger len = [encodedString length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encodedString characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encodedString characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        printf("\n[%f,", [latitude doubleValue]);
        printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:loc];
    }
    return array;
}

- (void)drawRoute
{
    int numPoints = [arrRoutePoints count];
    if (numPoints > 1)
    {
        CLLocationCoordinate2D* coords = malloc(numPoints * sizeof(CLLocationCoordinate2D));
        for (int i = 0; i < numPoints; i++)
        {
            CLLocation* current = [arrRoutePoints objectAtIndex:i];
            coords[i] = current.coordinate;
        }
        
        self.objPolyline = [MKPolyline polylineWithCoordinates:coords count:numPoints];
        free(coords);
        
        [self.mapView addOverlay:self.objPolyline];
        [self.mapView setNeedsDisplay];
    }
}

- (void)centerMap
{
    MKCoordinateRegion region;
    
    CLLocationDegrees maxLat = -90;
    CLLocationDegrees maxLon = -180;
    CLLocationDegrees minLat = 90;
    CLLocationDegrees minLon = 180;
    
    for(int idx = 0; idx < arrRoutePoints.count; idx++)
    {
        CLLocation* currentLocation = [arrRoutePoints objectAtIndex:idx];
        
        if(currentLocation.coordinate.latitude > maxLat)
            maxLat = currentLocation.coordinate.latitude;
        if(currentLocation.coordinate.latitude < minLat)
            minLat = currentLocation.coordinate.latitude;
        if(currentLocation.coordinate.longitude > maxLon)
            maxLon = currentLocation.coordinate.longitude;
        if(currentLocation.coordinate.longitude < minLon)
            minLon = currentLocation.coordinate.longitude;
    }
    
    region.center.latitude     = (maxLat + minLat) / 2;
    region.center.longitude    = (maxLon + minLon) / 2;
    region.span.latitudeDelta  = maxLat - minLat;
    region.span.longitudeDelta = maxLon - minLon;
    
   // [self.mapView setRegion:region animated:YES];
}

#pragma mark -
#pragma mark CMDirectDealerMailVCDelegate Methods
- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController withSend:(BOOL)isSend messageText:(NSString *)messageText{
    
    
 
    
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Notes saved successfully"
                                                       delegate:nil cancelButtonTitle:@"Thanks"
                                              otherButtonTitles:nil];
    [alertView show];
    
    [UIAppDelegate.userDefault setObject:messageText forKey:@"ParkingNotes"];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];


}
- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController;
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate Methods


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        bikeImage.image = image;
        
        [UIAppDelegate.userDefault setBool:YES forKey:IsBikeParkKey];
        
        [UIAppDelegate saveImage:image WithName:@"Bike.png"];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        
        
        
    }
}
#pragma mark - Collapse Click Delegate

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    //    NSMutableArray *contactArr = [[NSArray arrayWithObject:@"Reno's Powersports KC"] addObjectsFromArray:[contactListArr valueForKey:@"FName"]];// [contactListArr valueForKey:@"FName"];
    
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    
    NSDictionary *tempDic = [popUpDataArr objectAtIndex:anIndex];
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    NSString *senderOwnerId    = [dic stringValueForKey:@"OwnerId"];
    NSString *message ;
    NSString *ownerId ;//= [dic stringValueForKey:@"OwnerId"];;
    
    
      NSString *groupId    = @"";
    if ([[tempDic stringValueForKey:@"Status"] isEqualToString:@"Owner"]) {
      message  = [NSString stringWithFormat:@"Do you want to share Location with %@?", [[popUpDataArr valueForKey:@"Name"] objectAtIndex:anIndex]];
        
        ownerId = [[popUpDataArr  objectAtIndex:anIndex] stringValueForKey:@"ContactId"];
        groupId = @"0";

    }
   else{
        
    message  = [NSString stringWithFormat:@"Do you want to share Location with %@ Group?", [[popUpDataArr valueForKey:@"Name"] objectAtIndex:anIndex]];
       
       groupId = [[popUpDataArr  objectAtIndex:anIndex] stringValueForKey:@"ContactId"];
       ownerId = @"0";
        
    }

    
    
   
    
    
    
    [[UIAlertView alertViewWithTitle:@"Alert"
                             message:message cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"]
                           onDismiss:^(int buttonIndex) {
                               
                            
                               NSString *postionLet;
                               NSString *postionLon;
                               
                               if ([UIAppDelegate.userDefault boolForKey:IsBikeParkKey]) {
                                   bikeLocationDict = [[NSUserDefaults standardUserDefaults] objectForKey:BIKE_LOCATION_KEY];
                                   
                                   postionLet = [bikeLocationDict objectForKey:@"lat"];
                                   postionLon = [bikeLocationDict objectForKey:@"long"];
                                   

                               }
                               else{
                               
                                   
                                   postionLet = [NSString stringWithFormat:@"%.4f",shareLocationPoint.coordinate.latitude];
                                  postionLon = [NSString stringWithFormat:@"%.4f",shareLocationPoint.coordinate.longitude];
                               
                               }
                               
                               
                               
                               
                               
                             

                               
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:groupId,@"GroupId",ownerId, @"OwnerId",senderOwnerId,@"SenderOwnerId", @"0", @"LocationId", ownerId, @"OwnerId",postionLet,@"PostionLet",postionLon,@"PostionLon",nil];
                               
                               
                               
                               
                               
                               [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateShareLocation",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                   if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                       NSLog(@"*** getbrands %@ **",resultDict);
                                       
                                       [CMHelper alertMessageWithText:@"You have successfully shared your location to your buddy."];
                                       
                                   }
                               }];

                               
                               
                               
                               
                               
                               
                               
                           } onCancel:^{
                               
                               
                           }] show];
    
    
    
    
    /*----------------Get Selected Value[Single selection]-----------------*/
    
    //    NSMutableArray *contactArr = [[NSArray arrayWithObject:@"Reno's Powersports KC"] addObjectsFromArray:[contactListArr valueForKey:@"FName"]];// [contactListArr valueForKey:@"FName"];
    /*
    contactIndex = anIndex;
    
    [messageTypeBtn setTitle:[[popUpDataArr valueForKey:@"Name"] objectAtIndex:anIndex] forState:UIControlStateNormal];
    [messageTypeBtn setSelectedObjectID:[NSString stringWithFormat:@"%@",[[popUpDataArr valueForKey:@"ContactId"] objectAtIndex:anIndex]]];
    
    */
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}



@end
