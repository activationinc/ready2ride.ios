//
//  CMRideDetailController.m
//  Cyclemate
//
//  Created by Sourav on 03/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMRideDetailController.h"
#import "DropDownListView.h"


@interface CMRideDetailController ()<UITextFieldDelegate,kDropDownListViewDelegate>{
    IBOutlet UITextView *descriptionView;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIView *datePickerView;
    IBOutlet UITextField *datePickerTextField;
    IBOutlet UITextField *endDateTextField;
    UITextField *selectedTextField;
    DropDownListView * Dropobj;
    IBOutlet UIButton *rideTypeBtn;

}

-(IBAction)backBtnClicked:(id)sender;
- (IBAction)actionCancel:(id)sender;
- (IBAction)actionDone:(id)sender;

@property (nonatomic,strong)NSArray *rideTypeArrays;




@end

@implementation CMRideDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    descriptionView.layer.borderColor = [UIColor blackColor].CGColor;
    descriptionView.layer.borderWidth = 1.0;
    
    _rideTypeArrays = [NSArray arrayWithObjects:@"The Getaway",@"The Afternoon Delight",@"The Day Tripper",@"The Weekender",@"The Touring",@"Chaptering", nil];
    
    for (int i=101; i<105; i++) {
        
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        textField.delegate = self;
    }
    
//    UIImage *normalImage = [UIImage imageNamed:@"unchecked.png"];
//    UIImage *selectedImage = [UIImage imageNamed:@"checked.png"];

    
    for (int i= 201; i<203; i++) {
        
        UIButton *button = (UIButton *)[self.view viewWithTag:i];
        
        [button setImage:NormalImage forState:UIControlStateNormal];
        [button setImage:SelectedImage forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MAP & RIDE Track" navigationItem:self.navigationItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    button.selected = !button.selected;
}

#pragma mark
#pragma mark IBAction delegate methods

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark
#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if(textField == datePickerTextField || textField == endDateTextField){
        
    //[textField resignFirstResponder];
        
        selectedTextField = textField;
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         //datePicker.hidden = NO;
                         //self.toolbarCancelDone.hidden = NO;
                         datePickerView.hidden = NO;
                         //datePickerTextField.text = @"";
                         
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    datePickerView.hidden = NO;
    //datePicker.hidden = NO;
    //self.toolbarCancelDone.hidden = NO;
    //datePickerTextField.text = @"";
        
        return NO;
    }
    
    
    
    return YES;
    
}

- (IBAction)actionDone:(id)sender
{
    
     NSDate *chosen = [datePicker date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM, yyyy"];
    
    //Optionally for time zone conversions
    //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
    
    NSString *stringFromDate = [formatter stringFromDate:chosen];

    
    
    
    selectedTextField.text = stringFromDate;
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         datePickerView.hidden = YES;
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         
                     }];
    
    
    
}

- (IBAction)actionCancel:(id)sender
{
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         datePickerView.hidden = YES;

                     }
                     completion:^(BOOL finished){
                         
                         
                     }];
    
    
}

#pragma mark - Collapse Click Delegate

- (IBAction)DropDownSingle:(id)sender {
    
    UIButton *btn = ( UIButton *)sender;
    [Dropobj fadeOut];
  //  [self showPopUpWithTitle:@"Ride Type" withOption:_rideTypeArrays xy:CGPointMake(16, 150) size:CGSizeMake(287, 280) isMultiple:NO];
    
    [self showPopUpWithTitle:@"Ride Type" withOption:_rideTypeArrays xy:btn.frame.origin size:CGSizeMake(300, MIN(200, _rideTypeArrays.count*50+50)) isMultiple:NO];

}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    [rideTypeBtn setTitle:[_rideTypeArrays objectAtIndex:anIndex] forState:UIControlStateNormal];
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    

}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}

@end
