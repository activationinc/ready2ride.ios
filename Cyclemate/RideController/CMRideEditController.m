//
//  CMRideEditController.m
//  Cyclemate
//
//  Created by Sourav on 07/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMRideEditController.h"

@interface CMRideEditController(){
    //IBOutlet UITextView *editebaleTextField;
    
}

@property (nonatomic, readwrite, strong) NSIndexPath *indexPathInTableView;
@property (nonatomic, readwrite, strong) CMSaveRide *saveRide;
@property (nonatomic,readwrite,strong) IBOutlet UITextView *editebaleTextField;


-(IBAction)backBtnClicked:(id)sender;
-(IBAction)saveInfo:(id)sender;

@end

@implementation CMRideEditController


-(void)viewDidLoad{
    
    //editebaleTextField.text = [_dict objectForKey:@"note"];
    
    _editebaleTextField.text = self.saveRide.noteDescription;
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MAP & RIDE Track" navigationItem:self.navigationItem];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:MapBootomTabBar]];

}

- (void)initWithPhotoRecord:(CMSaveRide *)saveRide atIndexPath:(NSIndexPath *)indexPath delegate:(id<CMRideEditControllerDelegate>) theDelegate{
    
    //self.dict = dict;
    self.saveRide = saveRide;
    self.delagate = theDelegate;
    self.indexPathInTableView = indexPath;
    
    
}

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)saveInfo:(id)sender{
    
    self.saveRide.noteDescription = _editebaleTextField.text;
    
    if ([self.delagate respondsToSelector:@selector(saveInfo:)]) {
        [self.delagate saveInfo:self];
    }
    
}

#pragma mark
#pragma mark Delegate methods


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end
