//
//  CMRideSetUpController.m
//  Cyclemate
//
//  Created by Sourav on 02/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMRideSetUpController.h"
#import "CMRideDetailController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "CMCustomAnnotation.h"
#import "DropDownListView.h"




@interface CMRideSetUpController ()<CLLocationManagerDelegate,UITextFieldDelegate,MKMapViewDelegate,kDropDownListViewDelegate>{
   
    
    
    
        IBOutlet UITextView *descriptionView;
    
        IBOutlet UITextField *datePickerTextField;
        IBOutlet UITextField *endDateTextField;
    
          IBOutlet UITextField *rideNameTextField;

        IBOutlet UIView *segmentView;
        UITextField *selectedTextField;
        DropDownListView * Dropobj;

    
    CLLocationManager *locationManager;
    MKPointAnnotation *startPoint;
    CMCustomAnnotation *endPoint;
    
    
    IBOutlet UIView *rideSetUpView;
    IBOutlet UIView *rideDetailView;
    IBOutlet UIView *inviteView;
    
    IBOutlet UITextField *startTextField;
    IBOutlet UITextField *endTextField;
    
    IBOutlet UIButton *saveBtn;
    BOOL islocationSetup;
    
    IBOutlet UIButton *rideTypeBtn;
    IBOutlet UIButton *selectAllBtn;
    BOOL isAllSelected;
    IBOutlet UITableView *allContactTableView;
    
    NSArray *buddyList;
    MKPolyline *line;
    
    
    

}

@property(nonatomic,weak)IBOutlet MKMapView *mapView;
-(IBAction)segmentClick:(id)sender;
-(IBAction)categorySelectionBtnClicked:(id)sender;
- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;
@property(nonatomic,strong)IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong)NSArray *rideTypeArrays;
@property (nonatomic,strong)IBOutlet UIScrollView *rideDetailscrollView;
-(IBAction)saveBtnClick:(id)sender;



- (IBAction)actionCancel:(id)sender;
- (IBAction)actionDone:(id)sender;

@end

@implementation CMRideSetUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [datePickerTextField setTextFieldType:@"Date"];
    [endDateTextField setTextFieldType:@"Date"];

    // Do any additional setup after loading the view.
    
    [selectAllBtn setImage:NormalImage forState:UIControlStateNormal];
    [selectAllBtn setImage:SelectedImage forState:UIControlStateSelected];
    [selectAllBtn addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];

    
    buddyList = [NSArray arrayWithObjects:@"Bob Allen",@"Emma Reed",@"Susan Blunt",@"Bill Williams", nil];

    for (int i=601; i<603; i++) {
        
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        textField.delegate = self;
    }

    _rideTypeArrays = [NSArray arrayWithObjects:@"The Getaway",@"The Afternoon Delight",@"The Day Tripper",@"The Weekender",@"The Touring",@"Chaptering", nil];

    self.rideDetailscrollView.contentSize = CGSizeMake(320, 370);
    
    descriptionView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    descriptionView.textColor = [UIColor lightGrayColor];
    descriptionView.layer.borderWidth = 1.0;
    descriptionView.layer.cornerRadius = 5.0;
    descriptionView.backgroundColor = [UIColor colorWithRed:206.0f/255.0f green:206.0f/255.0f blue:206.0f/255.0f alpha:1];
    
    //[descriptionView setBackground:[UIImage imageNamed:@"textFieldBk"]];

    
    for (int i=101; i<105; i++) {
        
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        textField.delegate = self;
    }
    
    CGSize scrollContentSize = CGSizeMake(320, 443);
    self.scrollView.contentSize = scrollContentSize;

    
//    UIImage *normalImage = [UIImage imageNamed:@"unchecked.png"];
//    UIImage *selectedImage = [UIImage imageNamed:@"checked.png"];
    
    
    for (int i= 201; i<203; i++) {
        
        UIButton *button = (UIButton *)[self.view viewWithTag:i];
        
        
       
        [button setImage:NormalImage forState:UIControlStateNormal];
        [button setImage:SelectedImage forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];

    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MAP & RIDE Track" navigationItem:self.navigationItem];
    
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:MapBootomTabBar]];
    
    [[saveBtn layer] setCornerRadius:4.0f];
    
    
    if (!_isSetUp) {
        
        UIButton *healthBtn = (UIButton *)[segmentView viewWithTag:2];
        [healthBtn setBackgroundColor:segmentSelectedColor];
        UIButton *serviceBtn = (UIButton *)[segmentView viewWithTag:1];
        [serviceBtn setBackgroundColor:segmentUnSelectedColor];
        
        rideDetailView.hidden = NO;
        rideSetUpView.hidden = YES;
        
        islocationSetup = NO;
    }else{
        [self locationDetailSetUp];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)locationDetailSetUp{
    
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    self.mapView.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    islocationSetup = YES;

}

#pragma mark
#pragma mark IBAction delegate methods


- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    if (button == selectAllBtn) {
        button.selected = !button.selected;
        isAllSelected = !isAllSelected;
        [allContactTableView   reloadData];

    }else{
        button.selected = !button.selected;
        [selectAllBtn setSelected:NO];
        isAllSelected = NO;

    }
}


-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)saveBtnClick:(id)sender{
    
    NSLog(@"saveBtnClick");
    
    if ([rideNameTextField.text length]) {
        [CMHelper alertMessageWithText:@"Please enter ride name."];
    }else     if ([descriptionView.text length]) {
        [CMHelper alertMessageWithText:@"Please enter ride description."];
    }
    else     if ([datePickerTextField.text length]) {
        [CMHelper alertMessageWithText:@"Please enter start date."];
    }
    else     if ([endTextField.text length]) {
        [CMHelper alertMessageWithText:@"Please enter end date."];
    }
    else{
        
        NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys: @"1", @"CycleId",
                             rideNameTextField.text, @"Name",
                             @"1", @"RideTypeId",
                             descriptionView.text, @"Description",
                             startPoint.coordinate.latitude, @"StartGeoLat",
                            startPoint.coordinate.longitude, @"StartGeoLon",
                             endPoint.coordinate.latitude, @"EndGeoLat",
                             endPoint.coordinate.longitude, @"EndGeoLong",
                             startTextField.text, @"StartLocation",
                            endTextField.text, @"EndLocation",
                            datePickerTextField.text, @"StartDate",
                             endDateTextField.text, @"EndDate",
                             @"1", @"IsMileageShare",
                             @"1", @"IsWeatherAlert",
                             @"1", @"IsBuddyInvite",
                             @"0", @"RideId",
                             nil];
    
    [self postDataToServer:infoDic withPostURL:[NSString stringWithFormat:@"%@/InsertUpdateRide",Service_URL]];

    }
}

#pragma mark - WebService Implementation
-(void)postDataToServer:(NSDictionary *)withInfoDic withPostURL:(NSString *)urlStr
{
    // [self postData];return;
    
    // Name, Address, City, State, Zip, Mobile, Email, StyleId, Photo, PhotoPath, DLPhoto, DLPhotoPath, DLNumber, DLExpiration, IsActive, OwnerId
    
    [[CMNetworkManager sharedInstance] postDataResponse:urlStr param:withInfoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            
            
        }
    }];
    
    
    
    
}



#pragma mark
#pragma mark IBAction methods



-(IBAction)segmentClick:(id)sender{
    
    UIButton *button = (UIButton *) sender;
    NSInteger selectedSegmentTag = button.tag;
    
    rideDetailView.hidden = YES;
    rideSetUpView.hidden  = YES;
    inviteView.hidden = YES;

    if (selectedSegmentTag == 1) {
        
        rideSetUpView.hidden  = NO;
        
        if (!islocationSetup) {
            [self locationDetailSetUp];
        }
        
    }
    else if(selectedSegmentTag == 2){
        
        rideDetailView.hidden = NO;


    }else if(selectedSegmentTag == 3){
        inviteView.hidden = NO;

    }
    
    UIButton *infoBtn = (UIButton *)[segmentView viewWithTag:1];
    [infoBtn setBackgroundColor:segmentUnSelectedColor];
    UIButton *healthBtn = (UIButton *)[segmentView viewWithTag:2];
    [healthBtn setBackgroundColor:segmentUnSelectedColor];
    UIButton *serviceBtn = (UIButton *)[segmentView viewWithTag:3];
    [serviceBtn setBackgroundColor:segmentUnSelectedColor];
    
    UIButton *selectedBtn = (UIButton *)sender;
    [selectedBtn setBackgroundColor:segmentSelectedColor];
    

    
}



- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    
    /*CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];*/
    
}

-(IBAction)categorySelectionBtnClicked:(id)sender{
    
    
    if (((UIButton *)sender).tag == 1801) {
        
        [self.view viewWithTag:801].backgroundColor = [UIColor colorWithRed:244.0 green:0.0 blue:0.0 alpha:1.0];
        [self.view viewWithTag:802].backgroundColor = [UIColor whiteColor];

    }else{
        
        [self.view viewWithTag:802].backgroundColor = [UIColor colorWithRed:244.0 green:0.0 blue:0.0 alpha:1.0];
        [self.view viewWithTag:801].backgroundColor = [UIColor whiteColor];


    }
}



#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    //NSLog(@"didUpdateToLocation: %@", [locations objectAtIndex:0]);
    CLLocation *currentLocation = [locations objectAtIndex:0];
    
    if (currentLocation != nil) {
        
        [locationManager stopUpdatingLocation];
        
        
        static dispatch_once_t token;
        
        dispatch_once(&token, ^{
            
            startPoint = [[MKPointAnnotation alloc] init];
            startPoint.coordinate = currentLocation.coordinate;
            
            startPoint.title = @"Start Location";
            
            [self.mapView addAnnotation:startPoint];
            
            self.mapView.region = MKCoordinateRegionMake(currentLocation.coordinate, MKCoordinateSpanMake(kLatitudeDelta, kLongitudeDelta));
            
            CLLocationCoordinate2D endPointCoordinate;
            endPointCoordinate.latitude = currentLocation.coordinate.latitude + 0.0025;
            endPointCoordinate.longitude = currentLocation.coordinate.longitude + 0.0020;
            
            
            
            endPoint = [[CMCustomAnnotation alloc] init];
            endPoint.coordinate = endPointCoordinate;
            endPoint.title = @"Destination Location";
            [self.mapView addAnnotation:endPoint];

        });
        
        //self.mapView.region = MKCoordinateRegionMake(endPointCoordinate, MKCoordinateSpanMake(kLatitudeDelta, kLongitudeDelta));

        
        
    }
    
    // Stop Location Manager
 
}

#pragma markr
#pragma mark MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        NSLog(@"Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        
        CLGeocoder* geocoder = [[CLGeocoder alloc] init];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:droppedAt.latitude longitude:droppedAt.longitude];
        
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if(error){
                NSLog(@"%@", [error localizedDescription]);
            }
            
            CLPlacemark *placemark = [placemarks lastObject];
            NSString *addressString;
            
            addressString = [NSString stringWithFormat:@"%@ %@ %@ %@",
                                  placemark.name,
                                   placemark.locality,
                                  placemark.administrativeArea,
                                  placemark.country];
            
            if ([annotationView.annotation isKindOfClass:[CMCustomAnnotation class]]) {
                endTextField.text = addressString;
            }else{
                startTextField.text = addressString;
            }
            
            [self drawRoute1];
            
        }];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[CMCustomAnnotation class]])
    {
        
        
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            //pinView.animatesDrop = YES;
            pinView.canShowCallout = YES;
            //pinView.image = [UIImage imageNamed:@"pizza_slice_32.png"];
            //pinView.pinColor = MKPinAnnotationColorGreen;
            pinView.draggable = YES;
            
        }else{
            pinView.annotation = annotation;
        }
        
               return pinView;
    }else{
        
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            //pinView.animatesDrop = YES;
            pinView.canShowCallout = YES;
            //pinView.image = [UIImage imageNamed:@"pizza_slice_32.png"];
            pinView.pinColor = MKPinAnnotationColorGreen;
            pinView.draggable = YES;
            
        }else{
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    return nil;
}


-(void)drawRoute1{
    
    if (line) {
        [self.mapView removeOverlay:line];
    }
    
    MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:startPoint.coordinate addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    [srcMapItem setName:@""];
    
    MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:endPoint.coordinate addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    //    MKPlacemark *source = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(37.776142, -122.424774) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    //
    //    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    //    [srcMapItem setName:@""];
    //
    //    MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(37.73787, -122.373962) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    //    CLLocationCoordinate2D tempLocation;
    //    tempLocation.latitude  = 37.776142;
    //    tempLocation.longitude = -122.424774;
    //
    //
    //
    //    self.mapView.region = MKCoordinateRegionMake(tempLocation, MKCoordinateSpanMake(kLatitudeDelta, kLongitudeDelta));
    //
    
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    [distMapItem setName:@""];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeAutomobile];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        NSLog(@"response = %@",response);
        
        if (error == nil || response) {
            
            NSArray *arrRoutes = [response routes];
            [arrRoutes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                MKRoute *rout = obj;
                
                line = [rout polyline];
                [self.mapView addOverlay:line];
                NSLog(@"Rout Name : %@",rout.name);
                NSLog(@"Total Distance (in Meters) :%f",rout.distance);
                
                NSArray *steps = [rout steps];
                
                NSLog(@"Total Steps : %lu",(unsigned long)[steps count]);
                
                [steps enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSLog(@"Rout Instruction : %@",[obj instructions]);
                    NSLog(@"Rout Distance : %f",[obj distance]);
                }];
            }];
            
        }else{
            
            if ( endPoint.coordinate.latitude != 0 && endPoint.coordinate.longitude != 0 ){
                NSLog(@"Coordinate valid");
                _mapView.region = MKCoordinateRegionMake(endPoint.coordinate, MKCoordinateSpanMake(0.5,0.5));
            }
            /*
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry !" message:error.description delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"Ok action") otherButtonTitles:nil];
            [alertView show];*/
        }
    }];
}

- (MKOverlayView *)mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay{
    
    
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineView* aView = [[MKPolylineView alloc]initWithPolyline:(MKPolyline*)overlay] ;
        aView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        aView.lineWidth = 10;
        return aView;
    }
    return nil;
}



#pragma mark
#pragma mark UITextFieldDelegate Methods



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    CLGeocoder* geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:textField.text
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     for (CLPlacemark* aPlacemark in placemarks)
                     {
                         // Process the placemark.
                         
                         NSLog(@"Got Placemark : %@", aPlacemark);
                         
                         CLLocationCoordinate2D coordinate;
                         coordinate.latitude = aPlacemark.region.center.latitude;
                         coordinate.longitude = aPlacemark.region.center.longitude;
                         
                         if (textField == startTextField) {
                             startPoint.coordinate = coordinate;
                         }else if(textField == endTextField){
                             endPoint.coordinate = coordinate;
                         }
                         
                         
                         self.mapView.region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(kLatitudeDelta, kLongitudeDelta));
                         
                         [self drawRoute1];
                         
                     }
                 }];
    
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    
    if ([[textField textFieldType] isEqualToString:@"Date"]) {
        selectedField =textField;
        [self.view endEditing:YES];
        [self datePickerViewClicked:nil];
      
        return NO;
        
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
}

#pragma mark - IQActionSheet DatePicker


-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    
    [selectedField setText:[titles componentsJoinedByString:@" - "]];
#if 0
    switch (pickerView.tag)
    {
        case 1: [buttonSingle setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 2: [buttonDouble setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 3: [buttonTriple setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 4: [buttonRange setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 5: [buttonTripleSize setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 6: [buttonDate setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
            
        default:
            break;
    }
#endif
}

- (IBAction)datePickerViewClicked:(UIButton *)sender
{
    //IQActionSheetPickerView *
    
    if (!picker) picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    [picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}
#pragma mark
#pragma mark UITbaleView delagate methods

/*- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
 
 return 10;
 
 }*/

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return buddyList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"inviteBuddyCell"];
    UIButton *button = (UIButton *)[cell viewWithTag:11];
    
    if (isAllSelected) {
        [button setSelected:YES];
        
    }else{
        [button setSelected:NO];
    }
    
    [button setImage:NormalImage forState:UIControlStateNormal];
    [button setImage:SelectedImage forState:UIControlStateSelected];
    [button addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];

    
    
    UIImageView *iconImage = (UIImageView *)[cell viewWithTag:501];
    iconImage.image = [UIImage imageNamed:@"sun.png"];
    
    UILabel *groupName = (UILabel *)[cell viewWithTag:502];
    groupName.text = @"Adventure Budies";
    
    UILabel *groupCount = (UILabel *)[cell viewWithTag:503];
    groupCount.text = @"10";
    
    UILabel *labeName = (UILabel *)[cell viewWithTag:1];
    labeName.text = [buddyList objectAtIndex:indexPath.row];
        return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}

#pragma mark - Collapse Click Delegate

- (IBAction)DropDownSingle:(id)sender {
    UIButton *btn = ( UIButton *)sender;

    [Dropobj fadeOut];
   // [self showPopUpWithTitle:@"Ride Type" withOption:_rideTypeArrays xy:CGPointMake(16, 150) size:CGSizeMake(287, 280) isMultiple:NO];
    
    CGRect frame = [self.view convertRect:btn.frame fromView:rideDetailView];
    
    [self showPopUpWithTitle:@"Ride Type" withOption:_rideTypeArrays xy:frame.origin size:CGSizeMake(310, MIN(200, _rideTypeArrays.count*50+50)) isMultiple:NO];

}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    [rideTypeBtn setTitle:[_rideTypeArrays objectAtIndex:anIndex] forState:UIControlStateNormal];
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
    
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}





@end
