//
//  CMMapShowViewController.m
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMMapShowViewController.h"
#import "CMSaveRideController.h"
#import "CMRideSetUpController.h"
#import "CMMapController.h"
#import "CMRideDetailController.h"
#import "CMParkBikeController.h"

@interface CMMapShowViewController ()
{
    NSArray *itemsArry;
    IBOutlet UICollectionView *collectionView;
    NSDictionary *bikeLocationDict;
}
@end

@implementation CMMapShowViewController

-(IBAction)topMenuBtnClicked:(id)sender
{

}

-(IBAction)backBtnClicked:(id)sender
{
    [UIAppDelegate setSelectedController:[(UIButton *)sender tag]];


}
-(IBAction)topleftBtnClicked:(id)sender{
    
    
}

-(IBAction)bottomViewBtnClicked:(id)sender
{
    [UIAppDelegate setSelectedController:[(UIButton *)sender tag]];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:MapBootomTabBar]];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MAP & RIDE Track" navigationItem:self.navigationItem];
    
  
}

-(void)viewWillAppear:(BOOL)animated{
    
    bikeLocationDict = [[NSUserDefaults standardUserDefaults] objectForKey:BIKE_LOCATION_KEY];
    
    if (bikeLocationDict != nil) {
        itemsArry = [NSArray arrayWithObjects:@"ride setup",@"saved rides",@"share",@"find my vehicle",@"quick start a trip",@"map", nil];
        
    }else{
        
        itemsArry = [NSArray arrayWithObjects:@"ride setup",@"saved rides",@"share",@"park my vehicle",@"quick start a trip",@"map", nil];
    }
    
    [collectionView reloadData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showQuickRideAlertView{
    
    if ([UIAlertController class]) {

    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Quick Trip"
                                          message:@"Would you like to save or View the trip?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *saveAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"SAVE", @"SAVE action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"SAVE action");
                                   
                                   /*CMRideDetailController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideDetailController"];
                                   [self.navigationController pushViewController:controller animated:YES];*/
                                   
                                   CMRideSetUpController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideSetUpController"];
                                   controller.isSetUp = NO;
                                   [self.navigationController pushViewController:controller animated:YES];

                               }];
    
    UIAlertAction *viewAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"VIEW", @"VIEW action")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     NSLog(@"VIEW action");
                                     
                                     CMMapController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMMapController"];
                                     [self.navigationController pushViewController:controller animated:YES];

                                 }];

    
    [alertController addAction:saveAction];
    [alertController addAction:viewAction];

    [self presentViewController:alertController animated:YES completion:nil];
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Quick Trip" message:@"Would you like to save or View the trip?" delegate:self cancelButtonTitle:NSLocalizedString(@"SAVE", @"SAVE action") otherButtonTitles:NSLocalizedString(@"VIEW", @"VIEW action"), nil];
        alertView.tag  = 2000;
        [alertView show];
    }
}

-(void)showParkMyRideAlertView{
    
    if ([UIAlertController class]) {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Park My Vehicle"
                                          message:@"Do you want to save the current loation for this vehicle?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *saveAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"CANCEL", @"CANCEL action")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     NSLog(@"CANCEL action");
                                     
                                     
                                 }];
    
    UIAlertAction *viewAction = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"YES", @"YES action")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     NSLog(@"YES action");
                                     
                                     CMParkBikeController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
                                     [self.navigationController pushViewController:controller animated:YES];
                                     
                                 }];
    
    
    [alertController addAction:saveAction];
    [alertController addAction:viewAction];
    
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Park My Vehicle" message:@"Do you want to save the current loation for this vehicle?" delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", @"CANCEL action") otherButtonTitles:NSLocalizedString(@"YES", @"YES action"), nil];
        alertView.tag  = 1000;
        [alertView show];
    }
}


#pragma mark UIAlertViewDelagate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1000) {
        
    
    if (buttonIndex==0) {
        
        NSLog(@"CANCEL action");

    }
    else {
        
        NSLog(@"YES action");
        
        CMParkBikeController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    }else if(alertView.tag == 2000){
        
        if (buttonIndex==0) {
            
            NSLog(@"SAVE action");
            
            /*CMRideDetailController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideDetailController"];
            [self.navigationController pushViewController:controller animated:YES];*/
            
            CMRideSetUpController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideSetUpController"];
            controller.isSetUp = NO;
            [self.navigationController pushViewController:controller animated:YES];

        }
        else {
            
            NSLog(@"VIEW action");
            
            CMMapController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMMapController"];
            [self.navigationController pushViewController:controller animated:YES];
        }

    }


}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return itemsArry.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *identifier = @"ItemCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    //cell.layer.backgroundColor=[[UIColor darkGrayColor] CGColor];
    cell.layer.cornerRadius = 1;
    cell.layer.borderWidth = 1;
    cell.layer.masksToBounds = YES;
    cell.layer.borderColor=[[UIColor colorWithRed:201/255.0f green:208.0/255.0f blue:214.0/255.0f alpha:1] CGColor];
    
    NSString *title =[itemsArry objectAtIndex:indexPath.row];
    
    UILabel *recipeLblName = (UILabel *)[cell viewWithTag:101];
    recipeLblName.text =  [title uppercaseString] ;
    
    
    
    if (indexPath.row == 2 || indexPath.row == 3) {
        cell.backgroundColor = [UIColor colorWithRed:223/255.0f green:241.0/255.0f blue:244.0/255.0f alpha:1];
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    
    recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_map.png",title]];
    
    
    
    
    return cell;
    
    
    
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        
        CMRideSetUpController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideSetUpController"];
        controller.isSetUp = YES;
        [self.navigationController pushViewController:controller animated:YES];

        
    }
    else if (indexPath.row == 1) {
        
        CMSaveRideController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMSaveRideController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if(indexPath.row == 3){
        
        if (bikeLocationDict) {
            
            CMParkBikeController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
            [self.navigationController pushViewController:controller animated:YES];

            
        }else{
            
        [self showParkMyRideAlertView];
            
        }
        
    }
    else if(indexPath.row == 4){
        
        [self showQuickRideAlertView];
        
    }
    
    else if(indexPath.row == 5){
        
        CMMapController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMMapController"];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
}

#pragma mark - UICollectionViewDelegateFlowLayout


- (BOOL)collectionView:(UICollectionView *)collectionView
shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return YES;
}

@end
