//
//  CMNewRideSetUpVC.h
//  Cyclemate
//
//  Created by Rajesh on 3/9/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQActionSheetPickerView.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>

#import "UIViewController+MJPopupViewController.h"

@interface CMNewRideSetUpVC : UIViewController<IQActionSheetPickerViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    NSString *rideDate;
    
    IQActionSheetPickerView *picker;
    UITextField *selectedField;
    NSString *rideID;
    
   IBOutlet UIScrollView *rideScrollView;
    NSString *isRide;
    
    CLLocationCoordinate2D lastChord;
    CLLocationCoordinate2D currChord;
    
    CLLocationCoordinate2D forMapChord;


   // MKPolyline *line;
    
    BOOL isRidePermission;
    
    
    NSDictionary *activeRideDic;
    int locationUpdateID;

    
}
@property(nonatomic)BOOL isNewRide;

- (IBAction)expandOrShrinkView:(id)sender;
@property (assign, nonatomic) NSInteger locationRequestID;

@property(nonatomic,retain)NSDictionary *rideDic;

-(void)openRideSetupView;
@end
