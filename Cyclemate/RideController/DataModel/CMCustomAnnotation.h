//
//  CMCustomAnnotation.h
//  Cyclemate
//
//  Created by Sourav on 09/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface CMCustomAnnotation : MKPointAnnotation
@property(nonatomic)BOOL isCurrLocation;
@property(nonatomic)BOOL isShareCurrLocation;
@property(nonatomic,retain)NSString *imageUrl;
@property(nonatomic,retain)NSString *pinId;
@property(nonatomic,retain)NSDictionary *infoDic;




@end
