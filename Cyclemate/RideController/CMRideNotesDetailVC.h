//
//  CMRideNotesDetailVC.h
//  Cyclemate
//
//  Created by Rajesh on 4/1/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMRideNotesDetailVC : UIViewController
@property(nonatomic,retain)NSDictionary *infoDic;
@property(nonatomic)int currIndex;
@property(nonatomic,retain)NSArray *itemsArr;
@end
