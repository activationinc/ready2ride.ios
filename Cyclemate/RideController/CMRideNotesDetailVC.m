//
//  CMRideNotesDetailVC.m
//  Cyclemate
//
//  Created by Rajesh on 4/1/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import "CMRideNotesDetailVC.h"

@interface CMRideNotesDetailVC ()<UIGestureRecognizerDelegate>{
    
    UIImageView *imageView;
    
    NSMutableDictionary *fbDetailsDict;
    
    NSMutableDictionary *allDetailsDict;
    
    
    float heightForImage;
    IBOutlet UITableView *newsDetails;
}
@end

@implementation CMRideNotesDetailVC

@synthesize infoDic;

@synthesize currIndex;
@synthesize itemsArr;

- (void)swipeRightAction:(id)ignored
{
    // overlayView.backgroundColor = [UIColor lightGrayColor];
    int count = [itemsArr count];

    NSLog(@"swipeRightAction");

    
    if (currIndex>0) {
        currIndex--;
        infoDic = [itemsArr objectAtIndex:currIndex];
        
    }
    [newsDetails reloadData];

}

- (void)swipeLeftAction:(id)ignored
{
    //overlayView.backgroundColor = [UIColor lightGrayColor];
    int count = [itemsArr count];

    NSLog(@"swipeLeftAction");
    if (currIndex<(count-1)) {
        currIndex++;
        infoDic = [itemsArr objectAtIndex:currIndex];
    }
    [newsDetails reloadData];

}

-(IBAction)nextPrevAction:(id)sender{
    
    int count = [itemsArr count];
    //if ([sender isKindOfClass:[UISegmentedControl class]]) {
    int action = [sender tag];//[(UISegmentedControl *)sender selectedSegmentIndex];
        
        switch (action) {
            case 0:{
                if (currIndex<(count-1)) {
                    currIndex++;
                    infoDic = [itemsArr objectAtIndex:currIndex];
                }
                
            }
                // Prev
                break;
            case 1:{
                if (currIndex>0) {
                    currIndex--;
                    infoDic = [itemsArr objectAtIndex:currIndex];

                }
                
            }
                // Next
                break;
        }
   // }
    [newsDetails reloadData];
}

- (void)fixImagesOfSegmentedControlForiOS7:(UISegmentedControl *)toSegmentedControl
{
    NSInteger deviceVersion = [[UIDevice currentDevice] systemVersion].integerValue;
    if(deviceVersion < 7) // If this is not an iOS 7 device, we do not need to perform these customizations.
        return;
    
    for(int i=0;i<toSegmentedControl.numberOfSegments;i++)
    {
        UIImage* img = [toSegmentedControl imageForSegmentAtIndex:i];
        UIImage* goodImg = [img imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        // clone image with different rendering mode
        [toSegmentedControl setImage:goodImg forSegmentAtIndex:i];
    }
}
- (void)addNextPrevSegmentedControl {
    //imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
    UIBarButtonItem *nextBtn  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Gallery_next_hover.png"]   style:UIBarButtonItemStyleDone target:self action:@selector(nextPrevAction:)];
    nextBtn.tag = 0;
    
    UIBarButtonItem *preBtn  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Gallery_previous_active.png"]  style:UIBarButtonItemStyleDone target:self action:@selector(nextPrevAction:)];
    preBtn.tag = 1;

    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:nextBtn,preBtn, nil];
    
    /*
    // Prepare an array of segmented control items as images
    NSArray *nextPrevItems = [NSArray arrayWithObjects:[UIImage imageNamed:@"Gallery_previous.png"], [UIImage imageNamed:@"Gallery_next.png"], nil];
    // Create the segmented control with the array from above
    UISegmentedControl* nextPrevSegmentedControl = [[UISegmentedControl alloc] initWithItems:nextPrevItems];
    [nextPrevSegmentedControl addTarget:self action:@selector(nextPrevAction:) forControlEvents:UIControlEventValueChanged];
    // Create the bar button item with the segmented control from above
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:nextPrevSegmentedControl];
    // Add the bar button item from above to the navigation item
    [self.navigationItem setRightBarButtonItem:rightButton animated:YES];
   
    
    [self fixImagesOfSegmentedControlForiOS7:nextPrevSegmentedControl];
    */
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addNextPrevSegmentedControl];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    

    [CMAppDelegate setNavigationBarTitle:@"Notes Details" navigationItem:self.navigationItem];
    
    
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipeRightAction:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeRight.delegate = self;
    //[bigImage addGestureRecognizer:swipeRight];
    [newsDetails addGestureRecognizer:swipeRight];
    
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftAction:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeLeft.delegate = self;
    //  [bigImage addGestureRecognizer:swipeLeft];
    [newsDetails addGestureRecognizer:swipeLeft];
    

}
#pragma mark - Table view data source
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//
//    return  isVideo?@"                           Video                                 ":@"                           Newes                           ";
//}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int row =indexPath.row;
    
    if (![[infoDic objectForKey:@"ImageUrl"] length] && (row==0)) {
        row = 1;
    }

   
    
    
    CGSize titleSize;
    switch (row) {
        case 1:{
            
            NSString *dateStr = [infoDic stringValueForKey:@"CreatedDate"];
            
            NSString *title = [dateStr stringByAppendingFormat:@"\n%@", [infoDic stringValueForKey:@"Note"]];
            
            titleSize = [title sizeWithFont:[UIFont systemFontOfSize:(CGFloat)17.0]  constrainedToSize:CGSizeMake(280, 2000)  lineBreakMode:NSLineBreakByWordWrapping];
            
            
            return titleSize.height+25;
            
            break;
            
            
        }
        case 0:{
            
             return SCREEN_HEIGHT;
            
        }
        case 2:{
            
            
            // if (!isWebViewLoaded) {
            NSString *dateStr = [infoDic stringValueForKey:@"simplecontent"];
            
            
            titleSize = [dateStr sizeWithFont:[UIFont systemFontOfSize:(CGFloat)13.0]
                            constrainedToSize:CGSizeMake(280, 2000)
                                lineBreakMode:NSLineBreakByWordWrapping];
            
            return titleSize.height+15;
            break;
            
        }
            
        default:
            return 100;
            
            break;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[infoDic objectForKey:@"ImageUrl"] length]) {
        return  2;
    }
    else
        return  1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    int row =indexPath.row;
    
    if (![[infoDic objectForKey:@"ImageUrl"] length] && (row==0)) {
        row = 1;
    }
    

    
    
    UITableViewCell *cell;
    switch (row) {
        case 1:{
            static NSString *CellIdentifier = @"FirstCell";
            
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            
            
            UILabel *titleLbl = ( UILabel *)[cell viewWithTag:1];
            UILabel *dateLbl = ( UILabel *)[cell viewWithTag:2];
            
            titleLbl.textColor = [UIColor darkGrayColor];
            dateLbl.textColor = [UIColor darkGrayColor];
            
            titleLbl.text = [infoDic stringValueForKey:@"Note"];
            
            CGSize     titleSize = [titleLbl.text sizeWithFont:titleLbl.font                          constrainedToSize:CGSizeMake(280, 2000)
                                                 lineBreakMode:NSLineBreakByWordWrapping];
            
            CGRect frame = titleLbl.frame;
            frame.size.height = titleSize.height;
            titleLbl.frame = frame;
            
            
            frame = dateLbl.frame;
            frame.origin.y = CGRectGetMaxY(titleLbl.frame );
            frame.size.height = 20;//titleLbl.frame.size.height+titleLbl.frame.origin.y;
            dateLbl.frame = frame;
            
            NSString *dateStr = [infoDic stringValueForKey:@"CreatedDate"];
            
            
            dateLbl.text =dateStr;// [MCFDate dateChangeDateFormat:dateStr toFormat:@"dd MMM, yyyy hh:mm a"];
            
        }
            
            break;
        case 0:{
            static NSString *CellIdentifier = @"SecondCell";
            
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            //UIImageView *
            imageView = ( UIImageView *)[cell viewWithTag:1];
            
            imageView.frame = CGRectMake(0, 0, 320, SCREEN_HEIGHT);
            
            
            [imageView setImageWithURL:[NSURL URLWithString: [ImageServerPath stringByAppendingPathComponent:[infoDic stringValueForKey:@"ImageUrl"]]] placeholderImage:nil];
            
            
            
            
            
            
            
        }            break;
            
        case 2:{
            static NSString *CellIdentifier = @"ThirdCell";
            
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            UILabel *contentLbl = ( UILabel *)[cell viewWithTag:1];
            
            CGSize titleSize;
            
            NSString *dateStr = [infoDic stringValueForKey:@"Note"];
            
            
            titleSize = [dateStr sizeWithFont:[UIFont systemFontOfSize:(CGFloat)13.0]
                            constrainedToSize:CGSizeMake(280, 2000)
                                lineBreakMode:NSLineBreakByWordWrapping];
            
            CGRect frame =  contentLbl.frame;
            frame.size = titleSize;
            
            
            contentLbl.frame = frame;
            contentLbl.text = dateStr;
            
            
            
            
        }
            
            
            //ThirdCell
            break;
            
            
            
        default:
            break;
    }
    
    return cell;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
