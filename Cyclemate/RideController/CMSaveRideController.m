//
//  CMSaveRideController.m
//  Cyclemate
//
//  Created by Sourav on 02/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMSaveRideController.h"
#import "CMAddPhotoController.h"
#import "CMRideEditController.h"
#import "CMRideSetUpController.h"
#import "CMNewRideSetUpVC.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAILogger.h"
#import "GAIFields.h"
#import "GAITrackedViewController.h"
#import "GAITracker.h"
@interface CMSaveRideController ()<CMRideEditControllerDelegate>{
    IBOutlet UITableView *rideTableView;
    
    
    UIRefreshControl *refreshControl;
    

}

-(IBAction)segmentClick:(id)sender;

@end

@implementation CMSaveRideController

@synthesize isFromHome;

-(void)markAsRideRead{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             ownerId, @"OwnerId",nil];
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/MarkIsReadRide",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.rideData = [NSMutableArray new];
    
   // self.rideData = [[NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:@"10/12/2014",@"date",@"Smokey Mountain Trail",@"title",@"6 ride details",@"detail",@"smokey_mountain.png",@"image", nil], [NSDictionary dictionaryWithObjectsAndKeys:@"10/02/2014",@"date",@"US1 Florida City to Key West",@"title",@"2 ride details",@"detail",@"us1florida.png",@"image", nil], [NSDictionary dictionaryWithObjectsAndKeys:@"09/23/2014",@"date",@"Ormand Scenic Loop",@"title",@"4 ride details",@"detail",@"ormandscenic.png",@"image", nil], [NSDictionary dictionaryWithObjectsAndKeys:@"09/09/2014",@"date",@"Green Swamp Circle",@"title",@"7 ride details",@"detail",@"greenswamp.png",@"image", nil], nil] mutableCopy];
    
    
    
//    @"10/12/2014 Smokey Mountain Trail",@"10/02/2014 US1 Florida City to Key West",@"09/23/2014 Ormand Scenic Loop",@"09/09/2014 Green Swamp Circle"
//    @"6 ride details",@"2 ride details",@"4 ride details",@"7 ride details"

    
//    for (int i = 0; i< 10; i++) {
//        CMSaveRide *saveRide = [[CMSaveRide alloc] init];
//        saveRide.noteDescription = [NSString stringWithFormat:@"%d ride details",i];
//        
//        [self.rideData addObject:saveRide];
//    }
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MY RIDES" navigationItem:self.navigationItem];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:isFromHome?HomeBootomTabBar:MapBootomTabBar]];
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(loadRidesListData) forControlEvents:UIControlEventValueChanged];
    [rideTableView addSubview:refreshControl];

    

    [self markAsRideRead];
    @try
    {
        
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"My Rides"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }
    
    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView * alert= [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                            message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }

}
-(void)viewDidAppear:(BOOL)animated{
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:@"UA-80658429-4"];
    [self.tracker set:kGAIScreenName value:@"My Rides"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];

}
-(void)viewWillAppear:(BOOL)animated{

    [self loadRidesListData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [rideTableView setEditing:NO];
}

-(void)loadRidesListData{

    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
   // NSString *stringURL =  [NSString stringWithFormat:@"%@/GetRide/RideId=0/CycleId=0/OwnerId=%@",Service_URL,ownerId];
    
     NSString *stringURL =  [NSString stringWithFormat:@"%@/GetRideList/OwnerId=%@",Service_URL,ownerId];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        [refreshControl endRefreshing];
        if (resultDict != nil) {
            NSLog(@"*** eventdates %@ **",resultDict);
            
            if ([self.rideData count]) [self.rideData removeAllObjects];
            
            for (id obj in  [resultDict objectForKey:@"GetRideListResult"]) {
                [self.rideData addObject:obj];
            }
            [rideTableView reloadData];
        }
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    button.selected = !button.selected;
}
-(IBAction)addNewRideBtnClicked:(id)sender{
    
    
    
    CMNewRideSetUpVC *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMNewRideSetUpVC"];
    controller.isNewRide = YES;
    [self.navigationController pushViewController:controller animated:YES];

    /*
    CMRideSetUpController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideSetUpController"];
    controller.isSetUp = YES;
    [self.navigationController pushViewController:controller animated:YES];*/
}

#pragma mark
#pragma mark UITbaleView delagate methods


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return   UITableViewCellEditingStyleDelete;
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [[UIAlertView alertViewWithTitle:@"Alert"
                                 message:@"Do you want to Delete this Ride?" cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"]
                               onDismiss:^(int buttonIndex) {
                                   
                                   
                                   
                                   
                                   NSDictionary *tempDic = [self.rideData objectAtIndex:indexPath.row];
                              NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[tempDic stringValueForKey:@"RideId"],@"RideId",OWNER_ID,@"OwnerId", nil];
                                   
                                   
                                   [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteRide",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                       if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                           NSLog(@"*** getbrands %@ **",resultDict);
                                           
                                           [self.rideData removeObjectAtIndex:indexPath.row];
                                           [tableView reloadData];
                                       }
                                   }];
                                   
                                   
                                   
                                 
                                   
                               } onCancel:^{
                                   
                                   
                               }] show];
        
        
    }
    
}



- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 40)];
    footerView.backgroundColor = [tableView backgroundColor];
    
    UIButton *menuButton =[[UIButton alloc] initWithFrame:CGRectMake(180,0, 120, 30 )];
    menuButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];    //addMotorCycle.png
    
    [menuButton addTarget:self action:@selector(addNewRideBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"newRide.png"] forState:UIControlStateNormal];
    [menuButton setTitle:[@"NEW RIDE" uppercaseString] forState:UIControlStateNormal];
    [menuButton setBackgroundColor:[UIColor clearColor]];
    [menuButton setTitleColor:[UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f  blue:182.0f/255.0f  alpha:1] forState:UIControlStateNormal];
    menuButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);    //addMotorCycle.png
    [footerView addSubview:menuButton];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.rideData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RideCell"];
    
//    UIImage *normalImage = [UIImage imageNamed:@"unchecked.png"];
//    UIImage *selectedImage = [UIImage imageNamed:@"checked.png"];
    
    
       
    NSDictionary *saveRide = [self.rideData objectAtIndex:indexPath.row];
    
    
    
    UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    UILabel *titleLbl = (UILabel *)[cell viewWithTag:2];

    UILabel *desLbl  = (UILabel *)[cell viewWithTag:3];
    UILabel *dateLbl = (UILabel *)[cell viewWithTag:4];
    UILabel *ownerNameLbl = (UILabel *)[cell viewWithTag:5];
    
    UIImageView *shareImage = (UIImageView *)[cell viewWithTag:6];



    
    
    placeholderImage.image = nil;

  UIImage *placeholder = [UIImage imageNamed:@"Ride_Placeholder.png"];
    
    NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[saveRide stringValueForKey:@"ImageUrl"]];
    [placeholderImage setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:placeholder];
    

    
    titleLbl.text = [saveRide stringValueForKey:@"Name"];
   desLbl.text = [saveRide stringValueForKey:@"Description"];//[NSString stringWithFormat:@"%@-%@",[saveRide stringValueForKey:@"StartLocation"],[saveRide stringValueForKey:@"EndLocation"]];;
    dateLbl.text = [saveRide stringValueForKey:@"CreatedDate"];//[MCFDate dateStringFromDate:[NSDate date] format:@"MM/dd/yyyy"];// [NSString stringWithFormat:@"%@-%@",[saveRide stringValueForKey:@"StartLocation"],[saveRide stringValueForKey:@"EndLocation"]];;
    
    ownerNameLbl.text =  @"";
    shareImage.hidden = YES;
    if ([[saveRide stringValueForKey:@"OwnerName"] length]) {
        ownerNameLbl.text = [NSString stringWithFormat:@"%@",[saveRide stringValueForKey:@"OwnerName"]];
        shareImage.hidden = NO;

    }
   
   // OwnerName

    

    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    CMNewRideSetUpVC *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMNewRideSetUpVC"];
     controller.rideDic = [self.rideData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:controller animated:YES];

   /* CMRideEditController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideEditController"];
   
    
    //controller.dict = detaileDict;
    [controller initWithPhotoRecord:[self.rideData objectAtIndex:indexPath.row] atIndexPath:indexPath delegate:self];
    
    [self.navigationController pushViewController:controller animated:YES];*/
    
    
    //[self topMenuBtnClicked:nil];
    
}

#pragma mark
#pragma mark IBAction methods

-(IBAction)segmentClick:(id)sender{
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        CMAddPhotoController *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMAddPhotoController"];
        [self.navigationController pushViewController:controller animated:YES];

        
    }
    else{
        
    }
    
}

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

#pragma mark
#pragma mark CMRideEditControllerDelegate methods

-(void)saveInfo:(CMRideEditController *)controller{
    
    NSIndexPath *indexPath = controller.indexPathInTableView;
    // 2
    [rideTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    [self.navigationController popViewControllerAnimated:YES];

    
}





@end
