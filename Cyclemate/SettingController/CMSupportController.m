//
//  CMWeatherPreferencesController.m
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMSupportController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
@interface CMSupportController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>



@end

@implementation CMSupportController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:SettingBootomTabBar]];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"SUPPORT" navigationItem:self.navigationItem];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark IBAction methods


-(IBAction)callBtnClicked:(id)sender
{
    if([[[UIDevice currentDevice]model] isEqualToString:@"iPhone"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Do You Want to Call For R2RSUPPORT - 7049369712"] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        [alert show];
        
    }
    else {
        UIAlertView *dialmsg=[[UIAlertView alloc]initWithTitle:kAPPLICATION_NAME message:@"This feature is not available on your device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [dialmsg show];
    }

    

}
-(IBAction)smsBtnClicked:(id)sender
{
    
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[@"71441"];
    
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSLog(@"%@",ver);
    NSString *deviceType = [UIDevice currentDevice].model;
    NSLog(@"%@",deviceType);
    
    UIDevice *myDevice = [UIDevice currentDevice];
    NSString *deviceName = myDevice.name;
    NSString *deviceSystemName = myDevice.systemName;
    NSString *deviceOSVersion = myDevice.systemVersion;
    NSLog(@"%@ %@ %@",deviceName,deviceSystemName,deviceOSVersion);
    
    NSString *appVersion = [[NSBundle mainBundle]
                            objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
    
    NSString *appName = [bundleInfo objectForKey:@"CFBundleDisplayName"];
    NSString * message = [NSString stringWithFormat:@"\nDevice Information : %@ with %@ \nApp Information : %@ with %@ \nR2RSUPPORT \n",deviceName,ver,appName,appVersion];
    NSLog(@"message--%@",message);
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}
-(IBAction)emailBtnClicked:(id)sender
{
    
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSLog(@"%@",ver);
    NSString *deviceType = [UIDevice currentDevice].model;
    NSLog(@"%@",deviceType);
    
    UIDevice *myDevice = [UIDevice currentDevice];
    NSString *deviceName = myDevice.name;
    NSString *deviceSystemName = myDevice.systemName;
    NSString *deviceOSVersion = myDevice.systemVersion;
    NSLog(@"%@ %@ %@",deviceName,deviceSystemName,deviceOSVersion);
    
    NSString *appVersion = [[NSBundle mainBundle]
                            objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
    
    NSString *appName = [bundleInfo objectForKey:@"CFBundleDisplayName"];
    NSString * message = [NSString stringWithFormat:@"\nDevice Information : %@ with %@ \nApp Information : %@ with %@ \nR2RSUPPORT \n",deviceName,ver,appName,appVersion];
    NSLog(@"message--%@",message);

    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
        [mailCont setSubject:@"R2RSUPPORT"];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"info@ready2ridemobile.com"]];
        [mailCont setMessageBody:message isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    //handle any error
    [controller dismissViewControllerAnimated:YES completion:nil];
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if([(NSString *)[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Yes"]) {
        //   [self makePhoneCall:@"8169428900"];
        [self makePhoneCall:@"9178030001"];
        
        //getDealerPhoneNO
    }
    
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissModalViewControllerAnimated:YES];
    
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
        else if (result == MessageComposeResultSent)
            NSLog(@"Message sent");
            else
                NSLog(@"Message failed");
}
-(void)makePhoneCall:(NSString*)phoneNumber{
    
    NSString *numberURL = phoneNumber;
    if (![numberURL hasPrefix:@"tel:"]) {
        numberURL = [@"tel:" stringByAppendingString:numberURL];
    }
    
    NSString *escapedURL = [numberURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (!escapedURL) {
        escapedURL = [numberURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    }
    NSURL *url = [[NSURL alloc] initWithString:escapedURL];
    [[UIApplication sharedApplication] openURL:url];
    
}

@end
