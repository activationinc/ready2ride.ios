//
//  CMDashboardSettingsController.m
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMDashboardSettingsController.h"

@interface CMDashboardSettingsController ()<UIImagePickerControllerDelegate>{
    
    IBOutlet UIImageView *dashboardImageViewIcon;
}

-(IBAction)submitDashboardSettings:(id)sender;
-(IBAction)selectPhotoBtnClicked:(id)sender;
-(IBAction)backBtnClicked:(id)sender;


@end

@implementation CMDashboardSettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:SettingBootomTabBar]];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"SETTINGS & PREFERENCES" navigationItem:self.navigationItem];
//    UIImage *normalImage = [UIImage imageNamed:@"unchecked.png"];
//    UIImage *selectedImage = [UIImage imageNamed:@"checked.png"];

    for (int i= 101; i<105; i++) {
        
        UIButton *button = (UIButton *)[self.view viewWithTag:i];
        
        [button setImage:NormalImage forState:UIControlStateNormal];
        [button setImage:SelectedImage forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];

    }
    
    
    NSString *filePath = [self documentsPathForFileName:@"dashoboard_icon.png"];
    NSData *pngData = [NSData dataWithContentsOfFile:filePath];
    if (pngData) {
        dashboardImageViewIcon.image = [UIImage imageWithData:pngData];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    button.selected = !button.selected;
}


#pragma mark
#pragma mark IBAction methods

-(IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(IBAction)submitDashboardSettings:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(IBAction)selectPhotoBtnClicked:(id)sender{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

#pragma mark UIImagePickerControllerDelegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    dashboardImageViewIcon.image = info[UIImagePickerControllerEditedImage];
    
    
    NSData *pngData = UIImagePNGRepresentation(dashboardImageViewIcon.image);
    
    NSString *filePath = [self documentsPathForFileName:@"dashoboard_icon.png"]; //Add the file name
    [pngData writeToFile:filePath atomically:YES]; //Write the file
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


#pragma mark
#pragma mark Private methods

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}




@end
