//
//  CMSettingController.h
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "MPGTextField.h"
#import "CMDeviceContactsListController.h"

@interface CMSettingController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,MPGTextFieldDelegate,UIAlertViewDelegate>{
    IBOutlet UITextField *mcOwnernameField;
    IBOutlet UITextField *mcOwnerLastNameField;
    IBOutlet UITextField *addressField;
    IBOutlet MPGTextField *cityField;
    IBOutlet UITextField *stateField;
    IBOutlet UITextField *zipField;
    IBOutlet UITextField *mobileField;
    IBOutlet UITextField *altMobileField;
    IBOutlet UITextField *emailField;
    
    NSString *cycleOwnerId;
    
    NSString *photoPath;
    BOOL isNewImage;
    
    NSMutableDictionary *ownerInfoDic;
    
    
    IBOutlet  UIImageView *userImageView;
    
    IBOutlet TPKeyboardAvoidingScrollView *settingScroller;


}
-(IBAction)bottomViewBtnClicked:(id)sender;
-(IBAction)saveBtnAction:(id)sender;
-(IBAction)backBtnClicked:(id)sender;
-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)topMenuBtnClicked:(id)sender;

@end
