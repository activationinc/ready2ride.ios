//
//  CMUserProfileController.h
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUserProfileController : UIViewController



-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)topMenuBtnClicked:(id)sender;
-(IBAction)backBtnClicked:(id)sender;
@end
