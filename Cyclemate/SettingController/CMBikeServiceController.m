//
//  CMBikeServiceController.m
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#define kTabBarHeight 60.0

#import "CMBikeServiceController.h"

@interface CMBikeServiceController ()<UITextFieldDelegate>{
    BOOL keyboardIsShown;
}


@property(nonatomic,weak)IBOutlet UIScrollView *scrollView;
-(IBAction)backBtnClicked:(id)sender;
-(IBAction)submitInfo:(id)sender;

@end

@implementation CMBikeServiceController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:SettingBootomTabBar]];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"SETTINGS & PREFERENCES" navigationItem:self.navigationItem];
    // register for keyboard notifications
    /*[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
    //make contentSize bigger than your scrollSize (you will need to figure out for your own use case)
    CGSize scrollContentSize = CGSizeMake(320, 230);
    self.scrollView.contentSize = scrollContentSize;*/

    
//    UIImage *normalImage = [UIImage imageNamed:@"unchecked.png"];
//    UIImage *selectedImage = [UIImage imageNamed:@"checked.png"];
//    
    for (int i=111; i<116; i++) {
        
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        textField.delegate = self;
    }

    
    for (int i= 101; i<104; i++) {
        
        UIButton *button = (UIButton *)[self.view viewWithTag:i];
        
        [button setImage:NormalImage forState:UIControlStateNormal];
        [button setImage:SelectedImage forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
}

- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    button.selected = !button.selected;
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height += (keyboardSize.height - kTabBarHeight);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height -= (keyboardSize.height - kTabBarHeight);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}

#pragma mark
#pragma mark IBAction methods

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(IBAction)submitInfo:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
#pragma mark
#pragma mark UITextFieldDelaget Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
