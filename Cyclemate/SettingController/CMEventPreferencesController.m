//
//  CMEventPreferencesController.m
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMEventPreferencesController.h"

@interface CMEventPreferencesController ()

-(IBAction)backBtnClicked:(id)sender;
-(IBAction)submitEventPreferences:(id)sender;


@end

@implementation CMEventPreferencesController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:SettingBootomTabBar]];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"SETTINGS & PREFERENCES" navigationItem:self.navigationItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark IBAction methods

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];

    
}
-(IBAction)submitEventPreferences:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];

    
}




@end
