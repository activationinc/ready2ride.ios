//
//  CMUserProfileController.m
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMUserProfileController.h"
#import "TPKeyboardAvoidingScrollView.h"

#define kOFFSET_FOR_KEYBOARD 80.0
#define kTabBarHeight 60.0
#define EMAILERRORMESSAGE @"Please correct your email"
#define PHONEERRORMESSAGE @"Please correct your phone number"
#define FIELDSCOMPULSARYMESSAGE @"All the fields are compuslary"



@interface CMUserProfileController ()<UIImagePickerControllerDelegate,UITextFieldDelegate>{
    IBOutlet UIImageView *profileImageView;
    IBOutlet UITextField *cityTextField;
    IBOutlet UITextField *phoneTextField;
    IBOutlet UITextField *emailTextField;
    BOOL keyboardIsShown;
}


@property(nonatomic,strong)IBOutlet UIScrollView *scrollView;
-(IBAction)submitInfo:(id)sender;
-(IBAction)clickPhotoBtnClicked:(id)sender;
-(IBAction)selectPhotoBtnClicked:(id)sender;
-(IBAction)checkBoxClicked:(id)sender;


@end

@implementation CMUserProfileController


-(IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(IBAction)topleftBtnClicked:(id)sender{

}

-(IBAction)topMenuBtnClicked:(id)sender{
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for (int i=101; i<108; i++) {
        
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        textField.delegate = self;
    }
    
    NSString *filePath = [self documentsPathForFileName:@"user.png"];
    NSData *pngData = [NSData dataWithContentsOfFile:filePath];
    if (pngData) {
            profileImageView.image = [UIImage imageWithData:pngData];
    }
    
   
    CGSize scrollContentSize = CGSizeMake(320, 520);
    self.scrollView.contentSize = scrollContentSize;
    

    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:SettingBootomTabBar]];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"SETTINGS & PREFERENCES" navigationItem:self.navigationItem];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];

}

- (BOOL)validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}

// This regex needs to be changed As this is corresponding to India
- (BOOL)validNumber:(NSString*) phoneNumber {
    
    if([phoneNumber length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[0-9]{10}";
    
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:phoneNumber options:0 range:NSMakeRange(0, [phoneNumber length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}

-(BOOL)verifyEmail{
    
    UITextField *textField = (UITextField *)[self.view viewWithTag:107];
    NSString *parsedEmail = [CMHelper removeSpaceCharactersFromStart:[textField text]];
    BOOL result = [self validEmail:parsedEmail];
    if (!result) {
        [self showAlertView:EMAILERRORMESSAGE];
        return result;
    }
    
    
    return result;
}
-(BOOL)verifyPhoneNumber{
    
    return true;
    UITextField *textField = (UITextField *)[self.view viewWithTag:106];
    NSString *parsedPhoneNumber = [CMHelper removeSpaceCharactersFromStart:[textField text]];
    BOOL result = [self validNumber:parsedPhoneNumber];
    if (!result) {
        [self showAlertView:PHONEERRORMESSAGE];
        return result;
    }
    
    
    return result;

}

-(void)showAlertView:(NSString* )errorMessage{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert !!" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}


#pragma mark
#pragma mark Private methods

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}


#pragma mark
#pragma mark IBAction delegate methods

-(IBAction)submitInfo:(id)sender{
    
    for (int i=101; i<108; i++) {
        
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        NSString *textFieldValue= [CMHelper removeSpaceCharactersFromStart:[textField text]];
        if ([textFieldValue isEqualToString:@""]) {
            [self showAlertView:FIELDSCOMPULSARYMESSAGE];

            return;
        }
    }

    
    if([self verifyPhoneNumber] && [self verifyEmail]){
    [self.navigationController popViewControllerAnimated:YES];
    }

}


-(IBAction)clickPhotoBtnClicked:(id)sender{
    
    NSLog(@"IMAGESOURSETYPE-- >%ld",(long)IMAGESOURSETYPE);
    if (![UIImagePickerController isSourceTypeAvailable:IMAGESOURSETYPE]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error !!"
                                                              message:@"Current Device does not support the  camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        return;
        
    }
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    picker.sourceType = IMAGESOURSETYPE;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

-(IBAction)selectPhotoBtnClicked:(id)sender{
       NSLog(@"IMAGESOURSETYPE-- >%ld",(long)IMAGESOURSETYPE);
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

-(IBAction)checkBoxClicked:(id)sender{
    
}

#pragma mark UIImagePickerControllerDelegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    profileImageView.image = info[UIImagePickerControllerEditedImage];
    
    
    NSData *pngData = UIImagePNGRepresentation(profileImageView.image);
    
    NSString *filePath = [self documentsPathForFileName:@"user.png"]; //Add the file name

    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    //NSString *filePath = [documentsPath stringByAppendingPathComponent:@"user.png"]; //Add the file name
    [pngData writeToFile:filePath atomically:YES]; //Write the file

    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height += (keyboardSize.height - kTabBarHeight);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    //[self.scrollView setContentOffset:CGPointZero animated:YES];
    
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height -= (keyboardSize.height - kTabBarHeight);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}


#pragma mark
#pragma mark UITextFieldDelaget Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

/*
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    //[self.scrollView scrollRectToVisible:textField.frame animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}*/




@end
