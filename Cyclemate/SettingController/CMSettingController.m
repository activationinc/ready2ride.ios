//
//  CMSettingController.m
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMSettingController.h"
#import "CMUserProfileController.h"
#import "CMDashboardSettingsController.h"
#import "CMWeatherPreferencesController.h"
#import "CMEventPreferencesController.h"
#import "CMBikeHealthController.h"
#import "CMBikeServiceController.h"
#import "CMSocialPreferencesController.h"
#import "CMNewBikeInfoController.h"
#import "CMBikeInfoController.h"
#import "Mixpanel/Mixpanel.h"
@interface CMSettingController ()<kDropDownListViewDelegate,UITextFieldDelegate>
{
    DropDownListView * Dropobj;
    
    NSArray *itemsArry;
    NSArray *itemsImageArry;
    
    NSMutableArray *stateArr;
    NSMutableArray *cityArr;
    NSString *stateCode;
    NSArray *fliterArry;
    NSArray *cityDataArry;
    IBOutlet CMButton_DropDown *stateTypeBtn;
    IBOutlet CMButton_DropDown *cityTypeBtn;
    
}
@end

@implementation CMSettingController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)backBtnClicked:(id)sender{
    
    [UIAppDelegate setSelectedController:4];
    
}
- (UIBarButtonItem*)backBarButtonItem:(id)controller
{
    static UIBarButtonItem *backButton;
    
    if (IR_IS_SYSTEM_VERSION_LESS_THAN_7_0){
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [button setImage:[UIImage imageNamed:@"Back_Button.png"] forState:UIControlStateNormal];
        [button addTarget:controller action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    else{
        backButton =  [[UIBarButtonItem alloc]
                       initWithImage:[[UIImage imageNamed:@"Back_Button.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
                       target:controller
                       action:@selector(backBtnClicked:)];
    }
    
    
    
    return backButton;
}




-(void)getOwnerforCurrBike{
    
    
    //    if (![cycleOwnerId length]) {
    //        cycleOwnerId = @"0";
    //    }
    //
    //    if ([cycleOwnerId isEqualToString:@"0"]) {
    //        return;
    //    }
    
    // NSString *stringURL =[NSString stringWithFormat:@"%@%@/DealerId=%@",GetOwner,cycleOwnerId,DealerID];
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNo = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    NSString *stringURL =[NSString stringWithFormat:@"%@/GetOwnerByMobile/Mobile=%@",Service_URL,mobileNo];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic;
            
            if ([[resultDict objectForKey:@"GetOwnerByMobileResult"] count]){
                dic = [[resultDict objectForKey:@"GetOwnerByMobileResult"] objectAtIndex:0];
                
                ownerInfoDic = [[resultDict objectForKey:@"GetOwnerByMobileResult"] objectAtIndex:0];
                cycleOwnerId = [ownerInfoDic stringValueForKey:@"OwnerId"];
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
                [UIAppDelegate.userDefault setObject:data forKey:CycleValidationUUIDKey];
               
                NSArray *dataDisplay = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
                NSLog(@"dataDisplay  %lu **",(unsigned long)dataDisplay.count);
                if(dataDisplay.count>0)
                {
                    NSString *fName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"FName"];
                    
                    NSString *lName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"LName"];
                    
                    NSString *email    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"Email"];
                    
                    NSString *dealerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"DealerId"];
                    
                    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                    
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel identify:ownerId];
                    
                    [mixpanel.people set:@{
                                           @"FirstName": fName,
                                           @"LastName": lName,
                                           @"Email": email,
                                           @"DealerId": dealerId
                                           }];
                    
                }

            }
            
            
            //            cityField.text = [dic stringValueForKey:@"City"];
            //            stateField.text = [dic stringValueForKey:@"State"];
            
            NSString *state = [dic stringValueForKey:@"State"];
            
            NSArray *currStateArr = [state componentsSeparatedByString:@","];
            
            if ([currStateArr count]==1) {
                [stateTypeBtn setTitle:[currStateArr objectAtIndex:0] forState:UIControlStateNormal];
            }else  if ([currStateArr count]>=2) {
                [stateTypeBtn setTitle:[currStateArr objectAtIndex:0] forState:UIControlStateNormal];
                [stateTypeBtn setSelectedObjectID:[currStateArr objectAtIndex:1]];
            }
            
            //[cityTypeBtn setTitle:[dic stringValueForKey:@"City"] forState:UIControlStateNormal];
            
            cityField.text = [dic stringValueForKey:@"City"] ;
            mcOwnerLastNameField.text = [dic stringValueForKey:@"LName"] ;
            mcOwnernameField.text = [dic stringValueForKey:@"FName"];
            addressField.text = [dic stringValueForKey:@"Address"];
            if ([[UIAppDelegate.userDefault objectForKey:@"MOBILENO"] length]>0)
            {
                mobileField.text = [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
            }
            else
            {
            mobileField.text = [dic stringValueForKey:@"Mobile"];
            NSMutableString *stringts = [NSMutableString stringWithString:mobileField.text];
            [stringts insertString:@"-" atIndex:3];
            [stringts insertString:@"-" atIndex:7];
            mobileField.text = stringts;
            }
            
            
            emailField.text = [dic stringValueForKey:@"Email"];
            altMobileField.text = [dic stringValueForKey:@"Phone"];
            
            zipField.text = [dic stringValueForKey:@"Zip"];
            
            
            photoPath = [dic stringValueForKey:@"PhotoPath"];
            
            
            if ([[dic stringValueForKey:@"PhotoPath"] length]) {
                NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
                [userImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"products1.jpg"]];
            }
        }
    }];
    NSLog(@"123");
    
}

-(IBAction)saveBtnAction:(id)sender
{
    [self.view endEditing:YES];
    [self performSelector:@selector(postOwnerInfo:) withObject:sender afterDelay:0.2];
}


//https://idp.appery.io/idp/
//ID: owdan007@yahoo.com
//password:*Trigger@123*
-(void)postOwnerInfo:(id)sender
{
    
    //First Name, Last Name, Phone, Email and Zip Code
    
    NSString *unfilteredStringMobile = mobileField.text;
    
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    unfilteredStringMobile = [[unfilteredStringMobile componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    NSLog (@"Result: %@", unfilteredStringMobile);
    
    if (![mcOwnernameField.text length]) {
        [CMHelper alertMessageWithText:@"Please fill in First name."];
    }else if (![mcOwnerLastNameField.text length]) {
        [CMHelper alertMessageWithText:@"Please fill in Last name."];
    }
    else if (![mobileField.text length]) {
        [CMHelper alertMessageWithText:@"Mobile Number should not be blank"];
    }
    else if (![zipField.text length]) {
        [CMHelper alertMessageWithText:@"Please fill in Zip Code."];
    }
    else if (![emailField.text length]) {
        [CMHelper alertMessageWithText:@"Please fill in Email address."];
    }
    else if ([emailField.text length]&& ![Utilities isEmailValid:emailField.text ])
    {
        [CMHelper alertMessageWithText:@"Please enter vaild Email address."];
        
    } else if (![mobileField.text length]) {
        [CMHelper alertMessageWithText:@"Please fill in Mobile Number."];
    }else if (![Utilities isNumeric:unfilteredStringMobile]) {
        
        [CMHelper alertMessageWithText:@"Please enter vaild Mobile Number."];
        
    }else  if (unfilteredStringMobile.length<10 || unfilteredStringMobile.length>10){
        
        [CMHelper alertMessageWithText:@"Mobile Number should be 10 digit."];
        
    }
    
    
    else{
        
        if (![cycleOwnerId length]) cycleOwnerId = @"0";
        
        
        if (!photoPath || ![cycleOwnerId length]) photoPath = @"";
        
        //: FName, LName, Address, City, State, Zip, Mobile, Phone, Email, StyleId, Photo, PhotoPath, DLPhoto, DLPhotoPath, DLNumber, DLExpiration, IsActive, OwnerId, UUid
        NSString *firstName =[mcOwnernameField.text uppercaseString];
        NSString *lastName =[mcOwnerLastNameField.text uppercaseString];
        mcOwnernameField.text = firstName;
        mcOwnerLastNameField.text = lastName;
        
        NSString *state =  [stateTypeBtn currentTitle];
        
        if (state.length) {
            state = [NSString stringWithFormat:@"%@,%@", [stateTypeBtn currentTitle], [stateTypeBtn selectedObjectID]];
        }
        
        [UIAppDelegate.userDefault setObject:mobileField.text forKey:@"MOBILENO"];
        
        NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys: firstName, @"FName",lastName, @"LName", addressField.text, @"Address",
                                 // [cityTypeBtn currentTitle] , @"City", //cityField.text
                                 cityField.text , @"City",
                                 stateTypeBtn.titleLabel.text, @"State", //   stateField.text
                                 zipField.text, @"Zip",
                                 unfilteredStringMobile, @"Mobile",
                                 altMobileField.text, @"Phone",
                                 emailField.text, @"Email",
                                 @"1", @"StyleId",
                                 @"", @"DLPhoto",
                                 @"", @"DLPhotoPath",
                                 photoPath, @"PhotoPath",
                                 @"", @"Photo",
                                 @"1343hfdk28krk", @"DLNumber",
                                 @"11/10/14", @"DLExpiration",
                                 @"1", @"IsActive",
                                 cycleOwnerId, @"OwnerId",[FCUUID uuidForDevice],@"UUid",DealerID,@"DealerId",
                                 nil];
        
        
        /*
         mn – 10 Digit Mobile Number - REQUIRED
         did – Dealer ID assigned in the R2R system - REQUIRED
         
         first – User First Name
         last – User Last Name
         address – User Address
         city – User City
         state – User 2 Character State Abbreviation
         zip – User 5 Digit Zip
         alt_phone – User 10 Digit Alternate Phone #
         email – User Email Address
         */
        
        NSString *clientServiceURL = @"http://7mg.biglistof.com/7mg_r2r_update.php?";//[NSString stringWithFormat:@"http://7mg.biglistof.com/7mg_r2r_update.php?mn=%@&did=%@&first=%@&last=%@&address=%@&city=%@&state=%@&zip=%@&alt_phone=%@&email=%@",DealerID, mobileField.text,firstName,lastName,addressField.text,[cityTypeBtn currentTitle], [stateTypeBtn selectedObjectID],zipField.text,altMobileField.text,emailField.text];
        
        
        //        NSDictionary *clientDic = [NSDictionary dictionaryWithObjectsAndKeys:DealerID,@"did", mobileField.text,@"mn",firstName,@"first",lastName,@"last",addressField.text,@"address",[cityTypeBtn currentTitle],@"city", [stateTypeBtn selectedObjectID],@"state",zipField.text,@"zip",altMobileField.text,@"alt_phone",emailField.text,@"email", nil];
        
        NSDictionary *clientDic = [NSDictionary dictionaryWithObjectsAndKeys:DealerID,@"did", mobileField.text,@"mn",firstName,@"first",lastName,@"last",addressField.text,@"address",cityField.text,@"city", stateTypeBtn.titleLabel.text,@"state",zipField.text,@"zip",altMobileField.text,@"alt_phone",emailField.text,@"email", nil];
        
        [[CMNetworkManager sharedInstance] postUserDataOnClientServer:clientServiceURL withPostParameters:clientDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
            NSLog(@"%@",resultDict);
        }];//postUserDataOnClientServer
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        
        //InsertUpdateOwnerUsingMobile
        //InsertUpdateOwner
        
        [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateOwner",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            [SVProgressHUD dismiss];
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"*** getbrands %@ **",resultDict);
                
                if ([[resultDict stringValueForKey:@"response"] intValue]>=1)
                {
                    if (sender)
                        
//                        [CMHelper alertMessageWithText:[ownerInfoDic count]?@"You have successfully updated your profile.":@"Congratulations you have successfully registered." delegate:self tag:1];
                        
                        if ([ownerInfoDic count] > 0)
                        {
                            [CMHelper alertMessageWithText:@"You have successfully updated your profile." delegate:self tag:2];
                        }
                        else
                        {
                            [CMHelper alertMessageWithText:@"Congratulations you have successfully registered." delegate:self tag:1];
                        }
                      [self performSelector:@selector(runAfterOneSecond) withObject:nil afterDelay:0.6];
                    
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    if (userDefaults) {
                        [userDefaults setObject:[resultDict stringValueForKey:@"response"] forKey:@"userID"];
                        [userDefaults synchronize];
                    }
                    NSString *myString = [userDefaults stringForKey:@"userID"];
                    NSLog(@"myString-->%@",myString);

                    if (isNewImage ) {
                        [self postImageWithServiceType];
                    }
                    
                    
                    cycleOwnerId = [resultDict stringValueForKey:@"response"] ;
                    if (![ownerInfoDic count]) {
                        [self getOwnerforCurrBike];
                        
                    }
                    
                    
                    //                [[CMNetworkManager sharedInstance] getResponseWithPhone:mobileField.text requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    //                    NSLog(@"*** getbrands %@ **",resultDict);
                    //
                    //                }];
                    
                    
                    
                }
                else
                    [CMHelper alertMessageWithText:[cycleOwnerId intValue]?@"Please try again unable to Updated":@"Please try again unable to registered."];
                
            }
        }];
        
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
       
        NSString *alertText=[NSString stringWithFormat:@"Do you want to enter your bike information now or later?"];
        [[UIAlertView alertViewWithTitle:@""
                                 message:alertText cancelButtonTitle:@"I'll do it later"
                       otherButtonTitles:@[@"I'll do it now"]
                               onDismiss:^(int buttonIndex)
          {
              [self moveToBikeInfo];
          } onCancel:^{ [self moveToBuddyList]; }] show];
        
        
    }
    else if (alertView.tag==2)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [UIAppDelegate setSelectedController:4];

    }

    [self performSelector:@selector(runAfterOneSecond) withObject:nil afterDelay:0.6];
    
}

-(void)runAfterOneSecond{
     [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}
//move to save bike
-(void)moveToBikeInfo
{
    CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
    //[viewController performSelector:@selector(openHealthTab) withObject:nil afterDelay:.5];
    [self.navigationController pushViewController:viewController animated:YES];
}
//move to device contacts
-(void)moveToBuddyList
{
    UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"Buddy" bundle:nil];
    CMDeviceContactsListController *_CMDeviceContactsListController=[storyBoard instantiateViewControllerWithIdentifier:@"CMDeviceContactsListController"];
    [self.navigationController pushViewController:_CMDeviceContactsListController animated:YES];
}
-(void)postImageWithServiceType{
    
    UIImage *imageFile =  userImageView.image;
    NSLog(@"ImageUploadURL-->%@",ImageUploadURL);
    
    if (imageFile) {
        
        [[CMNetworkManager sharedInstance] postImage:ImageUploadURL  withImage:imageFile  param:nil  requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            NSString *imageUrl = @"";
            if ([resultDict count]) {
                NSDictionary *tempDic =  [(NSArray *)resultDict objectAtIndex:0];
                imageUrl = [tempDic stringValueForKey:@"image_url"];
                NSLog(@"tempDic = %@",tempDic);
            }
            
            photoPath = imageUrl;
            [self postOwnerInfo:0];
            isNewImage = NO;
            
            
        }];
    }
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [cityField setDelegate:self];
    
    stateArr = [NSMutableArray new];
    cityArr = [NSMutableArray new];
    
    
    //[UIAppDelegate.userDefault setObject:textField.text forKey:@"MOBILENO"];
    
    mobileField.delegate = self;
    mobileField.text = [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    
    [zipField setTextFieldType:@"Number"];
    [mobileField setTextFieldType:@"Number"];
    [altMobileField setTextFieldType:@"Number"];
    // Do any additional setup after loading the view.
    settingScroller.contentSize = CGSizeMake(320, 500);
    
#if 0
    itemsImageArry = [NSArray arrayWithObjects:@"user_profile",@"dashboard",@"weather",@"events",@"bike_health",@"Setting_services",@"motorcycle_info",@"social_links", nil];
    
    itemsArry = [NSArray arrayWithObjects:@"User Profile",@"Dashboard",@"Weather",@"Events",@"Bike Health",@"Services",@"Motorcycle info",@"Social links", nil];
    
#endif
    
    
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:SettingBootomTabBar]];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MY PROFILE" navigationItem:self.navigationItem];//SETTINGS & PREFERENCES
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    // NSString *ownerId    = [dic stringValueForKey:@"OwnerId"];
    
    NSString *ownerId    = [dic stringValueForKey:@"FName"];// [dic stringValueForKey:@"OwnerId"];
    cycleOwnerId  = [dic stringValueForKey:@"OwnerId"];
    if ([ownerId isEqualToString:@"0"] || ![ownerId length])
    {
        //cycleOwnerId = @"0";
        if ([[UIAppDelegate.userDefault objectForKey:@"VIN_Record"] isKindOfClass:[NSMutableDictionary class]] && !([UIAppDelegate.userDefault objectForKey:@"VIN_Record"]==NULL))
        {
            [self prePopulateTextFieldsWithVINDetails:[UIAppDelegate.userDefault objectForKey:@"VIN_Record"]];
        }

    }
    else{
        // cycleOwnerId = ownerId;
        
        [self getOwnerforCurrBike];
    }
    
    self.navigationItem.leftBarButtonItem = [self backBarButtonItem:self];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getOwnerforCurrBike) name:CM_OWNER_REGISTER object:nil];
    
    
    
}
-(void)prePopulateTextFieldsWithVINDetails:(NSDictionary *)VINDict
{
    mcOwnernameField.text=[VINDict objectForKey:@"OwnerFirstName"];
    mcOwnerLastNameField.text=[VINDict objectForKey:@"OwnerLastName"];
    addressField.text=[VINDict objectForKey:@"OwnerAddressLine1"];
    cityField.text=[VINDict objectForKey:@"OwnerCity"];
    stateField.text=[VINDict objectForKey:@"OwnerState"];
    zipField.text=[VINDict objectForKey:@"OwnerZipCode"];
    //mobileField.text=[VINDict objectForKey:@"OwnerPhone"];
    mobileField.text=[UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    altMobileField.text=@"";
    emailField.text=[VINDict objectForKey:@"OwnerEmail"];
}
-(void)viewWillAppear:(BOOL)animated
{
    if([mobileField.text length]<1)
    mobileField.text = [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
      NSLog(@"DealerID-- >%@",DealerID);
    //[self.navigationController.view addSubview:segmentView];
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)   name:UIKeyboardWillShowNotification object:nil];
}

-(IBAction)showUIImagePicker:(id)sender{
    
    
    if([sender tag]== kCHOOSEPICTURE_From_Library) {
        //  IstoCamera = NO;
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else if([sender tag]== kCHOOSEPICTURE_Take_Photo) {
        @try {
            //        IstoCamera = YES;
            
            dispatch_async(dispatch_get_current_queue(), ^(void){
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType=IMAGESOURSETYPE;
                imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
                
                
                
                [self.navigationController presentViewController:imagePicker animated:YES completion:NULL];
                
            } ) ;
            
        }
        @catch (NSException * e) {
            UIAlertView *alertmsg=[[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Couldn’t locate the camera, check your settings and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertmsg show];
        }
        @finally {
        }
    }
}
#pragma mark -
#pragma mark CMDirectDealerMailVCDelegate Methods
/*
 - (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController;
 {
 [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
 }
 */
#pragma mark -
#pragma mark UIImagePickerControllerDelegate Methods


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
        isNewImage = YES;
        
        userImageView.image =image;
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        
        
        
    }
}




-(IBAction)stateBtnClicked:(id)sender{
    
    
    if ([stateArr count]) {
        // [Dropobj fadeOut];
        [self showPopUpWithTitle:@"Select State" withOption:[stateArr valueForKey:@"State"] xy:stateTypeBtn.frame.origin size:CGSizeMake(300, MIN(200, stateArr.count*50+50)) isMultiple:NO withTagValue:999];
        
        
    }else
        [self getStateList];
    
}



-(void)getStateList{
    
    
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetState/StateId=%@",Service_URL,@"0"];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetStateResult %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetStateResult"];
            [stateArr removeAllObjects];
            
            
            for (NSDictionary *dic in arr) {
                [stateArr addObject:dic];
                
            }
            
            
            
        }
        
        if ([stateArr count]) {
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select State" withOption:[stateArr valueForKey:@"State"] xy:stateTypeBtn.frame.origin size:CGSizeMake(300, MIN(200, stateArr.count*50+50)) isMultiple:NO withTagValue:999];
            
            
        }
        [SVProgressHUD dismiss];
    }];
    
}

-(IBAction)cityBtnClicked:(id)sender{
    
    
    
    if (![[stateTypeBtn selectedObjectID] length] || [[stateTypeBtn selectedObjectID] isEqualToString:@"0"]) {
        [CMHelper alertMessageWithText:@"Please select state first."];
        return;
    }else if([stateCode isEqualToString:[stateTypeBtn selectedObjectID]]){
        
        if ([cityArr count]) {
            [self showPopUpWithTitle:@"Select City" withOption:cityArr xy:cityTypeBtn.frame.origin size:CGSizeMake(300, MIN(200, cityArr.count*50+50)) isMultiple:NO withTagValue:99];
            
        }
        return;
        
    }
    
    
    stateCode = [stateTypeBtn selectedObjectID];
    
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetCity/StateCode=%@",Service_URL,stateCode];
    
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetCityResult %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetCityResult"];
            [cityArr removeAllObjects];
            
            
            
            for (NSDictionary *dic in arr) {
                [cityArr addObject:[dic stringValueForKey:@"City"]];
                
            }
            
            
            
        }
        
        if ([cityArr count]) {
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select City" withOption:cityArr xy:cityTypeBtn.frame.origin size:CGSizeMake(300, MIN(200, cityArr.count*50+50)) isMultiple:NO withTagValue:99];
            
        }
        [SVProgressHUD dismiss];
    }];
    
}

#pragma Call City methods

- (void)callCityAPIAutoFillData
{
    if (![[stateTypeBtn selectedObjectID] length] || [[stateTypeBtn selectedObjectID] isEqualToString:@"0"]) {
        [CMHelper alertMessageWithText:@"Please select state first."];
        return;
    }else if([stateCode isEqualToString:[stateTypeBtn selectedObjectID]]){
        
        //        if ([cityArr count]) {
        //            [self showPopUpWithTitle:@"Select City" withOption:cityArr xy:cityTypeBtn.frame.origin size:CGSizeMake(300, MIN(200, cityArr.count*50+50)) isMultiple:NO withTagValue:99];
        //
        //        }
        return;
        
    }
    
    
    stateCode = [stateTypeBtn selectedObjectID];
    
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetCity/StateCode=%@",Service_URL,stateCode];
    
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetCityResult %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetCityResult"];
            [cityArr removeAllObjects];
            
            
            
            for (NSDictionary *dic in arr) {
                [cityArr addObject:[dic stringValueForKey:@"City"]];
                
            }
            
            
            
        }
        
        //        if ([cityArr count]) {
        //            [Dropobj fadeOut];
        //            [self showPopUpWithTitle:@"Select City" withOption:cityArr xy:cityTypeBtn.frame.origin size:CGSizeMake(300, MIN(200, cityArr.count*50+50)) isMultiple:NO withTagValue:99];
        //
        //        }
        [SVProgressHUD dismiss];
    }];
}
#pragma mark - Collapse Click Delegate

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    [self showPopUpWithTitle:popupTitle withOption:arrOptions xy:point size:size isMultiple:isMultiple withTagValue:0];
}
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple  withTagValue:(int)tagValue{
    [self.view endEditing:YES];
    
    //if (!Dropobj)
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    
    Dropobj.delegate = self;
    Dropobj.tag = tagValue;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    
    if (Dropobj.tag == 999) { //StateType Btn
        [stateTypeBtn setTitle:[[stateArr valueForKey:@"State"] objectAtIndex:anIndex] forState:UIControlStateNormal];
        [stateTypeBtn setSelectedObjectID:[[stateArr valueForKey:@"StateCode"] objectAtIndex:anIndex] ];
        // stateCode = [stateTypeBtn selectedObjectID];
        // NSLog(@"stateCode-->%@",stateCode);
        
        cityField.text = @"";
        [self callCityAPIAutoFillData];
    }
    else {
        [cityTypeBtn setTitle:[cityArr  objectAtIndex:anIndex] forState:UIControlStateNormal];
        cityDataArry = [[NSArray alloc] initWithArray:cityArr];
    }
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
    
}
- (void)DropDownListViewDidCancel{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - TextField Delegate for


- (NSArray *)dataForPopoverInTextField:(MPGTextField *)textField
{
    if ([textField isEqual:cityField]) {
        return cityArr;
    }
    else{
        return nil;
    }
}

- (BOOL)textFieldShouldSelect:(MPGTextField *)textField
{
    return YES;
}

- (void)textField:(MPGTextField *)textField didEndEditingWithSelection:(NSDictionary *)result
{
    //A selection was made - either by the user or by the textfield. Check if its a selection from the data provided or a NEW entry.
    //Selection from provided data
    if ([textField isEqual:cityField]) {
    }
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:mobileField] || [textField isEqual:altMobileField])
    {
    int length = (int)[self getLength:textField.text];
    //NSLog(@"Length  =  %d ",length);
    
    if(length == 10)
    {
        if(range.length == 0)
            return NO;
    }
    
    if(length == 3)
    {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"%@-",num];
        
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 6)
    {
        NSString *num = [self formatNumber:textField.text];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);
        textField.text = [NSString stringWithFormat:@"%@-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
    return YES;
    }
        else
        {
           return YES;
        }
}


- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}


- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//   NSLog(@"HERE %@",cityArr);
//    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",textField.text];
//    fliterArry = [cityDataArry filteredArrayUsingPredicate:bPredicate];
//    NSLog(@"HERE %@",fliterArry);
//    return YES;
//}
//
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    return YES;
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField{
//    
//    
//}
//
////implementation
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//
//}
//
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end

