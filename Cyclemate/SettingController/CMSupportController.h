//
//  CMWeatherPreferencesController.h
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMSupportController : UIViewController
{
    
}
-(IBAction)callBtnClicked:(id)sender;
-(IBAction)smsBtnClicked:(id)sender;
-(IBAction)emailBtnClicked:(id)sender;

@end
