//
//  CMBikeHealthController.m
//  Cyclemate
//
//  Created by Sourav on 26/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMBikeHealthController.h"

@interface CMBikeHealthController ()

-(IBAction)backBtnClicked:(id)sender;
-(IBAction)submitInfo:(id)sender;


@end

@implementation CMBikeHealthController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:SettingBootomTabBar]];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"SETTINGS & PREFERENCES" navigationItem:self.navigationItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark IBAction methods

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(IBAction)submitInfo:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}


@end
