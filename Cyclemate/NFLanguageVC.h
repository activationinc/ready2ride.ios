//
//  NFLanguageVC.h
//  NewsApp
//
//  Created by Rajesh on 3/31/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//
#import "UIViewController+MJPopupViewController.h"

#import <UIKit/UIKit.h>
@protocol NFLanguagePopupDelegate;
@interface NFLanguageVC : UIViewController
@property (assign, nonatomic) id <NFLanguagePopupDelegate>delegate;
@end
@protocol NFLanguagePopupDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(NFLanguageVC*)secondDetailViewController;
-(void)openBrowserClicked:(BOOL)isPrivacy;
@end