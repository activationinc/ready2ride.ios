//
//  CMBikeViewController.m
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMBikeViewController.h"
#import "CMBikeInfoController.h"
#import "CMMessageViewController.h"
#import "CMHelper.h"
#import "CMNewBikeInfoController.h"

@interface CMBikeViewController ()
{
    NSArray *bikeTitleArr;
    NSArray *bikeDesArr;
    NSArray *bikeImgeArr;
    
    NSMutableArray *itemArr;
    
    IBOutlet UITableView *listTableView;
    
    NSMutableDictionary *checkedDic;
    
    UIRefreshControl *refreshControl;
}
@end

@implementation CMBikeViewController

//-(IBAction)tableMassegeClicked:(id)sender{
//    [UIAppDelegate topNavMessageClicked:0];
//}


-(IBAction)bottomViewBtnClicked:(id)sender
{
    [UIAppDelegate setSelectedController:[(UIButton *)sender tag]];
    
}


-(void)checkForBikeColor{
    
    
    if ([itemArr count]) {
      NSDictionary *dic = [itemArr objectAtIndex:0];
        
        NSString *stringURL =[NSString stringWithFormat:@"%@%@",GetCyclePreference,[dic stringValueForKey:@"CycleId"]];
        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"***  %@ **",resultDict);
                
                NSDictionary *midDic = nil;
                if ([[resultDict objectForKey:@"GetCyclePreferenceResult"] count]){
                    midDic = [[resultDict objectForKey:@"GetCyclePreferenceResult"] objectAtIndex:0];
                    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:midDic];
                    [UIAppDelegate.userDefault setObject:data forKey:CyclePreferenceKey];
                }
                else
                    [UIAppDelegate.userDefault removeObjectForKey:CyclePreferenceKey];

                
                
                
                
            }
            
        }];
    }
    else{
    
     [UIAppDelegate.userDefault removeObjectForKey:CyclePreferenceKey];
    }
  
    
    
    
    
}

-(IBAction)deleteBikeBtnClicked:(id)sender
{
    NSLog(@"%@",checkedDic);
    
    if (![checkedDic count])
        return;
    
    NSString *keysStr = [[checkedDic allKeys] componentsJoinedByString:@","];
    
  //  NSString *stringURL = [NSString stringWithFormat:@"%@/DeleteCycle/CycleIds=%@",Service_URL,keysStr];
    
    NSLog(@"%@",keysStr);
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             keysStr, @"CycleIds",
                             nil];
  
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteCycle",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]==1) {
                [CMHelper alertMessageWithText:@"Cycle Deleted Successfully."];
            }
            
            NSMutableIndexSet *itemsToRemove = [NSMutableIndexSet new];
            NSUInteger index = 0;
            NSArray *keys = [checkedDic allKeys];

            for (NSString *item in keys) {
                    [itemsToRemove addIndex:[[checkedDic stringValueForKey:item] integerValue]];
                    ++index;
            }
            
            [itemArr removeObjectsAtIndexes:itemsToRemove];
            [checkedDic removeAllObjects];
            [listTableView reloadData];
            [self checkForBikeColor];
            
        }
    }];
    
    
    
    
    
  
}
-(IBAction)addNewBikeBtnClicked:(id)sender
{
    

    CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
    viewController.isNewBike = YES;
    UIAppDelegate.isAddBikeFlg = @"1";
    [self.navigationController pushViewController:viewController animated:YES];
}

-(IBAction)backBtnClicked:(id)sender
{
    //[UIAppDelegate setSelectedController:[(UIButton *)sender tag]];
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[CMBikeInfoController class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            
            break;
        }
    }
    
}
-(IBAction)topleftBtnClicked:(id)sender{
    
    
}

- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    button.selected = !button.selected;
    
    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:listTableView];
    NSIndexPath *indexPath = [listTableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil){
        NSDictionary *dic = [itemArr objectAtIndex:indexPath.row];
        NSString *isValue = [checkedDic stringValueForKey:[dic stringValueForKey:@"CycleId"]];

        if (!isValue || ![isValue length]) {
            [checkedDic setObject:[NSString stringWithFormat:@"%d", indexPath.row] forKey:[dic stringValueForKey:@"CycleId"]];
        }
        else
            [checkedDic removeObjectForKey:[dic stringValueForKey:@"CycleId"]];

        //CycleId
        
    }
    
    [listTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

}

-(void)topNavMenuClicked:(id)sender{

}

-(NSArray *)setUpPopButton{
    
    
    UIBarButtonItem *thirdButton;
    
    
    if (SYSTEM_VERSION_LESS_THAN(_IR_IOS_VERSION_7)){
        UIButton *button;
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [button setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(topNavMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
        thirdButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        
    }
    else{
        
        
        thirdButton          = [[UIBarButtonItem alloc]
                                initWithImage:[[UIImage imageNamed:@"menu.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain
                                target:self action:@selector(topNavMenuClicked:)];
        
        
        
    }
    return [NSArray arrayWithObjects:thirdButton, nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    checkedDic = [NSMutableDictionary new];
    itemArr = [NSMutableArray new];
  
    bikeTitleArr = [NSArray arrayWithObjects:@"Honda 595",@"Harley-Davidson Softail",@"Kawasaki ZX10R",@"KTM 640 Adventure",@"Yamaha FJR 1300", nil];
    bikeDesArr = [NSArray arrayWithObjects:@"A standard motorcycle with neutral ergonomics",@"A cruiser motorcycle for a relaxed ride",@"A sports motorcycle built for speed and handling",@"A duel sport motorcycle for on and off road",@"A tourer motorcycle built for long rides on and open road", nil];
    bikeImgeArr = [NSArray arrayWithObjects:@"honda595",@"Harley-davidson-softail",@"Kawasaki-zx10r",@"KTM-640-adventure",@"Yamaha-fjr-1300", nil];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:BikerBootomTabBar]];
  //

    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];//[self setUpPopButton];//
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];

   // [CMAppDelegate setNavigationBarTitle:@"Motorcycle Information" navigationItem:self.navigationItem];
    [CMAppDelegate setNavigationBarTitle:@"MY BIKE LIST" navigationItem:self.navigationItem];
    
    
    // UIRefreshControl
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(reloadBikeListingData) forControlEvents:UIControlEventValueChanged];
    [listTableView addSubview:refreshControl];

   
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self reloadBikeListingData];
}

-(void)reloadBikeListingData{
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    if (![primaryBikeId length]) {
        primaryBikeId  = [dic stringValueForKey:@"CycleId"];
    }
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];

    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        [refreshControl endRefreshing];
        
        NSUInteger bikeIndex = 0;

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            if ([itemArr count]) {
                [itemArr removeAllObjects];
            }
            
            for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                [itemArr addObject:obj];
                
                if ([[obj stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                    bikeIndex = [itemArr count]-1;
                }
                
                

            }
            
            if ([itemArr count]>1 && bikeIndex!=0) {
                 [itemArr exchangeObjectAtIndex:0 withObjectAtIndex:bikeIndex];
            }
           
         
            
            [listTableView reloadData];
            
        }
    }];
    
}

-(void)markMessageAsRead:(NSString *)cycleId{
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:   cycleId, @"CycleId",  @"Service", @"NotificationType",nil];
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/MarkIsReadNotificationByCycleId",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}
-(IBAction)tableMassegeClicked:(id)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:listTableView];
    NSIndexPath *indexPath = [listTableView indexPathForRowAtPoint:buttonPosition];
   // UITableViewCell *cell = [listTableView cellForRowAtIndexPath:indexPath];

    CMMessageViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMMessageViewController"];
    viewController.messCycleId = [[itemArr objectAtIndex:indexPath.row] stringValueForKey:@"CycleId"];
    //messCycleId
    
    [self markMessageAsRead:[[itemArr objectAtIndex:indexPath.row] stringValueForKey:@"CycleId"]];
    [self.navigationController pushViewController:viewController animated:YES];

}
#pragma mark -
#pragma mark UITableView Method Implementation Call

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 40)];
    footerView.backgroundColor = [tableView backgroundColor];

    UIButton *menuButton =[[UIButton alloc] initWithFrame:CGRectMake(150,0, 150, 40 )];
    menuButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];    //addMotorCycle.png

    [menuButton addTarget:self action:@selector(addNewBikeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"addMotorCycle.png"] forState:UIControlStateNormal];
    [menuButton setTitle:[@"add MotorCycle" uppercaseString] forState:UIControlStateNormal];
    [menuButton setBackgroundColor:[UIColor clearColor]];
    [menuButton setTitleColor:[UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f  blue:182.0f/255.0f  alpha:1] forState:UIControlStateNormal];
    menuButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);    //addMotorCycle.png
    [footerView addSubview:menuButton];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
        return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return   UITableViewCellEditingStyleDelete;

}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        NSDictionary *dic = [itemArr objectAtIndex:indexPath.row];
        
        
        NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 [dic stringValueForKey:@"CycleId"], @"CycleIds",
                                 nil];
        
        
        [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteCycle",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"*** getbrands %@ **",resultDict);
                
                if ([[resultDict stringValueForKey:@"response"] intValue]==1) {
                    [CMHelper alertMessageWithText:@"Cycle Deleted Successfully."];
                }
                
                
                [itemArr removeObjectAtIndex:indexPath.row];
                [listTableView reloadData];
                [self checkForBikeColor];
                
            }
        }];
        
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.1;
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
//{
//    return 0.1;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  itemArr.count;//5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BikeCell"];
    
   UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    
    UILabel *labeName = (UILabel *)[cell viewWithTag:2];
    UILabel *reviewText = (UILabel *)[cell viewWithTag:3];
    
    UIImageView *alertImg = (UIImageView *)[cell viewWithTag:4];
  //  UIImageView *messageImg = (UIImageView *)[cell viewWithTag:5];
    UIButton *messageImg = (UIButton *)[cell viewWithTag:5];
    
    UIButton *button = (UIButton *)[cell viewWithTag:11];
    
    [button setImage:NormalImage forState:UIControlStateNormal];
    [button setImage:SelectedImage forState:UIControlStateSelected];
    [button addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *dic = [itemArr objectAtIndex:indexPath.row];
    
    NSString *isValue = [checkedDic stringValueForKey:[dic stringValueForKey:@"CycleId"]];
    
   

    if (!isValue || ![isValue length]) {
        [button setImage:NormalImage forState:UIControlStateNormal];
        [button setImage:NormalImage forState:UIControlStateSelected];
           }
    else{
       
        [button setImage:SelectedImage forState:UIControlStateNormal];
        [button setImage:SelectedImage forState:UIControlStateSelected];

    
    }
    
    labeName.text  = [dic stringValueForKey:@"Name"];//[bikeTitleArr objectAtIndex:indexPath.row];// [NSString stringWithFormat:@"Motorcycle- %ld",(long)indexPath.row];//[tempDic stringValueForKey:@"user_name"];
    reviewText.text =  [NSString stringWithFormat:@"%@, %@",[dic stringValueForKey:@"BrandName"],[dic stringValueForKey:@"ModelName"]];//[bikeDesArr objectAtIndex:indexPath.row%[bikeDesArr count]];// @"description";//[tempDic stringValueForKey:@"review_text"];
    //timeLable.text =    [bikeImgeArr objectAtIndex:indexPath.row];// @"price";//[tempDic stringValueForKey:@"time"];
    
   // placeholderImage.image =  [UIImage imageNamed:[bikeImgeArr objectAtIndex:indexPath.row%[bikeImgeArr count]]];
    
        NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
        [placeholderImage setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"products1.jpg"]];
    
   
    
    if (![[dic stringValueForKey:@"MsgCount"] intValue]) {
        messageImg.hidden = YES;
    }else
        messageImg.hidden = NO;

    if (![[[dic stringValueForKey:@"Alert"] uppercaseString] isEqualToString:@"GREEN"]) {
        alertImg.hidden = NO;
    }else
        alertImg.hidden = YES;
    

    
    //[placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringForKeyFromJson:@"user_photo"]] placeholderImage:[UIImage imageNamed:@"photo_place_holder.png"]];
    messageImg.hidden = YES;

    
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
      [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

//    UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    CMBikeInfoController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"CMBikeInfoController"];
    
    //CMBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeInfoController"];
    CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
    
    
//    viewController.isFirstPopUp = YES;
    viewController.bikeDataArr = itemArr;
    viewController.bikeInfoDic = [itemArr objectAtIndex:indexPath.row];
    viewController.selectedBikeIndex = indexPath.row;
     UIAppDelegate.isAddBikeFlg = @"3";
    [self.navigationController pushViewController:viewController animated:YES];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
