//
//  CMDirectDealerMailVC.m
//  Cyclemate
//
//  Created by Rajesh on 10/13/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMDirectDealerMailVC.h"
#import "UIViewController+MJPopupViewController.h"

@interface CMDirectDealerMailVC ()
{
    IBOutlet UITextView *inputTextView;
    IBOutlet UIView *sendBuddyView;
    
    IBOutlet UITextView *sendBuddyTextView;
    
    IBOutlet UIButton *submitBtn;

    IBOutlet UILabel *titleLbl;
    
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *phoneTextField;

}
@end

@implementation CMDirectDealerMailVC
@synthesize isParkMyBike;
@synthesize delegate;
@synthesize isSendBuddy;
@synthesize controller;
@synthesize massegeText;
@synthesize titleText;

-(IBAction)postNotes{
    
    if (![inputTextView.text length]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                            message:@"Make sure you enter a Note."
                                                           delegate:Nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
         [self closePopUpWithStatus:YES];

}

- (IBAction)closePopup:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
    else
        [controller dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}
-(void)closePopUpWithStatus:(BOOL)withSuccess{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked: withSend: messageText:)]) {
        [self.delegate cancelButtonClicked:self withSend:withSuccess messageText:inputTextView.text];
    }
    else
        [controller dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
   


//- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController withSend:(BOOL)isSend messageText:(NSString *)messageText
}

-(IBAction)sendDirectNotification:(id)sender{
    
//    if (![emailTextField.text length] ){//&& ![phoneTextField.text length]
//    [CMHelper alertMessageWithText:@"Please enter email Address."];// or phone Number
//        
//    }
////     else if (![phoneTextField.text length]) {
////         [CMHelper alertMessageWithText:@"Please enter phone Number."];
////     }
//    else if ([emailTextField.text length]&& ![Utilities isEmailValid:emailTextField.text ])
//    {
//        [CMHelper alertMessageWithText:@"Please enter vaild email Address."];
//        
//    }
//    else{
//        
//        [self.view endEditing:YES];
//        [self closePopUpWithStatus:YES];
    
    
    if (![emailTextField.text length]  && ![phoneTextField.text length])
    {
        [CMHelper alertMessageWithText:@"Please enter email Address OR mobile Number"];// or phone Number
        
    }
    //   else if (![phoneTextField.text length]) {
    //             [CMHelper alertMessageWithText:@"Please enter Mobile Number."];
    //     }
    else if ([emailTextField.text length]&& ![Utilities isEmailValid:emailTextField.text ])
    {
        [CMHelper alertMessageWithText:@"Please enter vaild email Address."];
        
    }
    else{
        
        

        /*
        NSString *message =  [NSString stringWithFormat:@"Do you want to Send buddy Request to %@?",emailTextField.text];
        
        
        
        
        [[UIAlertView alertViewWithTitle:@"Alert"
                                 message:message cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"OK"]
                               onDismiss:^(int buttonIndex) {
                                   
                                   //OwnerId,Email,Description*/

                                         NSString *ownerId   = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                                   
                                   
                                  NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerId, @"OwnerId",emailTextField.text, @"Email",sendBuddyTextView.text, @"Description",   nil];
                                 //   NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerId, @"OwnerId",emailTextField.text, @"Email",sendBuddyTextView.text, @"Description",phoneTextField.text, @"Mobile",   nil];

                                   [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/SendEmail",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                       
                                       

                                       
                                       
                                       if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                           NSLog(@"*** GetBuddySearchData %@ **",[resultDict valueForKey:@"response"]);
                                           NSString *strResponse = [resultDict valueForKey:@"response"];
                                           if ([strResponse isEqualToString:@"1"])
                                           {
                                               [self.view endEditing:YES];
                                               [self closePopUpWithStatus:YES];
                                               [CMHelper alertMessageWithText:@"Join request has been sent successfully."];
                                           }
                                           

                                           
                                       }
                                  
                                       
                                   }]; /*
                               } onCancel:^{
                                   [self closePopUpWithStatus:YES];

                                   
                               }] show];
*/
       

    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    inputTextView.layer.borderWidth = 2;
    inputTextView.layer.cornerRadius = 4;
    inputTextView.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    
//    self.view.layer.borderWidth = 2;
//     self.view.layer.cornerRadius = 8;
//         self.view.layer.borderColor =  [UIColor lightGrayColor].CGColor;
//    
//    sendBuddyView.layer.borderWidth = 2;
//    sendBuddyView.layer.cornerRadius = 8;
//    sendBuddyView.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    
    sendBuddyView.hidden = !isSendBuddy;
    
    sendBuddyTextView.layer.borderWidth = 2;
    sendBuddyTextView.layer.cornerRadius = 4;
    sendBuddyTextView.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    
    if(isParkMyBike){
        /*
        [submitBtn removeTarget:nil
                           action:NULL
                 forControlEvents:UIControlEventTouchUpInside];
        inputTextView.text = massegeText;
        [submitBtn setTitle:@"ACCEPT" forState:UIControlStateNormal];
        [submitBtn addTarget:controller action:@selector(hideMessagePopUp) forControlEvents:UIControlEventTouchUpInside];
        inputTextView.editable = NO;*/
        
        [submitBtn setTitle:@"SAVE" forState:UIControlStateNormal];

        inputTextView.text = massegeText;

    }
    
    if ([titleText length]) {
        titleLbl.text = titleText;
    }
    else
        titleLbl.text = @"RENO'S POWERSPORTS KC";
    
    //RENO'S POWERSPORTS KC
    


}
#pragma mark
#pragma mark Delegate methods


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
