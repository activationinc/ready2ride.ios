//
//  CMEventsDetailController.h
//  Cyclemate
//
//  Created by Rajesh on 12/31/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownListView.h"

#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "UIViewController+MJPopupViewController.h"
#import "CMAddEventNotesVC.h"

@interface CMEventsDetailController : UIViewController<kDropDownListViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,AddEventNotesDelegate>
{
    NSArray *arryList;
    DropDownListView * Dropobj;
    CMButton_DropDown *selectedHealthBtn;
    NSInteger selectedSocialIndex;
    
    IBOutlet UIScrollView *scrollerView;
    
    IBOutlet UIView *eventsMapview;
    IBOutlet MKMapView *mapview;
    
    IBOutlet UIView *shareOverLay;


}
@property(nonatomic,retain)NSDictionary *eventDic;
@end
