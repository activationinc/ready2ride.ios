//
//  CMPhotoNotesVC.m
//  Cyclemate
//
//  Created by Rajesh on 4/23/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import "CMPhotoNotesVC.h"

@interface CMPhotoNotesVC ()
{
    IBOutlet UICollectionView *photoNotesCollection;
    NSMutableArray *notsArry;
    NSMutableArray *photoArry;
    
    IBOutlet UIButton *photoBtn;
    IBOutlet UIButton *commentBtn;

}
@end

@implementation CMPhotoNotesVC
@synthesize itemsArr;
@synthesize currIndex;
@synthesize isNotes;


-(IBAction)topSegementBtnClicked:(id)sender{
    
    
    photoBtn.backgroundColor = commentBtn.backgroundColor = DarkBlurColor;
    
    UIButton *selectesBtn = (UIButton *)sender;
    selectesBtn.backgroundColor = LightBlurColor;
    
    if ([sender tag]==1) {//Photo
        isNotes = NO;
    }else{
        isNotes = YES;

    
    }
    [photoNotesCollection reloadData];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    notsArry = [NSMutableArray new];
    photoArry = [NSMutableArray new];
    
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
  //  self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    
    
    [CMAppDelegate setNavigationBarTitle:self.title navigationItem:self.navigationItem];

    
    
    for (NSDictionary *dic in itemsArr) {
        // [dic stringValueForKey:@"Note"]
        if ([[dic stringValueForKey:@"Note"] length]) {
            [notsArry addObject:dic];
        }
        else
            [photoArry addObject:dic];

    }
    
    if (isNotes) {
        photoBtn.backgroundColor = commentBtn.backgroundColor = DarkBlurColor;
        commentBtn.backgroundColor = LightBlurColor;
    }
    else{
        photoBtn.backgroundColor = commentBtn.backgroundColor = DarkBlurColor;
        photoBtn.backgroundColor = LightBlurColor;
    
    }
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return isNotes?notsArry.count:photoArry.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSDictionary *dic = [isNotes?notsArry:photoArry objectAtIndex:indexPath.row];

    
    UICollectionViewCell *cell;
    
    if (isNotes) {
        static NSString *identifier1 = @"NotesCollCell";
         cell  = [collectionView dequeueReusableCellWithReuseIdentifier:identifier1 forIndexPath:indexPath];
        
        UIView *borderView = (UIView *)[cell viewWithTag:3];

        [[borderView layer] setCornerRadius:8];
        [[borderView layer] setBorderWidth:2];
        [[borderView layer] setBorderColor:[UIColor darkGrayColor].CGColor];
        
        
        UITextView *titletext = (UITextView *)[cell viewWithTag:1];
        titletext.text = [dic stringValueForKey:@"Note"];
        CGFloat height = [CMAppDelegate getHeightForText:titletext.text withFont:[UIFont boldSystemFontOfSize:15] andWidth:260];
        CGRect frame = titletext.frame;
        frame.size.height = MIN(MAX(height, 30), 300);
        frame.size.width = 260;
        titletext.frame = frame;
        [titletext sizeToFit];

        UILabel *detailLbl = (UILabel *)[cell viewWithTag:2];

         frame = detailLbl.frame;
         frame.origin.y = CGRectGetMaxY(titletext.frame );
         detailLbl.frame = frame;


        detailLbl.text = [NSString stringWithFormat:@"Posted by %@ %@",[dic stringValueForKey:@"Name"],[[[dic stringValueForKey:@"CreatedDate"] componentsSeparatedByString:@" "] objectAtIndex:0]];


    }
    else{
    static NSString *identifier2 = @"PhotoCollCell";
     cell  = [collectionView dequeueReusableCellWithReuseIdentifier:identifier2 forIndexPath:indexPath];
        
        UIImageView *eventImage = (UIImageView *)[cell viewWithTag:3];
        [eventImage setImageWithURL:[NSURL URLWithString: [ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"ImageUrl"]]] placeholderImage:nil];

        UILabel *titleLbl = (UILabel *)[cell viewWithTag:1];
        UILabel *detailLbl = (UILabel *)[cell viewWithTag:2];
        
        titleLbl.text = [NSString stringWithFormat:@"Posted by %@",[dic stringValueForKey:@"Name"]];
        detailLbl.text = [dic stringValueForKey:@"CreatedDate"];
    
    }
  
  

    
    return cell;
    
    
    
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{}


#pragma mark Collection view layout things
// Layout: Set cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // NSLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(320, 400);
    return mElementSize;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(-72,0,0,0);  // top, left, bottom, right
}
#pragma mark - UICollectionViewDelegateFlowLayout
- (BOOL)collectionView:(UICollectionView *)collectionView
shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView
shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
