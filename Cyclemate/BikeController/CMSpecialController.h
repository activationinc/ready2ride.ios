//
//  CMSpecialController.h
//  Cyclemate
//
//  Created by Rajesh on 12/12/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
@interface CMSpecialController : GAITrackedViewController
{

    NSMutableArray *dealerSpecialArry;
    IBOutlet UITableView *dealerSpecialTable;
}

@property (nonatomic) NSString *couponCode;

@end
