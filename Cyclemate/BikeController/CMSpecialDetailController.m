//
//  CMSpecialDetailController.m
//  Cyclemate
//
//  Created by Rajesh on 12/31/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMSpecialDetailController.h"
#import "CMSpecialController.h"
#import "CMNewBikeInfoController.h"
#import "CMVINEntryViewController.h"

@interface CMSpecialDetailController ()
{
    IBOutlet UILabel *titleLbl;
    IBOutlet UILabel *descriptionLbl;
    IBOutlet UILabel *expDateLbl;
    IBOutlet UIImageView *promoImageView;
    IBOutlet UILabel *footerdesLbl;
    IBOutlet UILabel *promoLbl;
    
    IBOutlet UIView *bottomView;
    IBOutlet UIScrollView *scroller;
    UIRefreshControl *refreshControl;
}
@end

@implementation CMSpecialDetailController



@synthesize specialDic;
@synthesize isFromWeather;
@synthesize redeemBtn;
-(IBAction)specialListClicked:(id)sender{
    
    if (isFromWeather)
    {
        CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
        [self.navigationController pushViewController:viewController animated:YES];

    }
    else
        
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"123..");
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self.redeemBtn.layer setBorderWidth:1.0];
//    [self.redeemBtn.layer setBorderColor:[[UIColor colorWithRed:45.0f/255.0f green:106.0f/255.0f  blue:169.0f/255.0f  alpha:1] CGColor]];
   
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    
    [self.view bringSubviewToFront:serviceBtn];
    
    [CMAppDelegate setNavigationBarTitle:@"Specials Details" navigationItem:self.navigationItem];
    // Do any additional setup after loading the view.
   
    
    NSString * strIsRedeemed =  [specialDic stringValueForKey:@"IsRedeemed"];
    NSLog(@"IsRedeemed ==>%@",strIsRedeemed);
    
    NSString * strIsCoupon =  [specialDic stringValueForKey:@"IsCoupon"];
    NSLog(@"strIsCoupon0 ==>%@",strIsCoupon);
       if ([strIsCoupon  isEqualToString:@"1"])
        {
            if ([strIsRedeemed  isEqualToString:@"1"])
            {
                self.redeemBtn.hidden = true;
            }
            else
            {
                 self.redeemBtn.hidden = false;
            }
        }
        else
        {
             self.redeemBtn.hidden = true;
        }

    if (isFromWeather) {
      
        titleLbl.text= [specialDic  stringValueForKey:@"Title"];
        descriptionLbl.text= [specialDic  stringValueForKey:@"MessageText"];
        expDateLbl.text= [NSString stringWithFormat:@"Expires on: %@",[MCFDate dateChangeDateFormat:[specialDic stringValueForKey:@"ExpirationDate"] toFormat:@"EEEE MM/dd/yyyy" currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"]];
        
        [promoImageView setImageWithURL:[NSURL URLWithString:[specialDic stringValueForKey:@"ImageUrl"]] placeholderImage:[UIImage imageNamed:@"special_Placeholder.png"]];
        
        promoLbl.text= @"";//[NSString stringWithFormat:@"Promo Code : %@",[specialDic  stringValueForKey:@"PromoCode"]];
        footerdesLbl.text= [specialDic  stringValueForKey:@"FooterDescription"];
        

        
        
    }else{
    
        NSString * strIsCoupon =  [specialDic stringValueForKey:@"IsCoupon"];
        NSLog(@"strIsCoupon1 ==>%@",strIsCoupon);
        if ([strIsCoupon  isEqualToString:@"1"])
            
        {
            titleLbl.text= [specialDic  stringValueForKey:@"Title"];
            descriptionLbl.text= [specialDic  stringValueForKey:@"Description"];
            
            NSString *requestDate = [specialDic stringValueForKey:@"CouponExpiryDate"];
            NSString *newString1 = [requestDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
            NSLog(@"newString1 ==>%@",newString1);
            NSArray *listItems = [newString1 componentsSeparatedByString:@"+"];
            NSString* dayString = [listItems objectAtIndex: 0];
            double timeStamp = [dayString doubleValue];
            NSTimeInterval timeInterval=timeStamp/1000;
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
            NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
            [dateformatter setDateFormat:@"MM/dd/yyyy"];
            NSString *strDateExpire=[dateformatter stringFromDate:date];
            expDateLbl.text = [NSString stringWithFormat:@"Expires on: %@",strDateExpire];
            
            
            [promoImageView setImageWithURL:[NSURL URLWithString:[specialDic stringValueForKey:@"CouponURL"]] placeholderImage:[UIImage imageNamed:@"special_Placeholder.png"]];
            
            promoLbl.text= [NSString stringWithFormat:@"Coupon code: %@",[specialDic  stringValueForKey:@"CouponCode"]];
            footerdesLbl.text= [specialDic  stringValueForKey:@"FooterDescription"];
        }
        else
        {
        titleLbl.text= [specialDic  stringValueForKey:@"Title"];
        descriptionLbl.text= [specialDic  stringValueForKey:@"Description"];
        expDateLbl.text= [NSString stringWithFormat:@"Expires on: %@",[MCFDate dateChangeDateFormat:[specialDic stringValueForKey:@"ExpDate"] toFormat:@" MM/dd/yyyy" currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"]];
        
        [promoImageView setImageWithURL:[NSURL URLWithString:[specialDic stringValueForKey:@"PromotionImagePath"]] placeholderImage:[UIImage imageNamed:@"special_Placeholder.png"]];
        
        promoLbl.text= [NSString stringWithFormat:@"Promo Code : %@",[specialDic  stringValueForKey:@"PromoCode"]];
        footerdesLbl.text= [specialDic  stringValueForKey:@"FooterDescription"];
        }

    
    }

    scroller.contentSize = CGSizeMake(SCREEN_WIDTH, 375);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)serviceRequestButtonClick:(id)sender
{
    // navigate to service page
   
    UIStoryboard* storyBoard = [CMStoryBoard storyboardWithName:@"Main" bundle:nil];
    CMNewBikeInfoController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
//    CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
    [viewController performSelector:@selector(openServiceTab) withObject:nil afterDelay:.5];
    //[navController pushViewController:viewController animated:YES];
    [self.navigationController pushViewController:viewController animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(IBAction)redeemButtonClick:(id)sender{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Display this to your %@ sales representative. Once activating, this coupon will expire at activation.",[specialDic stringValueForKey:@"DealerName"]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
//    [alert show];
    
    NSString * strMessage = [specialDic stringValueForKey:@"RedeemedMessage"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:strMessage delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    
    if([(NSString *)[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Ok"]) {
        [self loadingRedeemCouponAPI];
    }
    
}
-(void)loadingRedeemCouponAPI{
    
    NSString * strIsCoupon =  [specialDic stringValueForKey:@"IsCoupon"];
    NSString * strCouponCodeId  = [[NSString alloc]init];
    strCouponCodeId = [specialDic stringValueForKey:@"CouponId"];
    
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    [SVProgressHUD showWithStatus:@"Loading Redeem" maskType:SVProgressHUDMaskTypeGradient];
    //PromotionId=%@
   NSString *stringURL =  [NSString stringWithFormat:@"%@/RedeemCoupon/CouponId=%@/OwnerId=%@",Service_URL,strCouponCodeId,ownerId];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];
        [SVProgressHUD dismiss];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** RedeemCoupon Response %@ **",resultDict);
             if (isFromWeather)
             {
             CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
             [self.navigationController pushViewController:viewController animated:YES];
             }
             else
             [self.navigationController popViewControllerAnimated:YES];
            }
    }];
    
}

@end
