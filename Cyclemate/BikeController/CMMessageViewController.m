//
//  CMMessageViewController.m
//  Cyclemate
//
//  Created by Rajesh on 11/18/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMMessageViewController.h"
#import "DropDownListView.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAILogger.h"
#import "GAIFields.h"
#import "GAITrackedViewController.h"
#import "GAITracker.h"
@interface CMMessageViewController ()<kDropDownListViewDelegate>
{
    NSMutableArray *cycleMessageArr;
    IBOutlet UITableView *listTableView;
    
    NSDateFormatter *dateFormatter;
    NSString *ownerId ;
    DropDownListView * Dropobj;
    
    NSMutableArray *contactListArr;
    NSMutableArray *popUpDataArr;
    UIRefreshControl *refreshControl ;
    NSUInteger contactIndex;

}
@end

@implementation CMMessageViewController
@synthesize messCycleId;

-(void)getDropDownDataForMessage{

    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetContactListWithGroupName/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];

    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];

    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetContactListWithGroupName %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetContactListWithGroupNameResult"];
            [popUpDataArr removeAllObjects];
            
         
            
            for (id obj in arr  ) {
                    [popUpDataArr addObject:obj];
            }
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:popUpDataArr];
            [UIAppDelegate.userDefault setObject:data forKey:OwnerBuddyGroupListKey];
        }
        
        if ([popUpDataArr count]) {
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Recipient" withOption:[popUpDataArr valueForKey:@"Name"] xy:CGPointMake(10, 100) size:CGSizeMake(300, MIN(200, [popUpDataArr count]*50+50)) isMultiple:NO];
            
        }
        [SVProgressHUD dismiss];
    }];

}


-(IBAction)messageTypeBtnClicked:(id)sender{
    
    if ([popUpDataArr count]) {
        [Dropobj fadeOut];
        [self showPopUpWithTitle:@"Select Recipient" withOption:[popUpDataArr valueForKey:@"Name"] xy:CGPointMake(10, 100) size:CGSizeMake(300, MIN(200, [popUpDataArr count]*50+50)) isMultiple:NO];

    }
    else
        [self getDropDownDataForMessage];
    


}


-(IBAction)sendMessageBtnClicked:(id)sender{
    
    
    if (!([inputTextView.text length] && [subjectField.text length])) {
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please enter all fields."
                                                           delegate:Nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    
    //[MCFDate dateStringFromDate:picker.date format:@"MM/dd/yyyy"]
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    if (![primaryBikeId length]) {
        primaryBikeId  = [dic stringValueForKey:@"CycleId"];
    }
    
  
    NSString *delerId = [messageTypeBtn selectedObjectID];
    
    //SenderOwnerId
    NSString *senderOwnerId = @"0";
    
    //Status = Owner
    NSString *flag =@"1";
    
    NSString *groupId = @"0";
    
    if (isReplyMessage) {
        
        NSDictionary *tempDic = [cycleMessageArr objectAtIndex:selectedIndex];

        
        if ([[tempDic stringValueForKey:@"SentBy"] isEqualToString:@"2"]) {
            senderOwnerId = ownerId;
            ownerId = [tempDic stringValueForKey:@"SenderOwnerId"];
            flag = @"2";
            delerId = @"1";
        }
        else if ([[tempDic stringValueForKey:@"SentBy"] isEqualToString:@"0"]) {
            
            delerId= [tempDic stringValueForKey:@"DealerId"];
        
        }
        else if ([[tempDic stringValueForKey:@"SentBy"] isEqualToString:@"1"]) {
            
            return;
        }
        //SentBy
        
    }else{
    
    NSDictionary *tempDic = [popUpDataArr objectAtIndex:contactIndex];
    if ([[tempDic stringValueForKey:@"Status"] isEqualToString:@"Owner"]) {
        senderOwnerId = ownerId;
        ownerId = delerId;
        flag = @"2";
        delerId = @"1";
    }
    else if ([[tempDic stringValueForKey:@"Status"] isEqualToString:@"Dealer"]){
        //            ContactId = 14;
    }else{
        
        senderOwnerId = ownerId;
        ownerId = delerId;
        flag = @"2";
        delerId = @"1";
        
        groupId = [messageTypeBtn selectedObjectID];
    
    }
        
    }
    
 //   selectedIndex
        
        NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerId, @"OwnerId", DealerID, @"DealerId",
                                inputTextView.text, @"Description",
                                 flag, @"Flag",
                                 [MCFDate dateChangeDateFormat:dateTextField.text toFormat:@"yyyy-MM-dd" currentDateFormat:@"MM/dd/yyyy"], @"NotificationDate",
                                 subjectField.text,@"Subject",
                                 senderOwnerId,@"SenderOwnerId",
                                 groupId,@"GroupId",
                                 nil];
        
        [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateNotification",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {

        
        [SVProgressHUD dismiss];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            subjectField.text = @"";
            inputTextView.text = @"";
//            
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
//                                                                message:@"Message sent!"
//                                                               delegate:Nil cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//            [alertView show];
            
            
        }
            [self topSegmentBtnClicked:messageBtn];
            [self refreshNotificationData];

    }];
    
    

    ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];


}
-(void)replyOldBtnClicked{

    isReplyMessage = YES;
    [self replyBtnClicked];
}

-(void)replyBtnClicked{
    listTableView.hidden = YES;
    replyView.hidden = NO;

    messageBtn.backgroundColor  = DarkBlurColor;
    newmessageBtn.backgroundColor = LightBlurColor;
    
    if (selectedIndex>=0) {
        NSDictionary *tempDic = [cycleMessageArr objectAtIndex:selectedIndex];
        
        NSString *contactName = [tempDic stringValueForKey:@"SenderOwnerName"];
        if (![contactName length]) contactName = [tempDic stringValueForKey:@"FromName"];
        
        
        //[messageTypeBtn setTitle:@"Reno's Powersports KC" forState:UIControlStateNormal];
        [messageTypeBtn setTitle:contactName forState:UIControlStateNormal];

        //Reno's Powersports KC

        dateTextField.text = [MCFDate dateStringFromDate:[NSDate date] format:@"MM/dd/yyyy"];//  [[[tempDic stringValueForKey:@"CreatedDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
        subjectField.text = [tempDic stringValueForKey:@"Subject"] ;
    }

}
-(IBAction)topSegmentBtnClicked:(id)sender{
    
    
    isReplyMessage = NO;
    messageBtn.backgroundColor = newmessageBtn.backgroundColor = DarkBlurColor;
    
    UIButton *selectesBtn = (UIButton *)sender;
    selectesBtn.backgroundColor = LightBlurColor;
    
    switch ([sender tag]) {
        case 1:{
            isNewMessage = NO;
            replyView.hidden = YES;
            listTableView.hidden = NO;
            [listTableView reloadData];
            [messageBtn setImage:[UIImage imageNamed:@"openmessage.png"] forState:UIControlStateNormal];

            [newmessageBtn setImage:[UIImage imageNamed:@"new_message.png"] forState:UIControlStateNormal];
        }

            break;
        case 2:
//            isNewMessage = YES;
            [newmessageBtn setImage:[UIImage imageNamed:@"new_message_un.png"] forState:UIControlStateNormal];
            [messageBtn setImage:[UIImage imageNamed:@"openmessage_un.png"] forState:UIControlStateNormal];

            selectedIndex = -1;
            picker.date = [NSDate date];
            
             dateTextField.text = [MCFDate dateStringFromDate:[NSDate date] format:@"MM/dd/yyyy"];//  [MCFDate dateStringFromDate:picker.date format:@"MM/dd/yyyy"];

            [self replyBtnClicked];
            break;
            
        default:
            break;
    }
   }

-(void)refreshNotificationData{
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetNotificationByOwner/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];
 
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            if ([cycleMessageArr count]) {
                [cycleMessageArr removeAllObjects];
            }
            
            //GetNotificationByOwnerIdResult
            
            for (id obj in  [resultDict objectForKey:@"GetNotificationByOwnerResult"]) {
                [cycleMessageArr addObject:obj];
                
            }
            
            [listTableView reloadData];
            
        }
    }];
}

- (void)viewDidLoad {
    
   // UIRefreshControl
     refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshNotificationData) forControlEvents:UIControlEventValueChanged];
    [listTableView addSubview:refreshControl];
      
    [super viewDidLoad];
    picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    self->dateFormatter = [[NSDateFormatter alloc] init];

    [dateTextField setTextFieldType:@"Date"];

    // Do any additional setup after loading the view.
    cycleMessageArr = [NSMutableArray new];
    
    [CMAppDelegate setNavigationBarTitle:@"Messages" navigationItem:self.navigationItem];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
   // NSString *
    ownerId    = [dic stringValueForKey:@"OwnerId"];
    
    contactListArr = [NSMutableArray new];
    popUpDataArr   = [NSMutableArray new];

  //  [self getContactListForOwner];
    [self refreshNotificationData];

    
    @try
    {
        
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"Messages"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }
 
    

}
-(void)viewDidAppear:(BOOL)animated{
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:@"UA-80658429-4"];
    [self.tracker set:kGAIScreenName value:@"Messages"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
-(void)viewWillAppear:(BOOL)animated{
 //    self.screenName = @"Messages";
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Messages"];
    
   
}
- (void)viewWillDisappear:(BOOL)animated{
    [listTableView setEditing:NO];
}
#pragma mark - UITextView Delegate for

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        // Be sure to test for equality using the "isEqualToString" message
       [textView resignFirstResponder];
        
        // Return FALSE so that the final '\n' character doesn't get added
        return YES;
    }
    
    return YES;
}






#pragma mark - TextField Delegate for

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    return YES;
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    //Time
    
    if ([[textField textFieldType] isEqualToString:@"Date"]) {
        return NO;

        [self.view endEditing:YES];
        [self datePickerViewClicked:nil];
        selectedField = textField;
        
        
        return NO;
        
    }
    else  if ([[textField textFieldType] isEqualToString:@"Time"]) {
        
        
        return NO;
    }
    
    //    if ([[textField textFieldType] isEqualToString:@"Number"]){
    //        [doneButton setHidden:NO];
    //    }
    //    else
    //        [doneButton setHidden:YES];
    
    return YES;
}
- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
}
//implementation

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    
    
    return YES;
}

#pragma mark - IQActionSheet DatePicker
-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    
    [selectedField setText:[titles componentsJoinedByString:@" - "]];
#if 0
    switch (pickerView.tag)
    {
        case 1: [buttonSingle setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 2: [buttonDouble setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 3: [buttonTriple setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 4: [buttonRange setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 5: [buttonTripleSize setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 6: [buttonDate setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
            
        default:
            break;
    }
#endif
}
- (IBAction)datePickerViewClicked:(UIButton *)sender
{
    //IQActionSheetPickerView *
    
    if (!picker) picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    [picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}
#pragma mark -
#pragma mark UITableView Method Implementation Call



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{

    return   isNewMessage?UITableViewCellEditingStyleNone:UITableViewCellEditingStyleDelete;
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        
        
        
        
        
        [[UIAlertView alertViewWithTitle:@"Alert"
                                 message:@"Do you want to Delete this message?" cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"]
                               onDismiss:^(int buttonIndex) {
                                   
                                   
                                   
                                   NSDictionary *tempDic = [cycleMessageArr objectAtIndex:indexPath.row];
                                   NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerId,@"OwnerId",DealerID,@"DealerId",
                                                            [tempDic stringValueForKey:@"NotificationId"], @"NotificationId",
                                                            nil];
                                   
                                   
                                   [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteNotification",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                       if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                           NSLog(@"*** getbrands %@ **",resultDict);
                                           
                                           
                                           [cycleMessageArr removeObjectAtIndex:indexPath.row];
                                           [tableView reloadData];
                                           
                                           
                                       }
                                   }];
                                   
                               } onCancel:^{
                                   
                                   
                               }] show];
        
        
        

        
        
        
       
        
        
    }
    
}


- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    if (!isNewMessage) {
        return nil;
    }
    
    NSDictionary *tempDic = [cycleMessageArr objectAtIndex:selectedIndex];
    NSString *senderOwnerId = [tempDic stringValueForKey:@"SenderOwnerId"];
    
    if ([ownerId isEqualToString:senderOwnerId]) {
        return nil;
    }
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 27)];
    footerView.backgroundColor = [tableView backgroundColor];
    
    UIButton *menuButton =[[UIButton alloc] initWithFrame:CGRectMake(210,0, 100, 27 )];
    menuButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];    //addMotorCycle.png
    
    [menuButton addTarget:self action:@selector(replyOldBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"service_request.png"] forState:UIControlStateNormal];
    [menuButton setTitle:[@"REPLY" uppercaseString] forState:UIControlStateNormal];
    [menuButton setBackgroundColor:[UIColor clearColor]];
    [menuButton setTitleColor:[UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f  blue:182.0f/255.0f  alpha:1] forState:UIControlStateNormal];
    menuButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);    //addMotorCycle.png
    [footerView addSubview:menuButton];
    return footerView;
        
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 27;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isNewMessage){
        
        NSString *titleValue = @"";
        NSDictionary *tempDic = [cycleMessageArr objectAtIndex:selectedIndex];

        if (!indexPath.row)
        titleValue = [NSString stringWithFormat:@"%@\n%@",[tempDic stringValueForKey:@"Subject"],[tempDic stringValueForKey:@"CreatedDate"]];
        else
          titleValue =   [tempDic stringValueForKey:@"Description"];
        
       float height = [CMAppDelegate getHeightForText:titleValue withFont:[UIFont systemFontOfSize:15] andWidth:300];
        return MAX(height+20, 50);
        
    }
    return 80;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return isNewMessage?2:cycleMessageArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (isNewMessage) {
        NSDictionary *tempDic = [cycleMessageArr objectAtIndex:selectedIndex];

        if (!indexPath.row) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageSubjectCell"];
            
            //UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
            
            UILabel *labeName = (UILabel *)[cell viewWithTag:1];
            UILabel *timeLable = (UILabel *)[cell viewWithTag:2];
            labeName.numberOfLines = 0;
            
            
            labeName.text  = [tempDic stringValueForKey:@"Subject"];

            float height = [CMAppDelegate getHeightForText:labeName.text withFont:[UIFont systemFontOfSize:14] andWidth:300];
            
            CGRect frame = labeName.frame ;
            frame.size.height = height;
            labeName.frame =frame;
            
            

            
            frame = timeLable.frame ;
            frame.origin.y = CGRectGetMaxY(labeName.frame);
            timeLable.frame =frame;
            
            
            
            
            timeLable.text = [tempDic stringValueForKey:@"CreatedDate"];// [[[tempDic stringValueForKey:@"CreatedDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
            

            
            return  cell;

        }
        else
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageDetailCell"];
            
            UILabel *description = (UILabel *)[cell viewWithTag:1];
            description.text = [tempDic stringValueForKey:@"Description"];
            description.numberOfLines = 0;
            
            float height = [CMAppDelegate getHeightForText: description.text withFont:[UIFont systemFontOfSize:15] andWidth:300];

            CGRect frame = description.frame ;
            frame.size.height = height;
            description.frame =frame;
            
            return  cell;

        
        }
        
    }
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    
    //UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    
    UILabel *labeName = (UILabel *)[cell viewWithTag:2];
    UILabel *reviewText = (UILabel *)[cell viewWithTag:3];
    UILabel *timeLable = (UILabel *)[cell viewWithTag:4];
    
    
    UIImageView *stausImage = (UIImageView *)[cell viewWithTag:5];

    
    
    
    NSDictionary *tempDic = [cycleMessageArr objectAtIndex:indexPath.row];
    
    
    labeName.text  = [tempDic stringValueForKey:@"Subject"];
    reviewText.text = [tempDic stringValueForKey:@"Description"];
    timeLable.text =   [tempDic stringValueForKey:@"CreatedDate"];//[[[tempDic stringValueForKey:@"CreatedDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
    
    int flagValue = [[tempDic stringValueForKey:@"SentBy"] intValue];
    
    if (flagValue==0) { // Dealer to owner
        stausImage.image = [UIImage imageNamed:@"in_Message_arrow.png"];
        
    }else  if (flagValue==1) { //Owner to  Dealer
        stausImage.image = [UIImage imageNamed:@"out_Message_arrow.png"];

    }else{
    
        if ([[tempDic stringValueForKey:@"SenderOwnerId"] isEqualToString:ownerId])        stausImage.image = [UIImage imageNamed:@"out_Message_arrow.png"];
        else
            stausImage.image = [UIImage imageNamed:@"in_Message_arrow.png"];

    }
    
    
    
    
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (!isNewMessage) {
        NSDictionary *tempDic = [cycleMessageArr objectAtIndex:indexPath.row];
        [self markAsRead:[tempDic stringValueForKey:@"NotificationId"]];
    selectedIndex = indexPath.row;
    
    isNewMessage = YES;
    replyView.hidden = YES;
    listTableView.hidden = NO;
    [listTableView reloadData];
    }
    
    
}
-(void)markAsRead:(NSString *)notifyTypeID{
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             OWNER_ID, @"OwnerId",
                             @"0", @"CycleId",
                             @"Mail", @"NotificationType",notifyTypeID,@"NotificationId",nil];
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/MarkIsReadNotification",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** MarkIsReadNotification %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}
#pragma mark - Collapse Click Delegate

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    //    NSMutableArray *contactArr = [[NSArray arrayWithObject:@"Reno's Powersports KC"] addObjectsFromArray:[contactListArr valueForKey:@"FName"]];// [contactListArr valueForKey:@"FName"];

    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    
    //    NSMutableArray *contactArr = [[NSArray arrayWithObject:@"Reno's Powersports KC"] addObjectsFromArray:[contactListArr valueForKey:@"FName"]];// [contactListArr valueForKey:@"FName"];
    
    contactIndex = anIndex;

   [messageTypeBtn setTitle:[[popUpDataArr valueForKey:@"Name"] objectAtIndex:anIndex] forState:UIControlStateNormal];
    [messageTypeBtn setSelectedObjectID:[NSString stringWithFormat:@"%@",[[popUpDataArr valueForKey:@"ContactId"] objectAtIndex:anIndex]]];
    
    
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
