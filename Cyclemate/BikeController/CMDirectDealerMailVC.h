//
//  CMDirectDealerMailVC.h
//  Cyclemate
//
//  Created by Rajesh on 10/13/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DirectDealerMailDelegate;

@interface CMDirectDealerMailVC : UIViewController

@property (assign, nonatomic) id <DirectDealerMailDelegate>delegate;
@property(nonatomic)BOOL isSendBuddy;
@property(nonatomic)BOOL isParkMyBike;

@property (assign, nonatomic) id controller;
@property (assign, nonatomic) NSString *massegeText;
@property (assign, nonatomic) NSString *titleText;


-(IBAction)sendDirectNotification:(id)sender;
//-(void)hideMessagePopUp;
-(IBAction)postNotes;
@end



@protocol DirectDealerMailDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController;
- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController withSend:(BOOL)isSend messageText:(NSString *)messageText;
@end
