//
//  CMNewBikeInfoViewController.m
//  Cyclemate
//
//  Created by Rajesh on 12/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//


#import "CMPhotoViewVC.h"
#import "UIViewController+MJPopupViewController.h"
#import "CMDeviceContactsListController.h"

#import "CMNewBikeInfoController.h"
#import "CMEventsController.h"
#import "CMSpecialController.h"
#import "CMBikeViewController.h"
#import "CMHomeViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAILogger.h"
#import "GAIFields.h"
#import "GAITrackedViewController.h"
#import "GAITracker.h"
#import "CMBuddyContactsController.h"
#import "Mixpanel/Mixpanel.h"
@interface CMNewBikeInfoController (){

    
    UIRefreshControl *refreshControl;
}
@end

@implementation CMNewBikeInfoController
@synthesize bikeInfoDic;
@synthesize bikeDataArr;
@synthesize selectedBikeIndex;

@synthesize isFromSetting;
@synthesize isFromHome;

@synthesize isNewBike;



-(IBAction)openBikePhoto:(id)sender{
    if (motorcycleView.docImageView.image) {
        CMPhotoViewVC *secondDetailViewController = [[CMPhotoViewVC alloc] initWithNibName:@"CMPhotoViewVC" bundle:nil];
        secondDetailViewController.controller = self;
        
        secondDetailViewController.photoImage = motorcycleView.docImageView.image;
        //bikeImage
        [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
    }
    
}


-(IBAction)topSegmentBtnClicked:(id)sender;
{
    
//    healthIndex = (int)[sender tag];
//    if (healthIndex==[sender tag]) {
//        return;
//    }
    serviceStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = healthStatusBtn.backgroundColor = DarkBlurColor;

    UIButton *selectesBtn = (UIButton *)sender;
    selectesBtn.backgroundColor = LightBlurColor;
    
   
    
    switch ([sender tag]) {
        case 1:{
            
            [self serviceBtnClicked:0];
            }
            
            break;
            
        case 2:{
          
            [self healthReportBtnClicked:0];

        }            break;

        case 3:{
            
            [self bikeInfoBtnClicked:0];
            
        }
            
            break;
            
        default:
            break;
    }


}
#if 0
-(void)topNavMenuClicked:(id)sender{

    [self openPopUpMenu];

}

-(void)openPopUpMenu{
    

    
    //    servicePopupArr = [NSArray arrayWithObjects:@"Send Service Request", @"Email Direct Dealer", @"Service Notification", @"Cycle Health Certification", nil];
    //
    //    servicePopupImgArr = [NSArray arrayWithObjects:@"send_service_request.png", @"send_dealer_purchase.png", @"service_notification.png", @"cycle_health_certification.png", nil];
    //call dealer, cycle health report, Specials, Events)
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"Messages"
                     image:[UIImage imageNamed:@"menu_1.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      [KxMenuItem menuItem:@"Service"
                     image:[UIImage imageNamed:@"menu_2.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      [KxMenuItem menuItem:@"Bike Health"
                     image:[UIImage imageNamed:@"menu_3.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      [KxMenuItem menuItem:@"Weather" image:[UIImage imageNamed:@"menu_4.png"] target:self  action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Events"
                     image:[UIImage imageNamed:@"menu_5.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      [KxMenuItem menuItem:@"Specials"
                     image:[UIImage imageNamed:@"menu_6.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Buddies"
                     image:[UIImage imageNamed:@"menu_7.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      [KxMenuItem menuItem:@"My Rides"
                     image:[UIImage imageNamed:@"menu_8.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      [KxMenuItem menuItem:@"Park My Vehicle"
                     image:[UIImage imageNamed:@"menu_9.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      [KxMenuItem menuItem:@"My Vehicle List"
                     image:[UIImage imageNamed:@"menu_10.png"]
                    target:self
                    action:@selector(pushMenuItem:)],
      
      
      ];
//    
//    [KxMenuItem menuItem:@"Cycle Service Calender"
//                   image:nil
//                  target:self
//                  action:@selector(pushMenuItem:)],
    
    
    
    //    KxMenuItem *first = menuItems[0];
    //    first.foreColor = [UIColor blackColor];
    
    //[KxMenu setTintColor:[UIColor whiteColor]];
    
    [KxMenu showMenuInView:self.navigationController.view
                  fromRect:CGRectMake(250, 0, 100, 50)
                 menuItems:menuItems];
    
    
}

- (void) pushMenuItem:(id)sender
{
    NSLog(@"%@", sender);
    KxMenuItem *item = (KxMenuItem *)sender;
    
    if ([item.title isEqualToString:@"Bike Health"]) {
        [self healthReportBtnClicked:0];
    }
    else  if ([item.title isEqualToString:@"Specials"]) {
        
        CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
        [self.navigationController pushViewController:viewController animated:YES];
      
         }
    else  if ([item.title isEqualToString: @"Events"]) {
        CMEventsController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMEventsController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else  if ([item.title isEqualToString: @"Messages"]) {
        [self callDelerBtnClicked];
    }
    
    else  if ([item.title isEqualToString: @"My Bike List"]) {
        
        CMBikeViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeViewController"];
        
        
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    //Service
    
}

-(void)callDelerBtnClicked{

    CMDirectDealerMailVC *secondDetailViewController = [[CMDirectDealerMailVC alloc] initWithNibName:@"CMDirectDealerMailVC" bundle:nil];
    secondDetailViewController.delegate = self;
    secondDetailViewController.isSendBuddy = NO;
    
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
}
#endif
-(void)phoneNumberClicked:(id)sender {
  //  UIButton *button = (UIButton *)sender;
    //currentIndexPath = [detailsList indexPathForCell:(UITableViewCell *)[(UIButton *)sender superview]];
    if([[[UIDevice currentDevice]model] isEqualToString:@"iPhone"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Call %@",@"6246826368"] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
        [alert show];
        
    }
    else {
        UIAlertView *dialmsg=[[UIAlertView alloc]initWithTitle:kAPPLICATION_NAME message:@"This feature is not available on your device" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [dialmsg show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if([(NSString *)[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Call"]) {
        [self makePhoneCall:@"6246826368"];
    }
    
}

-(void)makePhoneCall:(NSString*)phoneNumber{
    NSString *numberURL = phoneNumber;
    if (![numberURL hasPrefix:@"tel:"]) {
        numberURL = [@"tel:" stringByAppendingString:numberURL];
    }
    
    NSString *escapedURL = [numberURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (!escapedURL) {
        escapedURL = [numberURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    }
    NSURL *url = [[NSURL alloc] initWithString:escapedURL];
    [[UIApplication sharedApplication] openURL:url];
}

-(IBAction)backBtnClicked:(id)sender{
    
   /* if (isComeFromService) {
        isComeFromService = NO;
        [self serviceBtnClicked:0];
    }
    else*/
        //[self.navigationController popViewControllerAnimated:YES];
    
    
 
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[CMHomeViewController class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            
            break;
        }
    }

}

- (UIBarButtonItem*)backBarButtonItem:(id)controller
{
    static UIBarButtonItem *backButton;
    
    if (IR_IS_SYSTEM_VERSION_LESS_THAN_7_0){
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [button setImage:[UIImage imageNamed:@"Back_Button.png"] forState:UIControlStateNormal];
        [button addTarget:controller action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    else{
        backButton =  [[UIBarButtonItem alloc]
                       initWithImage:[[UIImage imageNamed:@"Back_Button.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
                       target:controller
                       action:@selector(backBtnClicked:)];
    }
    
    
    
    return backButton;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLog(@"reached here 1");
      // Do any additional setup after loading the view.
    healthView = [[CMAppDelegate loadNibNamed:@"CMHealthView" owner:self options:nil] objectAtIndex:0];

    healthRequestArry = [NSMutableArray new];
    
    UINib * cellNib  = [UINib nibWithNibName:@"CMHealthCell" bundle:nil];
    [healthViewTable    registerNib:cellNib forCellReuseIdentifier:@"CMHealthCell"];
    
    cellNib  = [UINib nibWithNibName:@"CMHealthSelectedCell" bundle:nil];
    [healthViewTable    registerNib:cellNib forCellReuseIdentifier:@"CMHealthSelectedCell"];
    
    cellNib  = [UINib nibWithNibName:@"CMNotiSelectedCell" bundle:nil];
    [healthViewTable    registerNib:cellNib forCellReuseIdentifier:@"CMNotiSelectedCell"];
    
    
    
    motorcycleView = [[CMAppDelegate loadNibNamed:@"CMMotorcycleView" owner:self options:nil] objectAtIndex:0];

    
    
    serRequestService = [[CMAppDelegate loadNibNamed:@"CMSCNEWRequestService" owner:self options:nil] objectAtIndex:0];
    serRequestService.contentSize = CGSizeMake(320,400+(IS_IPHONE_5?0:88));
    
    motorcycleView.contentSize = CGSizeMake(320, 1050+(IS_IPHONE_5?0:88));
    CGRect cycleFrame =    motorcycleView.frame;
    cycleFrame.origin.y = 44;
    cycleFrame.size = self.view.frame.size;
    motorcycleView.frame =cycleFrame;// self.view.frame;
    motorcycleView.backgroundColor = [UIColor whiteColor];
    
    
    serRequestService.frame = healthView.frame = CGRectMake(0, 0, bottomView.frame.size.width,  bottomView.frame.size.height);
    
    [self.view addSubview:motorcycleView];
    [bottomView addSubview:serRequestService];
    [bottomView addSubview:healthView];
    
    [self.view addSubview:topSegmentView];

    //
    
    self.navigationItem.leftBarButtonItem = [self backBarButtonItem:self];
    
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    //[CMAppDelegate setNavigationBarTitle:@"Motorcycle Information" //[self setUpPopButton];//navigationItem:self.navigationItem];
    [CMAppDelegate setNavigationBarTitle:@"My Vehicle" navigationItem:self.navigationItem];

    
    [self.navigationController.view addSubview:[UIAppDelegate loadBottomView:(isFromSetting?SettingBootomTabBar:(isFromHome?HomeBootomTabBar:BikerBootomTabBar))]];
    serRequestService.hidden = YES;
    healthView.hidden = YES;
    
    if (![bikeDataArr count])
    {
        NSLog(@"gone to if");
        [self getOwnerforCurrBike];
        [self setCyclePreference];
        
    }
    else
    {
        NSLog(@"gone to else");
        
        cycleId = [bikeInfoDic stringValueForKey:@"CycleId"];
        if ([UIAppDelegate.isAddBikeFlg  isEqual: @"3"])
        {
            
        }
        else
        {
            [self setUpBikeDetails];
            [self serviceBtnClicked:0];
        }
        
    }
    
  
    
    if (isNewBike) {
        serviceStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = healthStatusBtn.backgroundColor = DarkBlurColor;
        
        bikeStatusBtn.backgroundColor = LightBlurColor;
    }
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(setUpHealthDetails) forControlEvents:UIControlEventValueChanged];
    [healthViewTable addSubview:refreshControl];
    
    
    if (!IS_IPHONE_5) {
        CGRect frame =  healthViewTable.frame;
        frame.size.height-=85;
        healthViewTable.frame = frame;
    }
    
    if ([UIAppDelegate.isAddBikeFlg  isEqual: @"1"])
    {
        //UIAppDelegate.isAddBikeFlg = @"2";
    }
    else if ([UIAppDelegate.isAddBikeFlg  isEqual: @"3"])
    {
        
        bikeInfoDic = [[bikeDataArr objectAtIndex:selectedBikeIndex] mutableCopy];
        [self getBikeInfoForCurrBike];
        [self getOwnerforCurrBike];
        [self getCyclePreferenceForCurrBike];
         UIAppDelegate.isAddBikeFlg = @"4";
    }
    else
    {
         [self openCycleInoTab];
    }
   NSLog(@"reached here 2");
   
}
-(void)setCyclePreference{
    
    NSDictionary * dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];

    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    if (![primaryBikeId length]) {
        primaryBikeId  = [dic stringValueForKey:@"CycleId"];
    }
    
    NSString *heladId = [dic stringValueForKey:@"ResolveHealthIssueID"];
    if(!heladId || [[heladId uppercaseString]  isEqualToString:[@"Resolve Health Issues" uppercaseString]])heladId=@"Dealer";
    
    [motorcycleView->resolveHealthBtn  setTitle:heladId forState:UIControlStateNormal];
    
    [motorcycleView->primatyBikeBtn  setSelectedObjectID:primaryBikeId];
    
    
    [motorcycleView->primatyBikeBtn  setTitle:[dic stringValueForKey:@"PrimaryBikeName"] forState:UIControlStateNormal];
}

-(IBAction)bikeInfoBtnClicked:(id)sender{
     healthIndex = 3;
    
    NSArray *contactList = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:OwnerCycleListKey]];
    if ([contactList count]==1) {
        NSDictionary *dic = [contactList objectAtIndex:0];
        [motorcycleView->primatyBikeBtn setTitle:[dic stringValueForKey:@"Name"] forState:UIControlStateNormal];
        [motorcycleView->primatyBikeBtn setSelectedObjectID:[NSString stringWithFormat:@"%@",[dic stringValueForKey:@"CycleId"]]];
        [motorcycleView->primatyBikeBtn setEnabled:NO];
        
    }else  if ([contactList count]>1){
        [motorcycleView->primatyBikeBtn setEnabled:YES];
        
        
    }
    else{
        [motorcycleView->primatyBikeBtn setEnabled:NO];
    }

    
    isComeFromService = YES;
    [CMAppDelegate setNavigationBarTitle:@"My Vehicle" navigationItem:self.navigationItem];

    serRequestService.hidden = YES;
    healthView.hidden = YES;
    motorcycleView.hidden = NO;
    
    mainView.hidden = YES;
}
-(IBAction)serviceBtnClicked:(id)sender
{
    healthIndex = 1;
    
    serviceStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = DarkBlurColor;
    serviceStatusBtn.backgroundColor = LightBlurColor;
    healthStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = DarkBlurColor;
    
    mainView.hidden = NO;

    [CMAppDelegate setNavigationBarTitle:@"SERVICE" navigationItem:self.navigationItem];

    
    [self setUpBikeColorPrefances];
    
    serRequestService->mcModelField.text = [bikeInfoDic stringValueForKey:@"ModelName"];
    serRequestService->mcBrandField.text = [bikeInfoDic stringValueForKey:@"BrandName"];
    
    serRequestService->mcVINField.text = [bikeInfoDic stringValueForKey:@"IdentityNumber"];
    serRequestService->mcCurrMilageField.text = [bikeInfoDic stringValueForKey:@"OdometerReading"];
    
    [serRequestService->serviceCategoryBnt setTitle:@"Service Category" forState:UIControlStateNormal];
    
    
    [serRequestService->serviceCategoryBnt setSelectedObjectID:@"0"];
    //Service Category
    serRequestService->mcReqDateField.text = @"";
    serRequestService->mcReqTimeField.text = @"";
    serRequestService->mcMemoField.text = @"";
    serRequestService->otherDesField.text = @"";
    
    serRequestService.hidden = NO;
    healthView.hidden = YES;
    motorcycleView.hidden = YES;
}
-(IBAction)healthReportBtnClicked:(id)sender{
    
    healthIndex = 2;
    serviceStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = DarkBlurColor;
    healthStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = DarkBlurColor;
    
    healthStatusBtn.backgroundColor = LightBlurColor;

    mainView.hidden = NO;

    [CMAppDelegate setNavigationBarTitle:@"BIKE HEALTH" navigationItem:self.navigationItem];

    [self setUpHealthDetails];
    motorcycleView.hidden = YES;
    serRequestService.hidden = YES;
    healthView.hidden = NO;
   

}

-(IBAction)healthCheckBtnClicked:(id)sender{
    
    
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:@"UA-80658429-4"];
    [self.tracker set:kGAIScreenName value:@"Bike Health"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //ResolvedMethod,ResolvedBy,OdometerReading, CycleServiceId
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:healthViewTable];
    NSIndexPath *indexPath = [healthViewTable indexPathForRowAtPoint:buttonPosition];
    UITableViewCell *cell = [healthViewTable cellForRowAtIndexPath:indexPath];
    
    
    
    UIButton *resolveBtn = (UIButton *)[cell viewWithTag:4];
    UITextField *odoField = (UITextField *)[cell viewWithTag:5];
    UITextField *resolField = (UITextField *)[cell viewWithTag:6];
    
    
    @try
    {
        
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"Bike Health"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }
    
    UIButton *button = (UIButton *)sender;
    
    button.selected = !button.selected;
    
    
    NSDictionary *tempDic =[healthRequestArry objectAtIndex:indexPath.row];
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             [tempDic stringValueForKey:@"CycleServiceId"], @"CycleServiceId",
                             resolField.text, @"ResolvedMethod",
                             [resolveBtn currentTitle], @"ResolvedBy",
                             odoField.text, @"OdometerReading",
                             nil];
    
    
    NSString *status  = [[tempDic stringValueForKey:@"Status"] uppercaseString];

    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/ResolveServiceHealth",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([status isEqualToString:[@"Declined" uppercaseString]]||[status isEqualToString:[@"Decline" uppercaseString]]  ) {
                
                NSMutableDictionary *tempDic  = [[healthRequestArry objectAtIndex:indexPath.row] mutableCopy];
                [tempDic setObject:@"Resolved" forKey:@"Status"];
                
                
                [healthRequestArry replaceObjectAtIndex:indexPath.row withObject:tempDic];

            }
            else{
            [healthRequestArry removeObjectAtIndex:indexPath.row];
            }
            selectedHelthIndex = -1;
            
            [healthViewTable reloadData];
            
            if ([[resolveBtn currentTitle] isEqualToString:@"Dealer"]) {
                [self serviceBtnClicked:0];

            }

            [self setUpBikeColorPrefances];
            
        }
    }];
    
    
}
-(void)healthDropDown:(id)sender{
    
    selectedHealthBtn = (UIButton *)sender;
    [self.view endEditing:YES];
    
    
    arryList = [NSArray arrayWithObjects:@"Dealer",@"Myself", nil];
    //  arryIDs  = [NSArray arrayWithObjects:@"0",@"1", nil];
    Dropobj.tag = 11;
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Resolved By" withOption:arryList xy:CGPointMake(10, 150) size:CGSizeMake(300, MIN(200, arryList.count*50+50)) isMultiple:NO withTag:11];
    
}

#pragma mark -
#pragma mark CMDirectDealerMailVCDelegate Methods

- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController;
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}
#pragma mark - GET Data Calls

-(void)setUpBikePopUp{
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=%@",Service_URL,@"0",ownerId];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [[resultDict objectForKey:@"GetCycleResult"] mutableCopy];
            

            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:bikeDataArr];
            [UIAppDelegate.userDefault setObject:data forKey:OwnerCycleListKey];
            
            
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:cycleId]) {
                    bikeInfoDic = [anObject mutableCopy];
                    selectedBikeIndex = idx;
                    
                }

                
            }];
          
            
            [self serviceBtnClicked:0];

            
            
        }
    }];
    
}
-(void)getBikesNotification{
    
    NSString *currCycleId = cycleId;// [bikeInfoDic stringValueForKey:@"CycleId"];
    
 NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycleServiceNotification/CycleId=%@/DealerId=%@",Service_URL,currCycleId,DealerID];

[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
    [self.view.window hideActivityViewWithAfterDelay:0];
    
    if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"*** getmodel %@ **",resultDict);
        
        NSArray *cycleServiceIds = [healthRequestArry valueForKey:@"CycleServiceId"];
        
        for (id obj in [resultDict objectForKey:@"GetCycleServiceNotificationResult"]) {
            
            NSString *cycleServiceId = [obj objectForKey:@"CycleServiceId"];
            
            if (![cycleServiceIds containsObject:cycleServiceId]) {
                [healthRequestArry addObject:obj];
            }
        }
        
        [healthViewTable reloadData];
        
        
        
    }}];

}
-(void)setUpBikeColorPrefances{
    
    bikeNameLbl.text     = [bikeInfoDic stringValueForKey:@"Name"];
    alertImageView.image = nil;
    healthLbl.text = @"";
    alerTypeImg.image =nil;
    inficatorView.hidden = NO;
    [healthStatusBtn setImage:nil forState:UIControlStateNormal];
    [ inficatorView startAnimating];
    
    
    NSString *stringURL =[NSString stringWithFormat:@"%@%@/DealerId=%@",GetCycleHealthColor,cycleId,DealerID];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic ;
            
            if ([[resultDict objectForKey:@"GetCycleHealthColorResult"] count]) dic = [[resultDict objectForKey:@"GetCycleHealthColorResult"] objectAtIndex:0];
            
            if ([[dic stringValueForKey:@"Color"] isEqualToString:@"Amber"]) {
                
                alertImageView.image = [UIImage imageNamed:@"health_amber_alert.png"];
                healthLbl.text = @"Attention Required";
                alerTypeImg.image = [UIImage imageNamed:@"white_icon.png"];
                [healthStatusBtn setImage:[UIImage imageNamed:@"health_home_yellow_small.png"] forState:UIControlStateNormal];

            }
            else if ([[dic stringValueForKey:@"Color"] isEqualToString:@"Red"]) {
              alertImageView.image = [UIImage imageNamed:@"health_amber_alert_red.png"];
                healthLbl.text = @"Warning: Unresolved Issue(s)";
               alerTypeImg.image = [UIImage imageNamed:@"stop-hand.png"];
                [healthStatusBtn setImage:[UIImage imageNamed:@"health_home_red_small.png"] forState:UIControlStateNormal];
            }
            else{
                
                alertImageView.image = [UIImage imageNamed:@"health_amber_alert_green.png"];
               healthLbl.text = @"Good Bike Health";
                
                alerTypeImg.image = [UIImage imageNamed:@"green_health.png"];
                  [healthStatusBtn setImage:[UIImage imageNamed:@"health_home_small.png"] forState:UIControlStateNormal];
            }
            
            
        }
        
        else{
        
            alertImageView.image = [UIImage imageNamed:@"health_amber_alert_green.png"];
            healthLbl.text = @"Good Bike Health";
            
            alerTypeImg.image = [UIImage imageNamed:@"green_health.png"];
            [healthStatusBtn setImage:[UIImage imageNamed:@"health_home_small.png"] forState:UIControlStateNormal];
            //healthStatusBtn.backgroundColor =  DarkBlurColor;
        }
        
        /*
           serRequestService->alertImageView.image = healthView->alertImageView.image  ;
           serRequestService->healthLbl.text =    healthView->healthLbl.text   ;
              serRequestService->alerTypeImg.image = healthView->alerTypeImg.image   ;
        [serRequestService->healthStatusBtn setImage:[healthView->healthStatusBtn currentImage] forState:UIControlStateNormal];
        healthView->inficatorView.hidden = YES;
        serRequestService->inficatorView.hidden = YES;
        
        */
        inficatorView.hidden = YES;
        
        [ healthView->inficatorView stopAnimating];
    }];
    
  

    
//    if ([bikeColor isEqualToString:@"Red"])
//        recipeImageView.image = [UIImage imageNamed:@"health_home_red.png"];
//    else if ([bikeColor isEqualToString:@"Amber"])
//        recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_home_yellow.png",title]];
//    else
//        recipeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_home.png",title]];
//    
}
-(void)setUpHealthDetails{
    
    
    
    
    [self setUpBikeColorPrefances];
    
    
        
        NSString *stringURL  =[NSString stringWithFormat:@"%@%@/DealerId=%@",GetCycleServiceList,cycleId,DealerID];
        
        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            [refreshControl endRefreshing];
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"***  %@ **",resultDict);
                
                if ([healthRequestArry count]) {
                    [healthRequestArry removeAllObjects];
                }
                for (id obj in [resultDict objectForKey:@"GetCycleServiceListResult"]) {
                    
                    
                    [healthRequestArry addObject:obj];
                }

                
                    
                }
                
#if 0
                if (![healthRequestArry count]) {
                    healthView->alertImageView.image = [UIImage imageNamed:@"health_amber_alert_green.png"];
                    healthView->healthLbl.text = @"Good Bike Health";
                    
                    healthView->alerTypeImg.image = [UIImage imageNamed:@"green_health.png"];
                }
#endif
            selectedHelthIndex = -1;

                [healthViewTable reloadData];
            [self getBikesNotification];
                
                
            }
        ];
        
    
}

-(void)setUpBikeDetails{
    
    NSLog(@"selectedBikeIndex-->%d",selectedBikeIndex);
    [self getBikeInfoForCurrBike];
    [self getOwnerforCurrBike];
    bikeInfoDic = [[bikeDataArr objectAtIndex:selectedBikeIndex] mutableCopy];
    NSLog(@"bikeInfoDic-->%@",bikeInfoDic);
    NSLog(@"cycleId-->%@",cycleId);
    if (cycleId==(id) [NSNull null] || [cycleId length]==0 || [cycleId isEqualToString:@"(null)"]||[cycleId isKindOfClass:[NSNull class]])
    {
         cycleId = [bikeInfoDic stringValueForKey:@"CycleId"];
    }
    [self getCyclePreferenceForCurrBike];
    
   }

-(void)getOwnerforCurrBike{
    
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    NSLog(@"%@",dic);
    
   cycleOwnerId    = [dic stringValueForKey:@"OwnerId"];

    NSString *stringURL =[NSString stringWithFormat:@"%@%@/DealerId=%@",GetOwner,cycleOwnerId,DealerID];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic;
            
            if ([[resultDict objectForKey:@"GetOwnerResult"] count]){
                dic = [[resultDict objectForKey:@"GetOwnerResult"] objectAtIndex:0];
                
                ownerInfoDic = [[resultDict objectForKey:@"GetOwnerResult"] objectAtIndex:0];
                cycleOwnerId = [ownerInfoDic stringValueForKey:@"OwnerId"];
            }
            
            

            
        }
    }];
    
}



-(void)getCyclePreferenceForCurrBike{
    
    
    NSString *stringURL =[NSString stringWithFormat:@"%@%@",GetCyclePreference,cycleId];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic = nil;
            if ([[resultDict objectForKey:@"GetCyclePreferenceResult"] count]){
                dic = [[resultDict objectForKey:@"GetCyclePreferenceResult"] objectAtIndex:0];
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
                [UIAppDelegate.userDefault setObject:data forKey:CyclePreferenceKey];
            }
            else
                dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
            
            
            cyclePreferenceId = [dic stringValueForKey:@"CyclePreferenceId"];
            
            motorcycleView->oilQtyField.text = [dic stringValueForKey:@"ScheduledOilChanges"];
            motorcycleView->maintQtyField.text = [dic stringValueForKey:@"ScheduledMaintenance"];
            
            
            NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
            if (![primaryBikeId length]) {
                primaryBikeId  = [dic stringValueForKey:@"CycleId"];
            }
            
            
            NSString *primaryBikeName = [dic stringValueForKey:@"PrimaryBikeName"];
            if (![primaryBikeName length]) {
                primaryBikeName  = [dic stringValueForKey:@"CycleName"];
            }
            
         
            
            NSString *heladId = [dic stringValueForKey:@"ResolveHealthIssueID"];
            if(!heladId)heladId=@"Dealer";
            
            [motorcycleView->resolveHealthBtn  setTitle:heladId forState:UIControlStateNormal];
            
            // [motorcycleView->resolveHealthBtn setSelectedObjectID:heladId];
            [motorcycleView->primatyBikeBtn  setSelectedObjectID:primaryBikeId];
            
            
            [motorcycleView->primatyBikeBtn  setTitle:primaryBikeName forState:UIControlStateNormal];
            
            /*
            if ([[dic stringValueForKey:@"IsHealthIndicator"] isEqualToString:@"1"] )
                [motorcycleView->addHealthBtn setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            else
                [motorcycleView->addHealthBtn setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];*/
            
        }
        
    }];
    
}

-(void)getBikeInfoForCurrBike{
    
    bikeInfoDic = [[bikeDataArr objectAtIndex:selectedBikeIndex] mutableCopy];
    
    
    if ([[bikeInfoDic stringValueForKey:@"PhotoPath"] length]) {
        NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[bikeInfoDic stringValueForKey:@"PhotoPath"]];
        [motorcycleView.docImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"products1.jpg"]];
    }
    
    // MotorcycleView.docImageView.image =  [UIImage imageNamed:[bikeImgeArr objectAtIndex:selectedBikeIndex%[bikeImgeArr count]]];
    
    // [MotorcycleView.docImageView.image setImageWithURL: placeholderImage:[UIImage imageNamed:@"profile-image-placeholder"]];
    
    
    
    
    motorcycleView->mcNameField.text = [bikeInfoDic stringValueForKey:@"Name"];
    motorcycleView->mcYearField.text = [bikeInfoDic stringValueForKey:@"Year"];
    motorcycleView->mcVINField.text  = [bikeInfoDic stringValueForKey:@"IdentityNumber"];
    motorcycleView->mcOdometerField.text = [bikeInfoDic stringValueForKey:@"OdometerReading"];
    motorcycleView.photoPath= [bikeInfoDic stringValueForKey:@"PhotoPath"];

    
    
    motorcycleView->mcTireSizeField.text = [bikeInfoDic stringValueForKey:@"TireSize"];
    motorcycleView->mcTirePressureField.text = [bikeInfoDic stringValueForKey:@"TirePressure"];
    
    
    motorcycleView->mcDatePurchaseField.text = [[[bikeInfoDic stringValueForKey:@"PurchaseDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
    motorcycleView->mcDateTireInField.text = [[[bikeInfoDic stringValueForKey:@"TireInstallDate"]componentsSeparatedByString:@" "] objectAtIndex:0];;
    motorcycleView->mcDateOilChangeField.text = [[[bikeInfoDic stringValueForKey:@"LastOilChangeDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
    motorcycleView->mcDateServiceMainField.text = [[[bikeInfoDic stringValueForKey:@"LastServiceDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
    
    
    if ([[bikeInfoDic stringValueForKey:@"DealerName"] length]) [motorcycleView->ccycleDealerBtn setTitle:[bikeInfoDic stringValueForKey:@"DealerName"] forState:UIControlStateNormal];
    
    if ([[bikeInfoDic stringValueForKey:@"BrandName"] length])
         motorcycleView->cycleBrandField.text = [bikeInfoDic stringValueForKey:@"BrandName"];
        //[motorcycleView->cycleBrandBtn setTitle:[bikeInfoDic stringValueForKey:@"BrandName"] forState:UIControlStateNormal];
    
    if ([[bikeInfoDic stringValueForKey:@"ModelName"] length])
         motorcycleView->ccycleModelField.text = [bikeInfoDic stringValueForKey:@"ModelName"];
    //  [motorcycleView->ccycleModelBtn setTitle:[bikeInfoDic stringValueForKey:@"ModelName"] forState:UIControlStateNormal];
    
    if ([[bikeInfoDic stringValueForKey:@"CategoryName"] length])
        [motorcycleView->ccycleCategoryBtn setTitle:[bikeInfoDic stringValueForKey:@"CategoryName"] forState:UIControlStateNormal];
    
    if ([[bikeInfoDic stringValueForKey:@"RideType"] length])
        [motorcycleView->ccycleRideingBtn setTitle:[bikeInfoDic stringValueForKey:@"RideType"] forState:UIControlStateNormal];
    
  
    
    [motorcycleView->ccycleDealerBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"DealerId"]];
   // [motorcycleView->cycleBrandBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"BrandId"]];
  //  [motorcycleView->ccycleModelBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"ModelId"]];
    motorcycleView->strBrandID = [bikeInfoDic stringValueForKey:@"BrandId"];
    motorcycleView->strModelID = [bikeInfoDic stringValueForKey:@"ModelId"];
    [motorcycleView->ccycleCategoryBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"CategoryId"]];
    [motorcycleView->ccycleRideingBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"RidingStyleId"]];
    
    
    
    
}


#pragma mark - Post Data Calls


-(IBAction)notificationAcceptStatus:(id)sender{
    //CycleServiceId, IsAccepted, Reason
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:healthViewTable];
    NSIndexPath *indexPath = [healthViewTable indexPathForRowAtPoint:buttonPosition];
  //  UITableViewCell *cell = [healthViewTable cellForRowAtIndexPath:indexPath];

    
    NSString *cycleServiceId = [[healthRequestArry objectAtIndex:indexPath.row] stringValueForKey:@"CycleServiceId"];
    //selectedNotiIndex
    
    //NotificationId
    NSString *isAccepted = @"";
    
    if ([sender tag]==5) {
        isAccepted = @"1";
    }else  if ([sender tag]==6) {
        isAccepted = @"0";
        
        
    }
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             cycleServiceId, @"CycleServiceId",
                             isAccepted, @"IsAccepted",
                             @"", @"Reason",
                             nil];
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/AcceptDeclineService",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
               // [CMHelper alertMessageWithText:[isAccepted intValue]?@"Notification Accepted Successfully":@"Notification Declined Successfully"];
            }
            else  if ([[resultDict stringValueForKey:@"response"] intValue]==-1) {
               // [CMHelper alertMessageWithText:[isAccepted intValue]?@"Notification failed to Accepted":@"Notification failed to Declined"];
                
            }
            
        }
    }];
    
    
    if ([isAccepted integerValue]==1) {
        selectedHelthIndex = -1;
       NSMutableDictionary *tempDic  = [[healthRequestArry objectAtIndex:indexPath.row] mutableCopy];
        [tempDic setObject:@"Scheduled" forKey:@"Status"];
        [tempDic setObject:[tempDic stringValueForKey:@"ServiceDate"] forKey:@"CreatedOn"];
    
    
        [healthRequestArry replaceObjectAtIndex:indexPath.row withObject:tempDic];


    }else {// if ([sender tag]==2) {
        
        
        selectedHelthIndex = -1;
        NSMutableDictionary *tempDic  = [[healthRequestArry objectAtIndex:indexPath.row] mutableCopy];
        [tempDic setObject:@"Declined" forKey:@"Status"];
        [tempDic setObject:[tempDic stringValueForKey:@"ServiceDate"] forKey:@"CreatedOn"];
        
        
        [healthRequestArry replaceObjectAtIndex:indexPath.row withObject:tempDic];
    }
    
    [healthViewTable reloadData];
    
    
    
    
    
}
-(IBAction)postRequestService:(id)sender{
    
    
   
    NSString *mcMemoStr =  serRequestService->mcMemoField.text ;
    
    NSString *mcCurrMilageStr =   [serRequestService->mcCurrMilageField.text length]?serRequestService->mcCurrMilageField.text:@"0";
    NSString *mcReqDateStr =  serRequestService->mcReqDateField.text ;
    NSString *mcOtherStr =  serRequestService->otherDesField.text ;
    NSString *mcVINStr =  serRequestService->mcVINField.text ;
    NSString *requestTime =  serRequestService->mcReqTimeField.text ;
    
    
    
    NSString *serviceCategoryId = [serRequestService->serviceCategoryBnt selectedObjectID];// MotorcycleView->mcDealerField.text;
    NSString *dealerLocId = @"1";// [serRequestService->dealerLocBtn selectedObjectID];;// MotorcycleView->mcDealerField.text;
    
    
    
    
    NSString *cycleServiceId = @"0";
    
    if ([bikeDataArr count]) {
        cycleServiceId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleServiceId"];
        //OwnerId
    }
    if (![cycleServiceId length])cycleServiceId = @"0";
    
    
    //CycleServiceId,CycleId, RequestDate, DealerLocationId, CurrentMileage, ServiceCategoryId, OtherCategory, Description, RequestFrom, IsAccepted, Reason
    //
    //    ownerView->mobileField.text, @"Reason",
    //    ownerView->emailField.text, @"IsAccepted",
    //    @"", @"Description",
    //    @"", @"OtherCategory",
    
    //if user select "Other" as service category then need to  open a text box and pass this text box value in @OtherCategory
    
    
    
    if (![mcReqDateStr length]) {
        [CMHelper alertMessageWithText:@"Please fill in service request date."];
        return;
    }else if (![dealerLocId integerValue]) {
        [CMHelper alertMessageWithText:@"Please fill in dealer location."];
        return;
    }else if (![serviceCategoryId integerValue]) {
        [CMHelper alertMessageWithText:@"Please fill in service category."];
        return;
    }else if ([[[serRequestService->serviceCategoryBnt currentTitle] uppercaseString] isEqualToString:@"OTHER"] && ![mcOtherStr length]) {
        [CMHelper alertMessageWithText:@"Please fill in other category."];
        return;
    }
    
    if ([bikeDataArr count]) {
        cycleId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"];
        
    }
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             @"U", @"RequestFrom",
                             serviceCategoryId, @"ServiceCategoryId",
                             mcCurrMilageStr, @"CurrentMileage",
                             dealerLocId, @"DealerLocationId",
                             mcReqDateStr, @"RequestDate",
                             cycleId, @"CycleId",
                             cycleServiceId, @"CycleServiceId",
                             mcMemoStr, @"Description",
                             mcOtherStr, @"OtherCategory",
                             mcVINStr, @"VIN",
                             requestTime, @"RequestTime",
                             DealerID, @"DealerId",
                             @"1",@"IsAccepted",
                             @"",@"Reason",
                                nil];
    
    
    
//    NSDictionary * infoDic = [NSDictionary
//                           dictionaryWithObjects:@[@"U",serviceCategoryId,mcCurrMilageStr,dealerLocId,mcReqDateStr,cycleId,cycleServiceId,mcMemoStr,mcOtherStr,mcVINStr,requestTime,DealerID]forKeys:@[@"RequestFrom",@"ServiceCategoryId",@"CurrentMileage",@"DealerLocationId",@"RequestDate",@"CycleId",@"CycleServiceId",@"Description",@"OtherCategory",@"VIN",@"RequestTime",@"DealerId"]];
    
     NSLog(@"infoDic--%@",infoDic);
    
    [bikeInfoDic setObject:mcVINStr forKey:@"IdentityNumber"];
  
    motorcycleView->mcVINField.text = [bikeInfoDic stringValueForKey:@"IdentityNumber"];

    [self.view endEditing:YES];

    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];

    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateCycleService",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        [SVProgressHUD dismiss];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
                //[CMHelper alertMessageWithText:@"Service Request Received. Someone will get back to you as soon as possible.\n Thank you."];
                [CMHelper alertMessageWithText:@"Service request received - Thank you for your request. Someone will get back to you as soon as possible."];
                //
                
            NSMutableDictionary *middic = [[bikeDataArr objectAtIndex:selectedBikeIndex] mutableCopy];
            [middic setObject:mcCurrMilageStr forKey:@"OdometerReading"];
            [bikeDataArr replaceObjectAtIndex:selectedBikeIndex withObject:middic];
                //OdometerReading
            }
            else  if ([[resultDict stringValueForKey:@"response"] intValue]==-1) {
                [CMHelper alertMessageWithText:[cycleServiceId intValue]?@"Request Service failed":@"Request Service failed"];
                
            }
            
            [self healthReportBtnClicked:0];
            
        }
    }];
    
    
    //Go to Health Section
    
    
    
}

-(void)postImageWithServiceType:(CMBikeInfoType)serviceType withUpdateStatus:(BOOL)isUpdate{
    
    UIImage *imageFile = nil;
    
    switch (serviceType) {
        case CMMotorcycleViewType:
            imageFile =  motorcycleView.docImageView.image;
            break;
        case CMDLViewType:
         //   imageFile = driverLicenseView.docImageView.image ;
            
            break;
            
        case CMInsuranceViewType:
         //   imageFile = insuranceView.docImageView.image;
            
            break;
        default:
            break;
    }
    
    if (imageFile) {
        [SVProgressHUD showWithStatus:@"Uploading Image.." maskType:SVProgressHUDMaskTypeClear];

        
        [[CMNetworkManager sharedInstance] postImage:ImageUploadURL  withImage:imageFile  param:nil  requestHandler:^(NSDictionary *resultDict, NSError *error) {
            [SVProgressHUD dismiss];
            NSString *imageUrl = @"";
            if ([resultDict count]) {
                NSDictionary *tempDic =  [(NSArray *)resultDict objectAtIndex:0];
                imageUrl = [tempDic stringValueForKey:@"image_url"];
                NSLog(@"tempDic = %@",tempDic);
            }
            
            switch (serviceType) {
                case CMMotorcycleViewType:{
                    
                    motorcycleView.photoPath = imageUrl;
                    [self postMotorCycleInfo:0];
                    motorcycleView.isNewImage = NO;
                    
                 //   [CMHelper alertMessageWithText:isUpdate?@"Bike updated successfully.":@"Bike added successfully."];
                    
                }
                    break;
                case CMDLViewType:
//                    driverLicenseView.photoPath = imageUrl;
//                    [self postOwnerInfo:0];
//                    driverLicenseView.isNewImage = NO;
                    
                    break;
                    
                case CMInsuranceViewType:
//                    insuranceView.photoPath = imageUrl;
//                    [self postInsuranceInfo:0];
//                    insuranceView.isNewImage = NO;
//                    
                    break;
                default:
                    break;
            }
            
            
            
        }];
    }
    
    
    
}

-(IBAction)postCyclePreferenceInfo:(id)sender{
    
    
    
    if ([bikeDataArr count]) {
        cycleId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"];
        
    }
    
    if (![cyclePreferenceId length])cyclePreferenceId = @"0";
    
   
    
    
    NSString *IsHealthIndicator = @"0";//[NSString stringWithFormat:@"%d",[motorcycleView->addHealthBtn tag]];
    
    NSString *resolveHealthStr     = [motorcycleView->resolveHealthBtn currentTitle];
    
    
    NSString *primaryBikeId   = [motorcycleView->primatyBikeBtn selectedObjectID];
    
    
    NSString *ScheduledOilChanges = [motorcycleView->oilQtyField.text length]?motorcycleView->oilQtyField.text:@"0";
    NSString *ScheduledMaintenance = [motorcycleView->maintQtyField.text length]?motorcycleView->maintQtyField.text:@"0";
    
    
    //[motorcycleView->addHealthBtn tag]
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:cyclePreferenceId, @"CyclePreferenceId",ScheduledMaintenance, @"ScheduledMaintenance",
                             ScheduledOilChanges, @"ScheduledOilChanges",
                             resolveHealthStr, @"ResolveHealthIssueID",
                             IsHealthIndicator, @"IsHealthIndicator",
                             cycleId, @"CycleId",
                             primaryBikeId, @"PrimaryBikeId",
                             nil];
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateCyclePreference",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);

        }
    }];
    
    
    
}

-(IBAction)postMotorCycleInfo:(id)sender{
    
    // motorcycleView
    
    
    NSString *mcNameStr = [motorcycleView->mcNameField.text uppercaseString];
    motorcycleView->mcNameField.text = mcNameStr;
    
    NSString *mcDatePurchaseStr = [[NSString alloc]init];
    mcDatePurchaseStr = motorcycleView->mcDatePurchaseField.text;
    
    if (![mcNameStr length]) {
        [CMHelper alertMessageWithText:@"Please fill in Vehicle name"];
        return;
    }
    NSLog(@"mcDatePurchaseStr-->%lu",(unsigned long)[mcDatePurchaseStr length]);
    if (mcDatePurchaseStr==(id) [NSNull null] || [mcDatePurchaseStr length]==1 || [mcDatePurchaseStr isEqualToString:@"(null)"]||[mcDatePurchaseStr isKindOfClass:[NSNull class]] ||  [mcDatePurchaseStr isEqualToString:@""])
    {
        [CMHelper alertMessageWithText:@"Please select vehicle purchase date"];
        return;
    }
    NSString *mcTirePressureStr = [motorcycleView->mcTirePressureField.text length]?motorcycleView->mcTirePressureField.text:@"0";
    NSString *mcOdometerStr = [motorcycleView->mcOdometerField.text length]?motorcycleView->mcOdometerField.text:@"0";
    
    
    NSString *mcYearStr = motorcycleView->mcYearField.text;//[ motorcycleView->mcYearField.text length]? motorcycleView->mcYearField.text:@"0";
    NSString *mcVINStr = motorcycleView->mcVINField.text;//[motorcycleView->mcVINField.text length]?motorcycleView->mcVINField.text:@"0";
    
    
    NSString *mcTireSizeStr =   @"";// motorcycleView->mcTireSizeField.text;//[motorcycleView->mcTireSizeField.text length]?motorcycleView->mcTireSizeField.text:@"0";
    
    NSString *mcDateTireInStr =  @"";//motorcycleView->mcDateTireInField.text;
    
    
  
   // NSString *mcDateOilChangeStr = motorcycleView->mcDateOilChangeField.text;
   // NSString *mcDateServiceMainStr = motorcycleView->mcDateServiceMainField.text;
    
    
    NSString *mcDateOilChangeStr = motorcycleView->mcDateOilChangeField.text;
    mcDateOilChangeStr = [mcDateOilChangeStr stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (mcDateOilChangeStr==(id) [NSNull null] || [mcDateOilChangeStr length] == 0 || [mcDateOilChangeStr isEqualToString:@"(null)"]||[mcDateOilChangeStr isKindOfClass:[NSNull class]] ||  [mcDateOilChangeStr isEqualToString:@""])
    {
        mcDateOilChangeStr = @"";
    }
    
    
    NSString *mcDateServiceMainStr = motorcycleView->mcDateServiceMainField.text;
    mcDateServiceMainStr = [mcDateServiceMainStr stringByTrimmingCharactersInSet:
                            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (mcDateServiceMainStr==(id) [NSNull null] || [mcDateServiceMainStr length] == 0 || [mcDateServiceMainStr isEqualToString:@"(null)"]||[mcDateServiceMainStr isKindOfClass:[NSNull class]] ||  [mcDateServiceMainStr isEqualToString:@""])
    {
        mcDateServiceMainStr = @"";
    }

    
    NSString *cycleBrandStr     = motorcycleView->strBrandID;
    NSString *ccycleModelStr    = motorcycleView->strModelID;
   // NSLog(@"model style..%@",ccycleModelStr);
//    NSString *cycleBrandStr     = [motorcycleView->cycleBrandBtn selectedObjectID];
//    NSString *ccycleModelStr    = [motorcycleView->ccycleModelBtn selectedObjectID];
    NSString *ccycleCategoryStr = [motorcycleView->ccycleCategoryBtn selectedObjectID];
    NSString *ccycleRideingStr = [motorcycleView->ccycleRideingBtn selectedObjectID];
    
    NSString *ccycleDealerStr = @"0";//[motorcycleView->ccycleDealerBtn selectedObjectID];;// motorcycleView->mcDealerField.text;
    
    NSString * brandId = [NSString stringWithFormat:@"%@",cycleBrandStr];
    NSString * modelId = [NSString stringWithFormat:@"%@",ccycleModelStr];
    if (brandId==(id) [NSNull null] || [brandId length]==0 || [brandId isEqualToString:@"(null)"]||[brandId isKindOfClass:[NSNull class]])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please Select Brand."
                                                           delegate:Nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else if (modelId==(id) [NSNull null] || [modelId length]==0 || [modelId isEqualToString:@"(null)"]||[modelId isKindOfClass:[NSNull class]])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please Select Model."
                                                           delegate:Nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
    cycleOwnerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
 
   
    if (![cycleOwnerId length])  cycleOwnerId = @"0";
    if (![cycleId length])  cycleId = @"0";
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:motorcycleView.photoPath, @"PhotoPath",
                             @"", @"Photo",
                             mcNameStr, @"Name",
                             cycleBrandStr, @"BrandId",
                             ccycleModelStr, @"ModelId",
                             cycleId, @"CycleId",
                             cycleOwnerId, @"OwnerId",
                             ccycleCategoryStr, @"CategoryId",
                             mcYearStr, @"Year",
                             mcVINStr, @"IdentityNumber",
                             mcOdometerStr, @"OdometerReading",
                             mcTireSizeStr, @"TireSize",
                             mcDateTireInStr, @"TireInstallDate",
                             mcTirePressureStr, @"TirePressure",
                             DealerID, @"DealerId",  mcDatePurchaseStr, @"PurchaseDate",
                             mcDateOilChangeStr, @"LastOilChangeDate",
                             mcDateServiceMainStr, @"LastServiceDate",
                             ccycleRideingStr, @"RidingStyleId",
                             nil];
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
       // bikeInfoDic = [infoDic mutableCopy];
    
    //if ([cycleId intValue]) {
        
        [bikeInfoDic setObject:motorcycleView.photoPath forKey:@"PhotoPath"];
        [bikeInfoDic setObject:mcNameStr forKey:@"Name"];
        [bikeInfoDic setObject:cycleBrandStr forKey:@"BrandId"];
        [bikeInfoDic setObject:ccycleModelStr forKey:@"ModelId"];
        [bikeInfoDic setObject:cycleId forKey:@"CycleId"];
        [bikeInfoDic setObject:cycleOwnerId forKey:@"OwnerId"];
        [bikeInfoDic setObject:ccycleCategoryStr forKey:@"CategoryId"];
        [bikeInfoDic setObject:mcYearStr forKey:@"Year"];
        [bikeInfoDic setObject:mcVINStr forKey:@"IdentityNumber"];
        [bikeInfoDic setObject:mcOdometerStr forKey:@"OdometerReading"];
    
    [bikeInfoDic setObject:mcTireSizeStr forKey:@"TireSize"];
    [bikeInfoDic setObject:mcDateTireInStr forKey:@"TireInstallDate"];
    [bikeInfoDic setObject:mcTirePressureStr forKey:@"TirePressure"];
  //  [bikeInfoDic setObject:ccycleDealerStr forKey:@"DealerId"];
    
    [bikeInfoDic setObject:mcDatePurchaseStr forKey:@"PurchaseDate"];
    [bikeInfoDic setObject:mcDateOilChangeStr forKey:@"LastOilChangeDate"];
    [bikeInfoDic setObject:mcDateServiceMainStr forKey:@"LastServiceDate"];
    [bikeInfoDic setObject:ccycleRideingStr forKey:@"RidingStyleId"];

 
    [bikeInfoDic setObject:motorcycleView->cycleBrandField.text forKey:@"BrandName"];
    [bikeInfoDic setObject:motorcycleView->ccycleModelField.text forKey:@"ModelName"];
    [bikeInfoDic setObject:[motorcycleView->ccycleCategoryBtn currentTitle] forKey:@"CategoryName"];
//  [bikeInfoDic setObject:[motorcycleView->cycleBrandBtn currentTitle] forKey:@"BrandName"];
//  [bikeInfoDic setObject:[motorcycleView->ccycleModelBtn currentTitle] forKey:@"ModelName"];
    [bikeInfoDic setObject:[motorcycleView->ccycleRideingBtn currentTitle] forKey:@"RideType"];
    
        
//        [motorcycleView->cycleBrandBtn selectedObjectID];
//        [motorcycleView->ccycleModelBtn selectedObjectID];
//        [motorcycleView->ccycleCategoryBtn selectedObjectID];
//        [motorcycleView->ccycleRideingBtn selectedObjectID];


    //}
    
        
        
    
    NSLog(@"info dict//// %@",infoDic);
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateCycle",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [SVProgressHUD dismiss];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
                BOOL isUpdate = NO;
                
                if (sender)
                    isUpdate=  [cycleId intValue]?YES:NO;
                    
                    //
                NSLog(@"cycle id..%@",cycleId);
                
                if (![cycleId intValue])
                {
                    cycleId = [resultDict stringValueForKey:@"response"];
                    if (!bikeInfoDic) {
                        bikeInfoDic = [infoDic mutableCopy];
                        [bikeInfoDic setObject:cycleId forKey:@"CycleId"];
                    }
      
                    [self setUpBikePopUp];
                    [UIAppDelegate  validationForUUID];

                }
                
                cycleId = [resultDict stringValueForKey:@"response"];
                
                [bikeInfoDic setObject:cycleId forKey:@"CycleId"];
                
                if (motorcycleView.isNewImage ) {
                    [self postImageWithServiceType:CMMotorcycleViewType withUpdateStatus:isUpdate];
                }
                else
                {
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                                        [mixpanel.people set:@{
                                                               @"Brand":  motorcycleView->cycleBrandField.text,
                                                               @"Category": [motorcycleView->ccycleCategoryBtn currentTitle],
                                                               @"IdentityNumber":  motorcycleView->mcVINField.text,
                                                               @"LastOilChangeDate": mcDateOilChangeStr,
                                                               @"LastServiceDate": mcDateServiceMainStr,
                                                               @"Model": motorcycleView->ccycleModelField.text,
                                                               @"Name": mcNameStr,
                                                               @"PurchaseDate": mcDatePurchaseStr
                                                               }];
                   // [CMHelper alertMessageWithText:isUpdate?@"Bike updated successfully.":@"Bike added successfully." delegate:self tag:2];
                  
                    if (isUpdate == YES)
                    {
                       [CMHelper alertMessageWithText:@"Bike updated successfully." delegate:self tag:2];
                    }
                    else
                    {
                        [CMHelper alertMessageWithText:@"Bike added successfully." delegate:self tag:1];
                    }
                     [self performSelector:@selector(runAfterOneSecond) withObject:nil afterDelay:0.6];
                
//                    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
//                                                                          message:isUpdate?@"Bike updated successfully.":@"Bike added successfully."
//                                                                         delegate:self
//                                                                cancelButtonTitle:@"OK"
//                                                                otherButtonTitles: nil];
//                    myAlertView.tag = 101;
//                    
//                    [myAlertView show];
                   

                }
                
                
            }else
                [CMHelper alertMessageWithText:[cycleId intValue]?@"Cycle failed to Updated.":@"Cycle unable to add."];
            
            
            [self serviceBtnClicked:0];
            [self postCyclePreferenceInfo:0];
            
        }
        else
            [CMHelper alertMessageWithText:[cycleId intValue]?@"Cycle failed to Updated.":@"Cycle unable to add."];
    }];
    }
}
#pragma mark- Alert methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
   if (alertView.tag==2)
   {
       for (UIViewController *controller in self.navigationController.viewControllers)
       {
           if ([controller isKindOfClass:[CMHomeViewController class]])
           {
               [self.navigationController popToViewController:controller animated:YES];
               
               break;
           }
       }
   }
    else if (alertView.tag == 1)
    {
        [UIAppDelegate.userDefault removeObjectForKey:@"VIN_Record"];
        if (buttonIndex == 0)
        {
            NSLog(@"array..%@",bikeDataArr);
            if ([bikeDataArr count]<=1)
            {
                NSString *alertText=[NSString stringWithFormat:@"By allowing access to your contacts will allow us to help you build your buddy list share rides,see your friends rides, etc"];
                [[UIAlertView alertViewWithTitle:@"Allow 'Ready2Ride' to access your contacts"
                                         message:alertText cancelButtonTitle:@"Don't Allow"
                               otherButtonTitles:@[@"Allow"]
                                       onDismiss:^(int buttonIndex)
                  {
//                      CMBuddyContactsController *buddyProfile = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBuddyContactsController"];
//                      buddyProfile.isFromHome = YES;
//                      [self.navigationController pushViewController:buddyProfile animated:YES];
                      
                      UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"Buddy" bundle:nil];
                      CMDeviceContactsListController *_CMDeviceContactsListController=[storyBoard instantiateViewControllerWithIdentifier:@"CMDeviceContactsListController"];
                      [self.navigationController pushViewController:_CMDeviceContactsListController animated:YES];
                      
                  } onCancel:^
                 {
                      for (UIViewController *controller in self.navigationController.viewControllers)
                      {
                          if ([controller isKindOfClass:[CMHomeViewController class]])
                          {
                              [self.navigationController popToViewController:controller animated:YES];
                              
                              break;
                          }
                      }
                  }] show];

               
            }
            else
            {
                
                //if normal process bike is saving
                CMBikeViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeViewController"];
                [self.navigationController pushViewController:viewController animated:YES];
                
            }
        }
        else
        {
            NSLog(@"Clicked button index other than 0");
            
            // Add another action here
        }
    }
    else
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Clicked button index 0");
            CMBikeViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else
        {
            NSLog(@"Clicked button index other than 0");
            
            // Add another action here
        }
    }
    
  [self performSelector:@selector(runAfterOneSecond) withObject:nil afterDelay:0.6];

}

-(void)runAfterOneSecond{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

#pragma mark - Calls Form Home
-(void)openCycleInoTab{
    
    mainView.hidden = YES;
    
    [CMAppDelegate setNavigationBarTitle:@"Info" navigationItem:self.navigationItem];
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    //[self.navigationController.view addSubview:segmentView];
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    if (![primaryBikeId length]) {
        primaryBikeId  = [dic stringValueForKey:@"CycleId"];
    }
    cycleId = primaryBikeId;
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=%@",Service_URL,@"0",ownerId];
    serRequestService->mcModelField.text = serRequestService->bikeNameLbl.text = [dic stringValueForKey:@"PrimaryBikeName"];
    

    
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            bikeDataArr = [[NSMutableArray alloc]init];
            bikeDataArr = [[resultDict objectForKey:@"GetCycleResult"] mutableCopy];
            
            if (bikeDataArr.count > 0)
            {
                [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                    
                    if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                        bikeInfoDic = [anObject mutableCopy];
                        selectedBikeIndex = idx;
                        *stop = YES;
                    }
                    
                    
                }];
                
                if (!bikeInfoDic && [bikeDataArr count]>0) {
                    bikeInfoDic = [[bikeDataArr lastObject] mutableCopy];
                    selectedBikeIndex = [bikeDataArr count]-1;
                    
                }
                
                [self setUpBikeDetails];
            }
            else
            {
                if (self.messageFlg == 0)
                {
                   if ([[UIAppDelegate.userDefault objectForKey:@"VIN_Record"] isKindOfClass:[NSMutableDictionary class]] && !([UIAppDelegate.userDefault objectForKey:@"VIN_Record"]==NULL))
                    {
                        // dont show alert
                    }
                    else
                        [CMHelper alertMessageWithText:[NSString stringWithFormat:@"To use this feature, you need to register your bike."]];
                    
                    self.messageFlg = 1;
                }
            }
           
         
            
            
            
        }
    }];
    
    
    
    
    serviceStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = healthStatusBtn.backgroundColor = DarkBlurColor;
    
    bikeStatusBtn.backgroundColor = LightBlurColor;
    [self bikeInfoBtnClicked:0];
    
}

-(void)openServiceTab{
    serviceStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = DarkBlurColor;
    serviceStatusBtn.backgroundColor = LightBlurColor;
    
    healthStatusBtn.backgroundColor = [UIColor clearColor];
    healthStatusBtn.backgroundColor =  DarkBlurColor;
    
    
    mainView.hidden = NO;

    [CMAppDelegate setNavigationBarTitle:@"SERVICE" navigationItem:self.navigationItem];
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];


    //[self.navigationController.view addSubview:segmentView];
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];

    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    if (![primaryBikeId length]) {
        primaryBikeId  = [dic stringValueForKey:@"CycleId"];
    }
    cycleId = primaryBikeId;

    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=%@",Service_URL,@"0",ownerId];
    serRequestService->mcModelField.text = serRequestService->bikeNameLbl.text = [dic stringValueForKey:@"PrimaryBikeName"];
    
    serRequestService.hidden = NO;
    healthView.hidden = YES;
    motorcycleView.hidden = YES;
    [self setUpBikeColorPrefances];

    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [[resultDict objectForKey:@"GetCycleResult"] mutableCopy];
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                    bikeInfoDic = [anObject mutableCopy];
                    selectedBikeIndex = idx;
                    *stop = YES;
                }
                
                
            }];
            
            if (!bikeInfoDic && [bikeDataArr count]>0) {
                bikeInfoDic = [[bikeDataArr lastObject] mutableCopy];
                selectedBikeIndex = [bikeDataArr count]-1;
                serviceStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = DarkBlurColor;
                serviceStatusBtn.backgroundColor = LightBlurColor;
                healthStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = DarkBlurColor;
              //  [self serviceBtnClicked:0];
                

            }
            
            serRequestService->mcModelField.text = [bikeInfoDic stringValueForKey:@"ModelName"];
            serRequestService->mcBrandField.text = [bikeInfoDic stringValueForKey:@"BrandName"];
            
            serRequestService->mcVINField.text = [bikeInfoDic stringValueForKey:@"IdentityNumber"];
            serRequestService->mcCurrMilageField.text = [bikeInfoDic stringValueForKey:@"OdometerReading"];
            
            [self setUpBikeColorPrefances];

            
            
            
            
        }
    }];
    
    
    
    
    
    
}

-(void)openHealthTab{
    
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:@"UA-80658429-4"];
    [self.tracker set:kGAIScreenName value:@"Bike Health"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
     serviceStatusBtn.backgroundColor = bikeStatusBtn.backgroundColor = DarkBlurColor;
    mainView.hidden = NO;

    [CMAppDelegate setNavigationBarTitle:@"BIKE HEALTH" navigationItem:self.navigationItem];

    healthView->alertImageView.image = nil;
    healthView->healthLbl.text = @"";
    healthView->alerTypeImg.image =nil;
    healthView->inficatorView.hidden = NO;
    [healthView->inficatorView startAnimating];
    
    
    @try
    {
        
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"Bike Health"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    
    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    if (![primaryBikeId length]) {
        primaryBikeId  = [dic stringValueForKey:@"CycleId"];
    }
    cycleId = primaryBikeId;
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=%@",Service_URL,@"0",ownerId];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [[resultDict objectForKey:@"GetCycleResult"] mutableCopy];
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                    bikeInfoDic = [anObject mutableCopy];
                    selectedBikeIndex = idx;
                    
                }
                
                
            }];
            
            
            if ([bikeDataArr count]){
                [self setUpBikeDetails];
                [self healthReportBtnClicked:0];

            }
            
            
            
            
        }
    }];
    
    motorcycleView.hidden = YES;
    serRequestService.hidden = YES;
    healthView.hidden = NO;
    
}
-(void)openEventDealerTab{
   
}
-(void)openSpecialDealerTab{
    
    
    
}
#pragma mark - UITextView Delegate for

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        // Be sure to test for equality using the "isEqualToString" message
        [textView resignFirstResponder];
        
        // Return FALSE so that the final '\n' character doesn't get added
        return FALSE;
    }
    
    return YES;
}


#pragma mark - TextField Delegate for

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    if ([[textField textFieldType] isEqualToString:@"Number"]) {
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSNumber* candidateNumber;
        
        NSString* candidateString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        range = NSMakeRange(0, [candidateString length]);
        
        [numberFormatter getObjectValue:&candidateNumber forString:candidateString range:&range error:nil];
        
        if (([candidateString length] > 0) && (candidateNumber == nil || range.length < [candidateString length])) {
            
            return NO;
        }
        else
        {
            return YES;
        }
        
    }
    
    
    return YES;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    //Time
    
    if ([[textField textFieldType] isEqualToString:@"Date"]) {
        [self.view endEditing:YES];
        selectedField = textField;

        [self datePickerViewClicked:nil];
        
     
        return NO;
        
    }
    else  if ([[textField textFieldType] isEqualToString:@"Time"]) {
        
        [self.view endEditing:YES];
        [self timePickerViewClicked:nil];
        selectedField = textField;
        
        
        return NO;
    }
    
    //    if ([[textField textFieldType] isEqualToString:@"Number"]){
    //        [doneButton setHidden:NO];
    //    }
    //    else
    //        [doneButton setHidden:YES];
    
    return YES;
}
- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    
}




//implementation
- (void)textFieldDidBeginEditing:(UITextField *)textField {
  
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    
    
    return YES;
}
#pragma mark - IQActionSheet DatePicker


-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    
    [selectedField setText:[titles componentsJoinedByString:@" - "]];
#if 0
    switch (pickerView.tag)
    {
        case 1: [buttonSingle setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 2: [buttonDouble setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 3: [buttonTriple setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 4: [buttonRange setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 5: [buttonTripleSize setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 6: [buttonDate setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
            
        default:
            break;
    }
#endif
}
- (IBAction)timePickerViewClicked:(UIButton *)sender
{
    //IQActionSheetPickerView *
    // @[  @[@"9:00 AM - 12:00 AM",@"12:00 AM - 3:00 PM",@"3:00 PM - 6:00 PM",@"6:00 PM - 9:00 PM"]];
    if (!picker) picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    
    picker.isRangePickerView= YES;
    picker.titlesForComponenets =  @[  @[@"7:00 AM",
                                           @"7:30 AM",
                                           @"8:00 AM",
                                           @"8:30 AM",
                                           @"9:00 AM",
                                           @"9:30 AM",
                                           @"10:00 AM",
                                           @"10:30 AM",
                                           @"11:00 AM",
                                           @"11:30 AM",
                                           @"12:00 PM",
                                           @"12:30 AM",
                                           @"1:00 PM",
                                           @"1:30 PM",
                                           @"2:00 PM",
                                           @"2:30 PM",
                                           @"3:00 PM",
                                           @"3:30 PM",
                                           @"4:00 PM",
                                           @"4:30 PM",
                                           @"5:00 PM",
                                           @"5:30 PM",
                                           @"6:00 PM",
                                           @"6:30 PM",
                                           @"7:00 PM",
                                           @"7:30 PM",
                                           @"8:00 PM",
                                           @"8:30 PM",
                                           @"9:00 PM",
                                           @"9:30 PM",
                                           @"10:00 PM",
                                           @"10:30 PM",
                                           @"11:00 PM",
                                         
                                           ], @[
                                           @"7:30 AM",
                                           @"8:00 AM",
                                           @"8:30 AM",
                                           @"9:00 AM",
                                           @"9:30 AM",
                                           @"10:00 AM",
                                           @"10:30 AM",
                                           @"11:00 AM",
                                           @"11:30 AM",
                                           @"12:00 PM",
                                           @"12:30 AM",
                                           @"1:00 PM",
                                           @"1:30 PM",
                                           @"2:00 PM",
                                           @"2:30 PM",
                                           @"3:00 PM",
                                           @"3:30 PM",
                                           @"4:00 PM",
                                           @"4:30 PM",
                                           @"5:00 PM",
                                           @"5:30 PM",
                                           @"6:00 PM",
                                           @"6:30 PM",
                                           @"7:00 PM",
                                           @"7:30 PM",
                                           @"8:00 PM",
                                           @"8:30 PM",
                                           @"9:00 PM",
                                           @"9:30 PM",
                                           @"10:00 PM",
                                           @"10:30 PM",
                                           @"11:00 PM",
                                           @"11:30 PM",
                                           ]];
    [picker setTag:7];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleTextPicker];
    [picker show];
}

- (IBAction)datePickerViewClicked:(UIButton *)sender
{
    //IQActionSheetPickerView *
    
    if (!picker) picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    [picker setTag:6];
    if (selectedField== serRequestService->mcReqDateField) {
        picker.minimumDate = [[NSDate date] dateByAddingTimeInterval:86400];;//[NSDate date];
    }
    else
        picker.minimumDate = nil;

    picker.isRangePickerView= NO;

    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}
#pragma mark - DropDwon Click Delegate

- (IBAction)DropDownSingle:(id)sender {
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Motorcycle" withOption:bikeDataArr xy:CGPointMake(10, 84) size:CGSizeMake(300, MIN(200, bikeDataArr.count*50+50)) isMultiple:NO withTag:111];
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple withTag:(int)tag{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    
    Dropobj.tag = tag;
    
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    
    if (dropdownListView.tag ==11) {
        
        
        [selectedHealthBtn setTitle:[arryList objectAtIndex:anIndex] forState:UIControlStateNormal];
        
    }
    else{
        
           
    }
    
    
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        
        
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}
/*
-(IBAction)showUIImagePicker:(id)sender{
    
    curType = (int)[sender tag];

//    curType = [sender status];
    if([sender tag]== kCHOOSEPICTURE_From_Library) {
      //  IstoCamera = NO;
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    if([sender tag]== kCHOOSEPICTURE_Take_Photo) {
         @try {
//        IstoCamera = YES;
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType=IMAGESOURSETYPE;
        
        
        // [self.navigationController pushViewController:imagePicker animated:YES];
        
        [self.navigationController presentViewController:imagePicker animated:YES completion:NULL];
        
                }
                @catch (NSException * e) {
                    UIAlertView *alertmsg=[[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Couldn’t locate the camera, check your settings and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertmsg show];
                }
                @finally {
                }
    }
}
*/

-(IBAction)showUIImagePicker:(id)sender{
    
    //curType = (int)[sender tag];
    curType = 1;
    NSLog(@" curType = (int)[sender tag];-- >%u", curType);
    if([sender tag]== kCHOOSEPICTURE_From_Library) {
        //  IstoCamera = NO;
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else if([sender tag]== kCHOOSEPICTURE_Take_Photo) {
        @try {
            //        IstoCamera = YES;
            
            dispatch_async(dispatch_get_current_queue(), ^(void){
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType=IMAGESOURSETYPE;
                imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
                
                
                
                [self.navigationController presentViewController:imagePicker animated:YES completion:NULL];
                
            } ) ;
            
        }
        @catch (NSException * e) {
            UIAlertView *alertmsg=[[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Couldn’t locate the camera, check your settings and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertmsg show];
        }
        @finally {
        }
    }
}
#pragma mark -
#pragma mark CMDirectDealerMailVCDelegate Methods
/*
- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController;
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}
*/
#pragma mark -
#pragma mark UIImagePickerControllerDelegate Methods


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
          NSLog(@" curType = curType -- >%u", curType);
        switch (curType) {
            case CMMotorcycleViewType:
                motorcycleView.isNewImage = YES;
                
                motorcycleView.docImageView.image =image;
                break;
            case CMDLViewType:
//                driverLicenseView.docImageView.image  =image;
//                driverLicenseView.isNewImage = YES;
                break;
                
            case CMInsuranceViewType:
//                insuranceView.docImageView.image  =image;
//                insuranceView.isNewImage = YES;
                
                break;
            default:
                break;
        }
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        
        
        
    }
}



#pragma mark -
#pragma mark UITableView Method Implementation Call


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
        NSDictionary *tempDic =[healthRequestArry objectAtIndex:indexPath.row];
        NSString *status  = [[tempDic stringValueForKey:@"Status"] uppercaseString];
        if ([status isEqualToString:@"ALERT"] || !status.length)
            return 200;
       else if ([status isEqualToString:[@"Certified" uppercaseString]]  || [status isEqualToString:[@"Auto Resolved" uppercaseString]]|| [status isEqualToString:[@"Created" uppercaseString]] ||[status isEqualToString:@"PENDING"]|| [status isEqualToString:[@"Resolved" uppercaseString]] |[[status uppercaseString]isEqualToString:[@"Canceled" uppercaseString]])
           return 60;

        else return 170;
        
    
    
    
    return 80;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return healthRequestArry.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
        NSDictionary *tempDic =[healthRequestArry objectAtIndex:indexPath.row];
        NSString *status  = [[tempDic stringValueForKey:@"Status"] uppercaseString];
        
       // if (indexPath.row==selectedHelthIndex) {
           //|| [status isEqualToString:[@"Pending" uppercaseString]]
          if ([status isEqualToString:@"ALERT"] || !status.length   || [status isEqualToString:[@"Scheduled" uppercaseString]]) {
              
              cell = [tableView dequeueReusableCellWithIdentifier:@"CMNotiSelectedCell"];
              
              
              
              UILabel *titleView = (UILabel *)[cell viewWithTag:1111];
              UILabel *labeName = (UILabel *)[cell viewWithTag:1];
              UILabel *reviewText = (UILabel *)[cell viewWithTag:2];
              UILabel *timeLable = (UILabel *)[cell viewWithTag:3];
              UITextView *infoTextView = (UITextView *)[cell viewWithTag:4];
              UIButton *acceptBtn = (UIButton *)[cell viewWithTag:5];

              UIButton *declineBtn = (UIButton *)[cell viewWithTag:6];
              
              UIImageView *alertImage = (UIImageView *)[cell viewWithTag:7];
              
              reviewText.hidden = YES;
              

              [acceptBtn addTarget:self action:@selector(notificationAcceptStatus:) forControlEvents:UIControlEventTouchUpInside];
              [declineBtn addTarget:self action:@selector(notificationAcceptStatus:) forControlEvents:UIControlEventTouchUpInside];


              labeName.text  =      [tempDic stringValueForKey:@"ServiceCategory"];
              reviewText.text =   [tempDic stringValueForKey:@"Status"];
             // timeLable.text =   [[[tempDic stringValueForKey:@"CreatedOn"] componentsSeparatedByString:@" "] objectAtIndex:0];
              NSString *requestDate = [tempDic stringValueForKey:@"RequestDate"];
              NSLog(@"requestDate ==>%@",requestDate);
              
              if (requestDate==(id) [NSNull null] || [requestDate length]==0 || [requestDate isEqualToString:@"(null)"]||[requestDate isKindOfClass:[NSNull class]])
              {
                  timeLable.text = @" ";
              }
              else
              {
                  NSString *newString1 = [requestDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
                  NSLog(@"newString1 ==>%@",newString1);
                  NSArray *listItems = [newString1 componentsSeparatedByString:@"+"];
                  NSString* dayString = [listItems objectAtIndex: 0];
                  double timeStamp = [dayString doubleValue];
                  NSTimeInterval timeInterval=timeStamp/1000;
                  NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                  NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                  [dateformatter setDateFormat:@"MM-dd-yyyy"];
                  timeLable.text=[dateformatter stringFromDate:date];
              }
            //  timeLable.text =   [[[tempDic stringValueForKey:@"servicedate"] componentsSeparatedByString:@" "] objectAtIndex:0];

              
              NSString *serviceCat = [tempDic stringValueForKey:@"ServiceDescription"];
              
              NSString *timeService = [tempDic stringValueForKey:@"TimeService"];
              NSString *serviceDate = [tempDic stringValueForKey:@"ServiceDate"];
              
              
              
              if ([timeService length] && [serviceDate length])
                serviceCat = [serviceCat stringByAppendingFormat:@" on %@ at %@",serviceDate,timeService];
              else  if ([serviceDate length])
                   serviceCat = [serviceCat stringByAppendingFormat:@" on %@",serviceDate];
              else  if ([timeService length])
                  serviceCat = [serviceCat stringByAppendingFormat:@" at %@",timeService];
              
              
              //TimeService
              
              
              if ([serviceCat isEqualToString:@"Other"]) {
                  serviceCat = [tempDic stringValueForKey:@"Description"];
              }
              
              if (!timeLable.text.length)
                  timeLable.text   =   [[[tempDic stringValueForKey:@"ServiceDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
              
              //infoTextView.text = [NSString stringWithFormat:@"%@\n\n%@\n%@\n\n\nYou are scheduled for your service on:\n%@\n",[tempDic stringValueForKey:@"ServiceCategory"],[tempDic stringValueForKey:@"DealerName"],[tempDic stringValueForKey:@"DealerLocation"],[[[tempDic stringValueForKey:@"ServiceDate"] componentsSeparatedByString:@" "] objectAtIndex:0]];
              
              infoTextView.text = [NSString stringWithFormat:@"%@\n\n%@",serviceCat,[tempDic stringValueForKey:@"DealerComment"]];

              
              //|| [status isEqualToString:@"PENDING"]
              if ([status isEqualToString:@"ALERT"]  || !status.length){
                  alertImage.hidden = NO; reviewText.hidden = YES;}
              else{
                  alertImage.hidden = YES; reviewText.hidden = NO;
              }
              titleView.hidden = NO;
              [infoTextView setBackgroundColor: [UIColor whiteColor]];
              
              CGRect farme = infoTextView.frame;
              farme.origin.y = 75;
              if ([status isEqualToString:[@"Scheduled" uppercaseString]]) {
                  titleView.hidden = YES;
                  farme.origin.y = 50;

                  [infoTextView setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];

                  
                 // infoTextView.text = [NSString stringWithFormat:@"Your Bike is scheduled for \"%@\" on %@ at %@",serviceCat,[MCFDate dateChangeDateFormat:[tempDic stringValueForKey:@"CreatedOn"] toFormat:@"EEEE" currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"],[MCFDate dateChangeDateFormat:[tempDic stringValueForKey:@"CreatedOn"] toFormat:@"HH:mm a" currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"]];
                  
                  infoTextView.text = [NSString stringWithFormat:@"%@\n\n%@",serviceCat,[tempDic stringValueForKey:@"DealerComment"]];

              }
              infoTextView.frame = farme;

            }
         
            
    
        else if ([status isEqualToString:[@"Certified" uppercaseString]]  || [status isEqualToString:[@"Auto Resolved" uppercaseString]]|| [status isEqualToString:[@"Created" uppercaseString]] ||[status isEqualToString:@"PENDING"]|| [status isEqualToString:[@"Resolved" uppercaseString]]||[[status uppercaseString]isEqualToString:[@"Canceled" uppercaseString]]) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"CMHealthCell"];
            
            UILabel *labeName = (UILabel *)[cell viewWithTag:1];
            UILabel *reviewText = (UILabel *)[cell viewWithTag:2];
            UILabel *timeLable = (UILabel *)[cell viewWithTag:3];
            
            UIImageView *alertImage = (UIImageView *)[cell viewWithTag:4];
            alertImage.image = [UIImage imageNamed:@"big_amber_alert"];
            
            labeName.text  =      [tempDic stringValueForKey:@"ServiceCategory"];
            reviewText.text =   [tempDic stringValueForKey:@"Status"];
           // timeLable.text =   [[[tempDic stringValueForKey:@"CreatedOn"] componentsSeparatedByString:@" "] objectAtIndex:0];
            NSString *requestDate = [tempDic stringValueForKey:@"RequestDate"];
            NSLog(@"requestDate ==>%@",requestDate);
            
            if (requestDate==(id) [NSNull null] || [requestDate length]==0 || [requestDate isEqualToString:@"(null)"]||[requestDate isKindOfClass:[NSNull class]])
            {
                timeLable.text = @" ";
            }
            else
            {
                NSString *newString1 = [requestDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
                NSLog(@"newString1 ==>%@",newString1);
                NSArray *listItems = [newString1 componentsSeparatedByString:@"+"];
                NSString* dayString = [listItems objectAtIndex: 0];
                double timeStamp = [dayString doubleValue];
                NSTimeInterval timeInterval=timeStamp/1000;
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                [dateformatter setDateFormat:@"MM-dd-yyyy"];
                timeLable.text=[dateformatter stringFromDate:date];
            }
            
            if (!timeLable.text.length)
                timeLable.text   =   [[[tempDic stringValueForKey:@"ServiceDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
            //|| [status isEqualToString:@"PENDING"]
            if ([status isEqualToString:@"ALERT"] || !status.length ||[status isEqualToString:[@"Certified" uppercaseString]]){
                alertImage.hidden = NO; reviewText.hidden = YES;}
            else{
                alertImage.hidden = YES; reviewText.hidden = NO;
            }
            
            if ([status isEqualToString:[@"Certified" uppercaseString]]) {
                alertImage.image = [UIImage imageNamed:@"Certified"];

            }
            
            //Certified.png

        }
        
        
        else{
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"CMHealthSelectedCell"];
            
            UILabel *labeName = (UILabel *)[cell viewWithTag:1];
            UILabel *reviewText = (UILabel *)[cell viewWithTag:2];
            UILabel *timeLable = (UILabel *)[cell viewWithTag:3];
            
            UIButton *resolveBtn = (UIButton *)[cell viewWithTag:4];
            UITextField *odoField = (UITextField *)[cell viewWithTag:5];
            UITextField *resolField = (UITextField *)[cell viewWithTag:6];
            
            
            
            
            UILabel *resolveLbl = (UILabel *)[cell viewWithTag:7];
            
            UIButton *checkBoxBtn = (UIButton *)[cell viewWithTag:8];
            
            UILabel *resolLbl = (UILabel *)[cell viewWithTag:9];
            
            
            [checkBoxBtn setImage:NormalImage forState:UIControlStateNormal];
            [checkBoxBtn setImage:SelectedImage forState:UIControlStateSelected];
            [checkBoxBtn addTarget:self action:@selector(healthCheckBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            resolveLbl.text = @"Notes:";//[NSString stringWithFormat:@"Resolved:\n%@",[UIAppDelegate.dateFormatter   stringFromDate:[NSDate date]]];
            resolLbl.text = [NSString stringWithFormat:@"Resolved: %@",[UIAppDelegate.dateFormatter   stringFromDate:[NSDate date]]];
            
            [resolveBtn addTarget:self action:@selector(healthDropDown:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            
            
            
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
            NSString *heladId = [dic stringValueForKey:@"ResolveHealthIssueID"];
            if(!heladId || [[heladId uppercaseString] isEqualToString:[@"Resolve Health Issues" uppercaseString]] )heladId=@"Dealer";
            [resolveBtn  setTitle:heladId forState:UIControlStateNormal];
            
            
            
            labeName.text  =      [tempDic stringValueForKey:@"ServiceCategory"];
            reviewText.text =   [tempDic stringValueForKey:@"Status"];
            //timeLable.text =   [[[tempDic stringValueForKey:@"CreatedOn"] componentsSeparatedByString:@" "] objectAtIndex:0];
         
            NSString *requestDate = [tempDic stringValueForKey:@"RequestDate"];
            NSLog(@"requestDate ==>%@",requestDate);
            
            if (requestDate==(id) [NSNull null] || [requestDate length]==0 || [requestDate isEqualToString:@"(null)"]||[requestDate isKindOfClass:[NSNull class]])
            {
                timeLable.text = @" ";
            }
            else
            {
                NSString *newString1 = [requestDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
                NSLog(@"newString1 ==>%@",newString1);
                NSArray *listItems = [newString1 componentsSeparatedByString:@"+"];
                NSString* dayString = [listItems objectAtIndex: 0];
                double timeStamp = [dayString doubleValue];
                NSTimeInterval timeInterval=timeStamp/1000;
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                [dateformatter setDateFormat:@"MM-dd-yyyy"];
                timeLable.text=[dateformatter stringFromDate:date];
            }
            
            odoField.text  =   [bikeInfoDic stringValueForKey:@"OdometerReading"];
            
            
        }
        
  

    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //|| [status isEqualToString:[@"Resolved" uppercaseString]]
    
#if 0
    if (tableView==healthViewTable){
       
        if (selectedHelthIndex==indexPath.section) {
            int midIndex = selectedHelthIndex;
            selectedHelthIndex =-1;
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:midIndex inSection:0 ]] withRowAnimation:UITableViewRowAnimationFade];
        }
    
        else{
        
        NSDictionary *tempDic =[healthRequestArry objectAtIndex:indexPath.row];
        NSString *status  = [[tempDic stringValueForKey:@"Status"] uppercaseString];
        if ([status isEqualToString:[@"Certified" uppercaseString]]  || [status isEqualToString:[@"Auto Resolved" uppercaseString]]|| [status isEqualToString:[@"Created" uppercaseString]] ||[status isEqualToString:@"PENDING"]|| [status isEqualToString:[@"Resolved" uppercaseString]]) {
            
            if (selectedHelthIndex>=0) {
                int midIndex = selectedHelthIndex;

                selectedHelthIndex =-1;
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:midIndex inSection:0 ]] withRowAnimation:UITableViewRowAnimationFade];
            }
         
            
        }
      
        else
            {
        
            if (selectedHelthIndex != indexPath.row) {
                
                
                if (selectedHelthIndex==-1) {
                    selectedHelthIndex = indexPath.row;
                    
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0 ]] withRowAnimation:UITableViewRowAnimationFade];
                    
                }
                else{
                    
                    int midIndex = selectedHelthIndex;
                    selectedHelthIndex = indexPath.row;
                    
                    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:midIndex inSection:0 ],[NSIndexPath indexPathForRow:selectedHelthIndex inSection:0 ],nil] withRowAnimation:UITableViewRowAnimationFade];
                }
                
                
            }

        }
        }
        
        
    }
#endif
        // [self setUpHealthListDetails:[healthRequestArry objectAtIndex:indexPath.row]];
  

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
