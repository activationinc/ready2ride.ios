//
//  CMPhotoViewVC.h
//  Cyclemate
//
//  Created by Rajesh on 4/13/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CMPhotoViewDelegate;

@interface CMPhotoViewVC : UIViewController
@property (assign, nonatomic) id <CMPhotoViewDelegate>delegate;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic)  UIImage *photoImage;
@property (assign, nonatomic) id controller;

@end
@protocol CMPhotoViewDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(CMPhotoViewVC*)secondDetailViewController;
@end