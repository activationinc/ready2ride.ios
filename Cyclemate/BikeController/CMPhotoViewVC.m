//
//  CMPhotoViewVC.m
//  Cyclemate
//
//  Created by Rajesh on 4/13/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import "CMPhotoViewVC.h"
#import "UIViewController+MJPopupViewController.h"

@interface CMPhotoViewVC ()

@end

@implementation CMPhotoViewVC
@synthesize delegate;
@synthesize controller;
@synthesize photoImage;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _imageView.image = photoImage;
}

- (IBAction)closePopup:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
    else
        [controller dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
