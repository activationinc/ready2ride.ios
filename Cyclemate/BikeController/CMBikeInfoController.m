//
//  CMBikeInfoController.m
//  Cyclemate
//
//  Created by Rajesh on 9/24/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "UIImageView+AFNetworking.h"


#import "DAFontSet.h"
#import "DAAttributedStringFormatter.h"
#import "DAAttributedLabel.h"

#import "KxMenu.h"





#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>

#import "CMBikeInfoController.h"
#import "CMDirectDealerMailVC.h"

static NSArray *bikeImgeArr;// = [NSArray arrayWithObjects:@"honda595",@"Harley-davidson-softail",@"Kawasaki-zx10r",@"KTM-640-adventure",@"Yamaha-fjr-1300", nil];


@interface CMBikeInfoController ()<DirectDealerMailDelegate,IQActionSheetPickerViewDelegate>
{
    NSArray *servicePopupArr;
    NSArray *servicePopupImgArr;
    
    NSArray *firstPopupArr;
    NSArray *firstPopupImgArr;
    
    NSArray *arryList;
    DropDownListView * Dropobj;
    
    IBOutlet  UIButton *selectedMotorCyBtn;
    
      UIButton *selectedHealthBtn;

    BOOL IstoCamera;
    
    NSMutableArray *dealerEventArry;
    NSMutableArray *dealerInfoArry;
    NSMutableArray *dealerSpecialArry;
    

    NSMutableArray *healthRequestArry;
    NSMutableArray *notificationArry;

}
@end

@implementation CMBikeInfoController
@synthesize segmentedControl;
@synthesize isFromHome;
@synthesize isFirstPopUp;

@synthesize isFromSetting;

@synthesize bikeInfoDic;
@synthesize bikeDataArr;
@synthesize selectedBikeIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


////for Home Button Action


-(void)openPopUpMenu{
    
#if 0
    [KxMenuItem menuItem:@"ACTION MENU"
                   image:nil
                  target:nil
                  action:NULL],
    
    [KxMenuItem menuItem:@"Send Service Request"
                   image:[UIImage imageNamed:@"send_service_request.png"]
                  target:self
                  action:@selector(pushMenuItem:)],
    
    [KxMenuItem menuItem:@"Email Direct Dealer"
                   image:[UIImage imageNamed:@"send_dealer_purchase.png"]
                  target:self
                  action:@selector(pushMenuItem:)],
    
    [KxMenuItem menuItem:@"Service Notification"
                   image:[UIImage imageNamed:@"service_notification.png"]
                  target:self
                  action:@selector(pushMenuItem:)],
    
    [KxMenuItem menuItem:@"Cycle Health Certification"
                   image:[UIImage imageNamed:@"cycle_health_certification.png"]
                  target:self
                  action:@selector(pushMenuItem:)],

#endif
    
//    servicePopupArr = [NSArray arrayWithObjects:@"Send Service Request", @"Email Direct Dealer", @"Service Notification", @"Cycle Health Certification", nil];
//    
//    servicePopupImgArr = [NSArray arrayWithObjects:@"send_service_request.png", @"send_dealer_purchase.png", @"service_notification.png", @"cycle_health_certification.png", nil];
    
    NSArray *menuItems =
    @[
      
    [KxMenuItem menuItem:@"Send Service Request"
                     image:nil
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Email Direct Dealer"
                     image:nil                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Service Notification"
                     image:nil
                    target:self
                    action:@selector(pushMenuItem:)],
      
      [KxMenuItem menuItem:@"Cycle Health Certification"
                     image:nil
                    target:self
                    action:@selector(pushMenuItem:)],
    [KxMenuItem menuItem:@"Cycle Service Calender"
                   image:nil
                  target:self
                  action:@selector(pushMenuItem:)],

 
      ];
    
    
    

    
//    KxMenuItem *first = menuItems[0];
//    first.foreColor = [UIColor blackColor];
    
    //[KxMenu setTintColor:[UIColor whiteColor]];
    
    [KxMenu showMenuInView:self.navigationController.view
                  fromRect:CGRectMake(250, 0, 100, 50)
                 menuItems:menuItems];
    
    
}

- (void) pushMenuItem:(id)sender
{
    NSLog(@"%@", sender);
    KxMenuItem *item = (KxMenuItem *)sender;
    
    if ([item.title isEqualToString:@"Send Service Request"]) {
        [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];
        [self setUpServiceRequest];
    }
    else  if ([item.title isEqualToString:@"Email Direct Dealer"]) {
        [UIAppDelegate topNavMessageClicked:0];
        isFirstPopUp = NO;
        servicePopUpView.hidden = YES;
    }
    else  if ([item.title isEqualToString: @"Service Notification"]) {
        [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];

    }
    else  if ([item.title isEqualToString: @"Cycle Health Certification"]) {
        [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];

    }
    else  if ([item.title isEqualToString: @"Cycle Service Calender"]) {
        [self openEventCalenderView:0];
    }
    
   
}


-(void)openPopUpMenu_old{
    
    if ( servicePopUpView.hidden) {
        
        servicePopUpView.hidden = NO;
        
        [self.view addSubview:servicePopUpView];
        [popUpTable reloadData];
    }
    else{
        servicePopUpView.hidden = YES;
        
    }

}



- (UIBarButtonItem*)backBarButtonItem
{
    static UIBarButtonItem *backButton;
    
        if (IR_IS_SYSTEM_VERSION_LESS_THAN_7_0){
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
            [button setImage:[UIImage imageNamed:@"nav_back_arrow.png"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        }
        else{
            backButton =  [[UIBarButtonItem alloc]
                           initWithImage:[UIImage imageNamed:@"nav_back_arrow.png"] style:UIBarButtonItemStylePlain
                           target:self
                           action:@selector(backBtnClicked:)];
        }
        
  
    return backButton;
}

-(IBAction)dealerSpecialBackClicked:(id)sender{
    [dealerSpecialDetailView removeFromSuperview];
}
-(IBAction)backToNotificationListClicked:(id)sender{

    
    [serNotificationService->notifyBottomView setHidden:NO];
    
    UITextView *infoTextView = (UITextView *)[serNotificationService viewWithTag:111];
    infoTextView.hidden = YES;

}
-(void)setUpServiceRequest{
    serHealthReport.hidden = YES;
    serRequestService.hidden = YES;
    serNotificationService.hidden = YES;
    
    serRequestService.hidden = NO;
    [self.view addSubview:serRequestService];
    
    
    serRequestService->mcModelField.text = [bikeInfoDic stringValueForKey:@"ModelName"];
    serRequestService->mcYearField.text = [bikeInfoDic stringValueForKey:@"Year"];
    serRequestService->mcVINField.text = [bikeInfoDic stringValueForKey:@"IdentityNumber"];
    serRequestService->mclastNameField.text = [ownerInfoDic stringValueForKey:@"LName"];
    serRequestService->mcFirstNameField.text = [ownerInfoDic stringValueForKey:@"FName"];
    serRequestService->mcPhField.text = [ownerInfoDic stringValueForKey:@"Mobile"];
    serRequestService->mcAlPhField.text = [ownerInfoDic stringValueForKey:@"Phone"];
    serRequestService->mcemailField.text = [ownerInfoDic stringValueForKey:@"Email"];
    
    serRequestService->mcCurrMilageField.text = [bikeInfoDic stringValueForKey:@"OdometerReading"];
    
    //ownerInfoDic
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetDealerLocation/DealerId=%@",Service_URL,DealerID];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict objectForKey:@"GetDealerLocationResult"] count]) {
                id obj =  [[resultDict objectForKey:@"GetDealerLocationResult"] objectAtIndex:0];
                NSString *delerLocation =  [NSString stringWithFormat:@"%@, %@",[obj stringValueForKey:@"DealerName"],[obj stringValueForKey:@"City"]];
                // arryList
                NSString *loactionID = [NSString stringWithFormat:@"%@",[obj stringValueForKey:@"DealerLocationId"]];
                
                [serRequestService->dealerLocBtn setSelectedObjectID:loactionID];
                [serRequestService->dealerLocBtn setTitle:delerLocation forState:UIControlStateNormal];
            }
           
          
            
        }
    }];
    

    
}
-(IBAction)serviceTabBtnClicked:(id)sender{

    
    serHealthReport.hidden = YES;
    serRequestService.hidden = YES;
    serNotificationService.hidden = YES;
    
    currServiceTabIndex = [sender tag];
    
    switch ([sender tag])
    {
   
        case 1:{
            serHealthReport.hidden = NO;
            
            [self.view addSubview:serHealthReport];
            //serHealthReport->reportView.frame
            healthViewTable.frame  = CGRectMake(0, CGRectGetMaxY(serHealthReport->stripView.frame), 320, CGRectGetHeight(healthViewTable.frame));
            
            [serHealthReport addSubview:healthViewTable];
            [self setUpHealthDetails];


        }
            break;
        case 2:{
            [self setUpServiceRequest];
        }
            break;
        default:
        case 3:{
            serNotificationService.hidden = NO;
            
            
         //   infoTextView.text =     @"12th October 2014 12:00AM\nNewyork City\nHarley davidson Softail\n\nDescription of Service:\n\nMotorcycle tire maintenance to keep your motorcycle at peak condition.";
            
            [self.view addSubview:serNotificationService];
            
            
           NSString *currCycleId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"];

            //DealerId
            NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycleServiceNotification/CycleId=%@",Service_URL,currCycleId];
           
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                [self.view.window hideActivityViewWithAfterDelay:0];
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"*** getmodel %@ **",resultDict);
                   
                    if ([notificationArry count])
                        [notificationArry removeAllObjects];
                    
                    for (id obj in [resultDict objectForKey:@"GetCycleServiceNotificationResult"]) {
                        [notificationArry addObject:obj];
                    }
                    
                    [notificationTable reloadData];
                    
                  
                    
                }}];
            
            break;
        }
      
          }

}
-(IBAction)dealerBackBtnClicked:(id)sender
{
    
[self valueChangesClicked:(UIButton *)[segmentView viewWithTag:4]];
    
    //[selectedBtn tag]-

}
-(IBAction)dealerTabBtnClicked:(id)sender{

    dealerInfoView.hidden = YES;
    dealerEventView.hidden = YES;
    dealerSpecialView.hidden = YES;
    
    switch ([sender tag])
    {
            
        case 1:{
            
            
            DAAttributedStringFormatter* formatter = [[DAAttributedStringFormatter alloc] init];
            formatter.defaultFontFamily = @"Helvetica";
            formatter.defaultColor = [UIColor darkGrayColor];
            formatter.fontFamilies = @[ @"Courier", @"Arial", @"Georgia" ];
            formatter.defaultPointSize = 15.0f;
            formatter.colors = @[ [UIColor blackColor], [UIColor redColor], [UIColor greenColor] ];
            
            
            dealerInfoView.hidden = NO;
            [self.view addSubview:dealerInfoView];
         //   UITextView *infoTextView = (UITextView *)[dealerInfoView viewWithTag:1];
            
            DAAttributedLabel *infoTextView = (DAAttributedLabel *)[dealerInfoView viewWithTag:1];

            
         //   infoTextView.text =   @"Reno's Powersports KC\n13611 Homlmes Road\nKansas City,MQ 64145\n\nContact\nPhone: (816)942-8900\nToll Free: (866)936-8900\nFax: (816)942-0208\n\nEmail: info@renospowersportskc.com\nweb: www.renospowersportskc.com";

            
           // return;
//#if 0
            [self.view.window showActivityViewWithLabel:@"Loading Dealer"];
            
          //  bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
            
            
            NSString *stringURL = [NSString stringWithFormat:@"%@%@",GetDealerByCycleId,[[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"]];
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                [self.view.window hideActivityViewWithAfterDelay:0];

                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"*** getmodel %@ **",resultDict);
                    dealerInfoArry = [resultDict objectForKey:@"GetDealerByCycleIdResult"];
                    
                    [self.view addSubview:dealerInfoView];

                    
                    NSString *textValue = @"";
                    
                    if ([dealerInfoArry count]) {
                        NSDictionary *dic = [dealerInfoArry objectAtIndex:0];
                        
                        BOOL isTakeKey = NO;
                        NSArray *keys =  [NSArray arrayWithObjects:@"About",@"Address",@"City",@"State",@"Zip",@"Phone",@"AlternatePhone",@"Fax",@"Email",@"WebSite", nil];//[dic allKeys];
//DealerLocationId
                        for (NSString *key in keys) {
                            
                            
                        NSString *value = [dic stringValueForKey:key];
                           
                            if (isTakeKey)
                                textValue = [textValue stringByAppendingFormat:@"%%B%@%%b:  %@\n",key,value];
                            else{
                                 if ([key isEqualToString:@"About"]) {
                                textValue = [textValue stringByAppendingFormat:@"%%24S%%1D%@%%d%%s\n",value];
                                 }
                                else if ([key isEqualToString:@"Address"]) {
                                    textValue = [textValue stringByAppendingFormat:@"%@\n",value];
                                }
                                else
                                    textValue = [textValue stringByAppendingFormat:@"%@, ",value];
                            }

                            
                        if ([key isEqualToString:@"Zip"]) {
                            textValue = [textValue stringByAppendingFormat:@"\n\n%%BContact%%b\n"];
                            isTakeKey = YES;
                        }
                        else  if ([key isEqualToString:@"Fax"]) {
                                textValue = [textValue stringByAppendingFormat:@"\n\n"];
                                isTakeKey = YES;
                        }
                        else  if ([key isEqualToString:@"About"]) {
                            textValue = [textValue stringByAppendingFormat:@"\n%%BAddress%%b\n"];

                        }
                         
                            }
                        
                       // UITextView *infoTextView = (UITextView *)[dealerInfoView viewWithTag:1];
                        
                        textValue = [textValue stringByReplacingOccurrencesOfString:@"AlternatePhone" withString:@"Toll Free"];
                        infoTextView.text = (id)[formatter formatString:textValue];
                        [infoTextView setPreferredHeight];

                    }
                    
                    
                }
            }];
            
//#endif
            
            
        }
            break;
        case 2:{
            dealerEventView.hidden = NO;
           //
            
            
          //  NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"",@"Name",@"",@"Description",@"",@"StartDate", nil];

            
          //  dealerEventArry = [[NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:@"Motorbike Apparel by Revo Industries - Bristol FL",@"Name",@"motorbikeapparel.png",@"image",@"06/11/2013\n06/27/2024",@"StartDate", nil],[NSDictionary dictionaryWithObjectsAndKeys:@"Biketoberfest 2014",@"Name",@"biketobefest.png",@"image",@"10/16/2014\n10/19/2014",@"StartDate", nil], nil] mutableCopy];
            
            [self.view addSubview:dealerEventView];
           // [dealerEventTable reloadData];
            
//#if 0
             [self.view.window showActivityViewWithLabel:@"Loading Events"];
            NSString *stringURL = [NSString stringWithFormat:@"%@%@",GetDealerEvent,[[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"DealerId"]];
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                [self.view.window hideActivityViewWithAfterDelay:0];

                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"*** getmodel %@ **",resultDict);
                    dealerEventArry = [resultDict objectForKey:@"GetDealerEventResult"];
                    
                    [self.view addSubview:dealerEventView];
                    [dealerEventTable reloadData];

                    
                }
            }];
//#endif
           
            
        }
            break;
        case 3:{
            dealerSpecialView.hidden = NO;
            
            //dealerSpecialArry =  [[NSArray arrayWithObjects:@"Exp: 1/16/2014\n$4.00 Off On Tickets to International Motorcycle Show",@"10% Off On Transfer of Motorcycle Insurance",nil] mutableCopy];
          
            [self.view addSubview:dealerSpecialView];
           // [dealerSpecialTable reloadData];
//#if 0
            [self.view.window showActivityViewWithLabel:@"Loading Dealer"];
            
            NSString *stringURL = [NSString stringWithFormat:@"%@%@",GetDealerSpeacialByOwnerId,[[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"OwnerId"]];
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                [self.view.window hideActivityViewWithAfterDelay:0];

                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"*** getmodel %@ **",resultDict);
                   dealerSpecialArry = [resultDict objectForKey:@"GetDealerSpeacialByOwnerIdResult"];
                    
                    [self.view addSubview:dealerSpecialView];
                    [dealerSpecialTable reloadData];

                }
            }];

            break;
        }
        case 5:{
            
            CMDirectDealerMailVC *secondDetailViewController = [[CMDirectDealerMailVC alloc] initWithNibName:@"CMDirectDealerMailVC" bundle:nil];
            secondDetailViewController.delegate = self;
            secondDetailViewController.isSendBuddy = NO;

            [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
        }
        default:
            break;
    }

}


-(IBAction)valueChangesClicked:(id)sender
{
    
    [self.view endEditing:YES];
    isFirstPopUp = NO;
    
    [dealerSpecialDetailView removeFromSuperview];
    
    serHealthReport.hidden = YES;
    serRequestService.hidden = YES;
    serNotificationService.hidden = YES;
    
    servicePopUpView.hidden = YES;

    dealerInfoView.hidden = YES;
    dealerEventView.hidden = YES;
    dealerSpecialView.hidden = YES;
    
    healthView.hidden = YES;
    //myCollapseClick.hidden = YES;
    MotorcycleView.hidden = YES;
    dealerView.hidden = YES;
    

    
    UIButton *infoBtn = (UIButton *)[segmentView viewWithTag:1];
    [infoBtn setBackgroundColor:segmentUnSelectedColor];
    UIButton *healthBtn = (UIButton *)[segmentView viewWithTag:2];
    [healthBtn setBackgroundColor:segmentUnSelectedColor];
    UIButton *serviceBtn = (UIButton *)[segmentView viewWithTag:3];
    [serviceBtn setBackgroundColor:segmentUnSelectedColor];

    UIButton *dealerBtn = (UIButton *)[segmentView viewWithTag:4];
    [dealerBtn setBackgroundColor:segmentUnSelectedColor];


    UIButton *selectedBtn = (UIButton *)sender;
    [selectedBtn setBackgroundColor:segmentSelectedColor];

    currtopTabIndex = [sender tag];
    
    NSString *titleStr ;

    switch ([selectedBtn tag]-1)
    {
        case 0:{
         //   myCollapseClick.hidden = NO;
            //            myCollapseClick.contentOffset = CGPointZero;

            MotorcycleView.hidden = NO;

            MotorcycleView.contentOffset = CGPointZero;
            
            titleStr = @"Motorcycle Information";
            break;
        }
        case 1:{
            healthView.hidden = NO;
            
                       healthViewTable.frame  = CGRectMake(0, CGRectGetMaxY(healthView->stripView.frame), 320, CGRectGetHeight(healthViewTable.frame));

            [self.view addSubview:healthView];
            [healthView addSubview:healthViewTable];

            [self setUpHealthDetails];
           
            titleStr = @"Motorcycle Health";

            //alertImageView
            break;
        }
        case 2:{
            serHealthReport.hidden = NO;
            [self.view addSubview:serHealthReport];
            titleStr = @"Motorcycle Service";
            currServiceTabIndex = 3;
            [self serviceTabBtnClicked:(UIButton *)[serRequestService viewWithTag:currServiceTabIndex]];

         //   [self serviceTabBtnClicked:0];

        }
            break;
        case 3:
            titleStr = @"Motorcycle Dealer";

            dealerView.hidden = NO;
            [self.view addSubview:dealerView];
            break;
        default:
            break;
    }
    
    [CMAppDelegate setNavigationBarTitle:titleStr navigationItem:self.navigationItem];

}




-(IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)viewWillAppear:(BOOL)animated{

    [self.navigationController.view addSubview:segmentView];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)   name:UIKeyboardWillShowNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [segmentView removeFromSuperview];
    
  ///  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedHelthIndex = -1;
    healthRequestArry = [NSMutableArray new];
    notificationArry = [NSMutableArray new];
    
    bikeImgeArr = [NSArray arrayWithObjects:@"honda595",@"Harley-davidson-softail",@"Kawasaki-zx10r",@"KTM-640-adventure",@"Yamaha-fjr-1300", nil];
    
    MotorcycleView = [[CMAppDelegate loadNibNamed:@"CMMotorcycleView" owner:self options:nil] objectAtIndex:0];

   
    currtopTabIndex = 1;
  
 
    
    // NSArray *
    firstPopupArr = [NSArray arrayWithObjects:@"Add Motorcycle",@"Delete Motorcycle",@"User Profile", nil];
    firstPopupImgArr = [NSArray arrayWithObjects:@"pop_add_motorcycle.png",@"pop_delete_motorcycle.png",@"user_profile.png", nil];
    

    // NSArray *
#if 0
    servicePopupArr = [NSArray arrayWithObjects:@"Send Service Request", @"Send Dealer Purchase", @"Request Service/Product", @"Service Notification", @"Cycle Health Certification", nil];
    
    servicePopupImgArr = [NSArray arrayWithObjects:@"send_service_request.png", @"send_dealer_purchase.png", @"request_service.png", @"service_notification.png", @"cycle_health_certification.png", nil];
    
#endif
    servicePopupArr = [NSArray arrayWithObjects:@"Send Service Request", @"Email Direct Dealer", @"Service Notification", @"Cycle Health Certification", nil];
    
    servicePopupImgArr = [NSArray arrayWithObjects:@"send_service_request.png", @"send_dealer_purchase.png", @"service_notification.png", @"cycle_health_certification.png", nil];
    

    // Do any additional setup after loading the view.
    
    ownerView = [[CMAppDelegate loadNibNamed:@"CMOwnerView" owner:self options:nil] objectAtIndex:0];

    
    driverLicenseView = [[CMAppDelegate loadNibNamed:@"CMDLView" owner:self options:nil] objectAtIndex:0];
    insuranceView = [[CMAppDelegate loadNibNamed:@"CMInsuranceView" owner:self options:nil] objectAtIndex:0];
    preferencesView = [[CMAppDelegate loadNibNamed:@"CMPreferencesView" owner:self options:nil] objectAtIndex:0];
    
    
    healthView = [[CMAppDelegate loadNibNamed:@"CMHealthView" owner:self options:nil] objectAtIndex:0];
    
     dealerView = [[CMAppDelegate loadNibNamed:@"CMBikeDealerView" owner:self options:nil] objectAtIndex:0];

    
    
    
     serHealthReport = [[CMAppDelegate loadNibNamed:@"CMSCHealthReport" owner:self options:nil] objectAtIndex:0];
    
   // CMSCRequestService *
    healthReqDetail  = [[CMAppDelegate loadNibNamed:@"CMHealthDetails" owner:self options:nil] objectAtIndex:0];

    
    healthReqDetail.contentSize = CGSizeMake(320, 490+(IS_IPHONE_5?0:88));

     serRequestService = [[CMAppDelegate loadNibNamed:@"CMSCRequestService" owner:self options:nil] objectAtIndex:0];
    serRequestService.contentSize = CGSizeMake(320,400+(IS_IPHONE_5?0:88));
    

     serNotificationService = [[CMAppDelegate loadNibNamed:@"CMSCNotificationService" owner:self options:nil] objectAtIndex:0];
    
   // MotorcycleView.frame = CGRectMake(myCollapseClick.frame.origin.x, myCollapseClick.frame.origin.y,myCollapseClick.frame.size.width,myCollapseClick.frame.size.height-(IS_IPHONE_5?0:88));
    myCollapseClick.frame = CGRectMake(myCollapseClick.frame.origin.x, myCollapseClick.frame.origin.y,myCollapseClick.frame.size.width,myCollapseClick.frame.size.height-(IS_IPHONE_5?0:88));

    MotorcycleView.frame =  myCollapseClick.frame;
    
    MotorcycleView.contentSize = CGSizeMake(320, 760+(IS_IPHONE_5?0:88));

   healthReqDetail.frame = serHealthReport.frame = serRequestService.frame =  serNotificationService.frame = healthView.frame = myCollapseClick.frame;
   //  = myCollapseClick.frame;
    
    dealerView.contentSize = dealerView.frame.size ;
    
    
     //  healthReqDetail.contentSize =
    serHealthReport.contentSize =  serNotificationService.contentSize = healthView.contentSize =  dealerView.contentSize;
    
    dealerView.frame = CGRectMake(0, 105-64,  dealerView.frame.size.width,dealerView.frame.size.height-(IS_IPHONE_5?0:88));
    
     dealerEventView  = [[CMAppDelegate loadNibNamed:@"CMDealerEventView" owner:self options:nil] objectAtIndex:0];
     dealerSpecialView  = [[CMAppDelegate loadNibNamed:@"CMDealerSpecialView" owner:self options:nil] objectAtIndex:0];
     dealerInfoView  = [[CMAppDelegate loadNibNamed:@"CMDealerInfoView" owner:self options:nil] objectAtIndex:0];
    
    
     servicePopUpView = [[CMAppDelegate loadNibNamed:@"CMServicePopUpView" owner:self options:nil] objectAtIndex:0];

    servicePopUpView.hidden = YES;

    
    dealerEventView.frame =dealerInfoView.frame= dealerSpecialView.frame = servicePopUpView.frame = dealerView.frame ;
    
    

//    myCollapseClick.CollapseClickDelegate = self;
//    [myCollapseClick reloadCollapseClick];
    
    arryList=@[@"Harley Davidson Softail",@"Ducati Monster 686",@"Yamaha Stryker",@"Can-Am Spyder ST-S",@"Ducati Diavel"];
    
    



    
    UINib *cellNib  = [UINib nibWithNibName:@"DealerEventCell" bundle:nil];
    [dealerEventTable    registerNib:cellNib forCellReuseIdentifier:@"DealerEventCell"];
    
     cellNib  = [UINib nibWithNibName:@"DealerSpecialCell" bundle:nil];
    [dealerSpecialTable    registerNib:cellNib forCellReuseIdentifier:@"DealerSpecialCell"];
    
    cellNib  = [UINib nibWithNibName:@"CMBikePopUpCell" bundle:nil];
    [popUpTable    registerNib:cellNib forCellReuseIdentifier:@"CMBikePopUpCell"];
    
    cellNib  = [UINib nibWithNibName:@"CMHealthCell" bundle:nil];
    [healthViewTable    registerNib:cellNib forCellReuseIdentifier:@"CMHealthCell"];
    
    cellNib  = [UINib nibWithNibName:@"CMHealthSelectedCell" bundle:nil];
    [healthViewTable    registerNib:cellNib forCellReuseIdentifier:@"CMHealthSelectedCell"];
    
    cellNib  = [UINib nibWithNibName:@"CMNotificationCell" bundle:nil];
    [notificationTable    registerNib:cellNib forCellReuseIdentifier:@"CMNotificationCell"];
  
    //CMNotificationCell
    
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"Motorcycle Information" navigationItem:self.navigationItem];
    
    
#if 0
    if (isFirstPopUp) {
        [self openPopUpMenu];
    }
#endif
    isFirstPopUp = NO;
    
    [self.navigationController.view addSubview:[UIAppDelegate loadBottomView:(isFromSetting?SettingBootomTabBar:(isFromHome?HomeBootomTabBar:BikerBootomTabBar))]];
    
    if (IS_IPHONE_5) {
        dealerView.scrollEnabled = serHealthReport.scrollEnabled =  serNotificationService.scrollEnabled = healthView.scrollEnabled =  dealerView.scrollEnabled = NO;

    }
    if (![bikeDataArr count]) {
        selectedMotorCyBtn.enabled = NO;
        [self getOwnerforCurrBike];
       // [self setUpDefaultPrefances];
        
    }else{
        
        [self setUpBikeDetails];
    }

    [self.view addSubview:MotorcycleView];
    
    [MotorcycleView setBackgroundColor:[UIColor whiteColor]];

}

-(IBAction)openEventCalenderView:(id)sender{
/*
    ABViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"ABViewController"];
    if (sender) {
        viewController.isEvent = YES;

    }
    else
        viewController.isEvent = NO;

    [self.navigationController pushViewController:viewController animated:YES];*/
}
-(void)openCalenderView:(NSArray *)eventsArr
{
   /* ABViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"ABViewController"];
    //
    
    [self.navigationController pushViewController:viewController animated:YES];*/
}


-(IBAction)postCyclePreferenceInfo:(id)sender{
    
    
    
    if ([bikeDataArr count]) {
        cycleId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"];
        
        
    }
    if (![cyclePreferenceId length])cyclePreferenceId = @"0";
    
    //    [ScheduledOilChanges] [int] NULL,
    //    [ScheduledMaintenance] [int] NULL,
    
    
    NSString *IsHealthIndicator = [NSString stringWithFormat:@"%d",[MotorcycleView->addHealthBtn tag]];
    
    // NSString *resolveHealthIssueID = [MotorcycleView->resolveHealthBtn selectedObjectID];
    NSString *resolveHealthStr     = [MotorcycleView->resolveHealthBtn currentTitle];
    
    
    NSString *primaryBikeId   = [MotorcycleView->primatyBikeBtn selectedObjectID];
    
    
    NSString *ScheduledOilChanges = [MotorcycleView->oilQtyField.text length]?MotorcycleView->oilQtyField.text:@"0";
    NSString *ScheduledMaintenance = [MotorcycleView->maintQtyField.text length]?MotorcycleView->maintQtyField.text:@"0";
    
    
    //[MotorcycleView->addHealthBtn tag]
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:cyclePreferenceId, @"CyclePreferenceId",ScheduledMaintenance, @"ScheduledMaintenance",
                             ScheduledOilChanges, @"ScheduledOilChanges",
                             resolveHealthStr, @"ResolveHealthIssueID",
                             IsHealthIndicator, @"IsHealthIndicator",
                             cycleId, @"CycleId",
                             primaryBikeId, @"PrimaryBikeId",
                             nil];
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateCyclePreference",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
#if 0
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
                [CMHelper alertMessageWithText:[cyclePreferenceId intValue]?@"Cycle Preference Updated Successfully":@"Cycle Preference Inserted Successfully"];
            }
            else
                [CMHelper alertMessageWithText:[cyclePreferenceId intValue]?@"Cycle Preference failed to Updated":@"Cycle Preference failed to Inserted"];
            
#endif
        }
    }];
    
    
    
}
-(IBAction)postInsuranceInfo:(id)sender{
    
    

    
    if ([bikeDataArr count]) {
        cycleId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"];
    }
    
    if (![cycleInsuranceId length])cycleInsuranceId = @"0";
    
    NSString *insuranceCarrierStr = insuranceView->nsuranceCarrier.text;
    NSString *insurancePolicyStr = insuranceView->nsurancePolicyField.text;
    NSString *insuranceExpiStr = insuranceView->nsuranceExpiField.text;
    NSString *insuranceCarPhStr = insuranceView->nsuranceCarPhField.text;
    NSString *insuranceAccPhStr = insuranceView->nsuranceAccPhField.text;
    
    

    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:insuranceView.photoPath, @"PhotoPath",
                             @"", @"Photo",
                             insuranceCarrierStr, @"Carrier",
                             insurancePolicyStr, @"PolicyNumber",
                             insuranceExpiStr, @"Expiration",
                             insuranceCarPhStr, @"CarrierPhone",
                             insuranceAccPhStr, @"AccidentPhone",
                             cycleId, @"CycleId",
                             cycleInsuranceId, @"CycleInsuranceId",
                             nil];
    
 /// [self postDataToServer:infoDic withPostURL:[NSString stringWithFormat:@"%@/InsertUpdateCycleInsurance",Service_URL]];
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateCycleInsurance",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
               
                if (sender)[CMHelper alertMessageWithText:[cycleInsuranceId intValue]?@"Cycle Insurance Updated Successfully":@"Cycle Insurance Inserted Successfully"];
                
                
                
                if (insuranceView.isNewImage ) {
                    [self postImageWithServiceType:CMInsuranceViewType];
                    
                }
            }else
                [CMHelper alertMessageWithText:[cycleInsuranceId intValue]?@"Cycle Insurance failed to Updated":@"Cycle Insurance failed to Inserted"];
            
            
        }
    }];
    

}


-(IBAction)postMotorCycleInfo:(id)sender{
    
   // MotorcycleView
    
    
    NSString *mcNameStr = MotorcycleView->mcNameField.text;
    
    if (![mcNameStr length]) {
        [CMHelper alertMessageWithText:@"Vehicle name should not be blank"];
        return;
    }
    NSString *mcTirePressureStr = [MotorcycleView->mcTirePressureField.text length]?MotorcycleView->mcTirePressureField.text:@"0";
    NSString *mcOdometerStr = [MotorcycleView->mcOdometerField.text length]?MotorcycleView->mcOdometerField.text:@"0";

    
    NSString *mcYearStr = MotorcycleView->mcYearField.text;//[ MotorcycleView->mcYearField.text length]? MotorcycleView->mcYearField.text:@"0";
    NSString *mcVINStr = MotorcycleView->mcVINField.text;//[MotorcycleView->mcVINField.text length]?MotorcycleView->mcVINField.text:@"0";
   
    
    NSString *mcTireSizeStr =   @"";// MotorcycleView->mcTireSizeField.text;//[MotorcycleView->mcTireSizeField.text length]?MotorcycleView->mcTireSizeField.text:@"0";
    
    NSString *mcDateTireInStr = @"";// MotorcycleView->mcDateTireInField.text;
    
    
    NSString *mcDatePurchaseStr = MotorcycleView->mcDatePurchaseField.text;
    NSString *mcDateOilChangeStr = MotorcycleView->mcDateOilChangeField.text;
    NSString *mcDateServiceMainStr = MotorcycleView->mcDateServiceMainField.text;

    
    NSString *cycleBrandStr     = [MotorcycleView->cycleBrandBtn selectedObjectID];
    NSString *ccycleModelStr    = [MotorcycleView->ccycleModelBtn selectedObjectID];
    NSString *ccycleCategoryStr = [MotorcycleView->ccycleCategoryBtn selectedObjectID];
    NSString *ccycleRideingStr = [MotorcycleView->ccycleRideingBtn selectedObjectID];
    
    NSString *ccycleDealerStr = [MotorcycleView->ccycleDealerBtn selectedObjectID];;// MotorcycleView->mcDealerField.text;

//ccycleDealerBtn

//OwnerId, Photo, PhotoPath, Name, BrandId, ModelId, CategoryId, Year, IdentityNumber, OdometerReading, TireSize, TireInstallDate, TirePressure, RidingStyleId, DealerId, PurchaseDate, CycleId
    
    
   // bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
  //
    

    
    if ([bikeDataArr count]) {
        cycleId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"];
        cycleOwnerId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"OwnerId"];
        //OwnerId
    }
    if (![cycleOwnerId length])  cycleOwnerId = @"0";
    if (![cycleId length])  cycleId = @"0";

    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:MotorcycleView.photoPath, @"PhotoPath",
                             @"", @"Photo",
                             mcNameStr, @"Name",
                             cycleBrandStr, @"BrandId",
                             ccycleModelStr, @"ModelId", cycleId, @"CycleId",
                             cycleOwnerId, @"OwnerId",
                             ccycleCategoryStr, @"CategoryId",
                             mcYearStr, @"Year",
                             mcVINStr, @"IdentityNumber",
                             mcOdometerStr, @"OdometerReading",
                             mcTireSizeStr, @"TireSize",
                             mcDateTireInStr, @"TireInstallDate",
                             mcTirePressureStr, @"TirePressure",
                             ccycleDealerStr, @"DealerId",  mcDatePurchaseStr, @"PurchaseDate",
                             mcDateOilChangeStr, @"LastOilChangeDate",
                             mcDateServiceMainStr, @"LastServiceDate",
                              ccycleRideingStr, @"RidingStyleId",
                             nil];
    

    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateCycle",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error)
     {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
                
                if (sender) [CMHelper alertMessageWithText:[cycleId intValue]?@"Bike Updated Successfully":@"Bike Inserted Successfully"];
                
                
                cycleId = [resultDict stringValueForKey:@"response"];

                if (![cycleId intValue]) {
                     [self setUpBikePopUp];
                }
               
                
                if (MotorcycleView.isNewImage ) {
                 [self postImageWithServiceType:CMMotorcycleViewType];
                }
                
                
            }else
                [CMHelper alertMessageWithText:[cycleId intValue]?@"Cycle failed to Updated":@"Cycle failed to Inserted"];
            
            
            
            [self postCyclePreferenceInfo:0];

        }
    }];

}

-(void)setUpBikePopUp{

    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=1",Service_URL,@"0"];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [resultDict objectForKey:@"GetCycleResult"];
            
            NSString *slectedBikeId = [[bikeDataArr lastObject] stringValueForKey:@"CycleId"];
            
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:slectedBikeId]) {
                    bikeInfoDic = anObject;
                    selectedBikeIndex = idx;
                    
                }
                
                
            }];
            
            if ([bikeDataArr count]){
                [self setUpBikeDetails];
                selectedMotorCyBtn.enabled = YES;
            }
            
            
            
            
        }
    }];

}

-(IBAction)postOwnerInfo:(id)sender{
    

    
    if ([bikeDataArr count]) {
        cycleId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"];
        cycleOwnerId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"OwnerId"];
        //OwnerId
    }
    if (![cycleOwnerId length]) cycleOwnerId=@"0";
    
    
    
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys: ownerView->mcOwnernameField.text, @"FName",ownerView->mcOwnerLastNameField.text, @"LName",
                          ownerView->addressField.text, @"Address",
                          ownerView->cityField.text, @"City",
                          ownerView->stateField.text, @"State",
                            ownerView->zipField.text, @"Zip",
                             ownerView->mobileField.text, @"Mobile",
                             ownerView->altMobileField.text, @"Phone",
                          ownerView->emailField.text, @"Email",
                         @"1", @"StyleId",
                         @"", @"DLPhoto",
                         driverLicenseView.photoPath, @"DLPhotoPath",
                         @"", @"PhotoPath",
                         @"", @"Photo",
                         @"1343hfdk28krk", @"DLNumber",
                         @"11/10/14", @"DLExpiration",
                         @"1", @"IsActive",
                         cycleOwnerId, @"OwnerId",
                         nil];

    
    //InsertUpdateOwnerUsingMobile
    //InsertUpdateOwner
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateOwnerUsingMobile",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
             if (sender)   [CMHelper alertMessageWithText:[cycleOwnerId intValue]?@"Owner Updated Successfully":@"Owner Inserted Successfully"];
                
                if (driverLicenseView.isNewImage ) {
                    [self postImageWithServiceType:CMDLViewType];
                    
                }

            }
            else
                [CMHelper alertMessageWithText:[cycleOwnerId intValue]?@"Owner failed to Updated":@"Owner failed to Inserted"];
            
        }
    }];

}



-(IBAction)postRequestService:(id)sender{
    



#if 0
    
   NSString *mcModelStr =  serRequestService->mcModelField.text;
   NSString *mcYearStr =  serRequestService->mcYearField.text ;
   NSString *mcdealerLocStr =  serRequestService->mcdealerLocField.text ;
   NSString *mcVINStr =  serRequestService->mcVINField.text ;
   NSString *mclastNameStr =  serRequestService->mclastNameField.text ;
   NSString *mcFirstNameStr =  serRequestService->mcFirstNameField.text ;
   NSString *mcPhStr =  serRequestService->mcPhField.text ;
   NSString *mcAlPhStr =  serRequestService->mcAlPhField.text ;
   NSString *mcemailStr =  serRequestService->mcemailField.text ;
#endif
   NSString *mcMemoStr =  serRequestService->mcMemoField.text ;

    NSString *mcCurrMilageStr =   [serRequestService->mcCurrMilageField.text length]?serRequestService->mcCurrMilageField.text:@"0";
    NSString *mcReqDateStr =  serRequestService->mcReqDateField.text ;
    NSString *mcOtherStr =  serRequestService->otherDesField.text ;
    

    
    NSString *serviceCategoryId = [serRequestService->serviceCategoryBnt selectedObjectID];;// MotorcycleView->mcDealerField.text;
    NSString *dealerLocId = [serRequestService->dealerLocBtn selectedObjectID];;// MotorcycleView->mcDealerField.text;

    
    
    NSString *cycleServiceId = @"0";
    
    if ([bikeDataArr count]) {
        cycleId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleId"];
        cycleServiceId =  [[bikeDataArr objectAtIndex:selectedBikeIndex] stringValueForKey:@"CycleServiceId"];
        //OwnerId
    }
    if (![cycleServiceId length])cycleServiceId = @"0";

    
    //CycleServiceId,CycleId, RequestDate, DealerLocationId, CurrentMileage, ServiceCategoryId, OtherCategory, Description, RequestFrom, IsAccepted, Reason
//
//    ownerView->mobileField.text, @"Reason",
//    ownerView->emailField.text, @"IsAccepted",
//    @"", @"Description",
//    @"", @"OtherCategory",
    
    //if user select "Other" as service category then need to  open a text box and pass this text box value in @OtherCategory
    
    
    
    if (![mcReqDateStr length]) {
        [CMHelper alertMessageWithText:@"Service Request Date should not be blank"];
        return;
    }else if (![dealerLocId integerValue]) {
        [CMHelper alertMessageWithText:@"Dealer Location should not be blank"];
        return;
    }else if (![serviceCategoryId integerValue]) {
        [CMHelper alertMessageWithText:@"Service Category should not be blank"];
        return;
    }else if ([[[serRequestService->serviceCategoryBnt currentTitle] uppercaseString] isEqualToString:@"OTHER"] && ![mcOtherStr length]) {
        [CMHelper alertMessageWithText:@"Other Category should not be blank"];
        return;
    }




    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             @"U", @"RequestFrom",
                             serviceCategoryId, @"ServiceCategoryId",
                             mcCurrMilageStr, @"CurrentMileage",
                             dealerLocId, @"DealerLocationId",
                             mcReqDateStr, @"RequestDate",
                             cycleId, @"CycleId",
                             cycleServiceId, @"CycleServiceId",
                             mcMemoStr, @"Description",
                             mcOtherStr,@"OtherCategory",
                             nil];
    
    
    
    NSLog(@"infoDic--%@",infoDic);
    
   // [self postDataToServer:infoDic withPostURL:[NSString stringWithFormat:@"%@/InsertUpdateCycleService",Service_URL]];
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateCycleService",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
                [CMHelper alertMessageWithText:[cycleServiceId intValue]?@"Request Service Date Submitted":@"Request Service Inserted Successfully"];
                
                
                NSMutableDictionary *dic = [[bikeDataArr objectAtIndex:selectedBikeIndex] mutableCopy];
                [dic setValue:mcCurrMilageStr forKey:@"OdometerReading"];
             
                [bikeDataArr replaceObjectAtIndex:selectedBikeIndex withObject:dic];
                //OdometerReading
            }
            else  if ([[resultDict stringValueForKey:@"response"] intValue]==-1) {
                [CMHelper alertMessageWithText:[cycleServiceId intValue]?@"Request Service failed":@"Request Service failed"];

            }
            
            [self serviceTabBtnClicked:(UIButton *)[serRequestService viewWithTag:1]];

        }
    }];
    

    //Go to Health Section
    
    
    
}

-(IBAction)notificationAcceptStatus:(id)sender{
    //CycleServiceId, IsAccepted, Reason
    
    NSString *cycleServiceId = [[notificationArry objectAtIndex:selectedNotiIndex] stringValueForKey:@"CycleServiceId"];
    //selectedNotiIndex
    
    //NotificationId
    NSString *isAccepted = @"";
    
    if ([sender tag]==1) {
        isAccepted = @"1";
    }else  if ([sender tag]==2) {
        isAccepted = @"0";
       
        
    }
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                            cycleServiceId, @"CycleServiceId",
                             isAccepted, @"IsAccepted",
                             @"", @"Reason",
                             nil];
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/AcceptDeclineService",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
                [CMHelper alertMessageWithText:[isAccepted intValue]?@"Notification Accepted Successfully":@"Notification Declined Successfully"];
            }
            else  if ([[resultDict stringValueForKey:@"response"] intValue]==-1) {
                [CMHelper alertMessageWithText:[isAccepted intValue]?@"Notification failed to Accepted":@"Notification failed to Declined"];
                
            }
            
        }
    }];
    
    
    [notificationArry removeObjectAtIndex:selectedNotiIndex];
    [notificationTable reloadData];
    [self backToNotificationListClicked:0];
    
    
    


}

-(void)postImageWithServiceType:(CMBikeInfoType)serviceType{
    
    UIImage *imageFile = nil;
    
    switch (serviceType) {
        case CMMotorcycleViewType:
          imageFile =  MotorcycleView.docImageView.image;
            break;
        case CMDLViewType:
            imageFile = driverLicenseView.docImageView.image ;
            
            break;
            
        case CMInsuranceViewType:
            imageFile = insuranceView.docImageView.image;
            
            break;
        default:
            break;
    }

    if (imageFile) {
        
        [[CMNetworkManager sharedInstance] postImage:ImageUploadURL  withImage:imageFile  param:nil  requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            NSString *imageUrl = @"";
            if ([resultDict count]) {
                NSDictionary *tempDic =  [(NSArray *)resultDict objectAtIndex:0];
                imageUrl = [tempDic stringValueForKey:@"image_url"];
                NSLog(@"tempDic = %@",tempDic);
            }
          
            switch (serviceType) {
                case CMMotorcycleViewType:{
                    
                    MotorcycleView.photoPath = imageUrl;
                    [self postMotorCycleInfo:0];
                    MotorcycleView.isNewImage = NO;

                }
                    break;
                case CMDLViewType:
                     driverLicenseView.photoPath = imageUrl;
                    [self postOwnerInfo:0];
                    driverLicenseView.isNewImage = NO;

                    break;
                    
                case CMInsuranceViewType:
                    insuranceView.photoPath = imageUrl;
                    [self postInsuranceInfo:0];
                    insuranceView.isNewImage = NO;

                    break;
                default:
                    break;
            }

            
            
        }];
    }

   

}

#pragma mark - WebService Implementation
-(void)postDataToServer:(NSDictionary *)withInfoDic withPostURL:(NSString *)urlStr
{
    
    [[CMNetworkManager sharedInstance] postDataResponse:urlStr param:withInfoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
           
        }
    }];
    
   
    
    
}

-(void)setUpDefaultPrefances{

   NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSString *heladId = [dic stringValueForKey:@"ResolveHealthIssueID"];
    if(!heladId)heladId=@"Dealer";
    [preferencesView->resolveHealthBtn  setTitle:heladId forState:UIControlStateNormal];
    
    // [preferencesView->resolveHealthBtn setSelectedObjectID:heladId];
    [preferencesView->primatyBikeBtn  setSelectedObjectID:[dic stringValueForKey:@"PrimaryBikeId"]];
    
    
    if ([[dic stringValueForKey:@"PrimaryBikeName"] length]) [preferencesView->primatyBikeBtn  setTitle:[dic stringValueForKey:@"PrimaryBikeName"] forState:UIControlStateNormal];
    


}

#pragma  mark - Get Service

-(void)setUpBikeColorPrefances{
    
    healthView->alertImageView.image = nil;
    healthView->healthLbl.text = @"";
    
    healthView->alerTypeImg.image =nil;
    
    
    healthView->inficatorView.hidden = NO;
    
    [ healthView->inficatorView startAnimating];
    
    bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
    
    NSString *stringURL =[NSString stringWithFormat:@"%@%@/DealerId=%@",GetCycleHealthColor,[bikeInfoDic stringValueForKey:@"CycleId"],DealerID];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic ;
            
            if ([[resultDict objectForKey:@"GetCycleHealthColorResult"] count]) dic = [[resultDict objectForKey:@"GetCycleHealthColorResult"] objectAtIndex:0];
            
            if ([[dic stringValueForKey:@"Color"] isEqualToString:@"Amber"]) {
                
                healthView->alertImageView.image = [UIImage imageNamed:@"health_amber_alert.png"];
                healthView->healthLbl.text = @"Attention Required";
                
                healthView->alerTypeImg.image = [UIImage imageNamed:@"white_icon.png"];
                
            }
            else if ([[dic stringValueForKey:@"Color"] isEqualToString:@"Red"]) {
                healthView->alertImageView.image = [UIImage imageNamed:@"health_amber_alert_red.png"];
                healthView->healthLbl.text = @"Warning: Unresolved Issue(s)";
                
                healthView->alerTypeImg.image = [UIImage imageNamed:@"stop-hand.png"];
                
                
            }
            else{
                
                healthView->alertImageView.image = [UIImage imageNamed:@"health_amber_alert_green.png"];
                healthView->healthLbl.text = @"Good Bike Health";
                
                healthView->alerTypeImg.image = [UIImage imageNamed:@"green_health.png"];
            }
            
            
            //healthView->healthLbl.text = [dic stringValueForKey:@"Description"];
        }
        
        
        healthView->inficatorView.hidden = YES;
        
        [ healthView->inficatorView stopAnimating];
    }];


}


-(IBAction)healthCheckBtnClicked:(id)sender{
    //ResolvedMethod,ResolvedBy,OdometerReading, CycleServiceId
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:healthViewTable];
    NSIndexPath *indexPath = [healthViewTable indexPathForRowAtPoint:buttonPosition];
    UITableViewCell *cell = [healthViewTable cellForRowAtIndexPath:indexPath];
    
 
    
    UIButton *resolveBtn = (UIButton *)[cell viewWithTag:4];
    UITextField *odoField = (UITextField *)[cell viewWithTag:5];
    UITextField *resolField = (UITextField *)[cell viewWithTag:6];
    
    if (![odoField.text length]) {
        [CMHelper alertMessageWithText:@"Odometer should not be blank"];
        return;
    }
//    else  if (![odoField.text length]) {
//        [CMHelper alertMessageWithText:@"Odometer should not be blank"];
//        return
//    }

    UIButton *button = (UIButton *)sender;
    
    button.selected = !button.selected;

    
    NSDictionary *tempDic =[healthRequestArry objectAtIndex:indexPath.row];
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             [tempDic stringValueForKey:@"CycleServiceId"], @"CycleServiceId",
                             resolField.text, @"ResolvedMethod",
                             [resolveBtn currentTitle], @"ResolvedBy",
                             odoField.text, @"OdometerReading",
                             nil];
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/ResolveServiceHealth",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            [healthRequestArry removeObjectAtIndex:indexPath.row];
            selectedHelthIndex = -1;

            [healthViewTable reloadData];
            
           
            [self setUpBikeColorPrefances];

        }
    }];
    

}

-(void)healthDropDown:(id)sender{
    
    selectedHealthBtn = (UIButton *)sender;
    [self.view endEditing:YES];
    
    
    arryList = [NSArray arrayWithObjects:@"Dealer",@"Myself", nil];
  //  arryIDs  = [NSArray arrayWithObjects:@"0",@"1", nil];
    Dropobj.tag = 11;
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Resolved By" withOption:arryList xy:CGPointMake(10, 150) size:CGSizeMake(300, MIN(200, arryList.count*50+50)) isMultiple:NO withTag:11];

}

//GetOwner

-(IBAction)backToHealthList:(id)sender{
    [healthReqDetail removeFromSuperview];
}
-(void)setUpHealthDetails{
    
    if (![bikeDataArr count])return;
    
   bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
    
  
    
    [self setUpBikeColorPrefances];

    //GetCycleServiceLog
    
    if (currtopTabIndex==3) {
        NSString *stringURL  =[NSString stringWithFormat:@"%@%@",GetCycleServiceLog,[bikeInfoDic stringValueForKey:@"CycleId"]];
        
        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"***  %@ **",resultDict);
                
                if ([healthRequestArry count]) {
                    [healthRequestArry removeAllObjects];
                }
                for (id obj in [resultDict objectForKey:@"GetCycleServiceLogResult"]) {
                    
                    
                    [healthRequestArry addObject:obj];
                }
#if 0
                if (![healthRequestArry count]) {
                    healthView->alertImageView.image = [UIImage imageNamed:@"health_amber_alert_green.png"];
                    healthView->healthLbl.text = @"Good Bike Health";
                    
                    healthView->alerTypeImg.image = [UIImage imageNamed:@"green_health.png"];
                }
#endif
                
                [healthViewTable reloadData];
                
                
            }
        }];

    }
    
    else{
    
     NSString *stringURL  =[NSString stringWithFormat:@"%@%@",GetCycleServiceList,[bikeInfoDic stringValueForKey:@"CycleId"]];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            if ([healthRequestArry count]) {
                [healthRequestArry removeAllObjects];
            }
            if (currtopTabIndex==3) {
                for (id obj in [resultDict objectForKey:@"GetCycleServiceListResult"]) {
                    
                    
                    [healthRequestArry addObject:obj];
                }
                
            }else
            {
                for (id obj in [resultDict objectForKey:@"GetCycleServiceListResult"]) {
                    if ([[obj stringValueForKey:@"IsShowOnReport"] isEqualToString:@"0"]) {
                        [healthRequestArry addObject:obj];

                    }
                    
                }
                
            
            }
            
#if 0
            if (![healthRequestArry count]) {
                healthView->alertImageView.image = [UIImage imageNamed:@"health_amber_alert_green.png"];
                healthView->healthLbl.text = @"Good Bike Health";
                
                healthView->alerTypeImg.image = [UIImage imageNamed:@"green_health.png"];
            }
#endif
           
            [healthViewTable reloadData];
            
            
        }
    }];
        
    }
}


-(void)setUpHealthListDetails:(NSDictionary *)resultDict{
    [self.view addSubview:healthReqDetail];


    healthReqDetail->mcYearField.text = [resultDict stringValueForKey:@"Year"];
    healthReqDetail->mcReqDateField.text = [resultDict stringValueForKey:@"RequestDate"];
    healthReqDetail->mcdealerLocField.text = [resultDict stringValueForKey:@"DealerLocation"];
    healthReqDetail->mcVINField.text = [resultDict stringValueForKey:@"VIN"];
    healthReqDetail->mclastNameField.text = [resultDict stringValueForKey:@""];
    healthReqDetail->mcFirstNameField.text = [resultDict stringValueForKey:@"Name"];
    healthReqDetail->mcPhField.text = [resultDict stringValueForKey:@"Phone"];
    healthReqDetail->mcAlPhField.text = [resultDict stringValueForKey:@"AlternatePhone"];
    healthReqDetail->mcemailField.text = [resultDict stringValueForKey:@"Email"];
    healthReqDetail->mcMemoField.text = [resultDict stringValueForKey:@""];
    
    healthReqDetail->mcCurrMilageField.text = [resultDict stringValueForKey:@"CurrentMileage"];
    
}

-(void)setUpBikeDetails{

    [self getBikeInfoForCurrBike];
    [self getOwnerforCurrBike];
    [self getCyclePreferenceForCurrBike];
    
    bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
    serRequestService.delerId = [bikeInfoDic stringValueForKey:@"DealerId"];
}

-(void)getOwnerforCurrBike{
    
    bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
    cycleOwnerId = [bikeInfoDic stringValueForKey:@"OwnerId"];
    
    if (![cycleOwnerId length]) {
        cycleOwnerId = @"0";
    }
    NSString *stringURL =[NSString stringWithFormat:@"%@%@/DealerId=%@",GetOwner,cycleOwnerId,DealerID];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic;
          
            if ([[resultDict objectForKey:@"GetOwnerResult"] count]){
                dic = [[resultDict objectForKey:@"GetOwnerResult"] objectAtIndex:0];
                
                ownerInfoDic = [[resultDict objectForKey:@"GetOwnerResult"] objectAtIndex:0];
                cycleOwnerId = [ownerInfoDic stringValueForKey:@"OwnerId"];
            }
           
            
            ownerView->mcOwnerLastNameField.text = [dic stringValueForKey:@"LName"] ;
            ownerView->mcOwnernameField.text = [dic stringValueForKey:@"FName"];
            ownerView->addressField.text = [dic stringValueForKey:@"Address"];
            ownerView->cityField.text = [dic stringValueForKey:@"City"];
            ownerView->stateField.text = [dic stringValueForKey:@"State"];
            ownerView->mobileField.text = [dic stringValueForKey:@"Mobile"];
            ownerView->emailField.text = [dic stringValueForKey:@"Email"];
            ownerView->altMobileField.text = [dic stringValueForKey:@"Phone"];

            ownerView->zipField.text = [dic stringValueForKey:@"Zip"];
            
            
            
            if ([[dic stringValueForKey:@"DLPhotoPath"] length]) {
                NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"DLPhotoPath"]];
                [driverLicenseView.docImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"products1.jpg"]];
            }
            

        }
    }];
    
}


-(void)getCycleInsuranceForCurrBike{
    
    bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
    
    NSString *stringURL =[NSString stringWithFormat:@"%@%@/CycleId=%@",GetCycleInsurance,@"0",[bikeInfoDic stringValueForKey:@"CycleId"]];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic ;//= [[resultDict objectForKey:@"GetCycleInsuranceResult"] objectAtIndex:0];
            if ([[resultDict objectForKey:@"GetCycleInsuranceResult"] count]) dic = [[resultDict objectForKey:@"GetCycleInsuranceResult"] objectAtIndex:0];
            
            cycleInsuranceId = [dic stringValueForKey:@"CycleInsuranceId"];
            
            
            insuranceView->nsuranceCarrier.text = [dic stringValueForKey:@"Carrier"];
            insuranceView->nsurancePolicyField.text = [dic stringValueForKey:@"PolicyNumber"];
            insuranceView->nsuranceExpiField.text = [[[dic stringValueForKey:@"Expiration"] componentsSeparatedByString:@" "] objectAtIndex:0];

            insuranceView->nsuranceCarPhField.text = [dic stringValueForKey:@"Carrier"];
            insuranceView->nsuranceAccPhField.text = [dic stringValueForKey:@"AccidentPhone"];
            
            
            if ([[dic stringValueForKey:@"PhotoPath"] length]) {
                NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[dic stringValueForKey:@"PhotoPath"]];
                [insuranceView.docImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"insurance_card.png"]];
            }
        }
    }];
    
}
-(void)getCyclePreferenceForCurrBike{
    
    bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
    
    NSString *stringURL =[NSString stringWithFormat:@"%@%@",GetCyclePreference,[bikeInfoDic stringValueForKey:@"CycleId"]];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic = nil;
            if ([[resultDict objectForKey:@"GetCyclePreferenceResult"] count]){
                dic = [[resultDict objectForKey:@"GetCyclePreferenceResult"] objectAtIndex:0];
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
                [UIAppDelegate.userDefault setObject:data forKey:CyclePreferenceKey];
            }
            else
                dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
            
            
            cyclePreferenceId = [dic stringValueForKey:@"CyclePreferenceId"];
            
            MotorcycleView->oilQtyField.text = [dic stringValueForKey:@"ScheduledOilChanges"];
            MotorcycleView->maintQtyField.text = [dic stringValueForKey:@"ScheduledMaintenance"];
            
            //    [MotorcycleView->ccycleDealerBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"DealerId"]];
            //@"Dealer",@"Myself"
            
            NSString *heladId = [dic stringValueForKey:@"ResolveHealthIssueID"];
            if(!heladId)heladId=@"Dealer";
            
            [MotorcycleView->resolveHealthBtn  setTitle:heladId forState:UIControlStateNormal];
            
            // [MotorcycleView->resolveHealthBtn setSelectedObjectID:heladId];
            [MotorcycleView->primatyBikeBtn  setSelectedObjectID:[dic stringValueForKey:@"PrimaryBikeId"]];
            
            
            [MotorcycleView->primatyBikeBtn  setTitle:[dic stringValueForKey:@"PrimaryBikeName"] forState:UIControlStateNormal];
            
            
            if ([[dic stringValueForKey:@"IsHealthIndicator"] isEqualToString:@"1"] )
                [MotorcycleView->addHealthBtn setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            else
                [MotorcycleView->addHealthBtn setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            
        }
        
    }];
    
}

-(void)getBikeInfoForCurrBike{
    
    bikeInfoDic = [bikeDataArr objectAtIndex:selectedBikeIndex];
    
    
    if ([[bikeInfoDic stringValueForKey:@"PhotoPath"] length]) {
        NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[bikeInfoDic stringValueForKey:@"PhotoPath"]];
           [MotorcycleView.docImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"products1.jpg"]];
    }
    
   // MotorcycleView.docImageView.image =  [UIImage imageNamed:[bikeImgeArr objectAtIndex:selectedBikeIndex%[bikeImgeArr count]]];
    
     // [MotorcycleView.docImageView.image setImageWithURL: placeholderImage:[UIImage imageNamed:@"profile-image-placeholder"]];


    [selectedMotorCyBtn setTitle:[bikeInfoDic stringValueForKey:@"Name"] forState:UIControlStateNormal];

    
    MotorcycleView->mcNameField.text = [bikeInfoDic stringValueForKey:@"Name"];
    MotorcycleView->mcYearField.text = [bikeInfoDic stringValueForKey:@"Year"];
    MotorcycleView->mcVINField.text  = [bikeInfoDic stringValueForKey:@"IdentityNumber"];
    MotorcycleView->mcOdometerField.text = [bikeInfoDic stringValueForKey:@"OdometerReading"];
    
    
    
    MotorcycleView->mcTireSizeField.text = [bikeInfoDic stringValueForKey:@"TireSize"];
    MotorcycleView->mcTirePressureField.text = [bikeInfoDic stringValueForKey:@"TirePressure"];
    
    
    MotorcycleView->mcDatePurchaseField.text = [[[bikeInfoDic stringValueForKey:@"PurchaseDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
    MotorcycleView->mcDateTireInField.text = [[[bikeInfoDic stringValueForKey:@"TireInstallDate"]componentsSeparatedByString:@" "] objectAtIndex:0];;
    MotorcycleView->mcDateOilChangeField.text = [[[bikeInfoDic stringValueForKey:@"LastOilChangeDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
    MotorcycleView->mcDateServiceMainField.text = [[[bikeInfoDic stringValueForKey:@"LastServiceDate"] componentsSeparatedByString:@" "] objectAtIndex:0];

    
    if ([[bikeInfoDic stringValueForKey:@"DealerName"] length]) [MotorcycleView->ccycleDealerBtn setTitle:[bikeInfoDic stringValueForKey:@"DealerName"] forState:UIControlStateNormal];
    
     if ([[bikeInfoDic stringValueForKey:@"BrandName"] length])
    [MotorcycleView->cycleBrandBtn setTitle:[bikeInfoDic stringValueForKey:@"BrandName"] forState:UIControlStateNormal];
    
     if ([[bikeInfoDic stringValueForKey:@"ModelName"] length])
    [MotorcycleView->ccycleModelBtn setTitle:[bikeInfoDic stringValueForKey:@"ModelName"] forState:UIControlStateNormal];
    
     if ([[bikeInfoDic stringValueForKey:@"CategoryName"] length])
    [MotorcycleView->ccycleCategoryBtn setTitle:[bikeInfoDic stringValueForKey:@"CategoryName"] forState:UIControlStateNormal];
    
     if ([[bikeInfoDic stringValueForKey:@"RideType"] length])
     [MotorcycleView->ccycleRideingBtn setTitle:[bikeInfoDic stringValueForKey:@"RideType"] forState:UIControlStateNormal];
    
       //RideType
    
    [MotorcycleView->ccycleDealerBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"DealerId"]];
    [MotorcycleView->cycleBrandBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"BrandId"]];
    [MotorcycleView->ccycleModelBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"ModelId"]];
    [MotorcycleView->ccycleCategoryBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"CategoryId"]];
    [MotorcycleView->ccycleRideingBtn setSelectedObjectID:[bikeInfoDic stringValueForKey:@"RidingStyleId"]];
    
    
  
    
}
#pragma mark - Calls Form Home
-(void)openCycleInoTab{
    //[self.navigationController.view addSubview:segmentView];
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSDictionary *notifyDic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleNotificationKey]];
    
    
    
    NSString *primaryBikeId = [notifyDic stringValueForKey:@"ServiceNotificationCycleId"];
    
    
    //NSString *
    
    if ([primaryBikeId length]|| ([primaryBikeId intValue]==0))  primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=1",Service_URL,@"0"];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [resultDict objectForKey:@"GetCycleResult"];
            
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                    bikeInfoDic = anObject;
                    selectedBikeIndex = idx;
                    
                }
                
                
            }];
            
            if ([bikeDataArr count]){
                [self setUpBikeDetails];
                selectedMotorCyBtn.enabled = YES;
                [self serviceTabBtnClicked:0];
            }
            
            
            
            
        }
    }];
    
    
    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];
    
    [self serviceTabBtnClicked:0];
    
    
    
}


-(void)openServiceTab{
    //[self.navigationController.view addSubview:segmentView];
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSDictionary *notifyDic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleNotificationKey]];
    
    

    NSString *primaryBikeId = [notifyDic stringValueForKey:@"ServiceNotificationCycleId"];
    
    
    //NSString *
    
    if ([primaryBikeId length]|| ([primaryBikeId intValue]==0))  primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=1",Service_URL,@"0"];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [resultDict objectForKey:@"GetCycleResult"];
            
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                    bikeInfoDic = anObject;
                    selectedBikeIndex = idx;

                }
                
                
            }];
            
            if ([bikeDataArr count]){
                [self setUpBikeDetails];
                selectedMotorCyBtn.enabled = YES;
                [self serviceTabBtnClicked:0];
            }

            
            
       
        }
    }];
    
    
    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];
    
    [self serviceTabBtnClicked:0];
    
    
    
}

-(void)openHealthTab{
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=1",Service_URL,@"0"];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [resultDict objectForKey:@"GetCycleResult"];
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                    bikeInfoDic = anObject;
                    selectedBikeIndex = idx;
                    
                }
                
                
            }];

            
            if ([bikeDataArr count]){
                selectedMotorCyBtn.enabled = YES;
                [self setUpBikeDetails];
                [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:2]];
            }
            

            
          
        }
    }];
    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:2]];
    
    
}
-(void)openEventDealerTab{
    CMUIButton *btn = (  CMUIButton *)[dealerView viewWithTag:2];
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=%@/OwnerId=1",Service_URL,@"0"];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [resultDict objectForKey:@"GetCycleResult"];
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                    bikeInfoDic = anObject;
                    selectedBikeIndex = idx;
                    
                }
                
                
            }];
            
            if ([bikeDataArr count]){
                selectedMotorCyBtn.enabled = YES;
                [self setUpBikeDetails];
                [self dealerTabBtnClicked:btn];

            }
            
    }
    }];
    
    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:4]];
    [self dealerTabBtnClicked:btn];
}
-(void)openSpecialDealerTab{
    
    
    CMUIButton *btn = (  CMUIButton *)[dealerView viewWithTag:3];
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
    
    NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,@"1"];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            bikeDataArr = [resultDict objectForKey:@"GetCycleResult"];
            [bikeDataArr enumerateObjectsUsingBlock:^(id anObject, NSUInteger idx, BOOL *stop) {
                
                if ([[anObject stringValueForKey:@"CycleId"] isEqualToString:primaryBikeId]) {
                    bikeInfoDic = anObject;
                    selectedBikeIndex = idx;
                    
                }
                
                
            }];
            
            if ([bikeDataArr count]){
                selectedMotorCyBtn.enabled = YES;
                [self setUpBikeDetails];
                [self dealerTabBtnClicked:btn];
                
            }
            
            
        }
    }];
    
    
    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:4]];
    
    
    
    [self dealerTabBtnClicked:btn];
}


#pragma mark - Collapse Click Delegate

// Required Methods
-(int)numberOfCellsForCollapseClick {
    return 5;
}

-(NSString *)titleForCollapseClickAtIndex:(int)index {

        switch (index) {
        case 0:
            return @"Motorcycle";
            break;
        case 1:
            return @"Owner";
            break;
        case 2:
                return @"Registration";
               // return @"Driver's License";
            break;
            
        case 3:
            return @"Insurance";
            break;
            
        case 4:
            return @"Preferences";
        default:
            return @"";
            break;
    }
}

-(UIView *)viewForCollapseClickContentViewAtIndex:(int)index {
    
//    
//    MotorcycleView;
//    ownerView;
//    driverLicenseView;
//    insuranceView;
//    preferencesView;
    switch (index) {
        case 0:
            return MotorcycleView;
            break;
        case 1:
            return ownerView;
            break;
        case 2:
            return driverLicenseView;
            break;
        case 3:
            return insuranceView;
            break;
        case 4:
            return preferencesView;
            break;
            
        default:
            return nil;
            break;
    }
}


// Optional Methods

-(UIColor *)colorForCollapseClickTitleViewAtIndex:(int)index {
    
   return [UIColor colorWithRed:202.0f/255.0f green:217.0f/255.0f blue:222.0f/255.0f alpha:1.0];

    switch (index) {
        case 0:
            return [UIColor colorWithRed:44/255.0f green:202/255.0f blue:192/255.0f alpha:1.0];
            break;
        case 1:
            return [UIColor colorWithRed:248/255.0f green:78/255.0f blue:65/255.0f alpha:1.0];
            break;
        case 2:
            return [UIColor colorWithRed:44/255.0f green:202/255.0f blue:192/255.0f alpha:1.0];
            break;
            
        default:
            return [UIColor colorWithRed:223/255.0f green:47/255.0f blue:51/255.0f alpha:1.0];
            break;
    }
    
}


-(UIColor *)colorForTitleLabelAtIndex:(int)index {
    return  [UIColor colorWithRed:0.0f/255.0f green:98.0f/255.0f blue:114.0f/255.0f alpha:0.90];
    //[UIColor colorWithWhite:1.0 alpha:0.85];
}

-(UIColor *)colorForTitleArrowAtIndex:(int)index {
    return [UIColor colorWithWhite:0.0 alpha:0.25];
}

-(void)didClickCollapseClickCellAtIndex:(int)index isNowOpen:(BOOL)open {
    NSLog(@"%d and it's open:%@", index, (open ? @"YES" : @"NO"));
}


#pragma mark - UITextView Delegate for

- (void)textViewDidBeginEditing:(UITextView *)textView
{
  
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        // Be sure to test for equality using the "isEqualToString" message
        [textView resignFirstResponder];
        
        // Return FALSE so that the final '\n' character doesn't get added
        return FALSE;
    }

    return YES;
}


#pragma mark - TextField Delegate for

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
 

    if ([[textField textFieldType] isEqualToString:@"Number"]) {

    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber* candidateNumber;
    
    NSString* candidateString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    range = NSMakeRange(0, [candidateString length]);
    
    [numberFormatter getObjectValue:&candidateNumber forString:candidateString range:&range error:nil];
    
    if (([candidateString length] > 0) && (candidateNumber == nil || range.length < [candidateString length])) {
        
        return NO;
    }
    else
    {
        return YES;
    }
        
    }

    
    return YES;

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    

    if ([[textField textFieldType] isEqualToString:@"Date"]) {
        [self.view endEditing:YES];
        [self datePickerViewClicked:nil];
        selectedField = textField;
        
        svos = myCollapseClick.contentOffset;
        CGPoint pt;
        CGRect rc = [textField bounds];
        rc = [textField convertRect:rc toView:myCollapseClick];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= 60;
        [myCollapseClick setContentOffset:pt animated:YES];
        return NO;

    }
    
//    if ([[textField textFieldType] isEqualToString:@"Number"]){
//        [doneButton setHidden:NO];
//    }
//    else
//        [doneButton setHidden:YES];

   return YES;
}
- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
 
}




//implementation
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    svos = myCollapseClick.contentOffset;
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:myCollapseClick];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [myCollapseClick setContentOffset:pt animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [myCollapseClick setContentOffset:svos animated:YES];
    [textField resignFirstResponder];
    
 
    
    return YES;
}
#pragma mark - IQActionSheet DatePicker


-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    
    [selectedField setText:[titles componentsJoinedByString:@" - "]];
#if 0
    switch (pickerView.tag)
    {
        case 1: [buttonSingle setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 2: [buttonDouble setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 3: [buttonTriple setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 4: [buttonRange setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 5: [buttonTripleSize setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
        case 6: [buttonDate setTitle:[titles componentsJoinedByString:@" - "] forState:UIControlStateNormal]; break;
            
        default:
            break;
    }
#endif
}

- (IBAction)datePickerViewClicked:(UIButton *)sender
{
    //IQActionSheetPickerView *
    
    if (!picker) picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    [picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}
#pragma mark - Collapse Click Delegate

- (IBAction)DropDownSingle:(id)sender {
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Motorcycle" withOption:bikeDataArr xy:CGPointMake(10, 84) size:CGSizeMake(300, MIN(200, bikeDataArr.count*50+50)) isMultiple:NO withTag:111];
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple withTag:(int)tag{

    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    
    Dropobj.tag = tag;

    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    
    if (dropdownListView.tag ==11) {
        
        
        [selectedHealthBtn setTitle:[arryList objectAtIndex:anIndex] forState:UIControlStateNormal];

    }
    else{
    
        bikeInfoDic =  [bikeDataArr objectAtIndex:anIndex];

        
        /*----------------Get Selected Value[Single selection]-----------------*/
        [selectedMotorCyBtn setTitle:[[bikeDataArr objectAtIndex:anIndex] stringValueForKey:@"Name"] forState:UIControlStateNormal];
        
        selectedBikeIndex = anIndex;
        
        selectedHelthIndex = -1;
        [self setUpBikeDetails];
        
        if (currtopTabIndex==2) {
            [self setUpHealthDetails];
        }
        else if (currtopTabIndex==3) {
            //  [self setUpHealthDetails];
            
            [self serviceTabBtnClicked:(UIButton *)[serRequestService viewWithTag:currServiceTabIndex]];

            
//            [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];
//            [self setUpServiceRequest];
        }

    }
    
    
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        
        
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}










-(IBAction)showUIImagePicker:(id)sender{
    
    curType = (int)[sender tag];
    //curType = [sender status];
    if([sender tag]== kCHOOSEPICTURE_From_Library) {
        IstoCamera = NO;
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
      
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
   if([sender tag]== kCHOOSEPICTURE_Take_Photo) {
       // @try {
            IstoCamera = YES;
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType=IMAGESOURSETYPE;
            
            
           // [self.navigationController pushViewController:imagePicker animated:YES];

            [self.navigationController presentViewController:imagePicker animated:YES completion:NULL];

//        }
//        @catch (NSException * e) {
//            UIAlertView *alertmsg=[[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Couldn’t locate the camera, check your settings and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertmsg show];
//        }
//        @finally {
//        }
    }
}
#pragma mark -
#pragma mark CMDirectDealerMailVCDelegate Methods

- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController;
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate Methods


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
        switch (curType) {
            case CMMotorcycleViewType:
                MotorcycleView.isNewImage = YES;
               
                MotorcycleView.docImageView.image =image;
                break;
            case CMDLViewType:
                driverLicenseView.docImageView.image  =image;
                driverLicenseView.isNewImage = YES;
                break;
                
            case CMInsuranceViewType:
                insuranceView.docImageView.image  =image;
                insuranceView.isNewImage = YES;

                break;
            default:
                break;
        }
//        if (picker.sourceType == IMAGESOURSETYPE) {
//            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
//        }
        
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        
        
        
    }
}

- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    button.selected = !button.selected;
}
#pragma mark -
#pragma mark UITableView Method Implementation Call


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (tableView==popUpTable) {
        return 40;
    }
    else if (tableView==notificationTable) {
        return 60;
    }
    else if (tableView==healthViewTable) {
        
        if (indexPath.row==selectedHelthIndex) {
            return 150;

        }
        return 60;

    }

        

    
    return 80;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==popUpTable) {
        return  isFirstPopUp?firstPopupArr.count:servicePopupArr.count;
        
    }
    else  if (tableView==dealerEventTable)
       return dealerEventArry.count;
    else  if (tableView==healthViewTable)
        return healthRequestArry.count;
    else  if (tableView==notificationTable)
        return notificationArry.count;
   else
    return dealerSpecialArry.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (tableView==popUpTable){
        cell = [tableView dequeueReusableCellWithIdentifier:@"CMBikePopUpCell"];
     
        UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
        UILabel *labeName = (UILabel *)[cell viewWithTag:2];
        
        
        
        
        
        labeName.text  =  [isFirstPopUp?firstPopupArr:servicePopupArr objectAtIndex:indexPath.row];
        
        placeholderImage.image =  [UIImage imageNamed:[isFirstPopUp?firstPopupImgArr:servicePopupImgArr objectAtIndex:indexPath.row]];
        return  cell;

    }
    
    else if (tableView==dealerEventTable){
        cell = [tableView dequeueReusableCellWithIdentifier:@"DealerEventCell"];

        UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
        UILabel *labeName = (UILabel *)[cell viewWithTag:2];
        UILabel *reviewText = (UILabel *)[cell viewWithTag:3];
        UILabel *timeLable = (UILabel *)[cell viewWithTag:4];
        
        
        
        NSDictionary *tempDic =[dealerEventArry objectAtIndex:indexPath.row];
        labeName.text  =  [tempDic stringValueForKey:@"Name"];
        reviewText.text =  [tempDic stringValueForKey:@"Description"];
        timeLable.text =   [NSString stringWithFormat:@"%@\n%@",[[[tempDic stringValueForKey:@"StartDate"] componentsSeparatedByString:@" "] objectAtIndex:0],[[[tempDic stringValueForKey:@"EndDate"] componentsSeparatedByString:@" "] objectAtIndex:0]];
        
        [placeholderImage setImage:[UIImage imageNamed:indexPath.row%2?@"motorbikeapparel.png":@"biketobefest.png"]];
        UIButton *button = (UIButton *)[cell viewWithTag:11];
        
        [button setImage:NormalImage forState:UIControlStateNormal];
        [button setImage:SelectedImage forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        return  cell;

    }
    
    else if (tableView==healthViewTable){
        
        
        
        if (indexPath.row==selectedHelthIndex) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"CMHealthSelectedCell"];
            
            UILabel *labeName = (UILabel *)[cell viewWithTag:1];
            UILabel *reviewText = (UILabel *)[cell viewWithTag:2];
            UILabel *timeLable = (UILabel *)[cell viewWithTag:3];
            
            UIButton *resolveBtn = (UIButton *)[cell viewWithTag:4];
            UITextField *odoField = (UITextField *)[cell viewWithTag:5];
            UITextField *resolField = (UITextField *)[cell viewWithTag:6];
            
            
            UILabel *resolveLbl = (UILabel *)[cell viewWithTag:7];

            UIButton *checkBoxBtn = (UIButton *)[cell viewWithTag:8];

            [checkBoxBtn setImage:NormalImage forState:UIControlStateNormal];
            [checkBoxBtn setImage:SelectedImage forState:UIControlStateSelected];
            [checkBoxBtn addTarget:self action:@selector(healthCheckBtnClicked:) forControlEvents:UIControlEventTouchUpInside];

            
            resolveLbl.text = [NSString stringWithFormat:@"Resolved:\n%@",[UIAppDelegate.dateFormatter   stringFromDate:[NSDate date]]];

            [resolveBtn addTarget:self action:@selector(healthDropDown:) forControlEvents:UIControlEventTouchUpInside];
            
            
            

            NSDictionary *tempDic =[healthRequestArry objectAtIndex:indexPath.row];
            
            
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CyclePreferenceKey]];
            NSString *heladId = [dic stringValueForKey:@"ResolveHealthIssueID"];
            if(!heladId || [[heladId uppercaseString] isEqualToString:[@"Resolve Health Issues" uppercaseString]] )heladId=@"Dealer";
            [resolveBtn  setTitle:heladId forState:UIControlStateNormal];

            
            
            labeName.text  =      [tempDic stringValueForKey:@"ServiceCategory"];
            reviewText.text =   [tempDic stringValueForKey:@"Status"];
          //  timeLable.text =   [[[tempDic stringValueForKey:@"CreatedOn"] componentsSeparatedByString:@" "] objectAtIndex:0];
            
            NSString *requestDate = [tempDic stringValueForKey:@"RequestDate"];
            NSLog(@"requestDate ==>%@",requestDate);
            
            if (requestDate==(id) [NSNull null] || [requestDate length]==0 || [requestDate isEqualToString:@"(null)"]||[requestDate isKindOfClass:[NSNull class]])
            {
                timeLable.text = @" ";
            }
            else
            {
                NSString *newString1 = [requestDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
                NSLog(@"newString1 ==>%@",newString1);
                NSArray *listItems = [newString1 componentsSeparatedByString:@"+"];
                NSString* dayString = [listItems objectAtIndex: 0];
                double timeStamp = [dayString doubleValue];
                NSTimeInterval timeInterval=timeStamp/1000;
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                [dateformatter setDateFormat:@"MM-dd-yyyy"];
                timeLable.text=[dateformatter stringFromDate:date];
            }
            
            odoField.text  =   [bikeInfoDic stringValueForKey:@"OdometerReading"]; //  [tempDic stringValueForKey:@"CurrentMileage"];
            //|| [[reviewText.text uppercaseString] isEqualToString:@"REJECTED"]
            if (currtopTabIndex==3 ) {
                resolveBtn.enabled = NO;
               odoField.enabled = NO;
             resolField.enabled = NO;
                checkBoxBtn.hidden = YES;
            }else{
                resolveBtn.enabled = YES;
                odoField.enabled = YES;
                resolField.enabled = YES;
                checkBoxBtn.enabled = YES;
                checkBoxBtn.hidden = NO;
            }

            
  
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"CMHealthCell"];
            
            UILabel *labeName = (UILabel *)[cell viewWithTag:1];
            UILabel *reviewText = (UILabel *)[cell viewWithTag:2];
            UILabel *timeLable = (UILabel *)[cell viewWithTag:3];
            NSDictionary *tempDic =[healthRequestArry objectAtIndex:indexPath.row];
            
            
            labeName.text  =      [tempDic stringValueForKey:@"ServiceCategory"];
            reviewText.text =   [tempDic stringValueForKey:@"Status"];
          //  timeLable.text =   [[[tempDic stringValueForKey:@"CreatedOn"] componentsSeparatedByString:@" "] objectAtIndex:0];
            
            NSString *requestDate = [tempDic stringValueForKey:@"RequestDate"];
            NSLog(@"requestDate ==>%@",requestDate);
            
            if (requestDate==(id) [NSNull null] || [requestDate length]==0 || [requestDate isEqualToString:@"(null)"]||[requestDate isKindOfClass:[NSNull class]])
            {
                timeLable.text = @" ";
            }
            else
            {
                NSString *newString1 = [requestDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
                NSLog(@"newString1 ==>%@",newString1);
                NSArray *listItems = [newString1 componentsSeparatedByString:@"+"];
                NSString* dayString = [listItems objectAtIndex: 0];
                double timeStamp = [dayString doubleValue];
                NSTimeInterval timeInterval=timeStamp/1000;
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
                [dateformatter setDateFormat:@"MM-dd-yyyy"];
                timeLable.text=[dateformatter stringFromDate:date];
            }
            
        }

        
      
        return cell;
    }
    else if (tableView==notificationTable){
        
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"CMNotificationCell"];
        
        UILabel *labeName = (UILabel *)[cell viewWithTag:1];
        UILabel *timeLable = (UILabel *)[cell viewWithTag:2];
       
        NSDictionary *tempDic =[notificationArry objectAtIndex:indexPath.row];
        
        
        labeName.text  =   [tempDic stringValueForKey:@"ServiceCategory"];// [NSString stringWithFormat:@"%@-%@\n%@",[tempDic stringValueForKey:@"ServiceCategory"],[tempDic stringValueForKey:@"DealerName"],[tempDic stringValueForKey:@"DealerLocation"]];//  [tempDic stringValueForKey:@"Description"];
        timeLable.text   =   [[[tempDic stringValueForKey:@"ServiceDate"] componentsSeparatedByString:@" "] objectAtIndex:0];
        
        return cell;
    }

    cell = [tableView dequeueReusableCellWithIdentifier:@"DealerSpecialCell"];

    
    UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    UILabel *labeName = (UILabel *)[cell viewWithTag:2];
    UILabel *reviewText = (UILabel *)[cell viewWithTag:3];
    UILabel *timeLable = (UILabel *)[cell viewWithTag:4];
    
    
    UIButton *button = (UIButton *)[cell viewWithTag:11];
    
    [button setImage:NormalImage forState:UIControlStateNormal];
    [button setImage:SelectedImage forState:UIControlStateSelected];
    [button addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
           NSDictionary *tempDic =[dealerSpecialArry objectAtIndex:indexPath.row];
    
    
    
    
    
   labeName.text  =      [tempDic stringValueForKey:@"VaildTill"];
    reviewText.text =   [tempDic stringValueForKey:@"Name"];
    timeLable.text =   [tempDic stringValueForKey:@"Description"];
    
    placeholderImage.image =  [UIImage imageNamed:indexPath.row?@"10%off.png":@"4%off.png"];
    
    //[placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringForKeyFromJson:@"user_photo"]] placeholderImage:[UIImage imageNamed:@"photo_place_holder.png"]];
    
   // labeName.text = [dealerSpecialArry objectAtIndex:indexPath.row];
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==popUpTable) {
        if (isFirstPopUp) {
            switch (indexPath.row) {
                case 0:
                    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:1]];

                    break;
                case 1:
                    
                    break;
                case 2:
                    
                    break;
                default:
                    break;
            }
        }
        
       else {
            switch (indexPath.row) {
                case 0:
                    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];
                    break;
                case 1:
                    [UIAppDelegate topNavMessageClicked:0];
                    isFirstPopUp = NO;
                    servicePopUpView.hidden = YES;

                    break;
                case 2:
                    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];
                    
                    
                case 3:
                    [self valueChangesClicked:(UIButton *)[segmentView viewWithTag:3]];
                    
                    break;
                default:
                    break;
            }
        }
//
  //         servicePopupArr = [NSArray arrayWithObjects:@"Send Service Request", @"Email Direct Dealer", @"Service Notification", @"Cycle Health Certification", nil];
    }
    else  if (tableView==dealerSpecialTable){
   
    
        if (!dealerSpecialDetailView)dealerSpecialDetailView = [[CMAppDelegate loadNibNamed:@"CMDealerSpecialDetailView" owner:self options:nil] objectAtIndex:0];
        
        dealerSpecialDetailView.frame = dealerEventView.frame;
        [self.view addSubview:dealerSpecialDetailView];
        
        UIView *subView  = ( UIView *)[dealerSpecialDetailView viewWithTag:101];
        subView.layer.borderWidth = 2;
        subView.layer.cornerRadius = 4;
        subView.layer.borderColor =  [UIColor lightGrayColor].CGColor;
        
        
        UIImageView *placeholderImage = (UIImageView *)[subView viewWithTag:1];
        UILabel *labeName = (UILabel *)[subView viewWithTag:2];
        UILabel *reviewText = (UILabel *)[subView viewWithTag:3];

        placeholderImage.image =  [UIImage imageNamed:indexPath.row?@"10%off.png":@"4%off.png"];
        labeName.text = [[dealerSpecialArry objectAtIndex:indexPath.row] stringValueForKey:@"Description"];
        reviewText.text = [NSString stringWithFormat:@"Promo Code: GX%dAD%d",arc4random()%9,arc4random()%99];
        //101
    }
    else  if (tableView==healthViewTable){
        
        if (currtopTabIndex==2) {

        if (selectedHelthIndex != indexPath.row) {
            
   
        if (selectedHelthIndex==-1) {
            selectedHelthIndex = indexPath.row;

            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0 ]] withRowAnimation:UITableViewRowAnimationFade];

        }
        else{
            
            int midIndex = selectedHelthIndex;
            selectedHelthIndex = indexPath.row;

            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:midIndex inSection:0 ],[NSIndexPath indexPathForRow:selectedHelthIndex inSection:0 ],nil] withRowAnimation:UITableViewRowAnimationFade];
        }

        
             }
        }

       // [self setUpHealthListDetails:[healthRequestArry objectAtIndex:indexPath.row]];
    }
      else if (tableView==notificationTable){
          
           [serNotificationService->notifyBottomView setHidden:YES];
          selectedNotiIndex = indexPath.row;
          NSDictionary  *dic  = [notificationArry objectAtIndex:indexPath.row];
          UITextView *infoTextView = (UITextView *)[serNotificationService viewWithTag:111];
          infoTextView.hidden = NO;

          // [NSString stringWithFormat:@"%@\n%@",[tempDic stringValueForKey:@"DealerName"],[tempDic stringValueForKey:@"DealerLocation"]]
          infoTextView.text = [NSString stringWithFormat:@"%@\n\n%@\n%@\n\n\nYou are scheduled for your service on:\n%@\n",[dic stringValueForKey:@"ServiceCategory"],[dic stringValueForKey:@"DealerName"],[dic stringValueForKey:@"DealerLocation"],[[[dic stringValueForKey:@"ServiceDate"] componentsSeparatedByString:@" "] objectAtIndex:0]];
          
      }
    //setUpHealthListDetails
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)keyboardWillShow:(NSNotification *)note {
    // create custom button
    //  UIButton *
    
    if (!doneButton) doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton.frame = CGRectMake(0, 163, 106, 53);
    doneButton.adjustsImageWhenHighlighted = NO;
    //    [doneButton setImage:[UIImage imageNamed:@"doneButtonNormal.png"] forState:UIControlStateNormal];
    //    [doneButton setImage:[UIImage imageNamed:@"doneButtonPressed.png"] forState:UIControlStateHighlighted];
    
    [doneButton setTitle:@"Done"  forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIView *keyboardView = [[[[[UIApplication sharedApplication] windows] lastObject] subviews] firstObject];
            [doneButton setFrame:CGRectMake(0, keyboardView.frame.size.height - 53, 106, 53)];
            [keyboardView addSubview:doneButton];
            [keyboardView bringSubviewToFront:doneButton];
            
            [UIView animateWithDuration:[[note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]-.02
                                  delay:.0
                                options:[[note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue]
                             animations:^{
                                 self.view.frame = CGRectOffset(self.view.frame, 0, 0);
                             } completion:nil];
        });
    }else {
        // locate keyboard view
        dispatch_async(dispatch_get_main_queue(), ^{
            UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
            UIView* keyboard;
            for(int i=0; i<[tempWindow.subviews count]; i++) {
                keyboard = [tempWindow.subviews objectAtIndex:i];
                // keyboard view found; add the custom button to it
                if([[keyboard description] hasPrefix:@"UIKeyboard"] == YES)
                    [keyboard addSubview:doneButton];
            }
        });
    }
    
    
}

@end
