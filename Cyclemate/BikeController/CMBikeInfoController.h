//
//  CMBikeInfoController.h
//  Cyclemate
//
//  Created by Rajesh on 9/24/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

//mm/dd/yyyy

#import "UIViewController+MJPopupViewController.h"
#import "IQActionSheetPickerView.h"


#import <UIKit/UIKit.h>
#import "CollapseClick.h"
#import "CMMotorcycleView.h"
#import "CMOwnerView.h"
#import "CMDLView.h"
#import "CMInsuranceView.h"
#import "CMPreferencesView.h"
#import "DropDownListView.h"

#import "CMHealthView.h"
#import "CMBikeDealerView.h"


#import "CMSCHealthReport.h"
#import "CMSCRequestService.h"
#import "CMSCNotificationService.h"


#import "CMDealerEventView.h"
#import  "CMDealerSpecialView.h"
#import "CMDealerInfoView.h"

#import "CMServicePopUpView.h"

#import "CMDealerSpecialDetailView.h"

@interface CMBikeInfoController : UIViewController<CollapseClickDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,kDropDownListViewDelegate>{

    
    UITextField *selectedField;
    __weak IBOutlet CollapseClick *myCollapseClick;

    ///View for Motorcycle info section
   CMMotorcycleView * MotorcycleView;
   CMOwnerView *ownerView;
   CMDLView* driverLicenseView;
   CMInsuranceView * insuranceView;
   CMPreferencesView *  preferencesView;
    ///View for Motorcycle info section

    
    //CMHealthDetails.xib
    
    CMHealthView *  healthView;
    
    CMBikeDealerView *  dealerView;
    
    CMSCRequestService *healthReqDetail;

    
    CMSCHealthReport *serHealthReport;
     CMSCRequestService *serRequestService;
    CMSCNotificationService *serNotificationService;
 
    CGPoint svos;

    IBOutlet UITextField *  mcName;
    IBOutlet UISegmentedControl *segmentedControl;

    CMBikeInfoType curType;

    
    
    
    IBOutlet UITableView *dealerSpecialTable;
    IBOutlet UITableView *dealerEventTable;
    IBOutlet UITableView *popUpTable;
    
    IBOutlet UITableView *healthViewTable;

    
    IBOutlet UITableView *notificationTable;

    
    CMDealerEventView *dealerEventView;
    CMDealerSpecialView *dealerSpecialView;
    CMDealerInfoView *dealerInfoView;
    
    
    CMServicePopUpView *servicePopUpView;
    
    
    CMDealerSpecialDetailView *dealerSpecialDetailView;
    
    IBOutlet UIView *segmentView;
    
    BOOL isFirstPopUp;
    
    NSMutableDictionary *bikeInfoDic;
    NSMutableArray *bikeDataArr;
    int selectedBikeIndex;
    
    int currtopTabIndex;
    int currServiceTabIndex;
    
    NSMutableDictionary *ownerInfoDic;
    
    IQActionSheetPickerView *picker;
    
    NSString *cycleInsuranceId;
    NSString *cyclePreferenceId;
    
    NSString *cycleOwnerId;
    NSString *cycleId;
    
    
    int selectedNotiIndex;
    int selectedHelthIndex;
    
    UIButton *doneButton;
//    UIView *keyboardView;
}
@property(nonatomic)int selectedBikeIndex;
@property(nonatomic,retain) NSMutableArray *bikeDataArr;
@property(nonatomic,retain)NSMutableDictionary *bikeInfoDic;

@property(nonatomic,retain)IBOutlet UISegmentedControl *segmentedControl;

-(IBAction)bottomViewBtnClicked:(id)sender;
-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)topMenuBtnClicked:(id)sender;
- (IBAction)DropDownSingle:(id)sender;

-(IBAction)showUIImagePicker:(id)sender;


-(IBAction)dealerTabBtnClicked:(id)sender;

-(IBAction)dealerBackBtnClicked:(id)sender;

-(IBAction)serviceTabBtnClicked:(id)sender;




-(void)openPopUpMenu;

-(void)openHealthTab;
-(void)openEventDealerTab;
-(void)openSpecialDealerTab;
-(void)openServiceTab;

-(IBAction)healthDropDown:(id)sender;
-(IBAction)healthCheckBtnClicked:(id)sender;


@property(nonatomic)BOOL isFromSetting;


@property(nonatomic)BOOL isFromHome;
@property(nonatomic)BOOL isFirstPopUp;

@end
