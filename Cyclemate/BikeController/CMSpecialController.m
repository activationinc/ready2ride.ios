//
//  CMSpecialController.m
//  Cyclemate
//
//  Created by Rajesh on 12/12/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMSpecialController.h"
#import "CMSpecialDetailController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAILogger.h"
#import "GAIFields.h"
#import "GAITrackedViewController.h"
#import "GAITracker.h"
@interface CMSpecialController (){
    
    
    UIRefreshControl *refreshControl;
    
}

@end

@implementation CMSpecialController

-(void)markPromotionAsRead:(NSString *)withPromotionId
{
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             withPromotionId, @"PromotionId",nil];
    
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/UpdatePromotion",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:@"UA-80658429-4"];
    [self.tracker set:kGAIScreenName value:@"Specials"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    @try
    {
        [dealerSpecialTable reloadData];
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"Specials"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }
    
    
     [self loadingDealerSpecial];
    
    
}
-(void)markAsReadSpecial{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             ownerId, @"OwnerId",DealerID, @"DealerId",nil];
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/MarkIsReadSpecial",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [dealerSpecialTable setEditing:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self markAsReadSpecial];
    
    dealerSpecialArry = [NSMutableArray new];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    
    
    [CMAppDelegate setNavigationBarTitle:@"Specials" navigationItem:self.navigationItem];
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(loadingDealerSpecial) forControlEvents:UIControlEventValueChanged];
    [dealerSpecialTable addSubview:refreshControl];
    
    
}

- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    button.selected = !button.selected;
}
-(void)loadingDealerSpecial{
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    // [self.view.window showActivityViewWithLabel:@"Loading Special"];
    [SVProgressHUD showWithStatus:@"Loading Special" maskType:SVProgressHUDMaskTypeGradient];
    //PromotionId=%@
    NSString *stringURL =  [NSString stringWithFormat:@"%@/GetPromotion/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];
        [SVProgressHUD dismiss];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** GetPromotionResult %@ **",resultDict);
            //  dealerSpecialArry = [resultDict objectForKey:@"GetDealerSpeacialByOwnerIdResult"];
            
            if ([dealerSpecialArry count]) {
                [dealerSpecialArry removeAllObjects];
            }
            for (id obj in [resultDict objectForKey:@"GetPromotionResult"]) {
                
                [dealerSpecialArry addObject:obj];
                
            }
            
            [dealerSpecialTable reloadData];
            
            
            
            if (![self.couponCode isEqual: @""]){
                for (int i = 0; i < [dealerSpecialArry count]; i++)
                {
                    
                    if  ([[dealerSpecialArry objectAtIndex:i] objectForKey:@"PromotionId"] != nil)
                        
                        if ([[[dealerSpecialArry objectAtIndex:i] objectForKey:@"PromotionId"] intValue]  == 2044){
                            [self pushView:i];
                            self.couponCode = @"";
                             break;
                        }
                }
            }
            
          
            
        }
    }];
    
}



#pragma mark -
#pragma mark UITableView Method Implementation Call
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return   UITableViewCellEditingStyleDelete;
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        
        [[UIAlertView alertViewWithTitle:@"Alert"
                                 message:@"Do you want to Delete this Offer?" cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"]
                               onDismiss:^(int buttonIndex) {
                                   
                                   NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                                   
                                   
                                   
                                   NSDictionary *tempDic = [dealerSpecialArry objectAtIndex:indexPath.row];
                                   NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[tempDic stringValueForKey:@"PromotionId"],@"PromotionId",ownerId,@"OwnerId", nil];
                                   
                                   
                                   [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeletePromotion",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                       if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                           NSLog(@"*** getbrands %@ **",resultDict);
                                           
                                           [dealerSpecialArry removeObjectAtIndex:indexPath.row];
                                           [tableView reloadData];
                                       }
                                   }];
                                   
                               } onCancel:^{
                                   
                                   
                               }] show];
        
        
        
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dealerSpecialArry.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    /* old code 28/9/2017
     UITableViewCell *cell;
     
     
     cell = [tableView dequeueReusableCellWithIdentifier:@"DealerSpecialCell"];
     
     
     UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
     UILabel *labeName = (UILabel *)[cell viewWithTag:2];
     UILabel *reviewText = (UILabel *)[cell viewWithTag:3];
     UILabel *timeLable = (UILabel *)[cell viewWithTag:4];
     
     
     
     
     NSDictionary *tempDic =[dealerSpecialArry objectAtIndex:indexPath.row];
     
     
     
     UIImageView *isreadImage = (UIImageView *)[cell viewWithTag:5];
     
     isreadImage.hidden = [[tempDic stringValueForKey:@"IsRead"] isEqualToString:@"False"]?NO:YES;
     
     
     
     
     labeName.text  =    [tempDic stringValueForKey:@"Title"]  ;
     reviewText.text =  [tempDic stringValueForKey:@"Description"] ;
     
     if ([[tempDic stringValueForKey:@"ExpDate"] length]) timeLable.text = [NSString stringWithFormat:@"Exp: %@",[MCFDate dateChangeDateFormat:[tempDic stringValueForKey:@"ExpDate"] toFormat:@"EEE MM/dd/yyyy" currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"]];//[tempDic stringValueForKey:@"VaildTill"] ;
     else
     timeLable.text = @"";
     
     [placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringValueForKey:@"PromotionImagePath"]] placeholderImage:[UIImage imageNamed:@"special_Placeholder.png"]];
     
     
     //[placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringForKeyFromJson:@"user_photo"]] placeholderImage:[UIImage imageNamed:@"photo_place_holder.png"]];
     
     // labeName.text = [dealerSpecialArry objectAtIndex:indexPath.row];
     return  cell;
     */
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"DealerSpecialCell"];
    
    
    
    UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    UILabel *labeName = (UILabel *)[cell viewWithTag:2];
    UILabel *reviewText = (UILabel *)[cell viewWithTag:3];
    UILabel *timeLable = (UILabel *)[cell viewWithTag:4];
    UIImageView *redeemedPlaceholderImage = (UIImageView *)[cell viewWithTag:6];
   
    NSDictionary *tempDic =[dealerSpecialArry objectAtIndex:indexPath.row];
    NSString * strIsCoupon =  [tempDic stringValueForKey:@"IsCoupon"];
    NSLog(@"strIsCoupon ==>%@",strIsCoupon);
    if ([strIsCoupon  isEqualToString:@"1"])
    {
        UIImageView *isreadImage = (UIImageView *)[cell viewWithTag:5];
        isreadImage.hidden = [[tempDic stringValueForKey:@"IsRead"] isEqualToString:@"False"]?NO:YES;
        
       
        labeName.text  =  [NSString stringWithFormat:@"Coupon code: %@",[tempDic stringValueForKey:@"CouponCode"]];
        reviewText.text =  [tempDic stringValueForKey:@"Description"] ;
        
        NSString *requestDate = [tempDic stringValueForKey:@"CouponExpiryDate"];
        NSString *newString1 = [requestDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
        NSLog(@"newString1 ==>%@",newString1);
        NSArray *listItems = [newString1 componentsSeparatedByString:@"+"];
        NSString* dayString = [listItems objectAtIndex: 0];
        double timeStamp = [dayString doubleValue];
        NSTimeInterval timeInterval=timeStamp/1000;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"MM/dd/yyyy"];
        NSString *strDateExpire=[dateformatter stringFromDate:date];
        timeLable.text = [NSString stringWithFormat:@"Exp: %@",strDateExpire];
        
        [placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringValueForKey:@"CouponURL"]] placeholderImage:[UIImage imageNamed:@"special_Placeholder.png"]];
        
        
        NSString * strIsRedeemed =  [tempDic stringValueForKey:@"IsRedeemed"];
       
        if ([strIsRedeemed  isEqualToString:@"1"])
        {
            redeemedPlaceholderImage.hidden = false;
            [redeemedPlaceholderImage setImageWithURL:[NSURL URLWithString:@"https://ready2rideblob.blob.core.windows.net/coupons/Redeemed.png"] placeholderImage:[UIImage imageNamed:@""]];
        }
        else
        {
            redeemedPlaceholderImage.hidden = true;
        }
        
    }
    else
    {
        UIImageView *isreadImage = (UIImageView *)[cell viewWithTag:5];
        isreadImage.hidden = [[tempDic stringValueForKey:@"IsRead"] isEqualToString:@"False"]?NO:YES;
        labeName.text  =    [tempDic stringValueForKey:@"Title"]  ;
        reviewText.text =  [tempDic stringValueForKey:@"Description"] ;
        
        if ([[tempDic stringValueForKey:@"ExpDate"] length]) timeLable.text = [NSString stringWithFormat:@"Exp: %@",[MCFDate dateChangeDateFormat:[tempDic stringValueForKey:@"ExpDate"] toFormat:@"MM/dd/yyyy" currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"]];//[tempDic stringValueForKey:@"VaildTill"] ;
        else
            timeLable.text = @"";
        
//        if ([[tempDic stringValueForKey:@"ExpDate"] length]) timeLable.text = [NSString stringWithFormat:@"Exp: %@",[MCFDate dateChangeDateFormat:[tempDic stringValueForKey:@"ExpDate"] toFormat:@"EEE MM/dd/yyyy" currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"]];//[tempDic stringValueForKey:@"VaildTill"] ;
//        else
//            timeLable.text = @"";
        [placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringValueForKey:@"PromotionImagePath"]] placeholderImage:[UIImage imageNamed:@"special_Placeholder.png"]];
        
        //[placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringForKeyFromJson:@"user_photo"]] placeholderImage:[UIImage imageNamed:@"photo_place_holder.png"]];
        
        // labeName.text = [dealerSpecialArry objectAtIndex:indexPath.row];
    }
    return  cell;
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self pushView:[indexPath row]];
    
}

-(void)pushView:(NSInteger)row{
    NSMutableDictionary *dic = [[dealerSpecialArry objectAtIndex:row] mutableCopy];
    [dic setObject:@"True" forKey:@"IsRead"];
    [dealerSpecialArry replaceObjectAtIndex:row withObject:dic];
    
    [self markPromotionAsRead:[dic stringValueForKey:@"PromotionId"]];
    
    CMSpecialDetailController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialDetailController"];
    viewController.specialDic = [dealerSpecialArry objectAtIndex:row];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
