//
//  CMAddEventNotesVC.m
//  Cyclemate
//
//  Created by Rajesh on 4/22/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//



#import "CMAddEventNotesVC.h"
#import "UIViewController+MJPopupViewController.h"

@interface CMAddEventNotesVC ()
{
    IBOutlet UITextView *inputTextView;
    
    IBOutlet UIButton *submitBtn;
    
    IBOutlet UILabel *titleLbl;
    
    
}
@end

@implementation CMAddEventNotesVC
@synthesize delegate;
@synthesize controller;
@synthesize massegeText;
@synthesize titleText;

- (IBAction)closePopup:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
    else
        [controller dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}
-(void)closePopUpWithStatus:(BOOL)withSuccess{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked: withSend: messageText:)]) {
        [self.delegate cancelButtonClicked:self withSend:withSuccess messageText:inputTextView.text];
    }
    else
        [controller dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    
    
}

-(IBAction)postNotesOnEvent:(id)sender{
    
   // Oops: Make sure you enter a note
    if (![inputTextView.text length]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                            message:@"Please fill in the in Note field."
                                                           delegate:Nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    [self closePopUpWithStatus:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    inputTextView.layer.borderWidth = 2;
    inputTextView.layer.cornerRadius = 4;
    inputTextView.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    
    //    self.view.layer.borderWidth = 2;
    //     self.view.layer.cornerRadius = 8;
    //         self.view.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    //
    //    sendBuddyView.layer.borderWidth = 2;
    //    sendBuddyView.layer.cornerRadius = 8;
    //    sendBuddyView.layer.borderColor =  [UIColor lightGrayColor].CGColor;
    

    
    
    
    
}
#pragma mark
#pragma mark Delegate methods


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
