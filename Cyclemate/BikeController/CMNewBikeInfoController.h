//
//  CMNewBikeInfoViewController.h
//  Cyclemate
//
//  Created by Rajesh on 12/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMMotorcycleView.h"
#import "CMSCRequestService.h"
#import "CMHealthView.h"

#import "KxMenu.h"

#import "UIImageView+AFNetworking.h"
#import "UIViewController+MJPopupViewController.h"
#import "IQActionSheetPickerView.h"
#import "CMDirectDealerMailVC.h"

#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "GAITrackedViewController.h"

@interface CMNewBikeInfoController : GAITrackedViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,kDropDownListViewDelegate,DirectDealerMailDelegate,IQActionSheetPickerViewDelegate,DirectDealerMailDelegate,UIAlertViewDelegate>
{
    
    int healthIndex;
    
    IBOutlet UIImageView *alertImageView;
    IBOutlet UILabel *healthLbl;
    IBOutlet UIImageView *alerTypeImg;
    
    IBOutlet UILabel *bikeNameLbl;
    IBOutlet UIButton *healthStatusBtn;
    IBOutlet UIButton *serviceStatusBtn;
    IBOutlet UIButton *bikeStatusBtn;
    
    
    IBOutlet UIActivityIndicatorView *inficatorView;
    
    
   CMMotorcycleView * motorcycleView;
    NSString *cyclePreferenceId;
    
    NSString *cycleOwnerId;
    NSString *cycleId;
    NSMutableDictionary *ownerInfoDic;
    
    
    CMSCRequestService *serRequestService;
    
    CMHealthView *healthView;
    IBOutlet UITableView *healthViewTable;
    
    NSMutableArray *healthRequestArry;
    
    int selectedHelthIndex;
    
    IQActionSheetPickerView *picker;

    UITextField *selectedField;
    NSArray *arryList;
    DropDownListView * Dropobj;
    UIButton *selectedHealthBtn;
    
    CMBikeInfoType curType;

    IBOutlet TPKeyboardAvoidingScrollView *bottomView;

    IBOutlet UIView *mainView;
    IBOutlet UIView *topSegmentView;

    BOOL isComeFromService;
}


@property(nonatomic,retain) NSMutableArray *bikeDataArr;
@property(nonatomic,retain)NSMutableDictionary *bikeInfoDic;
@property(nonatomic, assign)int selectedBikeIndex;
@property(nonatomic, assign)int messageFlg;
@property(nonatomic)BOOL isNewBike;


-(IBAction)topSegmentBtnClicked:(id)sender;


-(IBAction)serviceBtnClicked:(id)sender;

-(IBAction)healthReportBtnClicked:(id)sender;

-(IBAction)bikeInfoBtnClicked:(id)sender;

-(IBAction)postMotorCycleInfo:(id)sender;

-(IBAction)showUIImagePicker:(id)sender;

-(void)openCycleInoTab;

@property(nonatomic)BOOL isFromSetting;


@property(nonatomic)BOOL isFromHome;
@end
