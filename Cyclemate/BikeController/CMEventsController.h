//
//  CMEventsController.h
//  Cyclemate
//
//  Created by Rajesh on 12/12/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
@interface CMEventsController : GAITrackedViewController
{

    NSMutableArray *dealerEventArry;
    
    IBOutlet UITableView *dealerEventTable;

}
-(IBAction)topSegementBtnClicked:(id)sender;

@end
