//
//  CMEventsController.m
//  Cyclemate
//
//  Created by Rajesh on 12/12/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMEventsController.h"
#import "CMEventsDetailController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAILogger.h"
#import "GAIFields.h"
#import "GAITrackedViewController.h"
#import "GAITracker.h"

@interface CMEventsController (){
    
    IBOutlet UIButton *viewAllBtn;
    IBOutlet UIButton *acceptedAllBtn;
    IBOutlet UIButton *maybeBtn;
    int eventIndex;
    
    NSMutableArray *allEventsArr;
    
    
    
    UIRefreshControl *refreshControl;

}
@end

@implementation CMEventsController

-(IBAction)topSegementBtnClicked:(id)sender{

    if (eventIndex==[sender tag]) {
        return;
    }
    viewAllBtn.backgroundColor = acceptedAllBtn.backgroundColor = maybeBtn.backgroundColor = DarkBlurColor;
    
    UIButton *selectesBtn = (UIButton *)sender;
    selectesBtn.backgroundColor = LightBlurColor;
    
    eventIndex = [sender tag];
    
    if ([dealerEventArry count]) [dealerEventArry removeAllObjects];
   
    
    
    switch (eventIndex) {
        case 1:{
            
            for (id obj in allEventsArr)
            {
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss aaa"];
                NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
                
                NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
                
                NSString *dateStr=[obj stringValueForKey:@"ExpDate"];
                NSDateFormatter * formatter=[[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss aaa"];
                NSDate *date=[formatter dateFromString:dateStr];
                NSString *expDate=[formatter stringFromDate:date];
                
                NSLog(@"currentDate -- %@",currentDate);
                NSLog(@"expDate -- %@",expDate);
                if ([[dateFormatter stringFromDate:[NSDate date]] compare:[formatter stringFromDate:date]]  == NSOrderedDescending)
                {
                    
                }
                else
                {
                    [dealerEventArry addObject:obj];
                }            }
            
            
        }    break;
        case 2:{
            
            for (id obj in allEventsArr) {
                
                 if ([[obj stringValueForKey:@"Status"] isEqualToString:@"A"])
                   [dealerEventArry addObject:obj];
            }
            
        }    break;
        case 3:{
            
            for (id obj in allEventsArr) {
                
                if ([[obj stringValueForKey:@"Status"] isEqualToString:@"M"])
                [dealerEventArry addObject:obj];
            }
            
        }    break;
            
        default:
            break;
    }
    [dealerEventTable reloadData];
}

-(void)markEventAsRead:(NSString *)withEventId{
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             withEventId, @"EventId",nil];
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/UpdateEvent",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self getDealerEvent];


}


- (void)viewWillDisappear:(BOOL)animated{
    [dealerEventTable setEditing:NO];
    
    
}

-(void)markAsRead{
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             ownerId, @"OwnerId",nil];
    
    
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertIsReadEvents",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            
            
        }
    }];
    
    
    
    
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    eventIndex = 1;
    [self markAsRead];
    
    dealerEventArry = [NSMutableArray new];
    allEventsArr = [NSMutableArray new];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];

    [CMAppDelegate setNavigationBarTitle:@"Events" navigationItem:self.navigationItem];
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(getDealerEvent) forControlEvents:UIControlEventValueChanged];
    [dealerEventTable addSubview:refreshControl];
    
    @try
    {
        
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"Events"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }
    
}
-(void)viewDidAppear:(BOOL)animated
{
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:@"UA-80658429-4"];
    [self.tracker set:kGAIScreenName value:@"Events"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
   

}
- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    button.selected = !button.selected;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getDealerEvent{
//GetEvent

    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    [self.view.window showActivityViewWithLabel:@"Loading Events"];
 
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetEvent/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];
   
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];

        [self.view.window hideActivityViewWithAfterDelay:0];
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getmodel %@ **",resultDict);
          //  dealerEventArry = [resultDict objectForKey:@"GetDealerEventResult"];
            if ([dealerEventArry count])[dealerEventArry removeAllObjects];
            if ([allEventsArr count])[allEventsArr removeAllObjects];
            
            
            
            for (id obj in [resultDict objectForKey:@"GetEventResult"]) {
                
                if (![[obj stringValueForKey:@"Status"] isEqualToString:@"DD"]) {
                    [allEventsArr addObject:obj];
                    
                    
                    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss aaa"];
                    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
                    
                    NSString *currentDate=[dateFormatter stringFromDate:[NSDate date]];
                 
                    NSString *dateStr=[obj stringValueForKey:@"ExpDate"];
                    NSDateFormatter * formatter=[[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"MM/dd/yyyy hh:mm:ss aaa"];
                    NSDate *date=[formatter dateFromString:dateStr];
                    NSString *expDate=[formatter stringFromDate:date];
                   
                    NSLog(@"currentDate -- %@",currentDate);
                    NSLog(@"expDate -- %@",expDate);
                    if ([[dateFormatter stringFromDate:[NSDate date]] compare:[formatter stringFromDate:date]]  == NSOrderedDescending)
                    {
                        
                    }
                    else
                    {
                           [dealerEventArry addObject:obj];
                    }
                    
                }
              
            }
            
            [dealerEventTable reloadData];
            
            
        }
    }];
}

#pragma mark -
#pragma mark UITableView Method Implementation Call

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return   UITableViewCellEditingStyleDelete;
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [[UIAlertView alertViewWithTitle:@"Alert"
                                 message:@"Do you want to Delete this Event?" cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"]
                               onDismiss:^(int buttonIndex) {
                                   
                                   NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                                 
                                   
                                   NSDictionary *tempDic = [dealerEventArry objectAtIndex:indexPath.row];
                                   NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[tempDic stringValueForKey:@"EventId"],@"EventId",  ownerId,@"OwnerId", nil];
                                   
                                   
                                   [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteEvent",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                       if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                           NSLog(@"*** getbrands %@ **",resultDict);
                                           
                                           [dealerEventArry removeObjectAtIndex:indexPath.row];
                                           [tableView reloadData];
                                       }
                                   }];
                                   

                                   
                                 
         
         } onCancel:^{
             
             
         }] show];
        
      
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return .1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 75;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dealerEventArry.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
 
    
        cell = [tableView dequeueReusableCellWithIdentifier:@"DealerEventCell"];
        
        UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
        UILabel *labeName = (UILabel *)[cell viewWithTag:2];
        UILabel *reviewText = (UILabel *)[cell viewWithTag:3];
        UILabel *timeLable = (UILabel *)[cell viewWithTag:4];
    
   UIImageView *statusImage = (UIImageView *)[cell viewWithTag:5];

    NSDictionary *tempDic =[dealerEventArry objectAtIndex:indexPath.row];

//    UIImageView *isreadImage = (UIImageView *)[cell viewWithTag:6];
//    isreadImage.hidden = [[tempDic stringValueForKey:@"IsRead"] isEqualToString:@"False"]?NO:YES;
    
        //IsRead
        
        labeName.text  =  [tempDic stringValueForKey:@"EventName"];
        reviewText.text =  [tempDic stringValueForKey:@"EventDescription"];
   // timeLable.text =   [NSString stringWithFormat:@"%@ %@",[[[tempDic stringValueForKey:@"StartDate"] componentsSeparatedByString:@" "] objectAtIndex:0],[[[tempDic stringValueForKey:@"EndDate"] componentsSeparatedByString:@" "] objectAtIndex:0]];
    timeLable.text =   [NSString stringWithFormat:@"%@ to %@", [MCFDate dateChangeDateFormat:[tempDic stringValueForKey:@"StartDate"] toFormat:@"EEE MM/dd/yyyy" currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"], [MCFDate dateChangeDateFormat:[tempDic stringValueForKey:@"ExpDate"] toFormat:@"EEE MM/dd/yyyy"currentDateFormat:@"MM/dd/yyyy hh:mm:ss a"]];
    

        
        [placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringValueForKey:@"EventImagePath"]] placeholderImage:[UIImage imageNamed:@"event_Placeholder.png"]];
    
    
    //event-calendar.png
    
    if ([[tempDic stringValueForKey:@"Status"] isEqualToString:@"A"]) {
        statusImage.image = [UIImage imageNamed:@"events_accepted.png"];
    }
    else if ([[tempDic stringValueForKey:@"Status"] isEqualToString:@"M"]) {
        statusImage.image = [UIImage imageNamed:@"events_maybe.png"];
    }
    else if ([[tempDic stringValueForKey:@"IsRead"] isEqualToString:@"True"]) {
        statusImage.image = nil;
    }
    else  if (![[tempDic stringValueForKey:@"IsRead"] length] || [[tempDic stringValueForKey:@"IsRead"] isEqualToString:@"False"])
        statusImage.image = [UIImage imageNamed:@"events_dealer.png"];
    else  if (![[tempDic stringValueForKey:@"Status"] length])
        statusImage.image = [UIImage imageNamed:@"events_dealer.png"];
    else
        statusImage.image = nil;
    
    //IsRead = True;


    statusImage.contentMode = UIViewContentModeScaleAspectFit;
    
    
    //EventImagePath
        return  cell;
        
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSMutableDictionary *dic = [[dealerEventArry objectAtIndex:indexPath.row] mutableCopy];
    [dic setObject:@"True" forKey:@"IsRead"];
    [dealerEventArry replaceObjectAtIndex:indexPath.row withObject:dic];
    [self markEventAsRead:[[dealerEventArry objectAtIndex:indexPath.row] stringValueForKey:@"EventId"]];

    CMEventsDetailController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMEventsDetailController"];
    viewController.eventDic = [dealerEventArry objectAtIndex:indexPath.row];

    [self.navigationController pushViewController:viewController animated:YES];


}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
