//
//  CMSpecialDetailController.h
//  Cyclemate
//
//  Created by Rajesh on 12/31/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMSpecialDetailController : UIViewController
{
    IBOutlet UIButton *serviceBtn;
    IBOutlet UIButton *redeemBtn;
}
@property(nonatomic,retain)NSDictionary *specialDic;
@property(nonatomic)BOOL isFromWeather;
@property(nonatomic,retain)IBOutlet UIButton *redeemBtn;

-(IBAction)specialListClicked:(id)sender;
-(IBAction)serviceRequestButtonClick:(id)sender;
-(IBAction)redeemButtonClick:(id)sender;
@end
