//
//  CMEventsDetailController.m
//  Cyclemate
//
//  Created by Rajesh on 12/31/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//



#import "CMAddEventNotesVC.h"
#import "CMCustomAnnotation.h"
#import "CMRideNotesDetailVC.h"

#import "CMEventsDetailController.h"
#import "CMPhotoNotesVC.h"

@interface CMEventsDetailController ()
{
    IBOutlet UITextView *messageTextView;
    
    IBOutlet UIView *eventsView;
    IBOutlet  UILabel *startDate;
    IBOutlet  UILabel *endDate;
    IBOutlet  UILabel *eventTitle;
    IBOutlet  UILabel *eventDescrip;
    IBOutlet  UILabel *eventFooterDescrip;
    IBOutlet  UIImageView *eventImageView;

    
    IBOutlet   UIView *shareView;
    IBOutlet  UILabel *shareTitle;

    
    IBOutlet  UIButton *eventBtn;
    IBOutlet  UIButton *shareBtn;

    IBOutlet   UIView *eventStatusPopup;
    
    
    IBOutlet  UIButton *yesEventBtn;
    IBOutlet  UIButton *noEventBtn;
    IBOutlet  UIButton *maybeEventBtn;



    UIImagePickerController *imagePicker;
    CLLocationCoordinate2D  notesCord;
    NSMutableArray *popUpDataArr;
    NSString *ownerId;
    
    NSMutableArray *notesArr;
}
@end

@implementation CMEventsDetailController

@synthesize eventDic;

-(IBAction)eventStatusOptionClicked:(id)sender{
    NSString *status = @"";
    
    switch ([sender tag]) {
        case 1:
            status = @"A";
            break;
        case 2:
            status = @"M";
            break;
        case 3:
            status = @"D";
            break;
        case 4:
            status = @"R";
            break;
            
        default:
            break;
    }
    [yesEventBtn   setImage:[UIImage imageNamed:@"yes-icon.png"]   forState:UIControlStateNormal];
    [maybeEventBtn setImage:[UIImage imageNamed:@"maybe-icon.png"] forState:UIControlStateNormal];
    [noEventBtn    setImage:[UIImage imageNamed:@"no-icon.png"]    forState:UIControlStateNormal];
    
    if ([status isEqualToString:@"A"]) {
        [yesEventBtn setImage:[UIImage imageNamed:@"yes-icon_Select.png"] forState:UIControlStateNormal];;
    }
    else if ([status isEqualToString:@"M"]) {
        [maybeEventBtn setImage:[UIImage imageNamed:@"maybe-icon_Select.png"] forState:UIControlStateNormal];
    }
    else if ([status isEqualToString:@"D"]) {
        [noEventBtn setImage:[UIImage imageNamed:@"no-icon_Select.png"] forState:UIControlStateNormal];
    }

    // OwnerId , EventId, Status, EventStatusId

//    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    NSString *eventId = [eventDic stringValueForKey:@"EventId"];
    NSString *eventStatusId = [eventDic stringValueForKey:@"EventStatusId"];
    
    eventStatusId = [eventStatusId length]?eventStatusId:@"0";
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerId,@"OwnerId",
                             eventId, @"EventId",
                             eventStatusId,@"EventStatusId",
                             status,@"Status",
                             nil];
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateEventStatus",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
     
        
        //[CMHelper alertMessageWithText:[rideID intValue]?@"You have successfully updated your ride.":@"Congratulations you have successfully created your ride."];
        
       
        NSLog(@"*** save set up ride %@ **",resultDict);
        
    }
     ];

    [self closeEventStatusPop:0];
}


-(void)openEventStatusPop{
    eventStatusPopup.hidden = NO;
    [self.navigationController.view addSubview:eventStatusPopup];
}

-(IBAction)closeEventStatusPop:(id)sender{
    eventStatusPopup.hidden = YES;
    
    [eventStatusPopup removeFromSuperview];
}

-(IBAction)eventNotesBtnClicked:(id)sender{

    CMAddEventNotesVC *secondDetailViewController = [[CMAddEventNotesVC alloc] initWithNibName:@"CMAddEventNotesVC" bundle:nil];
    secondDetailViewController.delegate = self;

    
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
}

-(IBAction)eventTakePhotoClicked:(id)sender{
    [self showUIImagePicker];

    
}




-(IBAction)closeShareOverLayview:(id)sender{
    shareOverLay.hidden = YES;

    [shareOverLay removeFromSuperview];
}

-(IBAction)shareBtnClicked:(id)sender{

    if ([notesArr count]) {
        shareOverLay.hidden = NO;
        [self.navigationController.view addSubview:shareOverLay];
    }else
        [CMHelper alertMessageWithText:@"There is no Event Forum add on this Event."];
    
}

-(IBAction)topSegmentChange:(id)sender{
    
    
  
    
    
    eventBtn.backgroundColor = shareBtn.backgroundColor = DarkBlurColor;
    
    UIButton *selectesBtn = (UIButton *)sender;
    selectesBtn.backgroundColor = LightBlurColor;
    
    
    switch ([sender tag]) {
        case 1:{
           // eventsView.hidden = NO;
            eventsMapview.hidden = YES;

            [eventBtn setImage:[UIImage imageNamed:@"event_blue1.png"] forState:UIControlStateNormal];
            //[shareBtn setImage:[UIImage imageNamed:@"share_blue.png"] forState:UIControlStateNormal];
            
          //  [self.navigationController popViewControllerAnimated:YES];
        }
            
            break;
            
        case 2:{
           // eventsView.hidden = YES;
            eventsMapview.hidden = NO;

            [eventBtn setImage:[UIImage imageNamed:@"event_blue.png"] forState:UIControlStateNormal];
           // [shareBtn setImage:[UIImage imageNamed:@"share_blue1.png"] forState:UIControlStateNormal];
            
        }
            
            break;
            
        default:
            break;
    }

}




- (void)viewDidLoad {
    [super viewDidLoad];
    
    notesArr = [NSMutableArray new];
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    // NSString *
    ownerId    = [dic stringValueForKey:@"OwnerId"];
    // Do any additional setup after loading the view.
    imagePicker = [[UIImagePickerController alloc] init];
    popUpDataArr = [NSMutableArray new];
    
    shareView.layer.cornerRadius = 4;
    
    scrollerView.contentSize = CGSizeMake(320, 500);
    
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    
    [CMAppDelegate setNavigationBarTitle:@"Events Details" navigationItem:self.navigationItem];
    
    [[messageTextView layer]setCornerRadius:4];
    
    
    
    startDate.text = [NSString stringWithFormat:@"Start Date: %@", [eventDic stringValueForKey:@"StartDate"]];//[NSString stringWithFormat:@"Start Date: %@", [[[eventDic stringValueForKey:@"StartDate"] componentsSeparatedByString:@" "] objectAtIndex:0]];
    endDate.text = [NSString stringWithFormat:@"Exp. Date: %@", [eventDic stringValueForKey:@"ExpDate"]];//[NSString stringWithFormat:@"Exp. Date: %@", [[[eventDic stringValueForKey:@"ExpDate"] componentsSeparatedByString:@" "] objectAtIndex:0] ];
    eventDescrip.text = [eventDic stringValueForKey:@"EventDescription"];
    eventFooterDescrip.text = [eventDic stringValueForKey:@"FooterDescription"];
    
    shareTitle.text = eventTitle.text = [eventDic stringValueForKey:@"EventName"];
    
    [eventImageView setImageWithURL:[NSURL URLWithString:[eventDic stringValueForKey:@"EventImagePath"]] placeholderImage:[UIImage imageNamed:@"event_Placeholder.png"]];
    
    
    
    
    
    float height = [CMAppDelegate getHeightForText:eventTitle.text withFont:eventTitle.font andWidth:280];
    
    CGRect frame = eventTitle.frame;
    frame.size.height = height;
    frame.origin.y = 15;
    eventTitle.frame = frame;
    
    
    height = [CMAppDelegate getHeightForText:eventDescrip.text withFont:eventDescrip.font andWidth:280];
    frame = eventDescrip.frame;
    frame.origin.y = CGRectGetMaxY(eventTitle.frame)+10;
    frame.size.height = height;
    eventDescrip.frame = frame;
    
    frame = eventImageView.frame;
    frame.size.height = 180;
    frame.origin.y = CGRectGetMaxY(eventDescrip.frame)+10;
    eventImageView.frame = frame;
    
    
    height = [CMAppDelegate getHeightForText:eventFooterDescrip.text withFont:eventFooterDescrip.font andWidth:280];
    frame = eventFooterDescrip.frame;
    frame.origin.y = CGRectGetMaxY(eventImageView.frame)+10;
    frame.size.height = height;
    eventFooterDescrip.frame = frame;
    
    
    
    frame = startDate.frame;
    frame.origin.y = CGRectGetMaxY(eventFooterDescrip.frame)+10;
    frame.size.height = 20;
    startDate.frame = frame;
    
    frame = endDate.frame;
    frame.origin.x = startDate.frame.origin.x;
    frame.origin.y = CGRectGetMaxY(startDate.frame)+5;//CGRectGetMinY(startDate.frame);
    frame.size.height = 20;
    endDate.frame = frame;
    

    
    
    scrollerView.contentSize = CGSizeMake(scrollerView.frame.size.width,  CGRectGetMaxY(endDate.frame)+20);
    
    
    [self getNotesPhotoForEvents];
    
    
    
    
    if ([[eventDic stringValueForKey:@"Status"] isEqualToString:@"A"]) {
        [yesEventBtn setImage:[UIImage imageNamed:@"yes-icon_Select.png"] forState:UIControlStateNormal];;
    }
    else if ([[eventDic stringValueForKey:@"Status"] isEqualToString:@"M"]) {
        [maybeEventBtn setImage:[UIImage imageNamed:@"maybe-icon_Select.png"] forState:UIControlStateNormal];
    }
    else if ([[eventDic stringValueForKey:@"Status"] isEqualToString:@"D"]) {
        [noEventBtn setImage:[UIImage imageNamed:@"no-icon_Select.png"] forState:UIControlStateNormal];
    }
    
    
    CLLocationCoordinate2D  eventChord;
    eventChord.latitude = [[eventDic stringValueForKey:@"Lattitude"] floatValue];
    eventChord.longitude = [[eventDic stringValueForKey:@"Longititude"] floatValue];
    
    mapview.region = MKCoordinateRegionMake(eventChord,MKCoordinateSpanMake(0.2,0.1) );
    
    
    
    
    CMCustomAnnotation *currentLoc = [[CMCustomAnnotation  alloc] init];
    currentLoc.coordinate = eventChord;
    
    currentLoc.title = [eventDic stringValueForKey:@"EventName"];
    currentLoc.subtitle = [NSString stringWithFormat:@"%@, %@-%@",[eventDic stringValueForKey:@"Street"],[eventDic stringValueForKey:@"State"],[eventDic stringValueForKey:@"ZipCode"]];
    currentLoc.isCurrLocation = YES;
    [mapview addAnnotation:currentLoc];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    
    
    
    if ([popUpDataArr count])
        [popUpDataArr removeAllObjects];
    
    
    NSArray *contactList = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:OwnerBuddyGroupListKey]];
    for (NSDictionary *dic in contactList) {
        if (![[dic  stringValueForKey:@"Status"] isEqualToString:@"Dealer"]) {
            [popUpDataArr addObject:dic];
        }
    }
    
    
    
}

-(void)getNotesPhotoForEvents{

    
    
    NSString *eventId = [eventDic stringValueForKey:@"EventId"];

    //GetRideNote/OwnerId=0/RideId=0
    
    
    [[CMNetworkManager sharedInstance] getResponse:[NSString stringWithFormat:@"%@/GetEventNote/OwnerId=%@/EventId=%@",Service_URL,ownerId,eventId]  requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            
            if ([notesArr count]) {
                [notesArr removeAllObjects];
            }
             for (NSDictionary *dic  in [resultDict objectForKey:@"GetEventNoteResult"]) {
                 [notesArr addObject:dic];
             }
            
           /*  int k =0;
           
            for (NSDictionary *dic  in notesArr) {
                
                CLLocationCoordinate2D coordinate;
                coordinate.latitude = [[dic stringValueForKey:@"Lattitude"] floatValue];
                coordinate.longitude = [[dic stringValueForKey:@"Longititude"] floatValue];
                
                
                
                CMCustomAnnotation *bikeLocationPoint = [[CMCustomAnnotation  alloc] init];
                bikeLocationPoint.coordinate = coordinate;
                bikeLocationPoint.title = [dic stringValueForKey:@"Note"];
                bikeLocationPoint.imageUrl = [dic stringValueForKey:@"ImageUrl"];
                bikeLocationPoint.pinId = [NSString stringWithFormat:@"%d",k];
                bikeLocationPoint.infoDic = dic;
                
        if ( coordinate.latitude != 0 && coordinate.longitude != 0 )
                notesCord = coordinate;
                
                if (![bikeLocationPoint.title length]) bikeLocationPoint.title =  @"   ";
                
                [mapview addAnnotation:bikeLocationPoint];
                
                k++;
                
                
            }
            
//#if 0
            if ( notesCord.latitude != 0 && notesCord.longitude != 0 ){
                NSLog(@"Coordinate valid");
                mapview.region = MKCoordinateRegionMake(notesCord,MKCoordinateSpanMake(0.2,0.1) );
            }*/
//#endif
            
        }
    }];
    
    
}
-(void)showUIImagePicker{
    
    
    @try {
        //        IstoCamera = YES;
        
        // dispatch_async(dispatch_get_current_queue(), ^(void){
        
        imagePicker.delegate = self;
        imagePicker.sourceType = IMAGESOURSETYPE;//UIImagePickerControllerSourceTypePhotoLibrary;//;
        //IMAGESOURSETYPE
        imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
        [self.navigationController presentViewController:imagePicker animated:YES completion:NULL];
        
        //  } ) ;
        
    }
    @catch (NSException * e) {
        UIAlertView *alertmsg=[[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Couldn’t locate the camera, check your settings and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertmsg show];
    }
    @finally {
    }
    
}



#pragma mark -
#pragma mark AddEventNotesDelegate Methods
- (void)cancelButtonClicked:(CMAddEventNotesVC*)secondDetailViewController withSend:(BOOL)isSend messageText:(NSString *)messageText{
    

    
   //NSDictionary *ownerDic   = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    NSString *eventId = [eventDic stringValueForKey:@"EventId"];

    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:eventId, @"EventId",
                             ownerId, @"OwnerId", messageText, @"Note",
                             @"", @"ImageName",
                             @"", @"ImageUrl",
                             @"0", @"EventNoteId",
                             [UIAppDelegate.userDefault objectForKey:@"Lat"], @"Lattitude",
                             [UIAppDelegate.userDefault objectForKey:@"Lon"], @"Longititude",
                             nil];
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateEventNote",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** save set up ride %@ **",resultDict);
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""   message:@"Note has been added to the Event Forum successfully."    delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
            [self getNotesPhotoForEvents];
            
            /*
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = [[UIAppDelegate.userDefault objectForKey:@"Lat"] floatValue];
            coordinate.longitude = [[UIAppDelegate.userDefault objectForKey:@"Lon"] floatValue];
            
            
            
            CMCustomAnnotation *bikeLocationPoint = [[CMCustomAnnotation  alloc] init];
            bikeLocationPoint.coordinate = coordinate;
            bikeLocationPoint.title = messageText;
            [mapview addAnnotation:bikeLocationPoint];*/
        }
    }];
    
    

    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}


- (void)cancelButtonClicked:(CMAddEventNotesVC*)secondDetailViewController{
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate Methods


-(void)postImageWithServiceType:(UIImage * )imageFile{
    
    
    
    if (imageFile) {

        [[CMNetworkManager sharedInstance] postImage:ImageUploadURL  withImage:imageFile  param:nil  requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            NSString *imageUrl = @"";
            if ([resultDict count]) {
                NSDictionary *tempDic =  [(NSArray *)resultDict objectAtIndex:0];
                imageUrl = [tempDic stringValueForKey:@"image_url"];
                NSLog(@"tempDic = %@",tempDic);
                
                
                /*
                CLLocationCoordinate2D coordinate;
                coordinate.latitude = [[UIAppDelegate.userDefault objectForKey:@"Lat"] floatValue];
                coordinate.longitude = [[UIAppDelegate.userDefault objectForKey:@"Lon"] floatValue];
                
                CMCustomAnnotation *bikeLocationPoint = [[CMCustomAnnotation  alloc] init];
                bikeLocationPoint.coordinate = coordinate;
                bikeLocationPoint.imageUrl = imageUrl;
                
                bikeLocationPoint.title =  @"   ";
                [mapview addAnnotation:bikeLocationPoint];
                
*/
                
                
                
                
                
                
               
                
                NSString *eventId = [eventDic stringValueForKey:@"EventId"];
                
                
                
                NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:eventId, @"EventId",
                                         ownerId, @"OwnerId", @"", @"Note",
                                         [imageUrl lastPathComponent], @"ImageName",
                                         imageUrl, @"ImageUrl",
                                         @"0", @"EventNoteId",
                                         [UIAppDelegate.userDefault objectForKey:@"Lat"], @"Lattitude",
                                         [UIAppDelegate.userDefault objectForKey:@"Lon"], @"Longititude",
                                         nil];
                
                
                [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateEventNote",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                        NSLog(@"*** save set up ride %@ **",resultDict);
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""   message:([imageUrl length]?@"Photo has been added to the Event Forum successfully.":@"Note has been added to the Event Forum successfully.")    delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [alertView show];
                        
                        [self getNotesPhotoForEvents];

                       
                    }
                }];
                
                
            }
            
            
            
            
            
        }];

    }
    
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
        [self postImageWithServiceType:image];
        
        
        
        
        [imagePicker dismissViewControllerAnimated:YES completion:nil];
        
        
        
        
    }
}
#pragma mark - UITextView Delegate for

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        // Be sure to test for equality using the "isEqualToString" message
        [textView resignFirstResponder];
        
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    
    return YES;
}


#pragma mark - DropDwon Click Delegate



-(void)getDropDownDataForMessage{
    
    
    NSString * stringURL =  [NSString stringWithFormat:@"%@/GetContactListWithGroupName/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetContactListWithGroupName %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetContactListWithGroupNameResult"];
            [popUpDataArr removeAllObjects];
            
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
            [UIAppDelegate.userDefault setObject:data forKey:OwnerBuddyGroupListKey];
            
            for (NSDictionary *dic in arr) {
                if (![[dic  stringValueForKey:@"Status"] isEqualToString:@"Dealer"]) {
                    [popUpDataArr addObject:dic];
                }
            }
            
            
            
        }
        
        if ([popUpDataArr count]) {
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Event Share With" withOption:[popUpDataArr valueForKey:@"Name"] xy:CGPointMake(10, 100) size:CGSizeMake(300, MIN(200, [popUpDataArr count]*50+50)) isMultiple:NO withTag:0];
            
        }
        [SVProgressHUD dismiss];
    }];
    
}
- (IBAction)sendToDropDownSingle:(id)sender {
    
    selectedHealthBtn = (CMButton_DropDown *)sender;

    if ([popUpDataArr count]) {
        [Dropobj fadeOut];
        [self showPopUpWithTitle:@"Select Event Share With" withOption:[popUpDataArr valueForKey:@"Name"] xy:CGPointMake(10, 100) size:CGSizeMake(300, MIN(200, [popUpDataArr count]*50+50)) isMultiple:NO withTag:0];
        
    }
    else
        [self getDropDownDataForMessage];
    
    
    
}

- (IBAction)sendToDropDownSingle_old:(id)sender {
    
    selectedHealthBtn = (CMButton_DropDown *)sender;
    [Dropobj fadeOut];
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:@"Select Social Network" options:[NSArray arrayWithObjects:@"Facebook",@"Twitter",nil] xy:CGPointMake(15, 98+65) size:CGSizeMake(290, MIN(200,2*50+50)) isMultiple:NO];
    
    
    Dropobj.delegate = self;
    [Dropobj showInView:shareOverLay animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple withTag:(int)tag{
    
    if (!Dropobj)  Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    
    
    Dropobj.delegate = self;
   // [Dropobj showInView:self.view animated:YES];
    [Dropobj showInView:shareOverLay animated:YES];

    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
//    [eventDic stringValueForKey:@"FooterDescription"]
//    [eventDic stringValueForKey:@"EventName"]
    //[eventDic stringValueForKey:@"EventImagePath"]
    
    selectedSocialIndex = anIndex;
  //  NSDictionary *tempDic = [popUpDataArr objectAtIndex:anIndex];
    [selectedHealthBtn setTitle:[[popUpDataArr valueForKey:@"Name"] objectAtIndex:anIndex] forState:UIControlStateNormal];
    [selectedHealthBtn setSelectedObjectID:[NSString stringWithFormat:@"%@",[[popUpDataArr valueForKey:@"ContactId"] objectAtIndex:anIndex]]];
    
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}


-(IBAction)shareBtnClicke:(id)sender{

    if (![popUpDataArr count]) {
        [CMHelper alertMessageWithText:@"Please add Recipient first."];
        return;
        
    }else if ( selectedSocialIndex==-1) {
        [CMHelper alertMessageWithText:@"Please select Recipient."];
        return;
    }

    /*
    SHKItem *item = [SHKItem image:eventImageView.image title:[eventDic stringValueForKey:@"EventName"]];
    
    if (selectedSocialIndex == 0) { //facebook
        [SHKiOSFacebook shareItem:item];
        [selectedHealthBtn setTitle:@"Facebook" forState:UIControlStateNormal];
        
    }
    else{  //Twitter
        [selectedHealthBtn setTitle:@"Twitter" forState:UIControlStateNormal];
        
        [SHKiOSTwitter shareItem:item];
        
    }*/
    //Input Parameter: EventId, GroupId, OwnerId, SenderOwnerId, EventShareId(output parameter pass   always zero)
    
    //SenderOwnerId
    NSDictionary *tempDic = [popUpDataArr objectAtIndex:selectedSocialIndex];

    NSString *groupId = @"0";
    
    NSString *sendById = [tempDic stringValueForKey:@"ContactId"];

    
    if ([[tempDic stringValueForKey:@"Status"] isEqualToString:@"Owner"]) {
       
    }
   else{
        
        sendById = @"0";
        
        groupId =  [tempDic stringValueForKey:@"ContactId"];
        
    }
    
    NSString *eventId = [eventDic stringValueForKey:@"EventId"];

    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:sendById, @"OwnerId", @"0", @"EventShareId",
                             eventId, @"EventId",
                             ownerId,@"SenderOwnerId",
                             groupId,@"GroupId",
                             nil];
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateEventShare",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        [self closeShareOverLayview:0];

        [SVProgressHUD dismiss];
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            [CMHelper alertMessageWithText:@"Event has been shared successfully."];
            
            
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
//                                                                message:@"Message sent!"
//                                                               delegate:Nil cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//            [alertView show];
//            
            
        }
       
        
    }];

    

}


#pragma mark -
#pragma mark MKMapViewDelegate
// creates MKAnnotationViews
- (MKAnnotationView *)mapView:(MKMapView *)mView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    UIImageView *dropImage = nil;
    
    MKPinAnnotationView *pin = nil;
    static NSString *reuseId = @"StandardPin";
    pin =  (MKPinAnnotationView*)[mView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if(pin == nil) {
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        pin.canShowCallout = YES;
        
        
    }
    // pin.frame = CGRectMake(0, 0, 100, 60);
    NSLog(@"title = ====%@",annotation.title);
    
    pin.canShowCallout = YES;//[UIAppDelegate.userDefault boolForKey:IsBikeParkKey]?NO:YES;
    
    pin.leftCalloutAccessoryView = nil;
    CMCustomAnnotation *currLoc = (CMCustomAnnotation *)annotation;
    
    if (![annotation.title isEqualToString: @"   "]) {
       // pin.image = [UIImage imageNamed:@"note_event_pin.png"];
        pin.image = [UIImage imageNamed:@"parked.png"];

        //parked.png
        [dropImage removeFromSuperview];
        
    }
    else{
       // pin.image = [UIImage imageNamed:@"camera_event_pin.png"];
        pin.image = [UIImage imageNamed:@"parked.png"];

        dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        dropImage.tag = 11;
        //dropImage.image = [UIImage imageNamed:@"staining.png"];
        
        NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[currLoc imageUrl]];
        [dropImage setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil];
        
        pin.leftCalloutAccessoryView =dropImage ;
        
    }
    
    if (currLoc.isCurrLocation) {
        pin.canShowCallout = YES;
        pin.rightCalloutAccessoryView = nil;

    }
    else {
    //   pin.leftCalloutAccessoryView = currentView;
       pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        pin.canShowCallout = NO;

    }
    return pin;
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    CMCustomAnnotation *annotation = [view annotation];
    if(![annotation.title isEqualToString:@"Current Location"]) {
        
    }
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
   
    NSLog(@"annotation touched: %@", view.annotation.title);
    CMCustomAnnotation *annotation = [view annotation];
    
    
    if ([annotation isKindOfClass:[MKUserLocation class]] || annotation.isCurrLocation) {
            [view setCanShowCallout:YES];
      //  [mapView selectAnnotation:annotation animated:YES];
        //return ;
    }else{
        [mapView deselectAnnotation:annotation animated:YES];
        
        
        
        CMPhotoNotesVC *controller = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMPhotoNotesVC"];
        controller.itemsArr = notesArr;
        controller.currIndex = [annotation.pinId integerValue];
        controller.title = @"EVENT FORUM";
        controller.isNotes = [[annotation.infoDic stringValueForKey:@"Note"] length]?YES:NO;

    
        [self.navigationController pushViewController:controller animated:YES];

/*    CMRideNotesDetailVC *controller = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMRideNotesDetailVC"];
    controller.infoDic = annotation.infoDic;
    controller.itemsArr = notesArr;
    controller.currIndex = [annotation.pinId integerValue];
    [self.navigationController pushViewController:controller animated:YES];*/
    }
    //CMRideNotesDetailVC
}

-(IBAction)openNotesImagesDetails:(id)sender{

    if ([notesArr count]) {
        CMPhotoNotesVC *controller = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMPhotoNotesVC"];
        controller.itemsArr = notesArr;
        controller.currIndex = 0;
        controller.title = @"EVENT FORUM";
        controller.isNotes = [[[notesArr objectAtIndex:0] stringValueForKey:@"Note"] length]?YES:NO;
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{
    
        [CMHelper alertMessageWithText:@"There is no Event Forum add on this Event."];
    }
  
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
