//
//  CMPhotoNotesVC.h
//  Cyclemate
//
//  Created by Rajesh on 4/23/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMPhotoNotesVC : UIViewController
@property(nonatomic,retain)NSArray *itemsArr;
@property(nonatomic)int currIndex;
@property(nonatomic)BOOL isNotes;

@end
