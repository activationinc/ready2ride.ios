//
//  CMMessageViewController.h
//  Cyclemate
//
//  Created by Rajesh on 11/18/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQActionSheetPickerView.h"
#import "GAITrackedViewController.h"

@interface CMMessageViewController : GAITrackedViewController<IQActionSheetPickerViewDelegate>{

    BOOL isNewMessage;
    BOOL isReplyMessage;
    IBOutlet UIView *replyView;
    
    UITextField *selectedField;
    
    IBOutlet UITextField *dateTextField;
    IQActionSheetPickerView *picker;
    int selectedIndex;
    
    IBOutlet UITextView *inputTextView;
    IBOutlet UITextField *subjectField;
    
    
    IBOutlet UIButton *messageBtn;
    IBOutlet UIButton *newmessageBtn;

    
   IBOutlet CMButton_DropDown *messageTypeBtn;


}
@property(nonatomic,retain)NSString *messCycleId;

-(IBAction)topSegmentBtnClicked:(id)sender;
-(IBAction)messageTypeBtnClicked:(id)sender;
@end
