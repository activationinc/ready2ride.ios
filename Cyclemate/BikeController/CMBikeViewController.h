//
//  CMBikeViewController.h
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"


@interface CMBikeViewController : UIViewController
-(IBAction)bottomViewBtnClicked:(id)sender;

-(IBAction)backBtnClicked:(id)sender;
-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)addNewBikeBtnClicked:(id)sender;

-(IBAction)tableMassegeClicked:(id)sender;

@end
