//
//  CMAddEventNotesVC.h
//  Cyclemate
//
//  Created by Rajesh on 4/22/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddEventNotesDelegate;

@interface CMAddEventNotesVC : UIViewController


@property (assign, nonatomic) id <AddEventNotesDelegate>delegate;


@property (assign, nonatomic) id controller;
@property (assign, nonatomic) NSString *massegeText;
@property (assign, nonatomic) NSString *titleText;

//-(void)hideMessagePopUp;
@end



@protocol AddEventNotesDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(CMAddEventNotesVC*)secondDetailViewController;
- (void)cancelButtonClicked:(CMAddEventNotesVC*)secondDetailViewController withSend:(BOOL)isSend messageText:(NSString *)messageText;
@end
