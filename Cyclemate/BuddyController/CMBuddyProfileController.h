//
//  CMBuddyProfileController.h
//  Cyclemate
//
//  Created by Sourav on 29/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMBuddyProfileController : UIViewController
@property(nonatomic,retain)NSDictionary *buddyDetailDic;

@end
