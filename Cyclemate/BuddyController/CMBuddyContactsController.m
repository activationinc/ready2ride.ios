//
//  CMBuddyContactsController.m
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMBuddyContactsController.h"
#import "CMBuddyProfileController.h"
#import  "CMBuddySearchController.h"
#import  "CMBuddyNotificationVC.h"
#import "CMBuddyGroupsController.h"

#import "CMSearchResultController.h"
#import "CMDirectDealerMailVC.h"
#import "UIAlertView+MKBlockAdditions.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAILogger.h"
#import "GAIFields.h"
#import "GAITrackedViewController.h"
#import "GAITracker.h"
#import "CMDeviceContactsListController.h"
@interface CMBuddyContactsController ()<DirectDealerMailDelegate>
{

    NSMutableArray *buddyList;
    
    
    NSMutableArray *contactListArr;

    NSMutableArray *buddyGroupArr;
    NSMutableArray *groupBuddyListArr;

    
    IBOutlet UIButton *selectAllBtn;
    BOOL isAllSelected;
    IBOutlet UITableView *buddyTableView;
    
    UITableView *buddydetailTableView;
    
    IBOutlet UITextField *lastNmeField;
    IBOutlet UITextField *zipCodeField;
    IBOutlet UITextField *stateField;
    int selectedRidingID;
    
    NSString *appOwnerId;
    
    
    UIRefreshControl *refreshControl;
    
  
    
}

@property (nonatomic,strong)NSMutableArray *rideTypeArrays;
@property (nonatomic,strong)NSMutableArray *rideMemberArray;
@end

@implementation CMBuddyContactsController

@synthesize isFromHome;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)acceptBtnClicked:(id)sender{
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:buddyTableView];
    NSIndexPath *indexPath = [buddyTableView indexPathForRowAtPoint:buttonPosition];
    // UITableViewCell *cell = [listTableView cellForRowAtIndexPath:indexPath];
   // NSDictionary *contactDic = [contactListArr objectAtIndex:indexPath.row];

    [self acceptRejectBuddy:[contactListArr objectAtIndex:indexPath.row] withStatus:YES withIndex:indexPath.row];
}

-(IBAction)rejectBtnClicked:(id)sender{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:buddyTableView];
    NSIndexPath *indexPath = [buddyTableView indexPathForRowAtPoint:buttonPosition];
   // NSDictionary *contactDic = [contactListArr objectAtIndex:indexPath.row];
    [self acceptRejectBuddy:[contactListArr objectAtIndex:indexPath.row] withStatus:NO withIndex:indexPath.row];

}


-(void)acceptRejectBuddy:(NSMutableDictionary *)buddyDic withStatus:(BOOL)isAccept withIndex:(int)idx{
    
    
    NSString *message =   isAccept?[NSString stringWithFormat:@"Do you want to Accept %@ %@ as your Contact?",[buddyDic stringValueForKey:@"FName"],[buddyDic stringValueForKey:@"LName"]]:[NSString stringWithFormat:@"Do you want to Reject %@ %@ as your Contact?",[buddyDic stringValueForKey:@"FName"],[buddyDic stringValueForKey:@"LName"]];
    
    
    
    
    [[UIAlertView alertViewWithTitle:@"Alert"
                             message:message cancelButtonTitle:@"Cancel" otherButtonTitles:@[isAccept?@"Accept":@"Reject"]
                           onDismiss:^(int buttonIndex) {
                               
                               
                         //      NSString *ownerId   = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                               
                               
                               NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[buddyDic stringValueForKey:@"SenderId"], @"SenderId",[buddyDic stringValueForKey:@"ReceiverId"], @"ReceiverId",[buddyDic stringValueForKey:@"BuddyId"], @"BuddyId",(isAccept?@"A":@"R"),@"Status",
                                                        nil];
                               
                               [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateBuddy",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                   
                                   
                                   
                                  
                                   
                                   if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                       NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                                       //Contact has been added successfully.
                                       [CMHelper alertMessageWithText:isAccept?@"Contact has been added successfully.":@"Contact has been rejected successfully."];

                                       
                                      // [CMHelper alertMessageWithText:[NSString stringWithFormat:@"Request has been %@ successful.",isAccept?@"accepted":@"rejected"]];
                                       //isAccept?@"Accept":@"Reject"
                                       
                                       if (isAccept) {
                                           
                                           NSMutableDictionary *midDic = [buddyDic mutableCopy];
                                           [midDic setObject:@"" forKey:@"Status"];
                                           [midDic setObject:@"" forKey:@"StatusReject"];
                                           
                                           [contactListArr replaceObjectAtIndex:idx withObject:midDic];
                                       }else
                                           [contactListArr removeObject:buddyDic];

                                   }
                                   [buddyTableView reloadData];
                                   
                                   
                               }];
                           } onCancel:^{
                               
                               
                           }] show];
    
    
    
    
 
    
    
    
    
    
    
}

-(void)getContactListForOwner{

    
    
    NSString *ownerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    NSString *stringURL;// =  [NSString stringWithFormat:@"%@/GetBuddyByUserId/UserID=%@",Service_URL,ownerId];
    
      stringURL =  [NSString stringWithFormat:@"%@/GetBuddyContactsData/OwnerId=%@",Service_URL,ownerId];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetBuddySearchData %@ **",resultDict);
            
           // NSArray *arr = [resultDict objectForKey:@"GetBuddyByUserIdResult"];//GetBuddyContactsDataResult
            NSArray *arr = [resultDict objectForKey:@"GetBuddyContactsDataResult"];//GetBuddyContactsDataResult

            if ([arr count]) {
                [contactListArr removeAllObjects];
            }
            for (id obj in arr  ) {
                [contactListArr addObject:obj];
            }
            //GetGroupBuddyByOwnerIDResult
            [buddyTableView reloadData];
            
        }
    }];
    

}
-(void)getGroupBuddyListForOwner:(NSString *)withGroupId{
    
    NSString *ownerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    ownerGroupId = withGroupId;
    NSString *stringURL =  [NSString stringWithFormat:@"%@/GetBuddyByOwnerGroupId/OwnerGroupId=%@/OwnerId=%@",Service_URL,withGroupId,ownerId];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetBuddyByOwnerGroupIdResult %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetBuddyByOwnerGroupIdResult"];
            
            if ([arr count]) {
                [groupBuddyListArr removeAllObjects];
            }
            for (id obj in arr  ) {
                [groupBuddyListArr addObject:obj];
            }
            //GetGroupBuddyByOwnerIDResult
            [buddyTableView reloadData];

            [buddydetailTableView reloadData];
            
        }
    }];

}

-(IBAction)getGroupForOwner{
    
    
    NSString *ownerId   = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    NSString *stringURL =  [NSString stringWithFormat:@"%@/GetGroupBuddyByOwnerID/OwnerId=%@",Service_URL,ownerId];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetBuddySearchData %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetGroupBuddyByOwnerIDResult"];
            
            if ([arr count]) {
                [buddyGroupArr removeAllObjects];
            }
            
            
            for (id obj in arr  ) {
                [buddyGroupArr addObject:obj];
            }
            //GetGroupBuddyByOwnerIDResult
            [buddyTableView reloadData];
            
            }
    }];
    
    
    
    
}

-(IBAction)buddySearchBtnClicked:(id)sender{
    
    
    if (![lastNmeField.text length] && ![zipCodeField.text length]&& ![stateField.text length]) {
        //[CMHelper alertMessageWithText:@"Please fill in value in  atleast one field."];
        [CMHelper alertMessageWithText:@"Please fill in at least one field."];
        
        

        return;
    }
    
    
    // LName=call/Zip=''/State=''/RidingID=''
    //Input parameter: LName(string),Zip(String), State(String), RidingID(String)(as parameter with url)
    NSString *lastNmeStr = lastNmeField.text;
    lastNmeStr = [lastNmeStr length]?lastNmeStr:@"''";
    
    NSString *zipCodeStr = zipCodeField.text;
    zipCodeStr = [zipCodeStr length]?zipCodeStr:@"''";

    NSString *stateStr =   stateField.text;
    stateStr = [stateStr length]?stateStr:@"''";
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
     NSString *ownerId    = [dic stringValueForKey:@"OwnerId"];
    

    NSString *stringURL =  [NSString stringWithFormat:@"%@/GetBuddySearchData/OwnerId=%@/LName=%@/Zip=%@/State=%@/RidingID=%d",Service_URL,ownerId,lastNmeStr,zipCodeStr,stateStr,selectedRidingID];
    
        
    
        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                NSArray *arr = [resultDict objectForKey:@"GetBuddySearchDataResult"];
                
//                for (id obj in arr) {
//                    
//                    if ([obj stringValueForKey:@"0"]) {
//                        [searchResultArr addObject:obj];
//                    }
//                }
                
                
                if ([arr count]) {
                 
                    CMSearchResultController *buddyProfile = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMSearchResultController"];
                    buddyProfile.searchResultArr = arr;
                    buddyProfile.isFromGroup = isFromGroup;
                    buddyProfile.ownerGroupId = ownerGroupId;
                    [self.navigationController pushViewController:buddyProfile animated:YES];

                }
//                else  if ([arr count])
//                    [CMHelper alertMessageWithText:@"This buddy is already added to your contact."];
                else
                    [CMHelper alertMessageWithText:@"No records found with the searched criteria, please try again with revised searched criteria."];
               
                
                
              

            }
        }];
        
        
        
    
}
-(IBAction)buddyReqBtnClicked:(id)sender{

    CMDirectDealerMailVC *secondDetailViewController = [[CMDirectDealerMailVC alloc] initWithNibName:@"CMDirectDealerMailVC" bundle:nil];
    secondDetailViewController.delegate = self;
    secondDetailViewController.isSendBuddy = YES;
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
}

-(IBAction)segmentValueChangesClicked:(id)sender
{
    [Dropobj fadeOut];
    
    searchBtn.backgroundColor =   contactBtn.backgroundColor  =    groupBtn.backgroundColor  = DarkBlurColor;
    

    
    UIButton *selectedBtn = (UIButton *)sender;
    selectedBtn.backgroundColor =  LightBlurColor;

    
 
   
    searchView.hidden = YES;
    viewWithTable.hidden = YES;
    
    currTopIndex = [selectedBtn tag]-1;
    
    CGRect frame =  bottomLbl.frame ;
    frame.origin.x = selectedBtn.frame.origin.x;
    bottomLbl.frame = frame;
    
    switch ([selectedBtn tag]-1)
    {
        case 0:{
//            viewWithTable.hidden = NO;
//
//            isGroup = NO;
//            [buddyTableView reloadData];
            UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"Buddy" bundle:nil];
            CMDeviceContactsListController *_CMDeviceContactsListController=[storyBoard instantiateViewControllerWithIdentifier:@"CMDeviceContactsListController"];
            [self.navigationController pushViewController:_CMDeviceContactsListController animated:YES];
            isAddContact = YES;
        }
            break;
        case 1:
            isGroup = YES;
            viewWithTable.hidden = NO;
            isAddContact = NO;
            [buddyTableView reloadData];
            break;
        case 2:{
            searchView.hidden = NO;

        
//            CMBuddySearchController *buddyProfile = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMBuddySearchController"];
//            [self.navigationController pushViewController:buddyProfile animated:YES];

        }
            break;
        case 3:
            break;
        default:
            break;
    }
}

-(IBAction)radioBtnClicked:(id)sender{
    
    
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    
    
    [allDealerBtn setImage:redioInActiveImage forState:UIControlStateNormal];
    [nwDealerBtn setImage:redioInActiveImage forState:UIControlStateNormal];
    
    [sender setImage:redioActiveImage forState:UIControlStateNormal];
}
-(IBAction)topMenuBtnClicked:(id)sender
{
    
}




-(IBAction)buddyBottomBarClicked:(id)sender{

    switch ([sender tag]) {
        case 1:{
            CMBuddyProfileController *buddyProfile = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMBuddyProfileController"];
            [self.navigationController pushViewController:buddyProfile animated:YES];
            break;
        }
        case 2:{
           
            break;
        }
        case 3:{
         
            break;
        }
        case 4:{
            CMBuddyNotificationVC *buddyProfile = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMBuddyNotificationVC"];
            [self.navigationController pushViewController:buddyProfile animated:YES];
            break;
        }
        case 5:{
            CMBuddySearchController *buddyProfile = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMBuddySearchController"];
            [self.navigationController pushViewController:buddyProfile animated:YES];
            break;
        }
        default:
            break;
    }

}

-(void)goToBuddyGroups{
    
    CMBuddyGroupsController *buddyGropus = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMBuddyGroupsController"];
    [self.navigationController pushViewController:buddyGropus animated:YES];
    
}

-(IBAction)groupUpBtnClicked:(id)sender;
{
  
    selectedBuddyIndex = -1;
    
    [buddyTableView reloadData];
    
    
}

-(IBAction)groupDropDwnBtnClicked:(id)sender;
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:buddyTableView];
    NSIndexPath *indexPath = [buddyTableView indexPathForRowAtPoint:buttonPosition];
    // UITableViewCell *cell = [listTableView cellForRowAtIndexPath:indexPath];
    NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:indexPath.row];

    
//    if (<#condition#>) {
//        <#statements#>
//    }
    [self getGroupBuddyListForOwner:[buddyDic stringValueForKey:@"OwnerGroupId"]];
    selectedBuddyIndex = indexPath.row;
    
    [buddyTableView reloadData];


}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
       appOwnerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    
    selectedBuddyIndex = -1;

    _rideTypeArrays = [[NSArray arrayWithObjects:@"The Getaway Buddies",@"The Afternoon Delight Buddies",@"The Day Tripper Buddies",@"The Weekender Buddies",@"The Touring Buddies",@"Chaptering Buddies", nil] mutableCopy];
    
    _rideMemberArray = [[NSArray arrayWithObjects:@"3",@"2",@"10",@"6",@"7",@"8", nil] mutableCopy];
    
    // Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"BUDDIES" navigationItem:self.navigationItem];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:(isFromHome?HomeBootomTabBar:BuddyBootomTabBar)]];
    
  
    buddyList = [[NSArray arrayWithObjects:@"Bob Allen",@"Emma Reed",@"Susan Blunt",@"Bill Williams", nil] mutableCopy];

    
   // searchResultArr = [NSMutableArray new];
    mainContactList = [NSMutableArray new];
    contactListArr = [NSMutableArray new];
    groupBuddyListArr = [NSMutableArray new];
    buddyGroupArr = [NSMutableArray new];//[[NSArray arrayWithObjects:@"Bobby 123",@"Speed Reed",@"Susan444",@"TourBill", nil] mutableCopy];
    
    [selectAllBtn setImage:NormalImage forState:UIControlStateNormal];
    [selectAllBtn setImage:SelectedImage forState:UIControlStateSelected];
    [selectAllBtn addTarget:self action:@selector(buttonTouch:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshContactData) forControlEvents:UIControlEventValueChanged];
    [buddyTableView addSubview:refreshControl];
    
    
    @try
    {
        
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"Buddies"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }

}
-(void)viewDidAppear:(BOOL)animated{
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:@"UA-80658429-4"];
    [self.tracker set:kGAIScreenName value:@"Buddies"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
  
}
-(void)refreshContactData{
    
    
    if (currTopIndex==0) {
        [self getContactListForOwner];

    }else
         [self getGroupForOwner];
}

-(void)viewWillAppear:(BOOL)animated{
    currTopIndex  =0;
    
    
    [self getContactListForOwner];
    [self getGroupForOwner];
    
    contactBtn.backgroundColor  =    groupBtn.backgroundColor  = DarkBlurColor;
    viewWithTable.hidden = NO;
    
    isGroup = NO;
    [buddyTableView reloadData];
    isAddContact = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [buddyTableView setEditing:NO];
    [buddydetailTableView setEditing:NO];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    if (button == selectAllBtn) {
        button.selected = !button.selected;
        isAllSelected = !isAllSelected;
        [buddyTableView reloadData];
        
    }else{
        button.selected = !button.selected;
        [selectAllBtn setSelected:NO];
        isAllSelected = NO;
        
    }
}
-(IBAction)addContactOnGroupClicked:(id)sender{
  
    if ([mainContactList count]) {
        [mainContactList removeAllObjects];
    }
    
    for (id obj in contactListArr) {
        if (![[obj stringValueForKey:@"Status"] length]) {
            [mainContactList addObject:obj];
        }
    }
    
    
    if ([mainContactList count]) {
        NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:selectedBuddyIndex];
        ownerGroupId = [buddyDic stringValueForKey:@"OwnerGroupId"];
       
//mainContactList
        
        [Dropobj fadeOut];
        
        
        
        
        Dropobj = [[DropDownListView alloc] initWithTitle:@"Add buddy on Group" options:[mainContactList valueForKey:@"FName"] xy:CGPointMake(10, IS_IOS7?64:44) size:CGSizeMake(300, MIN(200, contactListArr.count*50+50)) isMultiple:NO];
        
        Dropobj.delegate = self;
        Dropobj.tag = 999;
        [Dropobj showInView:self.view animated:YES];
        
        /*----------------Set DropDown backGroundColor-----------------*/
        [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
        
        
    }
    else{
        [CMHelper alertMessageWithText:@"Currently there is no buddy available."];
        
    }

//    isFromGroup = YES;
//    [self segmentValueChangesClicked:searchBtn];

}

-(IBAction)addButtonClicked:(id)sender{
    
    if (!isGroup) {
        isFromGroup = NO;
        ownerGroupId = @"";
        [self segmentValueChangesClicked:searchBtn];
    }
    else{
    
        
        
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Group name"
                                  message:@"Please enter your Group name:"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:@"Add", nil];
        [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        /* Display a numerical keypad for this text field */
        UITextField *textField = [alertView textFieldAtIndex:0];
        textField.keyboardType = UIKeyboardTypeDefault;
        
        [alertView show];
        

    }

}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            if ([textField.text length]) {
             
                
                // (Need to pass OwnerGroupId, OwnerId AND Name(Name of group))

                
             
                NSString *ownerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

                
                NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         @"0", @"OwnerGroupId",
                                         ownerId, @"OwnerId",
                                         textField.text, @"Name",
                                         nil];
                
                
                [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateGroupOwner",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    
                    if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                       // [self getGroupForOwner];
                        
                        NSLog(@"*** GetBuddyByOwnerGroupIdResult %@ **",resultDict);
                        
                        NSMutableDictionary *dic = [NSMutableDictionary new];
                        [dic setObject:textField.text forKey:@"Name"];
                        [dic setObject:[resultDict stringValueForKey:@"response"] forKey:@"OwnerGroupId"];
                        [dic setObject:@"1" forKey:@"Count"];
                        [dic setObject:ownerId forKey:@"OwnerId"];
                        [buddyGroupArr addObject:dic];
                    
                        [buddyTableView reloadData];

                    }
                }];
                
               
            }
        }
    }
    
}



#pragma mark -
#pragma mark CMDirectDealerMailVCDelegate Methods

- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController;
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}
- (void)cancelButtonClicked:(CMDirectDealerMailVC*)secondDetailViewController withSend:(BOOL)isSend messageText:(NSString *)messageText{

    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}

#pragma mark -
#pragma mark UITableView Method Implementation Call

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
//    if (tableView==buddydetailTableView) {
//
//        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 40)];
//     //   footerView.backgroundColor = [tableView backgroundColor];
//        [footerView setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
//
//        UIButton *menuButton =[[UIButton alloc] initWithFrame:CGRectMake(150,0, 140, 30 )];
//        [menuButton addTarget:self action:@selector(addContactOnGroupClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [menuButton setBackgroundColor:[UIColor colorWithRed:1.0f/255.0f green:52.0f/255.0f blue:69.0f/255.0f alpha:1]];
//        [menuButton setImage:[UIImage imageNamed:@"add_motorcycle.png"] forState:UIControlStateNormal];
//        [[menuButton layer] setCornerRadius:4];
//
//        [menuButton setTitle:[@"add Contact" uppercaseString] forState:UIControlStateNormal];
//        [menuButton setTitleColor:[UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f  blue:182.0f/255.0f  alpha:1] forState:UIControlStateNormal];
//        menuButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:12];    //addMotorCycle.png
//        menuButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);    //addMotorCycle.png
//        menuButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);    //addMotorCycle.png
//        [footerView addSubview:menuButton];
//        return footerView;
//    }
//    
//    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 40)];
//    footerView.backgroundColor = [tableView backgroundColor];
//    
//    UIButton *menuButton =[[UIButton alloc] initWithFrame:CGRectMake(150,0, 150, 40 )];
//    [menuButton addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [menuButton setBackgroundImage:[UIImage imageNamed:@"addMotorCycle.png"] forState:UIControlStateNormal];
//    [menuButton setTitle:[isGroup?@"add group":@"add Contact" uppercaseString] forState:UIControlStateNormal];
//    [menuButton setBackgroundColor:[UIColor clearColor]];
//    [menuButton setTitleColor:[UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f  blue:182.0f/255.0f  alpha:1] forState:UIControlStateNormal];
//    menuButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];    //addMotorCycle.png
//    menuButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);    //addMotorCycle.png
//    [footerView addSubview:menuButton];
//    return footerView;
    
    if (isAddContact == NO)
    {
    if (tableView==buddydetailTableView) {
        
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 40)];
        //   footerView.backgroundColor = [tableView backgroundColor];
        [footerView setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
        
        UIButton *menuButton =[[UIButton alloc] initWithFrame:CGRectMake(150,0, 140, 30 )];
        [menuButton addTarget:self action:@selector(addContactOnGroupClicked:) forControlEvents:UIControlEventTouchUpInside];
        [menuButton setBackgroundColor:[UIColor colorWithRed:1.0f/255.0f green:52.0f/255.0f blue:69.0f/255.0f alpha:1]];
        [menuButton setImage:[UIImage imageNamed:@"add_motorcycle.png"] forState:UIControlStateNormal];
        [[menuButton layer] setCornerRadius:4];
        
        [menuButton setTitle:[@"add Contact" uppercaseString] forState:UIControlStateNormal];
        [menuButton setTitleColor:[UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f  blue:182.0f/255.0f  alpha:1] forState:UIControlStateNormal];
        menuButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:12];    //addMotorCycle.png
        menuButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);    //addMotorCycle.png
        menuButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);    //addMotorCycle.png
        [footerView addSubview:menuButton];
        return footerView;
    }
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320, 40)];
    footerView.backgroundColor = [tableView backgroundColor];
    
    UIButton *menuButton =[[UIButton alloc] initWithFrame:CGRectMake(150,0, 150, 40 )];
    [menuButton addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"addMotorCycle.png"] forState:UIControlStateNormal];
    [menuButton setTitle:[isGroup?@"add group":@"add Contact" uppercaseString] forState:UIControlStateNormal];
    [menuButton setBackgroundColor:[UIColor clearColor]];
    [menuButton setTitleColor:[UIColor colorWithRed:182.0f/255.0f green:182.0f/255.0f  blue:182.0f/255.0f  alpha:1] forState:UIControlStateNormal];
    menuButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:10];    //addMotorCycle.png
    menuButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);    //addMotorCycle.png
    [footerView addSubview:menuButton];
        return footerView;
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView==buddydetailTableView) {
                return   UITableViewCellEditingStyleDelete;
    }
    else{
        if (isGroup) {
            return   UITableViewCellEditingStyleDelete;//(selectedBuddyIndex!=indexPath.row)?UITableViewCellEditingStyleDelete:UITableViewCellEditingStyleNone;;
        }else
            return  UITableViewCellEditingStyleDelete;

     }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (tableView==buddydetailTableView) {
            NSDictionary *groupBuddyDic = [groupBuddyListArr objectAtIndex:indexPath.row];

            
            NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerGroupId, @"OwnerGroupId",[groupBuddyDic stringValueForKey:@"BuddyId"], @"BuddyId",
                                     nil];
            
            //[NSArray arrayWithObject:infoDic]
            
            [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteBuddyFromGroup",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                
                //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                    
                    [CMHelper alertMessageWithText:@"Buddy deleted from Group successfully."];
                    [groupBuddyListArr removeObject:groupBuddyDic];
                    [buddydetailTableView reloadData];
                }
                
                
                
            }];

        }
        else {
 
        if (!isGroup) {
            
            NSDictionary *contactDic = [contactListArr objectAtIndex:indexPath.row];

            NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[contactDic stringValueForKey:@"BuddyId"], @"BuddyId",
                                     nil];
            
            //[NSArray arrayWithObject:infoDic]
            
            [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteBuddy",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                
                //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                    
                    [CMHelper alertMessageWithText:@"Buddy deleted successfully."];
                    [contactListArr removeObject:contactDic];
                    [buddyTableView reloadData];
                }
                
                
                
            }];

        }
        else {
            if (selectedBuddyIndex==indexPath.row) {
            }
            else
            {
                NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:indexPath.row];
                
                NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[buddyDic stringValueForKey:@"OwnerGroupId"], @"OwnerGroupId",
                                         nil];
                
                //[NSArray arrayWithObject:infoDic]
                
                [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteGroup",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    
                    
                    
                    //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    
                    if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                        NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                        
                        [CMHelper alertMessageWithText:@"Group deleted successfully."];
                        [buddyGroupArr removeObject:buddyDic];
                        [buddyTableView reloadData];
                    }
                    
                    
                    
                }];
            }
            
            
        }
        }
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
     if (tableView==buddydetailTableView) {
         
         return 30;
     }
    else
        if (isGroup) {
            if (selectedBuddyIndex==indexPath.row) {
                
                /*
                NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:indexPath.row];

               int count  = [[buddyDic stringValueForKey:@"Count"] length]?[[buddyDic stringValueForKey:@"Count"] integerValue]:0;
                 */
                
               int count  =  groupBuddyListArr.count;

                return  30*count+80;
            }
            else return  40;

        } return  80;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==buddydetailTableView ) {
        if(selectedBuddyIndex>=0){
            /*
        NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:selectedBuddyIndex];
        int count  = [[buddyDic stringValueForKey:@"Count"] length]?[[buddyDic stringValueForKey:@"Count"] integerValue]:0;
            */
            
            int count  =  groupBuddyListArr.count;

        
        return count;
        }
        return 0;
    }
    else
       return   isGroup?buddyGroupArr.count:contactListArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (isGroup) {

        if (tableView==buddydetailTableView) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupContactCell"];

            NSDictionary *groupBuddyDic = [groupBuddyListArr objectAtIndex:indexPath.row];

            
            
            UILabel *buddyName = (UILabel *)[cell viewWithTag:502];
            buddyName.text = [groupBuddyDic stringValueForKey:@"FName"];;//[NSString stringWithFormat:@"TestUser %d",indexPath.row];
            
            UILabel *buddyNickName = (UILabel *)[cell viewWithTag:503];
            buddyNickName.text =  [groupBuddyDic stringValueForKey:@"LName"];//[NSString stringWithFormat:@"TestNick %d",indexPath.row];

            
            return  cell;

            
        }
        
        
        NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:indexPath.row];

        if (selectedBuddyIndex==indexPath.row) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupDetailCell"];
            
            UILabel *groupName = (UILabel *)[cell viewWithTag:502];
            groupName.text = [buddyDic stringValueForKey:@"Name"];
            
            UILabel *groupCount = (UILabel *)[cell viewWithTag:503];
            groupCount.text = [buddyDic stringValueForKey:@"Count"];
            groupCount.text = [groupCount.text length]?groupCount.text:@"0";
            
            buddydetailTableView = (UITableView *)[cell viewWithTag:555];
            
            int count  =  groupBuddyListArr.count;

            CGRect frame =  buddydetailTableView.frame ;
            frame.size.height = 30*count+40;
            buddydetailTableView.frame = frame;
            [buddydetailTableView reloadData];
            return  cell;


        }
        

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupCell"];
        
        

        UILabel *groupName = (UILabel *)[cell viewWithTag:502];
        groupName.text = [buddyDic stringValueForKey:@"Name"];
        
        UILabel *groupCount = (UILabel *)[cell viewWithTag:503];
        groupCount.text = [buddyDic stringValueForKey:@"Count"];
        groupCount.text = [groupCount.text length]?groupCount.text:@"0";

        return  cell;

    }
    

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BuddyCell"];
    
    NSDictionary *contactDic = [contactListArr objectAtIndex:indexPath.row];

        
   
//buddyList
    
    UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    
    UILabel *labeName = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *nickName = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *cityLble = (UILabel *)[cell.contentView viewWithTag:4];
    
    
    UIButton *acceptBtn = (UIButton *)[cell.contentView viewWithTag:5];
    UIButton *rejectBtn = (UIButton *)[cell.contentView viewWithTag:6];
    
    
    labeName.text = [contactDic stringValueForKey:@"FName"];
    nickName.text = [contactDic stringValueForKey:@"LName"];;
    cityLble.text = [contactDic stringValueForKey:@"City"];;
    if ([[contactDic stringValueForKey:@"State"] length] &&  [cityLble.text length]) {
        cityLble.text  = [  cityLble.text  stringByAppendingFormat:@", %@",[contactDic stringValueForKey:@"State"]];
    }
    else
        cityLble.text = [contactDic stringValueForKey:@"State"];
    
    
      NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[contactDic stringValueForKey:@"PhotoPath"]];
    
      [placeholderImage setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"b1_Placeholder.png"]];
    
    if ([[contactDic stringValueForKey:@"Status"] isEqualToString:@"Pending"]) {
        acceptBtn.hidden = NO;
        acceptBtn.enabled = YES;

        if ([appOwnerId isEqualToString:[contactDic stringValueForKey:@"SenderId"]]) {
            [acceptBtn setTitle:@"Pending" forState:UIControlStateNormal];
            rejectBtn.hidden = YES;
            acceptBtn.enabled = NO;
        }
        else{
            [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
            [rejectBtn setTitle:@"Reject" forState:UIControlStateNormal];
            rejectBtn.hidden = NO;

        }
        
        
    }
   else  if ([[contactDic stringValueForKey:@"Status"] isEqualToString:@"Accept"]) {
       
       acceptBtn.hidden = NO;
       acceptBtn.enabled = YES;
       
       if ([appOwnerId isEqualToString:[contactDic stringValueForKey:@"SenderId"]]) {
           [acceptBtn setTitle:@"Pending" forState:UIControlStateNormal];
           rejectBtn.hidden = YES;
           acceptBtn.enabled = NO;
       }
       else{
           [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
           [rejectBtn setTitle:@"Reject" forState:UIControlStateNormal];
           rejectBtn.hidden = NO;
           
       }

   }
    else{
        acceptBtn.hidden = YES;
        rejectBtn.hidden = YES;
    }
    
    
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView==buddyTableView) {
        
        if(!isGroup){
//            CMBuddyProfileController *buddyProfile = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMBuddyProfileController"];
//            buddyProfile.buddyDetailDic = [contactListArr objectAtIndex:indexPath.row];
//            [self.navigationController pushViewController:buddyProfile animated:YES];
        
        }

  
    
    }
    
}


-(IBAction)showRiderTypePopUp:(id)sender{
    //NSArray  *
    
    if (!arryList)  arryList = [NSArray arrayWithObjects:@"Cruiser/Touring Rider",@"Sport Rider", @"Sport Touring Rider",@"Adventure Touring Rider",@"Off Road Rider",@"Scooter Rider", nil];
    
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Rider Type" withOption:arryList xy:riderTypeBtn.frame.origin size:CGSizeMake(300, MIN(200, arryList.count*50+50)) isMultiple:NO];
    
    
    
    
    
}


#pragma mark - Collapse Click Delegate

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    [self showPopUpWithTitle:popupTitle withOption:arrOptions xy:point size:size isMultiple:isMultiple withTagValue:0];
}
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple  withTagValue:(int)tagValue{
    
    if (!Dropobj)  Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    
    Dropobj.delegate = self;
    Dropobj.tag = tagValue;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    
    if (Dropobj.tag == 999) {
        [self addBuddyOnGroup:[mainContactList objectAtIndex:anIndex]];
    }
    else {
    [riderTypeBtn setTitle:[arryList objectAtIndex:anIndex] forState:UIControlStateNormal];
    selectedRidingID = anIndex;
    }
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}
- (void)DropDownListViewDidCancel{

}
-(void)addBuddyOnGroup:(NSDictionary *)buddyDic{
    
    
    
    
    
    //   NSString *ownerId   = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    
    // (Need to pass OwnerGroupId, BuddyId)
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerGroupId, @"OwnerGroupId",[buddyDic stringValueForKey:@"BuddyId"], @"BuddyId",@"0",@"GroupBuddyId",
                             nil];
    
    //[NSArray arrayWithObject:infoDic]
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateBuddytoGroup",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        
        
        //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** GetBuddySearchData %@ **",resultDict);
            
            [CMHelper alertMessageWithText:@"Buddy added to group successfully."];
            
//            int buddyCount = [[buddyDic stringValueForKey:@"Count"] intValue]+1;
//            [mainContactList replaceObjectAtIndex:selectedBuddyIndex withObject:buddyDic];
            
            [self getGroupForOwner];

            selectedBuddyIndex = -1;
            
            //InsertUpdateBuddytoGroup
            
            [buddyTableView reloadData];

        }
        
        
        
    }];
    
    
    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
 //   [Dropobj fadeOut];

    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    //Tim
    
    return YES;
}
- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.view endEditing:YES];
    
    
}

//implementation
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    
    
    return YES;
}

@end
