//
//  CMBuddySearchController.h
//  Cyclemate
//
//  Created by Rajesh on 10/8/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownListView.h"

@interface CMBuddySearchController : UIViewController<kDropDownListViewDelegate>{
    
    DropDownListView * Dropobj;
    NSArray  *arryList;
    IBOutlet  UIButton *riderTypeBtn;
    
    IBOutlet UIButton *allDealerBtn;
    IBOutlet UIButton *nwDealerBtn;
}
-(IBAction)showRiderTypePopUp:(id)sender;




-(IBAction)radioBtnClicked:(id)sender;

-(IBAction)backBtnClicked:(id)sender;
-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)topMenuBtnClicked:(id)sender;
@end
