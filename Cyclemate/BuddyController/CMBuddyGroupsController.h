//
//  CMBuddyGroupsController.h
//  Cyclemate
//
//  Created by Sourav on 29/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMBuddyGroupsController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray  *arrayGroup;
    NSArray *sectionTitleArray;
    NSString *ownerGroupId;
    
    int selectedBuddyIndex;
    BOOL isFromGroup;
    int currTopIndex;
    BOOL isGroup;

}

@end
