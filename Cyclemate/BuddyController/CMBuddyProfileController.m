//
//  CMBuddyProfileController.m
//  Cyclemate
//
//  Created by Sourav on 29/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMBuddyProfileController.h"
#import "CMBuddyGroupsController.h"

#define kTabBarHeight 0.0


@interface CMBuddyProfileController ()<UITextFieldDelegate>{
    BOOL keyboardIsShown;
    IBOutlet UIImageView *iconImageView;
    
    
    
    IBOutlet UITextField *nameTextField;
    IBOutlet UITextField *lastNameTextField;
    IBOutlet UITextField *addressTextField;
    IBOutlet UITextField *cityTextField;
    IBOutlet UITextField *stateTextField;
    
    IBOutlet UITextField *mobileTextField;
    
    IBOutlet UITextField *zipCodeTextField;
    IBOutlet UITextField *emailTextField;

}
-(IBAction)backBtnClicked:(id)sender;
-(IBAction)submitInfo:(id)sender;
-(IBAction)goToBuddyGroups:(id)sender;
-(IBAction)takePhoto:(id)sender;
-(IBAction)chooseFromGallery:(id)sender;


@property(nonatomic,strong)IBOutlet UIScrollView *scrollView;


@end

@implementation CMBuddyProfileController

@synthesize buddyDetailDic;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for (int i=201; i<213; i++) {
        
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        textField.delegate = self;
    }
    
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
    //make contentSize bigger than your scrollSize (you will need to figure out for your own use case)
   // CGSize scrollContentSize = CGSizeMake(320, 498);
    //self.scrollView.contentSize = scrollContentSize;
    
    [CMAppDelegate setNavigationBarTitle:@"BUDDY CONTACTS" navigationItem:self.navigationItem];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:BuddyBootomTabBar]];
    
    _scrollView.contentSize = CGSizeMake(320, 610);
    
    NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[buddyDetailDic stringValueForKey:@"PhotoPath"]];
    
    
    
    [iconImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"b1_Placeholder.png"]];
    
    nameTextField.text     = [buddyDetailDic stringValueForKey:@"FName"];
    lastNameTextField.text = [buddyDetailDic stringValueForKey:@"LName"];
    addressTextField.text = [buddyDetailDic stringValueForKey:@"Address"];
    
    cityTextField.text = [buddyDetailDic stringValueForKey:@"City"];
    stateTextField.text = [buddyDetailDic stringValueForKey:@"State"];
    mobileTextField.text = [buddyDetailDic stringValueForKey:@"Mobile"];
    zipCodeTextField.text = [buddyDetailDic stringValueForKey:@"Zip"];
    emailTextField.text = [buddyDetailDic stringValueForKey:@"Email"];

    NSLog(@"buddyDetailDic = %@",buddyDetailDic);
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height += (keyboardSize.height - kTabBarHeight);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height -= (keyboardSize.height - kTabBarHeight);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}

#pragma mark
#pragma mark IBAction methods

-(IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)submitInfo:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];

}
-(IBAction)goToBuddyGroups:(id)sender{
 
    CMBuddyGroupsController *buddyGropus = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMBuddyGroupsController"];
    [self.navigationController pushViewController:buddyGropus animated:YES];

}


#pragma mark
#pragma mark UITextFieldDelaget Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)takePhoto:(id)sender{
    
    
    if (![UIImagePickerController isSourceTypeAvailable:IMAGESOURSETYPE]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error !!"
                                                              message:@"Current Device does not support the  camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        return;
        
    }
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    picker.sourceType = IMAGESOURSETYPE;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

-(IBAction)chooseFromGallery:(id)sender{

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = (id)self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

#pragma mark UIImagePickerControllerDelegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    iconImageView.image = info[UIImagePickerControllerEditedImage];
    
    
    NSData *pngData = UIImagePNGRepresentation(iconImageView.image);
    
    NSString *filePath = [self documentsPathForFileName:@"icon.png"]; //Add the file name
    
    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    //NSString *filePath = [documentsPath stringByAppendingPathComponent:@"user.png"]; //Add the file name
    [pngData writeToFile:filePath atomically:YES]; //Write the file
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark
#pragma mark Private methods

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}






@end
