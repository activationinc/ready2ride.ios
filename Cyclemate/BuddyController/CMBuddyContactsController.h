//
//  CMBuddyContactsController.h
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//
#import "UIViewController+MJPopupViewController.h"

#import <UIKit/UIKit.h>

#import "DropDownListView.h"
#import "GAITrackedViewController.h"
@interface CMBuddyContactsController : GAITrackedViewController<kDropDownListViewDelegate>{
    
    NSMutableArray *mainContactList;
    IBOutlet UISegmentedControl *segmentedControl;
    IBOutlet UIView *segmentView;
    
    
    IBOutlet UIView *searchView;
    IBOutlet UIView *viewWithTable;
    
    BOOL isGroup;
    IBOutlet UILabel *bottomLbl;
    
    IBOutlet UIButton *contactBtn;
    IBOutlet UIButton *searchBtn;
    IBOutlet UIButton *groupBtn;

    NSMutableArray *searchResultArr;
        
        DropDownListView * Dropobj;
        NSArray  *arryList;
        IBOutlet  UIButton *riderTypeBtn;
        
        IBOutlet UIButton *allDealerBtn;
        IBOutlet UIButton *nwDealerBtn;
    
    int selectedBuddyIndex;
    BOOL isFromGroup;
     NSString *ownerGroupId;
    
    int currTopIndex;
    BOOL isAddContact;
}
-(IBAction)showRiderTypePopUp:(id)sender;
-(IBAction)radioBtnClicked:(id)sender;
-(IBAction)bottomViewBtnClicked:(id)sender;

-(IBAction)backBtnClicked:(id)sender;
-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)topMenuBtnClicked:(id)sender;


-(IBAction)buddyBottomBarClicked:(id)sender;

-(IBAction)selectAllBuddyClicked:(id)sender;

-(IBAction)segmentValueChangesClicked:(id)sender;

-(IBAction)buddyReqBtnClicked:(id)sender;

-(IBAction)groupDropDwnBtnClicked:(id)sender;
-(IBAction)groupUpBtnClicked:(id)sender;
-(IBAction)buddySearchBtnClicked:(id)sender;

@property(nonatomic)BOOL isFromHome;

@end
