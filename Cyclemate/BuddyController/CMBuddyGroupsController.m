//
//  CMBuddyGroupsController.m
//  Cyclemate
//
//  Created by Sourav on 29/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMBuddyGroupsController.h"

@interface CMBuddyGroupsController ()<UITextFieldDelegate>
{
    UIAlertView *gropuAlertView;
    
    NSMutableArray *buddyList;
    
    
    NSMutableArray *contactListArr;
    
    NSMutableArray *buddyGroupArr;
    NSMutableArray *groupBuddyListArr;
    
    
    IBOutlet UIButton *selectAllBtn;
    BOOL isAllSelected;
    IBOutlet UITableView *buddyTableView;
    
    UITableView *buddydetailTableView;
    
    IBOutlet UITextField *lastNmeField;
    IBOutlet UITextField *zipCodeField;
    IBOutlet UITextField *stateField;
    int selectedRidingID;
    
    NSString *appOwnerId;
    UIRefreshControl *refreshControl;
}
-(IBAction)backBtnClicked:(id)sender;

@property (nonatomic,strong)NSArray *rideTypeArrays;
@property (nonatomic,strong)NSArray *rideMemberArray;





@end

@implementation CMBuddyGroupsController

- (void)viewDidLoad {
    selectedBuddyIndex = -1;
    appOwnerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:BikerBootomTabBar]];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"MY GROUPS" navigationItem:self.navigationItem];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:BuddyBootomTabBar]];
    [self initialization];
}
-(void)initialization
{
    groupBuddyListArr = [[NSMutableArray alloc]init];
    contactListArr = [NSMutableArray new];
    groupBuddyListArr = [NSMutableArray new];
    buddyGroupArr = [NSMutableArray new];//[[NSArray arrayWithObjects:@"Bobby 123",@"Speed Reed",@"Susan444",@"TourBill", nil] mutableCopy];
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshContactData) forControlEvents:UIControlEventValueChanged];
    [buddyTableView addSubview:refreshControl];
    isGroup = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark
#pragma mark IBAction methods

-(IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark
#pragma mark UITextfieldDelegate Methods methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField becomeFirstResponder];
    return YES;
}
-(IBAction)addContactOnGroupClicked:(id)sender{
   
        gropuAlertView = [[UIAlertView alloc]
                           initWithTitle:@"Group name"
                           message:@"Please enter your Group name"
                           delegate:self
                           cancelButtonTitle:@"Cancel"
                           otherButtonTitles:@"Add", nil];
        [gropuAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        /* Display a numerical keypad for this text field */
        UITextField *textField = [gropuAlertView textFieldAtIndex:0];
        textField.keyboardType = UIKeyboardTypeDefault;
    [gropuAlertView show];
}
-(IBAction)deleteOnGroupMemberClicked:(id)sender
{
}

-(void)getContactListForOwner{
    
    NSString *ownerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    NSString *stringURL;// =  [NSString stringWithFormat:@"%@/GetBuddyByUserId/UserID=%@",Service_URL,ownerId];
    
    stringURL =  [NSString stringWithFormat:@"%@/GetBuddyContactsData/OwnerId=%@",Service_URL,ownerId];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetBuddySearchData %@ **",resultDict);
            
            // NSArray *arr = [resultDict objectForKey:@"GetBuddyByUserIdResult"];//GetBuddyContactsDataResult
            NSArray *arr = [resultDict objectForKey:@"GetBuddyContactsDataResult"];//GetBuddyContactsDataResult
            
            if ([arr count]) {
                [contactListArr removeAllObjects];
            }
            for (id obj in arr  ) {
                [contactListArr addObject:obj];
            }
            //GetGroupBuddyByOwnerIDResult
            [buddyTableView reloadData];
            
        }
    }];
    
    
}
-(void)getGroupBuddyListForOwner:(NSString *)withGroupId{
    
    NSString *ownerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    ownerGroupId = withGroupId;
    NSString *stringURL =  [NSString stringWithFormat:@"%@/GetBuddyByOwnerGroupId/OwnerGroupId=%@/OwnerId=%@",Service_URL,withGroupId,ownerId];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetBuddyByOwnerGroupIdResult %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetBuddyByOwnerGroupIdResult"];
            
            if ([arr count]) {
                [groupBuddyListArr removeAllObjects];
            }
            for (id obj in arr  ) {
                [groupBuddyListArr addObject:obj];
            }
            //GetGroupBuddyByOwnerIDResult
            [buddyTableView reloadData];
            
            [buddydetailTableView reloadData];
            
        }
    }];
    
}

-(IBAction)getGroupForOwner{
    
    
    NSString *ownerId   = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    NSString *stringURL =  [NSString stringWithFormat:@"%@/GetGroupBuddyByOwnerID/OwnerId=%@",Service_URL,ownerId];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [refreshControl endRefreshing];
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            
            
            NSLog(@"*** GetBuddySearchData %@ **",resultDict);
            
            NSArray *arr = [resultDict objectForKey:@"GetGroupBuddyByOwnerIDResult"];
            
            if ([arr count]) {
                [buddyGroupArr removeAllObjects];
            }
            
            
            for (id obj in arr  ) {
                [buddyGroupArr addObject:obj];
            }
            //GetGroupBuddyByOwnerIDResult
            [buddyTableView reloadData];
            
        }
    }];
    
    
    
    
}

-(void)goToBuddyGroups{
    
    CMBuddyGroupsController *buddyGropus = [[CMStoryBoard storyboardWithName:CMBuddy] instantiateViewControllerWithIdentifier:@"CMBuddyGroupsController"];
    [self.navigationController pushViewController:buddyGropus animated:YES];
    
}

-(IBAction)groupUpBtnClicked:(id)sender;
{
    
    selectedBuddyIndex = -1;
    
    [buddyTableView reloadData];
    
    
}

-(IBAction)groupDropDwnBtnClicked:(id)sender;
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:buddyTableView];
    NSIndexPath *indexPath = [buddyTableView indexPathForRowAtPoint:buttonPosition];
    NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:indexPath.row];
    [self getGroupBuddyListForOwner:[buddyDic stringValueForKey:@"OwnerGroupId"]];
    selectedBuddyIndex = indexPath.row;
    
    [buddyTableView reloadData];
    
    
}

-(void)refreshContactData{
    
    
    if (currTopIndex==0) {
        [self getContactListForOwner];
        
    }else
        [self getGroupForOwner];
}

-(void)viewWillAppear:(BOOL)animated{
    currTopIndex  =0;
    
    
    [self getContactListForOwner];
    [self getGroupForOwner];
}

- (void)viewWillDisappear:(BOOL)animated{
    [buddyTableView setEditing:NO];
    [buddydetailTableView setEditing:NO];
    
    
}
- (void)buttonTouch:(UIButton *)button withEvent:(UIEvent *)event
{
    if (button == selectAllBtn) {
        button.selected = !button.selected;
        isAllSelected = !isAllSelected;
        [buddyTableView reloadData];
        
    }else{
        button.selected = !button.selected;
        [selectAllBtn setSelected:NO];
        isAllSelected = NO;
        
    }
}

#pragma mark - UIAlertViewDelegate
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            if ([textField.text length]) {
                
                
                // (Need to pass OwnerGroupId, OwnerId AND Name(Name of group))
                
                
                
                NSString *ownerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                
                
                NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         @"0", @"OwnerGroupId",
                                         ownerId, @"OwnerId",
                                         textField.text, @"Name",
                                         nil];
                
                
                [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateGroupOwner",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    
                    if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                        // [self getGroupForOwner];
                        
                        NSLog(@"*** GetBuddyByOwnerGroupIdResult %@ **",resultDict);
                        
                        NSMutableDictionary *dic = [NSMutableDictionary new];
                        [dic setObject:textField.text forKey:@"Name"];
                        [dic setObject:[resultDict stringValueForKey:@"response"] forKey:@"OwnerGroupId"];
                        [dic setObject:@"1" forKey:@"Count"];
                        [dic setObject:ownerId forKey:@"OwnerId"];
                        [buddyGroupArr addObject:dic];
                        
                        [buddyTableView reloadData];
                        
                    }
                }];
                
                
            }
        }
    }
    
}


#pragma mark -
#pragma mark UITableView Method Implementation Call

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 40;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView==buddydetailTableView) {
        return   UITableViewCellEditingStyleDelete;
    }
    else{
        if (isGroup) {
            return   UITableViewCellEditingStyleDelete;//(selectedBuddyIndex!=indexPath.row)?UITableViewCellEditingStyleDelete:UITableViewCellEditingStyleNone;;
        }else
            return  UITableViewCellEditingStyleDelete;
        
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (tableView==buddydetailTableView) {
            NSDictionary *groupBuddyDic = [groupBuddyListArr objectAtIndex:indexPath.row];
            
            
            NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerGroupId, @"OwnerGroupId",[groupBuddyDic stringValueForKey:@"BuddyId"], @"BuddyId",
                                     nil];
            
            //[NSArray arrayWithObject:infoDic]
            
            [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteBuddyFromGroup",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                
                //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                    
                    [CMHelper alertMessageWithText:@"Buddy deleted from Group successfully."];
                    [groupBuddyListArr removeObject:groupBuddyDic];
                    [buddydetailTableView reloadData];
                }
                
                
                
            }];
            
        }
        else {
            
            if (!isGroup) {
                
                NSDictionary *contactDic = [contactListArr objectAtIndex:indexPath.row];
                
                NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[contactDic stringValueForKey:@"BuddyId"], @"BuddyId",
                                         nil];
                
                //[NSArray arrayWithObject:infoDic]
                
                [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteBuddy",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    
                    
                    
                    //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                    
                    if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                        NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                        
                        [CMHelper alertMessageWithText:@"Buddy deleted successfully."];
                        [contactListArr removeObject:contactDic];
                        [buddyTableView reloadData];
                    }
                    
                    
                    
                }];
                
            }
            else {
                if (selectedBuddyIndex==indexPath.row) {
                }
                else
                {
                    NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:indexPath.row];
                    
                    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[buddyDic stringValueForKey:@"OwnerGroupId"], @"OwnerGroupId",
                                             nil];
                    
                    //[NSArray arrayWithObject:infoDic]
                    
                    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/DeleteGroup",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
                        
                        
                        
                        //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                        
                        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                            NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                            
                            [CMHelper alertMessageWithText:@"Group deleted successfully."];
                            [buddyGroupArr removeObject:buddyDic];
                            [buddyTableView reloadData];
                        }
                        
                        
                        
                    }];
                }
                
                
            }
        }
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView==buddydetailTableView) {
        
        return 30;
    }
    else
        if (isGroup) {
            if (selectedBuddyIndex==indexPath.row) {
                
                /*
                 NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:indexPath.row];
                 
                 int count  = [[buddyDic stringValueForKey:@"Count"] length]?[[buddyDic stringValueForKey:@"Count"] integerValue]:0;
                 */
                
                int count  =  groupBuddyListArr.count;
                
                return  30*count+80;
            }
            else return  40;
            
        } return  80;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==buddydetailTableView ) {
        if(selectedBuddyIndex>=0){
            /*
             NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:selectedBuddyIndex];
             int count  = [[buddyDic stringValueForKey:@"Count"] length]?[[buddyDic stringValueForKey:@"Count"] integerValue]:0;
             */
            
            int count  =  groupBuddyListArr.count;
            
            
            return count;
        }
        return 0;
    }
    else
        return   isGroup?buddyGroupArr.count:contactListArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (isGroup) {
        
        if (tableView==buddydetailTableView) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupContactCell"];
            
            NSDictionary *groupBuddyDic = [groupBuddyListArr objectAtIndex:indexPath.row];
            
            
            
            UILabel *buddyName = (UILabel *)[cell viewWithTag:502];
            buddyName.text = [groupBuddyDic stringValueForKey:@"FName"];;//[NSString stringWithFormat:@"TestUser %d",indexPath.row];
            
            UILabel *buddyNickName = (UILabel *)[cell viewWithTag:503];
            buddyNickName.text =  [groupBuddyDic stringValueForKey:@"LName"];//[NSString stringWithFormat:@"TestNick %d",indexPath.row];
            
            
            return  cell;
            
            
        }
        
        
        NSDictionary *buddyDic = [buddyGroupArr objectAtIndex:indexPath.row];
        
        if (selectedBuddyIndex==indexPath.row) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupDetailCell"];
            
            UILabel *groupName = (UILabel *)[cell viewWithTag:502];
            groupName.text = [buddyDic stringValueForKey:@"Name"];
            
            UILabel *groupCount = (UILabel *)[cell viewWithTag:503];
            groupCount.text = [buddyDic stringValueForKey:@"Count"];
            groupCount.text = [groupCount.text length]?groupCount.text:@"0";
            
            buddydetailTableView = (UITableView *)[cell viewWithTag:555];
            
            int count  =  groupBuddyListArr.count;
            
            CGRect frame =  buddydetailTableView.frame ;
            frame.size.height = 30*count+40;
            buddydetailTableView.frame = frame;
            [buddydetailTableView reloadData];
            return  cell;
            
            
        }
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupCell"];
        
        
        
        UILabel *groupName = (UILabel *)[cell viewWithTag:502];
        groupName.text = [buddyDic stringValueForKey:@"Name"];
        
        UILabel *groupCount = (UILabel *)[cell viewWithTag:503];
        groupCount.text = [buddyDic stringValueForKey:@"Count"];
        groupCount.text = [groupCount.text length]?groupCount.text:@"0";
        
        return  cell;
        
    }
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BuddyCell"];
    
    NSDictionary *contactDic = [contactListArr objectAtIndex:indexPath.row];
    
    
    
    //buddyList
    
    UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    
    UILabel *labeName = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *nickName = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *cityLble = (UILabel *)[cell.contentView viewWithTag:4];
    
    
    UIButton *acceptBtn = (UIButton *)[cell.contentView viewWithTag:5];
    UIButton *rejectBtn = (UIButton *)[cell.contentView viewWithTag:6];
    
    
    labeName.text = [contactDic stringValueForKey:@"FName"];
    nickName.text = [contactDic stringValueForKey:@"LName"];;
    cityLble.text = [contactDic stringValueForKey:@"City"];;
    if ([[contactDic stringValueForKey:@"State"] length] &&  [cityLble.text length]) {
        cityLble.text  = [  cityLble.text  stringByAppendingFormat:@", %@",[contactDic stringValueForKey:@"State"]];
    }
    else
        cityLble.text = [contactDic stringValueForKey:@"State"];
    
    
    NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[contactDic stringValueForKey:@"PhotoPath"]];
    
    [placeholderImage setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"b1_Placeholder.png"]];
    
    if ([[contactDic stringValueForKey:@"Status"] isEqualToString:@"Pending"]) {
        acceptBtn.hidden = NO;
        acceptBtn.enabled = YES;
        
        if ([appOwnerId isEqualToString:[contactDic stringValueForKey:@"SenderId"]]) {
            [acceptBtn setTitle:@"Pending" forState:UIControlStateNormal];
            rejectBtn.hidden = YES;
            acceptBtn.enabled = NO;
        }
        else{
            [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
            [rejectBtn setTitle:@"Reject" forState:UIControlStateNormal];
            rejectBtn.hidden = NO;
            
        }
        
        
    }
    else  if ([[contactDic stringValueForKey:@"Status"] isEqualToString:@"Accept"]) {
        
        acceptBtn.hidden = NO;
        acceptBtn.enabled = YES;
        
        if ([appOwnerId isEqualToString:[contactDic stringValueForKey:@"SenderId"]]) {
            [acceptBtn setTitle:@"Pending" forState:UIControlStateNormal];
            rejectBtn.hidden = YES;
            acceptBtn.enabled = NO;
        }
        else{
            [acceptBtn setTitle:@"Accept" forState:UIControlStateNormal];
            [rejectBtn setTitle:@"Reject" forState:UIControlStateNormal];
            rejectBtn.hidden = NO;
            
        }
        
    }
    else{
        acceptBtn.hidden = YES;
        rejectBtn.hidden = YES;
    }
    
    
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}



-(void)addBuddyOnGroup:(NSDictionary *)buddyDic{
    
    //   NSString *ownerId   = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    
    // (Need to pass OwnerGroupId, BuddyId)
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerGroupId, @"OwnerGroupId",[buddyDic stringValueForKey:@"BuddyId"], @"BuddyId",@"0",@"GroupBuddyId",
                             nil];
    
    //[NSArray arrayWithObject:infoDic]
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateBuddytoGroup",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        
        
        //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** GetBuddySearchData %@ **",resultDict);
            
            [CMHelper alertMessageWithText:@"Buddy added to group successfully."];
            
            //            int buddyCount = [[buddyDic stringValueForKey:@"Count"] intValue]+1;
            //            [mainContactList replaceObjectAtIndex:selectedBuddyIndex withObject:buddyDic];
            
            [self getGroupForOwner];
            
            selectedBuddyIndex = -1;
            
            //InsertUpdateBuddytoGroup
            
            [buddyTableView reloadData];
            
        }
        
        
        
    }];
    
    
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    //Tim
    
    return YES;
}
- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.view endEditing:YES];
    
    
}

//implementation
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}



@end
