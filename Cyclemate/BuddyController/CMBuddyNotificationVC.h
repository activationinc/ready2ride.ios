//
//  CMBuddyNotificationVC.h
//  Cyclemate
//
//  Created by Rajesh on 10/8/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMBuddyNotificationVC : UIViewController
-(IBAction)backBtnClicked:(id)sender;
-(IBAction)topleftBtnClicked:(id)sender;
-(IBAction)topMenuBtnClicked:(id)sender;

@property(nonatomic)BOOL isFromHome;
@end
