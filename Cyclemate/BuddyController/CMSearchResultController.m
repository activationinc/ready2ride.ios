//
//  CMSearchResultController.m
//  Cyclemate
//
//  Created by Rajesh on 1/16/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import "CMSearchResultController.h"
#import "UIAlertView+MKBlockAdditions.h"

@interface CMSearchResultController ()

@end

@implementation CMSearchResultController

@synthesize searchResultArr;
@synthesize isFromGroup;
@synthesize ownerGroupId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"BUDDY SEARCH RESULTS" navigationItem:self.navigationItem];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:HomeBootomTabBar]];

}
#pragma mark -
#pragma mark UITableView Method Implementation Call


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return   UITableViewCellEditingStyleDelete;
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //tableViewList.tag = indexPath.section;
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   return  50;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
           return    searchResultArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UITableViewCell *cell = nil;
    //BuddyContactCell
    NSDictionary *dic = [searchResultArr objectAtIndex:indexPath.row];

    if ([[dic stringValueForKey:@"status"] intValue]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"BuddyContactCell"];
    }else
        cell = [tableView dequeueReusableCellWithIdentifier:@"BuddySearchCell"];
    
    UILabel *labeName = (UILabel *)[cell viewWithTag:1];
    
    UILabel *labeCity = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel *labeState = (UILabel *)[cell.contentView viewWithTag:3];
       
    labeName.text = [NSString stringWithFormat:@"%@ %@",[dic stringValueForKey:@"FName"],[dic stringValueForKey:@"LName"]];
    labeCity.text = [dic stringValueForKey:@"City"];
    labeState.text = [dic stringValueForKey:@"State"];
    
    
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isFromGroup) {
        [self addBuddyOnGroup:[searchResultArr objectAtIndex:indexPath.row]];
    }
    else
       [self addBuddy:[searchResultArr objectAtIndex:indexPath.row]];
}

-(void)addBuddyOnGroup:(NSDictionary *)buddyDic{
    
    
    
    
    

    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerGroupId, @"OwnerGroupId",[buddyDic stringValueForKey:@"OwnerId"], @"BuddyId",@"0",@"GroupBuddyId",
                             nil];
    
   //[NSArray arrayWithObject:infoDic]
    
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateBuddytoGroup",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        
        
        //[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** GetBuddySearchData %@ **",resultDict);
            
            [CMHelper alertMessageWithText:@"Buddy request send successful."];
            
        }
        
        
        
    }];
    
    
    
    
}
-(void)addBuddy:(NSDictionary *)buddyDic{
    
    NSString *message =   [NSString stringWithFormat:@"Do you want to send buddy request to %@ %@?",[buddyDic stringValueForKey:@"FName"],[buddyDic stringValueForKey:@"LName"]];



    
    [[UIAlertView alertViewWithTitle:@"Alert"
                             message:message cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Send"]
                           onDismiss:^(int buttonIndex) {
                               
                               
                               NSString *ownerId   = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                               
                               
                               NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:ownerId, @"SenderId",[buddyDic stringValueForKey:@"OwnerId"], @"ReceiverId", @"0", @"BuddyId",@"P",@"Status",
                                                        nil];
                               
                               [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateBuddy",Service_URL] param:[NSArray arrayWithObject:infoDic] requestHandler:^(NSDictionary *resultDict, NSError *error) {
                                
                               
                                
                        
                                   
                                   if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                                       NSLog(@"*** GetBuddySearchData %@ **",resultDict);
                                       
                                       [CMHelper alertMessageWithText:@"Buddy request has been sent successfully."];
                                       
                                   }
                                   
                                   
                                   
                               }];
                           } onCancel:^{
                               
                               
                           }] show];
    
    
    
    
     
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
