//
//  CMBuddySearchController.m
//  Cyclemate
//
//  Created by Rajesh on 10/8/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMBuddySearchController.h"

@interface CMBuddySearchController ()

@end

@implementation CMBuddySearchController

-(IBAction)radioBtnClicked:(id)sender{
    
    
    
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];


    [allDealerBtn setImage:redioInActiveImage forState:UIControlStateNormal];
    [nwDealerBtn setImage:redioInActiveImage forState:UIControlStateNormal];
    
    [sender setImage:redioActiveImage forState:UIControlStateNormal];
}

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)topleftBtnClicked:(id)sender{
    
}


-(IBAction)topMenuBtnClicked:(id)sender{
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:BuddyBootomTabBar]];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    
    [CMAppDelegate setNavigationBarTitle:@"BUDDY SEARCH" navigationItem:self.navigationItem];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)showRiderTypePopUp:(id)sender{
    //NSArray  *
    arryList = [NSArray arrayWithObjects:@"Cruiser/Touring Rider",@"Sport Rider", @"Sport Touring Rider",@"Adventure Touring Rider",@"Off Road Rider",@"Scooter Rider", nil];
    
    
    
    [Dropobj fadeOut];
   // [self showPopUpWithTitle:@"Select Service Category" withOption:arryList xy:riderTypeBtn.frame.origin size:CGSizeMake(320, 200) isMultiple:NO];
    
    [self showPopUpWithTitle:@"Select Service Category" withOption:arryList xy:riderTypeBtn.frame.origin size:CGSizeMake(320, MIN(200, arryList.count*50+50)) isMultiple:NO];

    
}

#pragma mark - Collapse Click Delegate



-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    [self showPopUpWithTitle:popupTitle withOption:arrOptions xy:point size:size isMultiple:isMultiple withTagValue:0];

    
    
  
    
}
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple  withTagValue:(int)tagValue{
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    Dropobj.tag = tagValue;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
}


- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    [riderTypeBtn setTitle:[arryList objectAtIndex:anIndex] forState:UIControlStateNormal];
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}

- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}




@end
