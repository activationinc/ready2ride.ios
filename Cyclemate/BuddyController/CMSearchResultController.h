//
//  CMSearchResultController.h
//  Cyclemate
//
//  Created by Rajesh on 1/16/15.
//  Copyright (c) 2015 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMSearchResultController : UIViewController
@property(nonatomic,retain)NSArray *searchResultArr;
@property(nonatomic)BOOL isFromGroup;
@property(nonatomic,retain)NSString *ownerGroupId;


@end
