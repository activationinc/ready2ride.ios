//
//  CMBuddyNotificationVC.m
//  Cyclemate
//  Created by Rajesh on 10/8/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMBuddyNotificationVC.h"

@interface CMBuddyNotificationVC ()
{
    NSArray *bikeTitleArr;
    NSArray *bikeDesArr;
    NSArray *bikeImgeArr;


}
@end

@implementation CMBuddyNotificationVC

@synthesize isFromHome;

-(IBAction)backBtnClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)topleftBtnClicked:(id)sender{
    
}


-(IBAction)topMenuBtnClicked:(id)sender{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.view addSubview:[UIAppDelegate loadBottomView:BikerBootomTabBar]];
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];
    [CMAppDelegate setNavigationBarTitle:@"BUDDY CONTACTS" navigationItem:self.navigationItem];
    [self.navigationController.view  addSubview:[UIAppDelegate loadBottomView:(isFromHome?HomeBootomTabBar:BuddyBootomTabBar)]];

    bikeTitleArr = [NSArray arrayWithObjects:@"Honda 595",@"Harley-Davidson Softail",@"Kawasaki ZX10R",@"KTM 640 Adventure",@"Yamaha FJR 1300", nil];
    bikeDesArr = [NSArray arrayWithObjects:@"A standard motorcycle with neutral ergonomics",@"A cruiser motorcycle for a relaxed ride",@"A sports motorcycle built for speed and handling",@"A duel sport motorcycle for on and off road",@"A tourer motorcycle built for long rides on and open road", nil];
    bikeImgeArr = [NSArray arrayWithObjects:@"honda595",@"Harley-davidson-softail",@"Kawasaki-zx10r",@"KTM-640-adventure",@"Yamaha-fjr-1300", nil];


}

#pragma mark -
#pragma mark UITableView Method Implementation Call


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
    //return (tableView==venuListTableView)?80:120;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    
    return 1;
    
    
    //  return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return bikeTitleArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BikeCell"];
    
    UIImageView *placeholderImage = (UIImageView *)[cell viewWithTag:1];
    
    UILabel *labeName = (UILabel *)[cell viewWithTag:2];
    UILabel *reviewText = (UILabel *)[cell viewWithTag:3];
    UILabel *timeLable = (UILabel *)[cell viewWithTag:4];
    
    
    UIImageView *messageImage = (UIImageView *)[cell viewWithTag:100];
    UIImageView *addReqImage  = (UIImageView *)[cell viewWithTag:101];
    UIImageView *reqFlagImage = (UIImageView *)[cell viewWithTag:102];

    
    
    
   
    
    labeName.text  = [bikeTitleArr objectAtIndex:indexPath.row];// [NSString stringWithFormat:@"Motorcycle- %ld",(long)indexPath.row];//[tempDic stringValueForKey:@"user_name"];
    reviewText.text =  [bikeDesArr objectAtIndex:indexPath.row];// @"description";//[tempDic stringValueForKey:@"review_text"];
    //timeLable.text =    [bikeImgeArr objectAtIndex:indexPath.row];// @"price";//[tempDic stringValueForKey:@"time"];
    
    placeholderImage.image =  [UIImage imageNamed:[bikeImgeArr objectAtIndex:indexPath.row]];
    
    //[placeholderImage setImageWithURL:[NSURL URLWithString:[tempDic stringForKeyFromJson:@"user_photo"]] placeholderImage:[UIImage imageNamed:@"photo_place_holder.png"]];
    
    
    return  cell;
    
}
//
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:IS_IOS7?[UIColor groupTableViewBackgroundColor ]:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1]];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
