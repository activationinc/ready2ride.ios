//
//  CMAppDelegate.m
//  Cyclemate
//
//  Created by Rajesh on 9/23/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//
#define LIB_DIR_PATH    NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0]

#import "CMNewRideSetUpVC.h"

#include "CMSpecialController.h"
#include "CMEventsController.h"
#import "CMNewBikeInfoController.h"
#import "CMSaveRideController.h"
#import "CMParkBikeController.h"
#import "CMBuddyContactsController.h"
#import "CMWeatherAlertVC.h"
#import "KxMenu.h"

#import "CMMessageViewController.h"
#import "CMBikeInfoController.h"
#import "CMBikeViewController.h"
#import "CMSupportController.h"
#define kFONT_BentonSans_Medium @"BentonSans-Medium"

#import "CMAppDelegate.h"
#import "CMDirectDealerMailVC.h"
#import "UIViewController+MJPopupViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAILogger.h"
#import "GAIFields.h"
#import "GAITrackedViewController.h"
#import "Mixpanel/Mixpanel.h"
static NSString *apiKey = @"eJwVwckNACAIALC3w5BwqOhTEJYy7m5sqRB-jUctR7B3kZwQZgYqpBCME6puD_KVzus-D-ILPQ";


/******* Set your tracking ID here *******/
static NSString *const kTrackingId = @"UA-80658429-4";
//static NSString *const kTrackingId = @" ";
static NSString *const kAllowTracking = @"allowTracking";

@implementation CMAppDelegate

@synthesize locationRequestID;
@synthesize rearVC;

@synthesize userDefault;


@synthesize dateFormatter;

@synthesize isRidePermission;
@synthesize isAddBikeFlg;

- (UIBarButtonItem*)backBarButtonItem:(id)controller
{
    static UIBarButtonItem *backButton;
    
    if (IR_IS_SYSTEM_VERSION_LESS_THAN_7_0){
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [button setImage:[UIImage imageNamed:@"Back_Button.png"] forState:UIControlStateNormal];
        [button addTarget:controller action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
        backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    else{
        backButton =  [[UIBarButtonItem alloc]
                       initWithImage:[[UIImage imageNamed:@"Back_Button.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
                       target:controller
                       action:@selector(popViewControllerAnimated:)];
    }
    
    
    
    return backButton;
}
+(void)setNavigationBarTitle:(NSString*)title navigationItem:(UINavigationItem*)navigationItem{
    UILabel *titleLabel = (UILabel*)[navigationItem.titleView viewWithTag:100];
    if (!titleLabel) {
        UILabel *titleLabel = (UILabel*)[navigationItem.titleView viewWithTag:100];
        if (!titleLabel) {
            UIView *titleView = [[UIView alloc] initWithFrame:navigationItem.titleView.frame];
            // UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-115, -20, 194, 40)];
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-95, -20, 194, 40)];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.numberOfLines  = 0;
            titleLabel.font = [UIFont systemFontOfSize:12];
            
            titleLabel.textAlignment = NSTextAlignmentCenter;//NSTextAlignmentLeft;
            titleLabel.tag = 100;
            titleLabel.text = [title uppercaseString];
            [titleView addSubview:titleLabel];
            navigationItem.titleView = titleView;
        }
        else {
            titleLabel.text = [title uppercaseString];
        }
    }
    else {
        titleLabel.text = [title uppercaseString];
    }
    
    
}


-(void)phoneNumberClicked:(id)sender {
    //  UIButton *button = (UIButton *)sender;
    //currentIndexPath = [detailsList indexPathForCell:(UITableViewCell *)[(UIButton *)sender superview]];
    if([[[UIDevice currentDevice]model] isEqualToString:@"iPhone"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Do You Want to Call Your Dealer – %@?",[self getDealerInfo:@"OrganizationName"]] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        [alert show];
        
    }
    else {
        UIAlertView *dialmsg=[[UIAlertView alloc]initWithTitle:kAPPLICATION_NAME message:@"This feature is not available on your device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [dialmsg show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    
    if([(NSString *)[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Yes"]) {
        //   [self makePhoneCall:@"8169428900"];
        [self makePhoneCall:[self getDealerInfo:@"Phone"]];
        
        //getDealerPhoneNO
    }
    
}

-(void)makePhoneCall:(NSString*)phoneNumber{
    
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:kTrackingId];
    [self.tracker set:kGAIScreenName value:@"Call Dealer"];
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    @try
    {
        
    }
    @catch (NSException *exception) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder
                        createExceptionWithDescription:@"Call Dealer"  // Exception description. May be truncated to 100 chars.
                        withFatal:@NO] build]];  // isFatal (required). NO indicates non-fatal exception.
    }
    
    
    NSString *numberURL = phoneNumber;
    if (![numberURL hasPrefix:@"tel:"]) {
        numberURL = [@"tel:" stringByAppendingString:numberURL];
    }
    
    NSString *escapedURL = [numberURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (!escapedURL) {
        escapedURL = [numberURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    }
    NSURL *url = [[NSURL alloc] initWithString:escapedURL];
    [[UIApplication sharedApplication] openURL:url];
    
}

-(void)topNavMessageClicked:(id)sender{
    
    id controller =(UINavigationController *)rearVC.selectedViewController;// [(UINavigationController *)rearVC.selectedViewController visibleViewController];
#if 0
    
    CMMessageViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMMessageViewController"];
    viewController.messCycleId =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleNotificationKey]] stringValueForKey:@"MailNotificationCycleId"];
    [controller pushViewController:viewController animated:YES];
    
#endif
    
    CMDirectDealerMailVC *secondDetailViewController = [[CMDirectDealerMailVC alloc] initWithNibName:@"CMDirectDealerMailVC" bundle:nil];
    secondDetailViewController.isSendBuddy = NO;
    secondDetailViewController.controller = controller;
    [controller presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
    
    
    
}

-(void)topNavServicesClicked:(id)sender{
    
    
    rearVC.selectedIndex = 0;
    
    id controller = [(UINavigationController *)rearVC.selectedViewController visibleViewController];
    
    if ([controller isKindOfClass:[CMBikeInfoController class]]) {
        
        [controller openServiceTab];
        
    }
    else   if ([controller isKindOfClass:[CMBikeViewController class]]) {
        
        CMBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeInfoController"];
        [viewController performSelector:@selector(openServiceTab) withObject:nil afterDelay:.5];
        
        
        [(UINavigationController *)rearVC.selectedViewController pushViewController:viewController animated:NO];
    }
}

-(void)openPopUpMenu{
    
    if (UIAppDelegate.isRidePermission) {
        
        
        UINavigationController *controller = (UINavigationController *)rearVC.selectedViewController;
        
        NSArray *menuItems =
        @[
          
          [KxMenuItem menuItem:@"Messages"
                         image:[UIImage imageNamed:@"menu_1.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          [KxMenuItem menuItem:@"Service"
                         image:[UIImage imageNamed:@"menu_2.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          [KxMenuItem menuItem:@"Vehicle Health"
                         image:[UIImage imageNamed:@"menu_3.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          [KxMenuItem menuItem:@"Weather" image:[UIImage imageNamed:@"menu_4.png"] target:self  action:@selector(pushMenuItem1:)],
          
          [KxMenuItem menuItem:@"Events"
                         image:[UIImage imageNamed:@"menu_5.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          [KxMenuItem menuItem:@"Specials"
                         image:[UIImage imageNamed:@"menu_6.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          
          [KxMenuItem menuItem:@"Buddies"
                         image:[UIImage imageNamed:@"menu_7.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          [KxMenuItem menuItem:@"My Rides"
                         image:[UIImage imageNamed:@"menu_8.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          [KxMenuItem menuItem:[UIAppDelegate.userDefault boolForKey:IsBikeParkKey]?@"Find My Vehicle": @"Park My Vehicle"
                         image:[UIImage imageNamed:@"menu_9.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          [KxMenuItem menuItem:@"My Vehicle List"
                         image:[UIImage imageNamed:@"menu_10.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          [KxMenuItem menuItem:@"Support"
                         image:[UIImage imageNamed:@"menu_11.png"]
                        target:self
                        action:@selector(pushMenuItem1:)],
          
          
          ];
        
        
        [KxMenu showMenuInView:controller.view
                      fromRect:CGRectMake(330, 0, 100, 50)
                     menuItems:menuItems];
        
    }
    
    else{
        
        UINavigationController *navController = (UINavigationController *)rearVC.selectedViewController;
        CMNewRideSetUpVC *controller = (CMNewRideSetUpVC * )[navController visibleViewController];
        if ([controller isKindOfClass:[CMNewRideSetUpVC class]]) {
            [controller  openRideSetupView];
            
        }
    }
    
}

- (void) pushMenuItem1:(id)sender
{
    NSLog(@"%@", sender);
    
    KxMenuItem *item = (KxMenuItem *)sender;
    
    NSLog(@"item title..%@",item.title);
    
    UINavigationController *navController = (UINavigationController *)rearVC.selectedViewController;
    id controller = [navController visibleViewController];
    
    
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    NSLog(@"%@",dic);
    NSString *ownerId    = [dic stringValueForKey:@"FName"];// [dic stringValueForKey:@"OwnerId"];
    
    NSString *cycleCount = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:ValidationUUIDKey]] stringValueForKey:@"CycleCount"];
    
    
    if ([ownerId isEqualToString:@"0"] || ![ownerId length]) {
        
        if ([item.title isEqualToString:@"Specials"]) {
            if (![controller isKindOfClass:[CMSpecialController class]]) {
                
                CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
                [navController pushViewController:viewController animated:YES];
                
            }
            
        }
        else  if ([item.title isEqualToString: @"Events"]) {
            if (![controller isKindOfClass:[CMEventsController class]]) {
                
                CMEventsController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMEventsController"];
                [navController pushViewController:viewController animated:YES];
            }
        }
        else  if([item.title isEqualToString: @"Weather"]) {
            if (![controller isKindOfClass:[CMWeatherAlertVC class]]) {
                
                CMWeatherAlertVC *UPC = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMWeatherAlertVC"];
                [navController pushViewController:UPC animated:YES];
                
                //CycleWeatherKey
            }
        }
        else  if ([item.title isEqualToString: @"My Vehicle List"]) {
            
            
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CyclePreferenceKey]];
            
            NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
            if (![primaryBikeId length]) {
                primaryBikeId  = [dic stringValueForKey:@"CycleId"];
            }
            NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
            NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
            
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    
                    itemArr = [[NSMutableArray alloc]init];
                    if ([itemArr count]) {
                        [itemArr removeAllObjects];
                    }
                    
                    for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                        [itemArr addObject:obj];
                        
                    }
                    
                    
                    if (itemArr.count>0)
                    {
                        if (![controller isKindOfClass:[CMBikeViewController class]]) {
                            CMBikeViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeViewController"];
                            [navController pushViewController:viewController animated:YES];
                            
                        }
                    }
                    else
                    {
                        //if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                        CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                        [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                        viewController.isFromHome = YES;
                        viewController.messageFlg = 0 ;
                        [navController pushViewController:viewController animated:YES];
                        
                        // }
                    }
                    
                }
            }];
            
        }
        else  if([item.title isEqualToString: @"Park My Vehicle"] ||[item.title isEqualToString: @"Find My Vehicle"]) {
            if (![controller isKindOfClass:[CMParkBikeController class]]) {
                
                CMParkBikeController *viewController = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
                
                [navController pushViewController:viewController animated:YES];
            }
        }
        else if ([item.title isEqualToString:@"Support"]) {
            if (![controller isKindOfClass:[CMSupportController class]]) {
                
                CMSupportController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSupportController"];
                [navController pushViewController:viewController animated:YES];
                
            }
            
        }
        else
            [self goToSettingScreen];
        
    }
    else if ([cycleCount isEqualToString:@"0"] || ![cycleCount length]) {
        
        if ([item.title isEqualToString:@"Specials"]) {
            if (![controller isKindOfClass:[CMSpecialController class]]) {
                
                CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
                [navController pushViewController:viewController animated:YES];
                
            }
            
        }
        else  if ([item.title isEqualToString: @"Events"]) {
            if (![controller isKindOfClass:[CMEventsController class]]) {
                
                CMEventsController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMEventsController"];
                [navController pushViewController:viewController animated:YES];
            }
        }
        else  if([item.title isEqualToString: @"Weather"]) {
            if (![controller isKindOfClass:[CMWeatherAlertVC class]]) {
                
                CMWeatherAlertVC *UPC = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMWeatherAlertVC"];
                [navController pushViewController:UPC animated:YES];
                
                //CycleWeatherKey
            }
        }
        else  if ([item.title isEqualToString: @"My Vehicle List"]) {
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CyclePreferenceKey]];
            
            NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
            if (![primaryBikeId length]) {
                primaryBikeId  = [dic stringValueForKey:@"CycleId"];
            }
            NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
            NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
            
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"***  %@ **",resultDict);
                    
                    itemArr = [[NSMutableArray alloc]init];
                    if ([itemArr count]) {
                        [itemArr removeAllObjects];
                    }
                    
                    for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                        [itemArr addObject:obj];
                    }
                    
                    if (itemArr.count>0)
                    {
                        if (![controller isKindOfClass:[CMBikeViewController class]]) {
                            CMBikeViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeViewController"];
                            [navController pushViewController:viewController animated:YES];
                            
                        }
                    }
                    else
                    {
                        if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                            CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                            [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                            viewController.isFromHome = YES;
                            viewController.messageFlg = 0 ;
                            [navController pushViewController:viewController animated:YES];
                            
                        }
                    }
                    
                }
            }];
        }
        else  if([item.title isEqualToString: @"Park My Vehicle"] ||[item.title isEqualToString: @"Find My Vehicle"]) {
            if (![controller isKindOfClass:[CMParkBikeController class]]) {
                
                CMParkBikeController *viewController = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
                
                [navController pushViewController:viewController animated:YES];
            }
        }
        else  if([item.title isEqualToString: @"Buddies"]) {
            if (![controller isKindOfClass:[CMBuddyContactsController class]]) {
                
                CMBuddyContactsController *buddyProfile = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBuddyContactsController"];
                
                
                [navController pushViewController:buddyProfile animated:YES];
            }
            
            
        }
        else if ([item.title isEqualToString:@"Support"]) {
            if (![controller isKindOfClass:[CMSupportController class]]) {
                
                CMSupportController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSupportController"];
                [navController pushViewController:viewController animated:YES];
                
            }
            
        }
        else  if ([item.title isEqualToString: @"My Rides"]) {
            
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CyclePreferenceKey]];
            
            NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
            if (![primaryBikeId length]) {
                primaryBikeId  = [dic stringValueForKey:@"CycleId"];
            }
            NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
            NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
            
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"***  %@ **",resultDict);
                    itemArr = [[NSMutableArray alloc]init];
                    if ([itemArr count]) {
                        [itemArr removeAllObjects];
                    }
                    
                    for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                        [itemArr addObject:obj];
                    }
                    
                    NSLog(@"count --> %lu",(unsigned long)itemArr.count);
                    if (itemArr.count>0)
                    {
                        if (![controller isKindOfClass:[CMSaveRideController class]]) {
                            
                            CMSaveRideController *viewController = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMSaveRideController"];
                            [navController pushViewController:viewController animated:YES];
                        }
                    }
                    else
                    {
                        //if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                        CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                        [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                        viewController.isFromHome = YES;
                        viewController.messageFlg = 0 ;
                        [navController pushViewController:viewController animated:YES];
                        
                        //  }
                    }
                    
                }
            }];
        }
        else if ([item.title isEqualToString:@"Vehicle Health"]) {
            
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CyclePreferenceKey]];
            
            NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
            if (![primaryBikeId length]) {
                primaryBikeId  = [dic stringValueForKey:@"CycleId"];
            }
            NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
            NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
            
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"***  %@ **",resultDict);
                    itemArr = [[NSMutableArray alloc]init];
                    if ([itemArr count]) {
                        [itemArr removeAllObjects];
                    }
                    
                    for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                        [itemArr addObject:obj];
                    }
                    
                    NSLog(@"count --> %lu",(unsigned long)itemArr.count);
                    if (itemArr.count>0)
                    {
                        if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                            
                            CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                            [viewController performSelector:@selector(openHealthTab) withObject:nil afterDelay:.5];
                            [navController pushViewController:viewController animated:YES];
                        }
                        else
                            [controller healthReportBtnClicked:0];
                    }
                    else
                    {
                        //if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                        CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                        [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                        viewController.isFromHome = YES;
                        viewController.messageFlg = 0 ;
                        [navController pushViewController:viewController animated:YES];
                        
                        //  }
                    }
                    
                }
            }];
            
        }
        else if ([item.title isEqualToString:@"Service"]) {
            
            
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CyclePreferenceKey]];
            
            NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
            if (![primaryBikeId length]) {
                primaryBikeId  = [dic stringValueForKey:@"CycleId"];
            }
            NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
            NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
            
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"***  %@ **",resultDict);
                    itemArr = [[NSMutableArray alloc]init];
                    if ([itemArr count]) {
                        [itemArr removeAllObjects];
                    }
                    
                    for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                        [itemArr addObject:obj];
                    }
                    
                    NSLog(@"count --> %lu",(unsigned long)itemArr.count);
                    if (itemArr.count>0)
                    {
                        if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                            
                            CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                            [viewController performSelector:@selector(openServiceTab) withObject:nil afterDelay:.5];
                            [navController pushViewController:viewController animated:YES];
                        }
                        else
                            [controller serviceBtnClicked:0];
                        
                    }
                    else
                    {
                        //if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                        CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                        [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                        viewController.isFromHome = YES;
                        viewController.messageFlg = 0 ;
                        [navController pushViewController:viewController animated:YES];
                        
                        //  }
                    }
                    
                }
            }];
            // [self healthReportBtnClicked:0];
        }
        else if ([item.title isEqualToString: @"Messages"]) {
            if (![controller isKindOfClass:[CMMessageViewController class]]) {
                
                
                CMMessageViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMMessageViewController"];
                viewController.messCycleId =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleNotificationKey]] stringValueForKey:@"MailNotificationCycleId"];
                [navController pushViewController:viewController animated:YES];
            }
            
            
        }
        else
            [self goToAddBikeScreen];
        
    }
    else{
        
        if ([item.title isEqualToString:@"Vehicle Health"]) {
            if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                
                CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                [viewController performSelector:@selector(openHealthTab) withObject:nil afterDelay:.5];
                [navController pushViewController:viewController animated:YES];
            }
            else
                [controller healthReportBtnClicked:0];
        }
        else if ([item.title isEqualToString:@"Service"]) {
            if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                
                CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                [viewController performSelector:@selector(openServiceTab) withObject:nil afterDelay:.5];
                [navController pushViewController:viewController animated:YES];
            }
            else
                [controller serviceBtnClicked:0];
            
            
            // [self healthReportBtnClicked:0];
        }
        else  if ([item.title isEqualToString:@"Specials"]) {
            if (![controller isKindOfClass:[CMSpecialController class]]) {
                
                CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
                [navController pushViewController:viewController animated:YES];
                
            }
            
        }
        else  if ([item.title isEqualToString: @"Events"]) {
            if (![controller isKindOfClass:[CMEventsController class]]) {
                
                CMEventsController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMEventsController"];
                [navController pushViewController:viewController animated:YES];
            }
        }
        else  if ([item.title isEqualToString: @"My Vehicle List"]) {
            
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CyclePreferenceKey]];
            
            NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
            if (![primaryBikeId length]) {
                primaryBikeId  = [dic stringValueForKey:@"CycleId"];
            }
            NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
            NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
            
            [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
                
                
                if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                    NSLog(@"***  %@ **",resultDict);
                    itemArr = [[NSMutableArray alloc]init];
                    if ([itemArr count]) {
                        [itemArr removeAllObjects];
                    }
                    
                    for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                        [itemArr addObject:obj];
                    }
                    
                    NSLog(@"count --> %lu",(unsigned long)itemArr.count);
                    if (itemArr.count>0)
                    {
                        if (![controller isKindOfClass:[CMBikeViewController class]]) {
                            CMBikeViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeViewController"];
                            [navController pushViewController:viewController animated:YES];
                            
                        }
                    }
                    else
                    {
                        //if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                        CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                        [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                        viewController.isFromHome = YES;
                        viewController.messageFlg = 0 ;
                        [navController pushViewController:viewController animated:YES];
                        
                        //  }
                    }
                    
                }
            }];
        }
        else  if ([item.title isEqualToString: @"Messages"]) {
            if (![controller isKindOfClass:[CMMessageViewController class]]) {
                
                
                CMMessageViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMMessageViewController"];
                viewController.messCycleId =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleNotificationKey]] stringValueForKey:@"MailNotificationCycleId"];
                [navController pushViewController:viewController animated:YES];
            }
            
            
        }
        else  if([item.title isEqualToString: @"My Rides"]) {
            if (![controller isKindOfClass:[CMSaveRideController class]]) {
                
                CMSaveRideController *viewController = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMSaveRideController"];
                [navController pushViewController:viewController animated:YES];
            }
        }
        else  if([item.title isEqualToString: @"Park My Vehicle"] ||[item.title isEqualToString: @"Find My Vehicle"]) {
            if (![controller isKindOfClass:[CMParkBikeController class]]) {
                
                CMParkBikeController *viewController = [[CMStoryBoard storyboardWithName:CMMapride] instantiateViewControllerWithIdentifier:@"CMParkBikeController"];
                
                [navController pushViewController:viewController animated:YES];
            }
        }
        else  if([item.title isEqualToString: @"Buddies"]) {
            if (![controller isKindOfClass:[CMBuddyContactsController class]]) {
                
                CMBuddyContactsController *buddyProfile = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBuddyContactsController"];
                
                
                [navController pushViewController:buddyProfile animated:YES];
                
                
                
            }
            
            
        }
        else  if([item.title isEqualToString: @"Weather"]) {
            if (![controller isKindOfClass:[CMWeatherAlertVC class]]) {
                
                CMWeatherAlertVC *UPC = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMWeatherAlertVC"];
                [navController pushViewController:UPC animated:YES];
                
                //CycleWeatherKey
            }
        }
        else if ([item.title isEqualToString:@"Support"]) {
            if (![controller isKindOfClass:[CMSupportController class]]) {
                
                CMSupportController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSupportController"];
                [navController pushViewController:viewController animated:YES];
                
            }
            
        }
    }
    
}
-(void)resetBtnClicked:(id)sender{
    id controller = [(UINavigationController *)rearVC.selectedViewController visibleViewController];
    
    if ([controller respondsToSelector:@selector(resetBtnClicked)]) {
        //[[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:0.0f/255.0f  blue:23.0f/255.0f  alpha:1]];
        [controller resetBtnClicked];
        
        
    }
    
}

-(void)getNavigateView
{
    UINavigationController *navController = (UINavigationController *)rearVC.selectedViewController;
    id controller = [navController visibleViewController];
    if (itemArr.count>0)
    {
        if (![controller isKindOfClass:[CMBikeViewController class]]) {
            CMBikeViewController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBikeViewController"];
            [navController pushViewController:viewController animated:YES];
            
        }
    }
    else
    {
        if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
            CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
            [navController pushViewController:viewController animated:YES];
            
        }
    }
}
-(void)topNavMenuClicked:(id)sender{
    
    
    // NSArray *controllersArr = [rearVC viewControllers];
    
    id controller = [(UINavigationController *)rearVC.selectedViewController visibleViewController];
    
    if ([controller respondsToSelector:@selector(openPopUpMenu)]) {
        //[[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:0.0f/255.0f  blue:23.0f/255.0f  alpha:1]];
        [controller openPopUpMenu];
        
        
    }
}

-(void)goToAddBikeScreen{
    
    UINavigationController *navController = (UINavigationController *)rearVC.selectedViewController;
    id controller = [navController visibleViewController];
    
    if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
        CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
        viewController.isNewBike = YES;
        [navController pushViewController:viewController animated:YES];
    }
    
    //[CMHelper alertMessageWithText:[NSString stringWithFormat:@"To use this feature, you need to register your bike with %@.",OrgnizationName]];
    //[CMHelper alertMessageWithText:[NSString stringWithFormat:@"To use this feature, you need to register your bike."]];
    
}
-(void)goToSettingScreen{
    
    if (rearVC.selectedIndex!=3) {
        [UIAppDelegate setSelectedController:3];
    }
    
    
    id controller = (UINavigationController *)rearVC.selectedViewController;
    if ([controller respondsToSelector:@selector(popToRootViewControllerAnimated:)])
        [controller popToRootViewControllerAnimated:YES];
    
    
    [CMHelper alertMessageWithText:[NSString stringWithFormat:@"To use this feature, you need to register yourself with %@.",OrgnizationName]];
    
    
}
- (NSArray*)rightBarButtonsWithResetBtn
{
    static UIBarButtonItem *firstButton;
    static UIBarButtonItem *secondButton;
    UIBarButtonItem *thirdButton;
    
    
    if (SYSTEM_VERSION_LESS_THAN(_IR_IOS_VERSION_7)){
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [button setImage:[UIImage imageNamed:@"ipad_reset.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(resetBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        firstButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        /*
         button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
         [button setImage:[UIImage imageNamed:@"services.png"] forState:UIControlStateNormal];
         [button addTarget:self action:@selector(topNavServicesClicked:) forControlEvents:UIControlEventTouchUpInside];
         secondButton = [[UIBarButtonItem alloc] initWithCustomView:button];
         */
        button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [button setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(openPopUpMenu) forControlEvents:UIControlEventTouchUpInside];
        thirdButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        
    }
    else{
        
        
        firstButton         = [[UIBarButtonItem alloc]
                               initWithImage:[[UIImage imageNamed:@"ipad_reset.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain
                               target:self
                               action:@selector(resetBtnClicked:)];
        //
        firstButton.imageInsets = UIEdgeInsetsMake(0.0, 0.0, 0, -25);
        
        /*  secondButton          = [[UIBarButtonItem alloc]
         initWithImage:[[UIImage imageNamed:@"services.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain
         target:self action:@selector(topNavServicesClicked:)];
         
         secondButton.imageInsets = UIEdgeInsetsMake(0.0, 0.0, 0, -25);
         */
        
        thirdButton          = [[UIBarButtonItem alloc]
                                initWithImage:[[UIImage imageNamed:@"menu.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain
                                target:self action:@selector(openPopUpMenu)];
        
        
        
    }
    
    //  });
    
    //testing.......
    return [NSArray arrayWithObjects:thirdButton,firstButton, nil];;
    
    
    NSDictionary *notifyDic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleNotificationKey]];
    
    NSString* serviceCount = [notifyDic stringValueForKey:@"ServiceNotification"];
    NSString* mailCount = [notifyDic stringValueForKey:@"HealthNotification"];
    if ((![serviceCount length] || [serviceCount isEqualToString:@"0"]) && (![mailCount length] || [mailCount isEqualToString:@"0"])) {
        
        return [NSArray arrayWithObjects:thirdButton, nil];;
    }
    else  if ((![serviceCount isEqualToString:@"0"]) ) {
        return [NSArray arrayWithObjects:thirdButton,secondButton, nil];;
        
    }
    else  if (( ![mailCount isEqualToString:@"0"]) ) {
        return [NSArray arrayWithObjects:thirdButton,firstButton, nil];;
        
    }
    
    
    return [NSArray arrayWithObjects:thirdButton,secondButton,firstButton, nil];;
}

- (NSArray*)rightBarButtons
{
    static UIBarButtonItem *firstButton;
    static UIBarButtonItem *secondButton;
    UIBarButtonItem *thirdButton;
    
    
    if (SYSTEM_VERSION_LESS_THAN(_IR_IOS_VERSION_7)){
        /*
         UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
         [button setImage:[UIImage imageNamed:@"message.png"] forState:UIControlStateNormal];
         [button addTarget:self action:@selector(topNavMessageClicked:) forControlEvents:UIControlEventTouchUpInside];
         firstButton = [[UIBarButtonItem alloc] initWithCustomView:button];
         
         
         button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
         [button setImage:[UIImage imageNamed:@"services.png"] forState:UIControlStateNormal];
         [button addTarget:self action:@selector(topNavServicesClicked:) forControlEvents:UIControlEventTouchUpInside];
         secondButton = [[UIBarButtonItem alloc] initWithCustomView:button];
         */
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [button setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(openPopUpMenu) forControlEvents:UIControlEventTouchUpInside];
        thirdButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        
    }
    else{
        
        /*
         firstButton         = [[UIBarButtonItem alloc]
         initWithImage:[[UIImage imageNamed:@"message.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain
         target:self
         action:@selector(topNavMessageClicked:)];
         
         firstButton.imageInsets = UIEdgeInsetsMake(0.0, 0.0, 0, -75);
         
         secondButton          = [[UIBarButtonItem alloc]
         initWithImage:[[UIImage imageNamed:@"services.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain
         target:self action:@selector(topNavServicesClicked:)];
         
         secondButton.imageInsets = UIEdgeInsetsMake(0.0, 0.0, 0, -25);
         */
        
        thirdButton          = [[UIBarButtonItem alloc]
                                initWithImage:[[UIImage imageNamed:@"menu.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain
                                target:self action:@selector(openPopUpMenu)];
        
        
        
    }
    
    //  });
    
    //testing.......
    return [NSArray arrayWithObjects:thirdButton, nil];;
    
    
    NSDictionary *notifyDic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleNotificationKey]];
    
    NSString* serviceCount = [notifyDic stringValueForKey:@"ServiceNotification"];
    NSString* mailCount = [notifyDic stringValueForKey:@"HealthNotification"];
    if ((![serviceCount length] || [serviceCount isEqualToString:@"0"]) && (![mailCount length] || [mailCount isEqualToString:@"0"])) {
        
        return [NSArray arrayWithObjects:thirdButton, nil];;
    }
    else  if ((![serviceCount isEqualToString:@"0"]) ) {
        return [NSArray arrayWithObjects:thirdButton,secondButton, nil];;
        
    }
    else  if (( ![mailCount isEqualToString:@"0"]) ) {
        return [NSArray arrayWithObjects:thirdButton,firstButton, nil];;
        
    }
    
    
    return [NSArray arrayWithObjects:thirdButton,secondButton,firstButton, nil];;
}

-(UIView *)loadBottomView:(int)withIndex{
    
    UINavigationController *controller =(UINavigationController *)rearVC.selectedViewController;
    UIView *bottomView = (UIView *)[controller.view viewWithTag:999];
    if (!bottomView) bottomView = [[CMAppDelegate loadNibNamed:@"TabBarViewNew" owner:self options:nil] objectAtIndex:0];
    bottomView.tag = 999;
    CGRect frame = bottomView.frame;
    frame.size.width = [CMConstants screenSize].width;
    frame.origin.y = [CMConstants screenSize].height-frame.size.height;//-(controller.navigationBar.hidden?0:64);
    bottomView.frame =frame ;
    return bottomView;
}
-(void)bottomTabBarClicked:(id)sender{
    
    if (UIAppDelegate.isRidePermission) {
        if ([sender tag]==99) {
            [self phoneNumberClicked:0];
        }
        else {
            UIButton *button = (UIButton *)sender;
            if (([button tag]-1000)!=rearVC.selectedIndex) {
                [UIAppDelegate setSelectedController:[button tag]-1000];
            }
            
            id controller = (UINavigationController *)rearVC.selectedViewController;//[(UINavigationController *)rearVC.selectedViewController visibleViewController];
            
            if ([controller respondsToSelector:@selector(popToRootViewControllerAnimated:)])
                [controller popToRootViewControllerAnimated:YES];
        }
        
    }
    else{
        
        UINavigationController *navController = (UINavigationController *)rearVC.selectedViewController;
        CMNewRideSetUpVC * controller = (CMNewRideSetUpVC * )[navController visibleViewController];
        if ([controller isKindOfClass:[CMNewRideSetUpVC class]]) {
            [controller  openRideSetupView];
            
        }
    }
}

-(IBAction)setSelectedController:(int)withIndex{
    
    rearVC.selectedIndex = withIndex;
}

- (void) hideTabBar:(UITabBarController *) tabbarcontroller
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    tabbarcontroller.tabBar.hidden = true;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    float fHeight = screenRect.size.height;
    if(  UIDeviceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) )
    {
        fHeight = screenRect.size.width;
    }
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, fHeight, view.frame.size.width, view.frame.size.height)];
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, fHeight)];
            view.backgroundColor = [UIColor blackColor];
        }
    }
    [UIView commitAnimations];
}
- (void) customizeAppearance
{
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:(IR_IS_SYSTEM_VERSION_LESS_THAN_7_0?@"HeaderMenu":@"HeaderMenu_ios7")] forBarMetrics:UIBarMetricsDefault];
    
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor],
      UITextAttributeTextColor,
      [UIColor clearColor],
      UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
      UITextAttributeTextShadowOffset,
      [UIFont fontWithName:kFONT_BentonSans_Medium size:19.0f],
      UITextAttributeFont,
      nil]];
    
    // Remove shadow from iOS6.0
    UINavigationBar *navBar = [[UINavigationBar alloc] init];
    if ([navBar respondsToSelector:@selector(shadowImage)]){
        [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    }
    
    
}

- (void)startLocationRequest
{
    __weak __typeof(self) weakSelf = self;
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                                                timeout:10
                                                   delayUntilAuthorized:YES
                                                                  block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                      __typeof(weakSelf) strongSelf = weakSelf;
                                                                      
                                                                      if (status == INTULocationStatusSuccess) {
                                                                          // achievedAccuracy is at least the desired accuracy (potentially better)
                                                                          statusVale = [NSString stringWithFormat:@"Location request successful! Current Location:\n%@", currentLocation];
                                                                      }
                                                                      else if (status == INTULocationStatusTimedOut) {
                                                                          // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                                                                          statusVale = [NSString stringWithFormat:@"Location request timed out. Current Location:\n%@", currentLocation];
                                                                      }
                                                                      else {
                                                                          // An error occurred
                                                                          if (status == INTULocationStatusServicesNotDetermined) {
                                                                              statusVale = @"Error: User has not responded to the permissions alert.";
                                                                          } else if (status == INTULocationStatusServicesDenied) {
                                                                              statusVale = @"Error: User has denied this app permissions to access device location.";
                                                                          } else if (status == INTULocationStatusServicesRestricted) {
                                                                              statusVale = @"Error: User is restricted from using location services by a usage policy.";
                                                                          } else if (status == INTULocationStatusServicesDisabled) {
                                                                              statusVale = @"Error: Location services are turned off for all apps on this device.";
                                                                          } else {
                                                                              statusVale = @"An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)";
                                                                          }
                                                                      }
                                                                      NSLog(@"%@",statusVale);
                                                                      strongSelf.locationRequestID = NSNotFound;
                                                                  }];
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //for Testing
    
    
    //    self.tracker = [[GAI sharedInstance] trackerWithName:@"allowTracking"
    //                                              trackingId:kTrackingId];
    //    [GAI sharedInstance].optOut =
    //    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    //    [GAI sharedInstance].dispatchInterval = 20;
    //    [GAI sharedInstance].trackUncaughtExceptions = NO;
    //    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    //
    //    self.tracker.allowIDFACollection = YES;
    //    [self.tracker set:kGAISessionControl value:@"start"];
    //    [self.tracker set:kGAIUseSecure value:[@NO stringValue]];
    //    [self.tracker set:kGAIScreenName value:@"Messages"];
    //
    //    [self.tracker send:[[GAIDictionaryBuilder createAppView] build]];
    //    [self.tracker send:[[[GAIDictionaryBuilder createEventWithCategory:@"UX"
    //                                                                action:@"appstart"
    //                                                                 label:nil
    //                                                                 value:nil] set:@"start" forKey:kGAISessionControl] build]];
    //
    
   // [Mixpanel sharedInstanceWithToken:@"25a48ce006a2d978a55fdbd8d2eaa708"];

    [Mixpanel sharedInstanceWithToken:@"5c9c3e523db79672ee8ce2b6560973b3"];
    
    
    //
    NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    // User must be able to opt out of tracking
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    
    
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Ready2Ride - iOS"
                                              trackingId:kTrackingId];
    //[self.tracker set:kGAIScreenName value:@""];
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [self.tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    [[GAI sharedInstance] dispatch];
    
    
    
    UIAppDelegate.isRidePermission = YES;
    
    
    userDefault = [NSUserDefaults standardUserDefaults];
    [self startLocationRequest];
    [self insertUUID];
    
    
    NSLog(@"uuidForDevice = %@ uuidsOfUserDevices= %@ ",[FCUUID uuidForDevice],[FCUUID uuidsOfUserDevices]);
    
    // [[CMHelper new] startFindingLocation:0];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"MM/dd/yyyy" options:0 locale:[NSLocale currentLocale]];
    // Override point for customization after application launch.
    
    [[UIApplication sharedApplication]  setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication]  setStatusBarHidden:NO];
    
    
    
    UIStoryboard* sidebarStoryboard = [CMStoryBoard storyboardWithName:@"Main"];
    //    // UITabBarController *
    //
    rearVC = [sidebarStoryboard instantiateViewControllerWithIdentifier:@"tabController"];
    rearVC.selectedIndex = 4;
    self.window.rootViewController = rearVC;
    
    [self hideTabBar:rearVC];
    
    //[self.mainNavigationController.navigationBar setTintColor:[IRAppDelegate navigationBarColor]];
    
    //    [self customizeAppearance];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if ([[UINavigationBar appearance] respondsToSelector:@selector(setBarTintColor:)]) {
        // [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:245.0f/255.0f green:0.0f/255.0f  blue:23.0f/255.0f  alpha:1]];
        // [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
        
    }
    
    if ([[UINavigationBar appearance] respondsToSelector:@selector(setTintColor:)]) {
        //[[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:0.0f/255.0f  blue:23.0f/255.0f  alpha:1]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        
        
    }
    
    //[self validationForUUID];
    [self.window makeKeyAndVisible];
    
    // Register for Push Notitications, if running iOS 8
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes       categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else {
        // Register for Push Notifications before iOS 8
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                         UIRemoteNotificationTypeAlert |
                                                         UIRemoteNotificationTypeSound)];
    }
    
    if (apiKey.length == 0)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^
         {
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"No App Key"
                                                                            message:@"Please visit my.skyhookwireless.com to create app key, then edit AppDelegate"
                                         @" to initialize the apiKey variable and rebuild the app."
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
         }];
        
    }
    else
    {
        _accelerator = [[SHXAccelerator alloc] initWithKey:apiKey];
        self.accelerator.optedIn = YES;
        // self.accelerator.userID = @"20e1109f41a4a67c25091e2c433f8591d0b0bb98";
        [self.accelerator startMonitoringForAllCampaigns];
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound
                                                                                        categories:nil]];
    }
    
    return YES;
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    
    NSLog(@"%@",url);
    UINavigationController *navController = (UINavigationController *)rearVC.selectedViewController;
    
    if ([[url absoluteString] containsString:@"services"]) {
        
        
        id controller = [navController visibleViewController];
        
        NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CyclePreferenceKey]];
        
        NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
        if (![primaryBikeId length]) {
            primaryBikeId  = [dic stringValueForKey:@"CycleId"];
        }
        NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
        NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
        
        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"***  %@ **",resultDict);
                itemArr = [[NSMutableArray alloc]init];
                if ([itemArr count]) {
                    [itemArr removeAllObjects];
                }
                
                for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                    [itemArr addObject:obj];
                }
                
                NSLog(@"count --> %lu",(unsigned long)itemArr.count);
                if (itemArr.count>0)
                {
                    if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                        
                        CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                        [viewController performSelector:@selector(openServiceTab) withObject:nil afterDelay:.5];
                        [navController pushViewController:viewController animated:YES];
                    }
                    else
                        [controller serviceBtnClicked:0];
                    
                }
                else
                {
                    //if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                    CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                    [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                    viewController.isFromHome = YES;
                    viewController.messageFlg = 0 ;
                    [navController pushViewController:viewController animated:YES];
                    
                    //  }
                }
                
            }
        }];
    }else if ([[url absoluteString] containsString:@"couponcode"]){
        CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
        
        NSArray *array = [[url absoluteString] componentsSeparatedByString:@"couponcode="];
        NSString *code = [array objectAtIndex:1];
        
        viewController.couponCode = code;
        [navController pushViewController:viewController animated:YES];
    }else{
        CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
        viewController.couponCode = @"";
        [navController pushViewController:viewController animated:YES];
    }
    return true;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self checkNumberForGride];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame{
    [UIView animateWithDuration:0.35 animations:^{
        CGRect windowFrame = ((UINavigationController *)((UITabBarController *)self.window.rootViewController).viewControllers[0]).view.frame;
        UINavigationController *controller =(UINavigationController *)rearVC.selectedViewController;
        UIView *bottomView = (UIView *)[controller.view viewWithTag:999];
        CGRect bottomFrame = bottomView.frame ;
        if (newStatusBarFrame.size.height > 20) {
            windowFrame.origin.y = newStatusBarFrame.size.height - 20 ;// old status bar frame is 20
        }
        else{
            windowFrame.origin.y = 0.0;
            
        }
        ((UINavigationController *)((UITabBarController *)self.window.rootViewController).viewControllers[0]).view.frame = windowFrame;
        bottomFrame.origin.y = windowFrame.size.height-50;
        
        
        bottomView.frame = bottomFrame;
        
        
        if (CGRectGetHeight(newStatusBarFrame)==20) {
            CGRect frame = bottomView.frame;
            frame.size.width = [CMConstants screenSize].width;
            frame.origin.y = [CMConstants screenSize].height-frame.size.height;//-(controller.navigationBar.hidden?0:64);
            bottomView.frame =frame ;
        }
        
        
        
    }];
}
//Load NibFiles
+ (NSArray *)loadNibNamed:(NSString *)name owner:(id)owner options:(NSDictionary *)options
{
    NSArray *arrayOfViews = nil;
    
    
    @try {
        
        if ([[NSBundle mainBundle] pathForResource:name ofType:@"nib"] != nil) {
            UINib *nibFile = [UINib nibWithNibName:name bundle:nil];
            arrayOfViews = [nibFile instantiateWithOwner:owner options:options];
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    
    return arrayOfViews;
}
+(UIColor*)navigationBarColor{
    return [UIColor colorWithRed:(72.0f/255.0f) green:(72.0f/255.0f) blue:(72.0f/255.0f) alpha:0.3];
}
-(void)checkNumberForGride{
    
    
    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
    
    
    NSString *stringURL =[NSString stringWithFormat:@"%@/GetAllNotification/OwnerId=%@/DealerId=%@",Service_URL,ownerId,DealerID];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            if ([[resultDict objectForKey:@"GetAllNotificationResult"] count]){
                NSDictionary *bikeNumberDic = [[resultDict objectForKey:@"GetAllNotificationResult"] objectAtIndex:0];
                
                NSString *counter = [bikeNumberDic stringValueForKey:@"TotalCount"];
                if ([counter length]) {
                    [UIApplication sharedApplication].applicationIconBadgeNumber = [counter integerValue];
                }else
                    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                
                
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:bikeNumberDic];
                [UIAppDelegate.userDefault setObject:data forKey:CycleNotificationKey];
            }
            
            
            
        }
        
    }];
    
    
    
    
}

-(void)insertUUID{
    
    //198.12.154.24/7mediaService/CycleMate.svc/InsertMobile
    //Parameter:- UUid,Mobile,DeviceToken,DeviceType,DealerId,Uid
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNo = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    if (!mobileNo) {
        mobileNo = @"";
    }
    
    NSString *deviceToken =  [UIAppDelegate.userDefault objectForKey:@"DeviceToken"];
    if (!deviceToken)
        deviceToken = @"";
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
                             [FCUUID uuidForDevice], @"UUid",
                             DealerID,@"DealerId",@"1",@"DeviceType",
                             deviceToken,@"DeviceToken",mobileNo,@"Mobile",
                             nil];
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertMobile",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** InsertUUidAPI %@ **",resultDict);
            
            
        }}];
    
    
    /*     NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:
     [FCUUID uuidForDevice], @"UUid",
     DealerID,@"DealerId",@"1",@"DeviceType",
     [UIAppDelegate.userDefault objectForKey:@"DeviceToken"],@"DeviceToken",
     nil];
     
     [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUUid",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
     
     
     if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
     NSLog(@"*** InsertUUidAPI %@ **",resultDict);
     
     
     }}];*/
    //InsertUUidAPI
    
}

-(void)validationForUUID{
    
    //diaService/CycleMate.svc/GETVALIDATIONMObile/Mobile=4052557777
    
    //NSString *mobileNo = [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNo = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    NSString *stringURL = [NSString stringWithFormat:@"%@/GETVALIDATIONMObile/Mobile=%@",Service_URL,mobileNo];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** GETVALIDATIONMObileResult %@ **",resultDict);
            
            if ([[resultDict objectForKey:@"GETVALIDATIONMObileResult"] count]) {
                NSDictionary *dic = [[resultDict objectForKey:@"GETVALIDATIONMObileResult"] objectAtIndex:0];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
                [userDefault setObject:data forKey:CycleValidationUUIDKey];
                [userDefault setObject:data forKey:ValidationUUIDKey];
                // [self getOwnerforCurrBike];
                
                
                
                
                NSArray *dataDisplay = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
                NSLog(@"dataDisplay  %lu **",(unsigned long)dataDisplay.count);
                if(dataDisplay.count>0)
                {
                    NSString *fName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"FName"];
                    
                    NSString *lName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"LName"];
                    
                    NSString *email    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"Email"];
                    
                    NSString *dealerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"DealerId"];
                    
                    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                    
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel identify:ownerId];
                    
                    [mixpanel.people set:@{
                                           @"FirstName": fName,
                                           @"LastName": lName,
                                           @"Email": email,
                                           @"DealerId": dealerId
                                           }];
                  
                }

                
                [self getOwnerByMobile];
                
                
                
            }
            
            
        }}];
    
    /*
     NSString *stringURL = [NSString stringWithFormat:@"%@/GetValidationUUID/UUid=%@",Service_URL,[FCUUID uuidForDevice]];
     
     [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
     
     if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
     NSLog(@"*** GetValidationUUIDResult %@ **",resultDict);
     
     if ([[resultDict objectForKey:@"GetValidationUUIDResult"] count]) {
     NSDictionary *dic = [[resultDict objectForKey:@"GetValidationUUIDResult"] objectAtIndex:0];
     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
     [userDefault setObject:data forKey:CycleValidationUUIDKey];
     [userDefault setObject:data forKey:ValidationUUIDKey];
     
     [self getOwnerforCurrBike];
     
     
     }
     
     
     }}];*/
    
}
//9840379066
-(void)postBlankOwnerInfo{
    
    //First Name, Last Name, Phone, Email and Zip Code
    /*
     address = "245 Clardy Rd";
     city = Unionville;
     first = Matthew;
     last = Isley;
     mobile = 9314921770;
     state = TN;
     status = ok;
     zip = 37180;
     
     */
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    
    
    
    
    NSString *lastName = [dic stringValueForKey:@"LName"] ?[dic stringValueForKey:@"LName"] :@"";
    NSString *firstName = [dic stringValueForKey:@"FName"]?[dic stringValueForKey:@"FName"]:@"";
    NSString *addressStr = [dic stringValueForKey:@"Address"]?[dic stringValueForKey:@"Address"]:@"";
    NSString *cityStr = [dic stringValueForKey:@"City"]?[dic stringValueForKey:@"City"]:@"";
    NSString *stateStr = [dic stringValueForKey:@"State"]?[dic stringValueForKey:@"State"]:@"";
    NSString *emailFieldStr = [dic stringValueForKey:@"Email"]?[dic stringValueForKey:@"Email"]:@"";
    NSString *altMobileFieldStr = [dic stringValueForKey:@"Phone"]?[dic stringValueForKey:@"Phone"]:@"";
    NSString *zipFieldStr = [dic stringValueForKey:@"Zip"]?[dic stringValueForKey:@"Zip"]:@"";
    NSString *photoPathStr = [dic stringValueForKey:@"PhotoPath"]?[dic stringValueForKey:@"PhotoPath"]:@"";
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileStr = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    
    
    NSDictionary *infoDic = [[NSDictionary alloc] initWithObjectsAndKeys: firstName, @"FName",lastName, @"LName", addressStr, @"Address",
                             cityStr, @"City",
                             stateStr, @"State",
                             zipFieldStr, @"Zip",
                             altMobileFieldStr, @"Phone",
                             emailFieldStr, @"Email",
                             @"1", @"StyleId",
                             @"", @"DLPhoto",
                             @"", @"DLPhotoPath",
                             photoPathStr, @"PhotoPath",
                             @"", @"Photo",
                             @"1343hfdk28krk", @"DLNumber",
                             @"11/10/14", @"DLExpiration",
                             @"1", @"IsActive",
                             OWNER_ID, @"OwnerId",[FCUUID uuidForDevice],@"UUid",DealerID,@"DealerId",
                             mobileStr, @"Mobile",  nil];
    
    
    //InsertUpdateOwnerUsingMobile
    //InsertUpdateOwner
    
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/InsertUpdateOwnerUsingMobile",Service_URL] param:infoDic requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            UIAppDelegate.rearVC.selectedIndex = 3;
            
            
            [self insertUUID];
            [self getOwnerByMobile];
            
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            [notificationCenter postNotificationName:CM_OWNER_REGISTER object:self];
            
        }
    }];
    
    
}

-(void)getOwnerforCurrBike{
    
    if ([OWNER_ID isEqualToString:@"0"] || !OWNER_ID.length) {
        return;
    }
    
    NSString *stringURL =[NSString stringWithFormat:@"%@%@/DealerId=%@",GetOwner,OWNER_ID,DealerID];
    
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic;
            
            if ([[resultDict objectForKey:@"GetOwnerResult"] count]){
                dic = [[resultDict objectForKey:@"GetOwnerResult"] objectAtIndex:0];
                
                if ([[dic stringValueForKey:@"Mobile"] length])  [UIAppDelegate.userDefault setObject:[dic stringValueForKey:@"Mobile"] forKey:@"MOBILENO"];
                
            }
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
            [userDefault setObject:data forKey:CycleValidationUUIDKey];
           
            
            
            
            NSArray *dataDisplay = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
            NSLog(@"dataDisplay  %lu **",(unsigned long)dataDisplay.count);
            if(dataDisplay.count>0)
            {
                NSString *fName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"FName"];
                
                NSString *lName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"LName"];
                
                NSString *email    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"Email"];
                
                NSString *dealerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"DealerId"];
                
                NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel identify:ownerId];
                
                [mixpanel.people set:@{
                                       @"FirstName": fName,
                                       @"LastName": lName,
                                       @"Email": email,
                                       @"DealerId": dealerId
                                       }];
               
            }

            
            
        }
    }];
    
}


-(void)getOwnerByMobile{
    
    
    NSString *mobileNo1 =  [UIAppDelegate.userDefault objectForKey:@"MOBILENO"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *mobileNo = [[mobileNo1 componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    
    NSString *stringURL =[NSString stringWithFormat:@"%@/GetOwnerByMobile/Mobile=%@",Service_URL,mobileNo];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"***  %@ **",resultDict);
            
            NSDictionary *dic;
            
            if ([[resultDict objectForKey:@"GetOwnerByMobileResult"] count]){
                dic = [[resultDict objectForKey:@"GetOwnerByMobileResult"] objectAtIndex:0];
                
                if ([[dic stringValueForKey:@"Mobile"] length])  [UIAppDelegate.userDefault setObject:[dic stringValueForKey:@"Mobile"] forKey:@"MOBILENO"];
                
            }
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
            [userDefault setObject:data forKey:CycleValidationUUIDKey];
            
            
            NSArray *dataDisplay = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
            NSLog(@"dataDisplay  %lu **",(unsigned long)dataDisplay.count);
            if(dataDisplay.count>0)
            {
                NSString *fName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"FName"];
                
                NSString *lName    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"LName"];
                
                NSString *email    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"Email"];
                
                NSString *dealerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"DealerId"];
                
                NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
                
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel identify:ownerId];
                
                [mixpanel.people set:@{
                                       @"FirstName": fName,
                                       @"LastName": lName,
                                       @"Email": email,
                                       @"DealerId": dealerId
                                       }];
              }
            
        }
    }];
    
}


- (void)saveImage:(UIImage *)image WithName:(NSString *)imageName
{
    // If File Exist then read it otherwise creat new
    NSMutableDictionary *imageInfoDict;
    if([[NSFileManager defaultManager] fileExistsAtPath:[LIB_DIR_PATH stringByAppendingPathComponent:@"imageInfo.plist"]])
    {
        NSData *fileData = [NSData dataWithContentsOfFile:[LIB_DIR_PATH stringByAppendingPathComponent:@"imageInfo.plist"]];
        imageInfoDict = [NSMutableDictionary dictionaryWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData:fileData]];
    }
    else
        imageInfoDict = [NSMutableDictionary dictionaryWithCapacity:0];
    
    // Add Single Image to Dictionary
    [imageInfoDict setValue:image forKey:imageName];
    
    // Convert Main info Dictionary to `NSData` to Save on Disc
    [NSKeyedArchiver archiveRootObject:imageInfoDict toFile:[LIB_DIR_PATH stringByAppendingPathComponent:@"imageInfo.plist"]];
    
    
}


-(UIImage *)readImageFromPlistByKey:(NSString *)keyName
{
    // If File Exist then read it otherwise creat new
    NSMutableDictionary *imageInfoDict;
    if([[NSFileManager defaultManager] fileExistsAtPath:[LIB_DIR_PATH stringByAppendingPathComponent:@"imageInfo.plist"]])
    {
        NSData *fileData = [NSData dataWithContentsOfFile:[LIB_DIR_PATH stringByAppendingPathComponent:@"imageInfo.plist"]];
        if([fileData length] > 0)
        {
            // Read Plist
            imageInfoDict = [NSMutableDictionary dictionaryWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData:fileData]];
            
            // Here is your Image
            return imageInfoDict[keyName];
        }
        else
            return nil;
        
    }
    else
    {
        // Return Default Image if not Found
        return nil;
    }
}


/**
 *  Class Methods
 */

+(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = totalHeight;
    return height;
}


-(NSString *)getDealerInfo:(NSString *)withKey{
    
    NSDictionary *userDic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleDealerInfoKey]];
    NSString *dealerID = [userDic stringValueForKey:withKey];
    if (![dealerID length])
        dealerID = @"0";
    
    //getDealerPhoneNO
    return dealerID ;
    
}

-(NSString *)getDealerID{
    
    NSDictionary *userDic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleDealerInfoKey]];
    NSString *dealerID = [userDic stringValueForKey:@"DealerId"];
    if (![dealerID length])
        dealerID = @"0";
    
    
    return dealerID ;
    
}



-(NSString *)getOwnerID{
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]];
    
    NSString * patientId    = [dic stringValueForKey:@"OwnerId"];
    
    
    
    if (![patientId length])
        patientId = @"0";
    
    
    return patientId ;
    
}


- (void)application:(UIApplication *)application
didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}
// Delegation methods
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    // const void *devTokenBytes = [devToken bytes];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel.people addPushDeviceToken:devToken];
    NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    
    
    NSLog(@"%@ bytes", deviceToken);
    
    // self.registered = YES;
    NSLog(@"deviceToken: %@", devToken);
    
    [userDefault setObject:deviceToken forKey:@"DeviceToken"];
    [self insertUUID];
    
    
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSString *message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    //DebugLog(@"Received remote notification (in appDelegate): %@", userInfo);
    
    //application.applicationIconBadgeNumber = 0;
    [self openViewReportScreen];
    
    
    
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [Mixpanel sharedInstanceWithToken:@"5c9c3e523db79672ee8ce2b6560973b3"];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    if (application.applicationState != UIApplicationStateBackground) {
        // [[UAPush shared] resetBadge];
        //application.applicationIconBadgeNumber = 0;
    }
    
    UINavigationController *navController = (UINavigationController *)rearVC.selectedViewController;
    NSLog(@"alertBody = %@",userInfo[@"aps"]);
    
    if ([[userInfo[@"aps"] valueForKey:@"alert"] containsString:@"services"]){
        id controller = [navController visibleViewController];
        
        NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CyclePreferenceKey]];
        
        NSString *primaryBikeId = [dic stringValueForKey:@"PrimaryBikeId"];
        if (![primaryBikeId length]) {
            primaryBikeId  = [dic stringValueForKey:@"CycleId"];
        }
        NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
        NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
        
        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"***  %@ **",resultDict);
                itemArr = [[NSMutableArray alloc]init];
                if ([itemArr count]) {
                    [itemArr removeAllObjects];
                }
                
                for (id obj in  [resultDict objectForKey:@"GetCycleResult"]) {
                    [itemArr addObject:obj];
                }
                
                NSLog(@"count --> %lu",(unsigned long)itemArr.count);
                if (itemArr.count>0)
                {
                    if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                        
                        CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                        [viewController performSelector:@selector(openServiceTab) withObject:nil afterDelay:.5];
                        [navController pushViewController:viewController animated:YES];
                    }
                    else
                        [controller serviceBtnClicked:0];
                    
                }
                else
                {
                    //if (![controller isKindOfClass:[CMNewBikeInfoController class]]) {
                    CMNewBikeInfoController *viewController = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMNewBikeInfoController"];
                    [viewController performSelector:@selector(openCycleInoTab) withObject:nil afterDelay:.5];
                    viewController.isFromHome = YES;
                    viewController.messageFlg = 0 ;
                    [navController pushViewController:viewController animated:YES];
                    
                    //  }
                }
                
            }
        }];
    }else if ([[userInfo[@"aps"] valueForKey:@"alert"] containsString:@"couponcode"]){
        CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
        
        NSArray *array = [[userInfo[@"aps"] valueForKey:@"alert"] componentsSeparatedByString:@"couponcode="];
        NSString *code = [array objectAtIndex:1];
        
        viewController.couponCode = code;
        [navController pushViewController:viewController animated:YES];
    }else{
        CMSpecialController *viewController = [[CMStoryBoard storyboardWithName:CMNewBike] instantiateViewControllerWithIdentifier:@"CMSpecialController"];
        viewController.couponCode = @"";
        [navController pushViewController:viewController animated:YES];
    }
    
    //    NSString *contentType  = userInfo[@"aps"][@"contenttype"];
    
    
    if (([UIApplication sharedApplication].applicationState == UIApplicationStateInactive)||([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)) {
        
        
    }
    
    [mixpanel identify:mixpanel.distinctId];
    
    
    
    [self openViewReportScreen];
    
    
    completionHandler(UIBackgroundFetchResultNoData);
    
    
}

-(void)openViewReportScreen{
    
    
}

- (void)failIfSimulator {
    if ([[[UIDevice currentDevice] model] rangeOfString:@"Simulator"].location != NSNotFound) {
        UIAlertView *someError = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                            message:@"You will not be able to receive push notifications in the simulator."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        
        // Let the UI finish launching first so it doesn't complain about the lack of a root view controller
        // Delay execution of the block for 1/2 second.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [someError show];
        });
        
    }
}

@end
