//
//  CMDeviceContactsListController.h
//  7Media
//
//  Created by Parvin Babi on 26/07/17.
//  Copyright © 2017 com.ready2ride. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import <MessageUI/MessageUI.h>
@interface CMDeviceContactsListController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,MFMessageComposeViewControllerDelegate,UINavigationControllerDelegate>
{
    IBOutlet UIButton *sendBtn;
    IBOutlet UIButton *cancelBtn;
    IBOutlet UITableView *_UITableView;
    IBOutlet UISearchBar *_UIsarchBar;
}
-(IBAction)sendButtonAction :(id)sender;
-(IBAction)cancelBtnClicked :(id)sender;
@end
