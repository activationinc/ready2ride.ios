//
//  CMDeviceContactsListController.m
//  7Media
//
//  Created by Parvin Babi on 26/07/17.
//  Copyright © 2017 com.ready2ride. All rights reserved.
//

#import "CMDeviceContactsListController.h"
#import "CMBuddyContactsController.h"
@interface CMDeviceContactsListController ()
{
    NSMutableArray *contactsArray;
    NSMutableDictionary *sectionsDict;
    NSMutableDictionary *searchSectionsDict;
    NSMutableArray *selectedContactsArray;
    NSMutableArray *selectBuddyArray;
    BOOL searchingFlag;
}

@end

@implementation CMDeviceContactsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchContactsandAuthorization:self];
    [CMAppDelegate setNavigationBarTitle:@"CONTACTS" navigationItem:self.navigationItem];
    self.navigationItem.rightBarButtonItems = [UIAppDelegate rightBarButtons];//[self setUpPopButton];//
    self.navigationItem.leftBarButtonItem = [UIAppDelegate backBarButtonItem:self.navigationController ];
       selectBuddyArray= [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// Fetchcontacts from Address Book
/***************************************************************
 Author : Parvin babi shaik
 Desc.  : Get access from address book
 **************************************************************/
-(void)fetchContactsandAuthorization:(id)sender
{
    contactsArray=[[NSMutableArray alloc] init];
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error)
     {
         if (granted == YES)
         {
             //keys with fetching properties
             NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey,CNContactEmailAddressesKey];
             CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
             NSError *error;
             BOOL success = [store enumerateContactsWithFetchRequest:request error:&error usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop) {
                 if (error) {
                     NSLog(@"error fetching contacts %@", error);
                 }
                 else
                 {
                     // copy data to my custom Contact class.
                     
                     NSString *firstName = contact.givenName;
                     NSString *lastName = contact.familyName;
                     NSString *fullName;
                     NSString *emailStr=@"";
                     BOOL hadPhone = false;
                     if (lastName == nil) {
                         fullName=[NSString stringWithFormat:@"%@",firstName];
                     }else if (firstName == nil){
                         fullName=[NSString stringWithFormat:@"%@",lastName];
                     }
                     else{
                         fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                     }
                     for (CNLabeledValue *label in contact.emailAddresses)
                     {
                         NSLog(@"label value..%@",label.value);
                         NSString *email = label.value;
                         if ([email length] > 0)
                         {
                             emailStr=[NSString stringWithString:email];
                             break ;
                         }
                     }
                     for (CNLabeledValue *label in contact.phoneNumbers)
                     {
                         
                         NSLog(@"label value..%@",[label.value stringValue]);
                         NSString *phone = [label.value stringValue];
                         if ([phone length] > 0)
                         {
                             hadPhone=YES;
                             NSMutableDictionary *newContact = [[NSMutableDictionary alloc] init];
                             [newContact setObject:fullName forKey:@"Name"];
                             [newContact setObject:phone forKey:@"PhoneNumber"];
                             NSLog(@"email str..%@",emailStr);
                             [newContact setObject:emailStr forKey:@"email"];
                             NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"] invertedSet];
                             NSString *resultString = [[label.label componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
                             NSLog (@"Result: %@", resultString);
                             if (resultString)
                             {
                                 [newContact setObject:resultString forKey:@"PhoneType"];
                             }
                             else
                                 [newContact setObject:@"" forKey:@"PhoneType"];
                             
                             [newContact setObject:@"0" forKey:@"isSelected"];
                             NSLog(@"new contact..%@",newContact);
                             [contactsArray addObject:newContact];
                         }
                     }
//                     //adding with email
//                     if(hadPhone==NO && [emailStr length]>3)
//                     {
//                         NSMutableDictionary *newContact = [[NSMutableDictionary alloc] init];
//                         [newContact setObject:fullName forKey:@"Name"];
//                         [newContact setObject:@"" forKey:@"PhoneNumber"];
//                         [newContact setObject:@"" forKey:@"PhoneType"];
//                         [newContact setObject:emailStr forKey:@"email"];
//                         [newContact setObject:@"0" forKey:@"isSelected"];
//                         [contactsArray addObject:newContact];
//                     }
                     
                 }
             }];
             
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                //Update UI on the Main thread, like reloading a UITableView
                                    
                                    [self loadContacts:contactsArray];
                            });
             
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                [UIAlertView alertViewWithTitle:@"No Access" message:@"Please go to Settings > Privacy > Contacts > Ready2Ride and enable contacts access."];
                            });
         }
     }];
    
}
-(void)loadContacts:(NSMutableArray *)contacts
{
    sectionsDict=[self prepareSectionDictionaries
                  :contactsArray];
    selectedContactsArray=[[NSMutableArray alloc] init];
    NSLog(@"are I am getting this too");
    [_UITableView reloadData];
}
-(IBAction)sendButtonAction:(id)sender
{
    [self doneSelectingContacts];
}
-(IBAction)cancelBtnClicked:(id)sender
{
  [self.navigationController popToRootViewControllerAnimated:YES];
//    CMBuddyContactsController *buddyProfile = [[CMStoryBoard storyboardWithName:CMMain] instantiateViewControllerWithIdentifier:@"CMBuddyContactsController"];
//    buddyProfile.isFromHome = YES;
//    [self.navigationController pushViewController:buddyProfile animated:YES];
   // UIAppDelegate.rearVC.selectedIndex = 4;
    
}

#pragma mark TableView delegate & DataSource Methods
/*************************************************************
 Author : Parvin Babi Shaik
 Desc.  : Tableview delegate and datasource methods
 *************************************************************/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([contactsArray count]==0)
    {
        return 1;
    }
    else
    {
        if (searchingFlag)
        {
            return [[searchSectionsDict allKeys]count];
        }
        else
        {
            return [[sectionsDict allKeys] count];
        }
    }
}
//returns title
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([contactsArray count]>0)
    {
        if (searchingFlag)
        {
            return [[[searchSectionsDict allKeys] sortedArrayUsingSelector
                     :@selector(localizedCaseInsensitiveCompare:)]
                    objectAtIndex:section];
        }
        else
        {
            return [[[sectionsDict allKeys] sortedArrayUsingSelector
                     :@selector(localizedCaseInsensitiveCompare:)]
                    objectAtIndex:section];
        }
    }
    return @"";
}
//view for header of table view
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([contactsArray count]>0)
    {
        //header view
        float xPos = 0;
        float yPos = 0;
        float width = tableView.bounds.size.width;
        float height = 30;
        CGRect rect=CGRectMake(xPos, yPos, width, height);
        UIView *headerView = [[UIView alloc] initWithFrame:rect];
        headerView.backgroundColor = [UIColor lightGrayColor];
        //alphabetical label
        xPos = 9;
        yPos = 3;
        width = 20;
        height = 17;
        rect=CGRectMake(xPos, yPos, width, height);
        UILabel *alphabetLab = [[UILabel alloc] initWithFrame:rect];
        alphabetLab.textColor=[UIColor blackColor];
        alphabetLab.font=[UIFont boldSystemFontOfSize:14];
        alphabetLab.textAlignment = NSTextAlignmentCenter;
        [alphabetLab.layer setBorderWidth:1];
        alphabetLab.layer.borderColor = [[UIColor darkGrayColor]
                                         CGColor];
        [headerView addSubview:alphabetLab];
        if (searchingFlag)
        {
            alphabetLab.text = [[[searchSectionsDict allKeys]
                                 sortedArrayUsingSelector
                                 :@selector(localizedCaseInsensitiveCompare:)]
                                objectAtIndex:section];
        }
        else
        {
            alphabetLab.text = [[[sectionsDict allKeys] sortedArrayUsingSelector
                                 :@selector(localizedCaseInsensitiveCompare:)]
                                objectAtIndex:section];
        }
        return headerView;
    }
    return nil;
}
//no.of rows
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([contactsArray count]==0)
    {
        return 1;
    }
    else
    {
        if (searchingFlag)
        {
            return [[searchSectionsDict valueForKey:[[[searchSectionsDict allKeys]
                                                      sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]
                                                     objectAtIndex:section]] count];
        }
        else
        {
            return [[sectionsDict valueForKey:[[[sectionsDict allKeys]
                                                sortedArrayUsingSelector
                                                :@selector(localizedCaseInsensitiveCompare:)]
                                               objectAtIndex:section]] count];
        }
    }
}
//section index titles for table view
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if ([contactsArray count]>0)
    {
        if (searchingFlag)
        {
            return [[searchSectionsDict allKeys]
                    sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        }
        else
        {
            return [[sectionsDict allKeys]
                    sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        }
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //change frames
    CGFloat rowHeight;
    rowHeight=70;
    return rowHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =[NSString stringWithFormat:@"%ld %ld",(long)indexPath.
                               section,(long)indexPath.row];
    UITableViewCell *cell=nil;
    if(cell ==nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                     reuseIdentifier:cellIdentifier];
    }
    
    if ([contactsArray count]==0)
    {
        UILabel * nameLbl =[[UILabel alloc]initWithFrame:CGRectMake(5, 6, 400, 30)];
        [nameLbl setFont:[UIFont boldSystemFontOfSize:14]];
        [nameLbl setBackgroundColor
         :[UIColor clearColor]];
        nameLbl.textColor =
        [UIColor blackColor];
        [cell addSubview:nameLbl];
        nameLbl.text=@"";
    }
    
    
    else
    {
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        float xPos;
        float yPos;
        float width;
        float height;
        CGRect rect;
        
        
        
        //name label
        xPos=15;
        yPos=5;
        width=180;
        height=20;
        rect= CGRectMake(xPos, yPos, width, height);
        
        UILabel *tableViewCellNameLabel = [[UILabel alloc] initWithFrame:rect];
        tableViewCellNameLabel.textColor=[UIColor blackColor];
        tableViewCellNameLabel.font=[UIFont fontWithName:@"arial" size:15];
        tableViewCellNameLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:tableViewCellNameLabel];
        
        
        //phone  label
        xPos=15;
        yPos=28;
        width=200;
        height=15;
        rect= CGRectMake(xPos, yPos, width, height);
        
        UILabel *tableViewCellPhoneLabel = [[UILabel alloc] initWithFrame:rect];
        tableViewCellPhoneLabel.textColor=[UIColor blackColor];
        tableViewCellPhoneLabel.font=[UIFont fontWithName:@"arial" size:12];
        tableViewCellPhoneLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:tableViewCellPhoneLabel];
        
        //email  label
        xPos=15;
        yPos=47;
        width=200;
        height=15;
        rect= CGRectMake(xPos, yPos, width, height);
        
        UILabel *tableViewCellemailLabel = [[UILabel alloc] initWithFrame:rect];
        tableViewCellemailLabel.textColor=[UIColor blackColor];
        tableViewCellemailLabel.font=[UIFont fontWithName:@"arial" size:12];
        tableViewCellemailLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:tableViewCellemailLabel];
        
        //type label
        xPos=300;
        yPos=27;
        width=65;
        height=15;
        NSLog(@"screen width..%f",[UIScreen mainScreen].bounds.size.width);
        if ([UIScreen mainScreen].bounds.size.width == 320)
        {
            xPos=210;
        }
        else if ([UIScreen mainScreen].bounds.size.width == 375)
        {
            xPos = 255;
            
        }
        
        rect= CGRectMake(xPos, yPos, width, height);
        UILabel *tableViewCellPhoneTypeLabel = [[UILabel alloc] initWithFrame:rect];
        tableViewCellPhoneTypeLabel.textColor=[UIColor colorWithRed:0.26 green:0.83 blue:0.9
                                                              alpha:1];
        tableViewCellPhoneTypeLabel.font=[UIFont fontWithName:@"arial" size:12];
        tableViewCellPhoneTypeLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:tableViewCellPhoneTypeLabel];
        //[tableViewCellPhoneTypeLabel setBackgroundColor:[UIColor redColor]];
        [cell addSubview:tableViewCellPhoneTypeLabel];
        
        xPos = 360;
        yPos = 27;
        UIImage *image=[UIImage imageNamed:@"radiobutton"];
        width = image.size.width;
        height = image.size.height;
        
        if ([UIScreen mainScreen].bounds.size.width == 320)
        {
            xPos=275;
        }
        else if ([UIScreen mainScreen].bounds.size.width == 375)
        {
            xPos = 325;
            
        }
        
        rect=CGRectMake(xPos, yPos, width, height);
        UIImageView *cellImgView = [[UIImageView alloc] initWithFrame:rect];
        cellImgView.image=image;
        [cell addSubview:cellImgView];
        
        //showing data to labels
        NSMutableDictionary *_ContactsListParameters;
        if (searchingFlag==YES)
        {
            _ContactsListParameters = [[searchSectionsDict
                                        valueForKey:[[[searchSectionsDict allKeys]
                                                      sortedArrayUsingSelector
                                                      :@selector(localizedCaseInsensitiveCompare:)]
                                                     objectAtIndex:indexPath.section]]
                                       objectAtIndex:indexPath.row];
        }
        else
        {
            _ContactsListParameters = [[sectionsDict valueForKey
                                        :[[[sectionsDict allKeys] sortedArrayUsingSelector
                                           :@selector(localizedCaseInsensitiveCompare:)]
                                          objectAtIndex:indexPath.section]] objectAtIndex
                                       :indexPath.row];
        }
        
        
        // Configure the cell...
        tableViewCellNameLabel.text=[_ContactsListParameters objectForKey:@"Name"];
        tableViewCellPhoneLabel.text=[_ContactsListParameters objectForKey:@"PhoneNumber"];
         tableViewCellPhoneTypeLabel.text=[_ContactsListParameters objectForKey:@"PhoneType"];
        tableViewCellemailLabel.text=[_ContactsListParameters objectForKey:@"email"];
        if ([tableViewCellemailLabel.text length]<2)
        {
            tableViewCellemailLabel.text=@"";
        }
        if ([tableViewCellPhoneLabel.text length]<1)
        {
            tableViewCellPhoneLabel.text=@"";
        }
        
        
        if ([[_ContactsListParameters objectForKey:@"isSelected"] isEqualToString:@"1"])
        {
            
            cellImgView.image=[UIImage imageNamed:@"radiobuttonselect"];
        }
        else
        {
            cellImgView.image=image;
        }
        
        
//        if (selectedIndexPath == indexPath)
//        {
//            _DeviceContactsUICreator.getCellImageViewInstance.image =
//            [LYTThemeCreator getApplicantSelectImage];
//        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *_ContactsListParameters;
    if ([contactsArray count]>0)
    {
        //showing data to labels
        
        if (searchingFlag==YES)
        {
            _ContactsListParameters = [[searchSectionsDict
                                        valueForKey:[[[searchSectionsDict allKeys]
                                                      sortedArrayUsingSelector
                                                      :@selector(localizedCaseInsensitiveCompare:)]
                                                     objectAtIndex:indexPath.section]]
                                       objectAtIndex:indexPath.row];
            
            if ([[_ContactsListParameters objectForKey:@"isSelected"] isEqualToString:@"1"])
            {
                [[[searchSectionsDict valueForKey:[[[searchSectionsDict allKeys]
                                                    sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]
                                                   objectAtIndex:indexPath.section]]
                  objectAtIndex:indexPath.row] setObject:@"0" forKey:@"isSelected"];
                [selectedContactsArray removeObject:_ContactsListParameters];
            }
            else
            {
                [[[searchSectionsDict valueForKey:[[[searchSectionsDict allKeys]
                                                    sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]
                                                   objectAtIndex:indexPath.section]]
                  objectAtIndex:indexPath.row] setObject:@"1" forKey:@"isSelected"];
                
                [selectedContactsArray addObject:_ContactsListParameters];
                
            }
            [_UITableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            
        }
        else
        {
            _ContactsListParameters = [[sectionsDict valueForKey
                                        :[[[sectionsDict allKeys] sortedArrayUsingSelector
                                           :@selector(localizedCaseInsensitiveCompare:)]
                                          objectAtIndex:indexPath.section]] objectAtIndex
                                       :indexPath.row];
            if ([[_ContactsListParameters objectForKey:@"isSelected"] isEqualToString:@"1"])
            {
                [[[sectionsDict valueForKey:[[[sectionsDict allKeys]
                                              sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]
                                             objectAtIndex:indexPath.section]]
                  objectAtIndex:indexPath.row] setObject:@"0" forKey:@"isSelected"];
                [selectedContactsArray removeObject:_ContactsListParameters];
            }
            else
            {
                [[[sectionsDict valueForKey:[[[sectionsDict allKeys]
                                              sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]
                                             objectAtIndex:indexPath.section]]
                  objectAtIndex:indexPath.row] setObject:@"1" forKey:@"isSelected"];
                
                [selectedContactsArray addObject:_ContactsListParameters];
                
            }
            [_UITableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    //selectedIndexPath=nil;
    
}


//- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
//                 didFinishWithResult:(MessageComposeResult)result
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popToRootViewControllerAnimated:YES];
//    UIAppDelegate.rearVC.selectedIndex = 4;
//    
//}

//prepare sections dict to show contacts in alphabetical order
-(NSMutableDictionary *)prepareSectionDictionaries:(NSMutableArray *)contactsListArray
{
    NSLog(@"get into it");
    BOOL found;
    NSMutableDictionary *sectionsDictionary = [[NSMutableDictionary alloc] init];
    // Loop through the contacts and create our keys
    for (NSMutableDictionary *_ContactsListParameters in contactsListArray)
    {
        if ([[_ContactsListParameters objectForKey:@"Name"] length]>0)
        {
            NSString *indexStr = [[_ContactsListParameters
                                   objectForKey:@"Name"] substringToIndex:1];
            found = NO;
            for (NSString *str in [sectionsDictionary allKeys])
            {
                if ([str isEqualToString:indexStr])
                {
                    found = YES;
                }
            }
            if (!found)
            {
                [sectionsDictionary setValue:[[NSMutableArray alloc]
                                              init] forKey:indexStr];
            }
        }
    }
    // Loop again and sort the contacts into their respective keys
    for( NSMutableDictionary *_ContactsListParameters in contactsListArray)
    {
        [[sectionsDictionary objectForKey:[[_ContactsListParameters objectForKey:@"Name"]
                                           substringToIndex:1]]
         addObject:_ContactsListParameters];
    }
    // Sort each section array
    for (NSString *key in [sectionsDictionary allKeys])
    {
        [[sectionsDictionary objectForKey:key] sortUsingDescriptors
         :[NSArray arrayWithObject:[NSSortDescriptor
                                    sortDescriptorWithKey:@"localFirstNameIP"
                                    ascending:YES]]];
    }
    NSLog(@"leave  it");
    return sectionsDictionary;
}
#pragma mark - search Bar delagate methods
//Search bar starts editing
- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    //show cancel btn of search bar
//    [_UIsarchBar
//     setShowsCancelButton:YES animated:YES];
    if(!searchingFlag==YES)
        return;
    searchingFlag = YES;
    //set scroll disable while searching
    [_UITableView setScrollEnabled:NO];
}
//search bar text change - typing text in search bar
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    [searchSectionsDict removeAllObjects];
    if([searchText length] > 0)
    {
        searchingFlag = YES;
        [_UITableView setScrollEnabled:YES];
        [self searchContacts:searchText];
    }
    else
    {
        searchingFlag = NO;
        [_UITableView setScrollEnabled:NO];
    }
    [_UITableView reloadData];
}
/*************************************************************
 Author : Parvin babi shaik
 Desc.  : seaches contacts based on text change in search bar
 Input: search bar text
 Output: searched contacts array
 *************************************************************/
- (void)searchContacts:(NSString *)searchBarText
{
    NSMutableArray *refSearchArray=[[NSMutableArray alloc] init];
    NSPredicate * predicate =
    [NSPredicate predicateWithFormat
     :@"Name contains[cd] %@ OR PhoneNumber contains[cd] %@ OR email contains[cd] %@",
     searchBarText, searchBarText,searchBarText,searchBarText];
    refSearchArray = [[contactsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    //setting searched array into alphabetical order
    searchSectionsDict=[[self prepareSectionDictionaries:refSearchArray] mutableCopy];
}
//search bar search button click call
- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [_UIsarchBar resignFirstResponder];
    [self searchContacts:theSearchBar.text];
    [_UIsarchBar
     setShowsCancelButton:YES animated:YES];
}
//search bar cancel button click
- (void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar
{
    _UIsarchBar.text = @"";
    [_UIsarchBar
     setShowsCancelButton:NO animated:YES];
    //resign key pad
    [_UIsarchBar resignFirstResponder];
    //update search flag
    searchingFlag = NO;
    //enable table scroll
    [_UITableView setScrollEnabled:YES];
    //load all connections
    [_UITableView reloadData];
}
-(void)doneSelectingContacts
{
    /// [self callRequestbuddy];
   
    NSLog(@"contacts array..%@",selectedContactsArray);
//    NSMutableArray *emeilContactsArray=[[NSMutableArray alloc] init];
//    for (NSMutableDictionary *dict in selectedContactsArray)
//    {
//        if ([[dict objectForKey:@"PhoneNumber"] length]<10 && [[dict objectForKey:@"email"] length]>2)
//        {
//            [emeilContactsArray addObject:dict];
//            [selectedContactsArray removeObject:dict];
//        }
//    }
//    NSLog(@"contacts email array..%@",emeilContactsArray);
    if ([selectedContactsArray count]>0)
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.navigationBar.barTintColor = [UIColor blackColor];
        if([MFMessageComposeViewController canSendText])
        {
            //https://itunes.apple.com/in/app/ready2ride/id1051276597?mt=8
           // controller.body = @"Join me as a buddy on Ready2Ride - the \"Ultimate Riding Companion App\".http://apple.co/2wdmym3";
//            controller.body = @"Join me as a buddy on Ready2Ride - the \"Ultimate Riding Companion App\"..[http://apple.co/2wdmym3]";
            controller.body = @"Join me as a buddy on Ready2Ride - the \"Ultimate Riding Companion App\"..[iPhone: http://apple.co/2dr13I4  Android: http://bit.ly/2dr1cLG]";
            controller.recipients = [selectedContactsArray mutableArrayValueForKey:@"PhoneNumber"];
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
        
    }
    //[_DeviceContactsViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
//            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
          /*  [self dismissViewControllerAnimated:YES completion:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
            UIAppDelegate.rearVC.selectedIndex = 4;*/
             [self callRequestbuddy];
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 2)
    {
        [self moveBackward];
    }
}
-(void)moveBackward
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)callRequestbuddy
{
 
    NSLog(@"selectedContactsArray--->%@",selectedContactsArray);
    NSString *appOwnerId   =  [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];
   
    for (int i = 0;i< selectedContactsArray.count;i++)
    {
        NSMutableDictionary *buddyDic = [[NSMutableDictionary alloc] init];
        NSString * strPhonenumber = [[selectedContactsArray  objectAtIndex:i]valueForKey:@"PhoneNumber"];
        strPhonenumber = [strPhonenumber stringByReplacingOccurrencesOfString:@"\u00a0" withString:@""];
        NSString * strEmail = [[selectedContactsArray  objectAtIndex:i]valueForKey:@"email"];
        [buddyDic setObject:strPhonenumber forKey:@"Mobile"];
        [buddyDic setObject:strEmail forKey:@"Email"];
        [buddyDic setObject:appOwnerId forKey:@"SenderId"];
        [selectBuddyArray addObject:buddyDic];

    }
       //
    NSLog(@"selectBuddyArray--->%@",selectBuddyArray);
    [[CMNetworkManager sharedInstance] postDataResponse:[NSString stringWithFormat:@"%@/SendBuddyRequest",Service_URL] param:selectBuddyArray requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** SendBuddyRequest %@ **",resultDict);
            
            if ([[resultDict stringValueForKey:@"response"] intValue]>=1) {
                
            }else
            {
                
            }
            
        }
    }];

}

@end
