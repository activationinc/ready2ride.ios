//
//  CMNetworkManager.h
//  Cyclemate
//
//  Created by Sourav on 28/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMHTTPRequestOperation.h"

@interface CMNetworkManager : NSObject

typedef void (^CMAPIRequestHandler)(NSDictionary *resultDict, NSError *error);


+ (CMNetworkManager *) sharedInstance;

- (CMHTTPRequestOperation*)getResponse:(NSString *)webServicePath requestHandler:(CMAPIRequestHandler)responseHandle;
-(CMHTTPRequestOperation *)getPostDataResponse:(NSString *)webServicePath param:(NSString *)stringPostData requestHandler:(CMAPIRequestHandler)responseHandler;
-(void)postResponse:(NSString *)url param:(NSDictionary *)params requestHandler:(CMAPIRequestHandler)responseHandler;

-(CMHTTPRequestOperation *)postDataResponse:(NSString *)webServicePath param:(id)params requestHandler:(CMAPIRequestHandler)responseHandler;

-(void )postImage:(NSString *)webServicePath withImage:(UIImage*)image param:(NSDictionary *)params requestHandler:(CMAPIRequestHandler)responseHandler;
-(void )postImage:(NSString *)webServicePath withImage:(UIImage*)image withImageName:(NSString *)imageName withImageSize:(CGSize)imageSize param:(NSDictionary *)params requestHandler:(CMAPIRequestHandler)responseHandler;

- (void)postUserDataOnClientServer:(NSString *)webServicePath  withPostParameters:(NSDictionary *)postDic   requestHandler:(CMAPIRequestHandler)responseHandler;


- (void)getResponseWithPhone:(NSString *)phoneNO requestHandler:(CMAPIRequestHandler)responseHandler;
@end
