//
//  CMNetworkManager.m
//  Cyclemate
//
//  Created by Sourav on 28/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMNetworkManager.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "UIImage+ResizeMagick.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <AssetsLibrary/AssetsLibrary.h>

#import "SDImageCache.h"
#import "SDWebImageManager.h"


@implementation CMNetworkManager

static CMNetworkManager *sharedInstance = nil;
static ALAssetsLibrary *library = nil;




// Get the shared instance and create it if necessary.
+ (id) sharedInstance {
    @synchronized(self) {
        if (sharedInstance == nil)
            sharedInstance = [[super allocWithZone:nil] init];
    }
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        
        //dictionaryOfRequests = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}
- (void)postUserDataOnClientServer:(NSString *)webServicePath withPostParameters:(NSDictionary *)postDic  requestHandler:(CMAPIRequestHandler)responseHandler{
    

    webServicePath = [webServicePath stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    webServicePath = [webServicePath stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    webServicePath = [webServicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:webServicePath]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];

    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:webServicePath parameters:postDic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
       // NSString *response = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
       // NSLog(@"description %@",response);
        
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        if (responseDict != nil){
            //NSLog(@" Response from MCFNetworkManger %@",responseDict);
            responseHandler(responseDict,nil);
        }else{
            responseHandler(nil,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error description %@",error.description);
        
        // NSString *data = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        
        if (operation.responseData) {
            if ([operation.responseData length] > 0) {
            NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
            responseHandler(responseDic,nil);
            }
            else {
            responseHandler(nil,error);
            }
        }else
            responseHandler(nil,error);
        
    }];

}
//
- (void )getResponseWithPhone:(NSString *)phoneNO requestHandler:(CMAPIRequestHandler)responseHandler{
    
    
    NSString *webServicePath = [NSString stringWithFormat:@"http://7mg.biglistof.com/7mg_r2r.php?did=%@&mn=%@",DealerID,phoneNO];
    webServicePath = [webServicePath stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    //webServicePath = @"http://7mg.biglistof.com/7mg_r2r.php?did=10067&mn=9314921770";
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:webServicePath]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:webServicePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSString *response = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"error description %@",response);
        
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        if (responseDict != nil){
            responseHandler(responseDict,nil);
        }else{
            responseHandler(nil,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error description %@",error.description);
        
       // NSString *data = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        
        if (operation.responseData) {
            if ([operation.responseData length] > 0) {
                
                NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingMutableContainers error:nil];
                
                responseHandler(responseDic,nil);
            }
            else {
                responseHandler(nil,error);

            }
        }else
            responseHandler(nil,error);

        
        
       // NSLog(@"error description %@",data);
        
        
    }];
}



- (CMHTTPRequestOperation*)getResponse:(NSString *)webServicePath requestHandler:(CMAPIRequestHandler)responseHandler{
    
    
    webServicePath = [webServicePath stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    webServicePath = [webServicePath stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    webServicePath = [webServicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"webServicePath= %@",webServicePath);
    
    NSURL *URL = [NSURL URLWithString:webServicePath];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    //request.timeoutInterval = 30.0;
    
    // Initialize Request Operation
    CMHTTPRequestOperation *requestOperation = [[CMHTTPRequestOperation alloc] initWithRequest:request];
    
    // Configure Request Operation
    [requestOperation setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Process Response Object
        
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        
        if (responseDict != nil){
            //NSLog(@" Response from MCFNetworkManger %@",responseDict);
            responseHandler(responseDict,nil);
        }else{
            responseHandler(nil,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Handle Error
        
        NSLog(@" Error from MCFNetworkManger %@",[error debugDescription]);
        
        responseHandler(nil,error);
    }];
    
    // Start Request Operation
    [requestOperation start];
    
    return requestOperation;
}


-(CMHTTPRequestOperation *)getPostDataResponse:(NSString *)webServicePath param:(NSString *)stringPostData requestHandler:(CMAPIRequestHandler)responseHandler{
    
    
    
    webServicePath = [webServicePath stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    webServicePath = [webServicePath stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    webServicePath = [webServicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"%@",webServicePath);
    NSURL *URL = [NSURL URLWithString:webServicePath];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    request.timeoutInterval = 30;
    
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[stringPostData length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[stringPostData dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Initialize Request Operation
    CMHTTPRequestOperation *requestOperation = [[CMHTTPRequestOperation alloc] initWithRequest:request];
    
    // Configure Request Operation
    [requestOperation setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Process Response Object
        
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        
        if (responseDict != nil){
            //NSLog(@" Response from MCFNetworkManger %@",responseDict);
            responseHandler(responseDict,nil);
        }else{
            responseHandler(nil,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Handle Error
        
        NSLog(@" Error from MCFNetworkManger %@",[error debugDescription]);
        
        responseHandler(nil,error);
    }];
    
    // Start Request Operation
    [requestOperation start];
    
    return requestOperation;
}

-(void)postResponse:(NSString *)url param:(NSDictionary *)params requestHandler:(CMAPIRequestHandler)responseHandler
{
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    manager.requestSerializer =  [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSString *response = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"error description %@",response);

        NSDictionary *responseDict = (NSDictionary *)responseObject;
        if (responseDict != nil){
            //NSLog(@" Response from MCFNetworkManger %@",responseDict);
            responseHandler(responseDict,nil);
        }else{
            responseHandler(nil,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error description %@",error.description);
        responseHandler(nil,error);
    }];
}


-(CMHTTPRequestOperation *)postDataResponse:(NSString *)webServicePath param:(id)params requestHandler:(CMAPIRequestHandler)responseHandler{
    
    

    webServicePath = [webServicePath stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    webServicePath = [webServicePath stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    webServicePath = [webServicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
  //  NSLog(@"%@ AND POSTDIC = %@",webServicePath,params);
     NSLog(@"%@  %@",webServicePath,params);
    
    NSURL *URL = [NSURL URLWithString:webServicePath];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    request.timeoutInterval = 30;
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];

    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postdata length]] forHTTPHeaderField:@"Content-Length"];
    
    
    [request setHTTPBody: postdata];
    

    
    // Initialize Request Operation
    CMHTTPRequestOperation *requestOperation = [[CMHTTPRequestOperation alloc] initWithRequest:request];
    
    // Configure Request Operation
    
    //[requestOperation setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [requestOperation setResponseSerializer:[AFJSONResponseSerializer serializerWithReadingOptions:0]];
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Process Response Object
        
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        
        if (responseDict != nil){
            //NSLog(@" Response from MCFNetworkManger %@",responseDict);
            responseHandler(responseDict,nil);
        }else{
            responseHandler(nil,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Handle Error
        
        NSLog(@" Error from MCFNetworkManger %@ %@",[error debugDescription],operation.response);
        
        responseHandler(nil,error);
    }];
    
    // Start Request Operation
    [requestOperation start];
    
    return requestOperation;
}
-(void )postImage:(NSString *)webServicePath withImage:(UIImage*)image withImageName:(NSString *)imageName withImageSize:(CGSize)imageSize param:(NSDictionary *)params requestHandler:(CMAPIRequestHandler)responseHandler{
    //    self.library = [[ALAssetsLibrary alloc] init];
    if (!library) library = [[ALAssetsLibrary alloc] init];
    
    
   UIImage *newImage = [image resizedImageWithMinimumSize:imageSize];
    
    NSData *imageData = UIImageJPEGRepresentation(newImage,.8);

    if (![imageName length]) imageName = [NSString stringWithFormat:@"%@_user_%d.png",[[FCUUID uuidForDevice] substringFromIndex:10],arc4random()%9999];
    
    
    NSString *imagePath = [ImageServerPath stringByAppendingPathComponent:[NSString stringWithFormat:@"uploads/%@",imageName]];
    NSLog(@"imagePath: %@",imagePath);
    NSURL *  imageURL = [NSURL URLWithString:imagePath];
    NSLog(@"imageURL: %@",imageURL);

    [[SDImageCache sharedImageCache] storeImage:newImage forKey:[[SDWebImageManager sharedManager] cacheKeyForURL:imageURL] toDisk:YES];
    
    
    [library saveImage:image toAlbum:kAPPLICATION_NAME withCompletionBlock:^(NSError *error) {
        if (error!=nil) {
            NSLog(@"Big error: %@", [error description]);
        }
    }];
    
    
    
    NSString *queryStringss = webServicePath;
    
    queryStringss = [queryStringss stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer =  [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:queryStringss parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         
         
         [formData appendPartWithFileData:imageData name:@"photos" fileName:imageName mimeType:@"image/jpeg"];
         
         
         
     }
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         
         
         // NSDictionary *dict = [responseObject objectForKey:@"Result"];
         
         NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
         
         responseHandler(responseObject,nil);
         
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@ *****", operation.responseString);
         //  NSLog(@"Error: %@ ***** %@", operation.responseString, error);
         responseHandler(nil,error);
     }];

}
-(void )postImage:(NSString *)webServicePath withImage:(UIImage*)image param:(NSDictionary *)params requestHandler:(CMAPIRequestHandler)responseHandler
{
    [self postImage:webServicePath withImage:image withImageName:[NSString stringWithFormat:@"products%d.png",arc4random()%9999 ] withImageSize:CGSizeMake(150, 150) param:params requestHandler:responseHandler];

   
        
      
}

@end
