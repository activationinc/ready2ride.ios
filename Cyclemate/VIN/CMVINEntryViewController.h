//
//  CMVINEntryViewController.h
//  7Media
//
//  Created by Parvin Babi on 25/07/17.
//  Copyright © 2017 com.ready2ride. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMHomeViewController.h"
#import "NFLanguageVC.h"
@interface CMVINEntryViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UIButton *willDoItLaterBtn;
    IBOutlet UIButton *saveBtn;
    IBOutlet UITextField *VINTextField;
    IBOutlet UIView *backView;
    IBOutlet UIScrollView *backScroll;
    
}
@property (nonatomic,strong)CMHomeViewController *_CMHomeViewController;
-(IBAction)willEnterLaterAction:(id)sender;
-(IBAction)saveVINAction:(id)sender;
- (void)cancelButtonClicked1:(NFLanguageVC*)secondDetailViewController;
@end
