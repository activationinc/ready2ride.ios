//
//  CMVINEntryViewController.m
//  7Media
//
//  Created by Parvin Babi on 25/07/17.
//  Copyright © 2017 com.ready2ride. All rights reserved.
//

#import "CMVINEntryViewController.h"
#import "NFLanguageVC.h"

@interface CMVINEntryViewController ()
{
     UIAlertView *VINAlertView;
}
@end

@implementation CMVINEntryViewController
@synthesize _CMHomeViewController;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"view frame..%f",self.view.frame.size.height);
    [backView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"signup_background"]]];
    [VINTextField setBackground:[UIImage imageNamed:@"textfiled"]];
    [VINTextField setFont:[UIFont fontWithName:@"arial" size:18]];
   // [backView setBackgroundColor:[UIColor clearColor]];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)willEnterLaterAction:(id)sender
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    //show dealer selection page
    [_CMHomeViewController showLanguageSelection];
//    // move to home
//    UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    NFLanguageVC * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"NFLanguageVC"];
//    viewController.delegate= _CMHomeViewController;
//    
//    [self presentPopupViewController:viewController animationType:MJPopupViewAnimationFade];
}
-(IBAction)saveVINAction:(id)sender
{
   //Call VIN web service
    [backView setFrame:CGRectMake(0, 0, 320, 568)];
    [VINTextField resignFirstResponder];
    [self performSelector:@selector(callVINWebService:) withObject:VINTextField.text afterDelay:0.5];
    //[self callVINWebService:VINTextField.text];
    
}
-(void)callVINWebService:(NSString *)VINStr
{
    NSLog(@"etered here");
    
    //[self endEditing:YES];
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetVIN/VIN=%@",Service_URL,VINStr];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error)
    {
        NSLog(@"*** get vin file %@ **",resultDict);
        if ([resultDict objectForKey:@"GetVINResult"] != nil && [[resultDict objectForKey:@"GetVINResult"] count]>0)
        {
            NSMutableDictionary *VINDict=[[NSMutableDictionary alloc] init];
            VINDict=[[resultDict objectForKey:@"GetVINResult"] objectAtIndex:0];
            
            NSString *alertText=[NSString stringWithFormat:@"The VIN you entered is a %@ %@ registered to %@ %@. \n Is this you?",[VINDict objectForKey:@"Brand"],[VINDict objectForKey:@"TrueModel"],[VINDict objectForKey:@"OwnerFirstName"],[VINDict objectForKey:@"OwnerLastName"]];
            [[UIAlertView alertViewWithTitle:@""
                                     message:alertText cancelButtonTitle:@"NO"
                           otherButtonTitles:@[@"Yes"]
                                   onDismiss:^(int buttonIndex)
              {
                  [self moveForward:VINDict];
              } onCancel:^{   }] show];
        }
        else
        {
             //[backView setFrame:CGRectMake(0, 0, 320, 568)];
         //   [CMHelper alertMessageWithText:@"Invalid VIN re-enter"];
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:nil cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
//            UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 80)];
//            
//            UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 270, 50)];
//            titleLabel.text = title;
//            titleLabel.font = [UIFont boldSystemFontOfSize:20];
//            titleLabel.numberOfLines = 2;
//            titleLabel.textColor = [UIColor redColor];
//            titleLabel.textAlignment = NSTextAlignmentCenter;
//            
//            [subView addSubview:titleLabel];
//            
//            UILabel *messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, 270, 50)];
//            messageLabel.text = message;
//            messageLabel.font = [UIFont systemFontOfSize:18];
//            messageLabel.numberOfLines = 2;
//            messageLabel.textColor = [UIColor redColor];
//            messageLabel.textAlignment = NSTextAlignmentCenter;
//            
//            [subView addSubview:messageLabel];
//            
//            [alertView setValue:subView forKey:@"accessoryView"];
//            [alertView show];
            //File not found
            VINTextField.text = @"";
            [self addYourVINNumber];
            
        }
    }];
}


-(void)addYourVINNumber{
    
    
    
    if (!VINAlertView) {
        
     /*   UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 80)];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 270, 50)];
        titleLabel.text = @"Invalid VIN re-enter";
        titleLabel.font = [UIFont boldSystemFontOfSize:10];
        titleLabel.numberOfLines = 2;
        titleLabel.textColor = [UIColor redColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [subView addSubview:titleLabel];
       
       
        VINAlertView = [[UIAlertView alloc]
                           initWithTitle:@""
                           message:@"Enter Your Bike VIN"
                           delegate:self
                           cancelButtonTitle:@"I'll enter later"
                           otherButtonTitles:@"Add", nil];
        [VINAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];

        [VINAlertView setValue:subView forKey:@"accessoryView"];
               /* Display a numerical keypad for this text field */
        
        VINAlertView = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"I'll enter later" otherButtonTitles:@"Add", nil];
        UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 80)];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 270, 50)];
        titleLabel.text = @"Invalid VIN re-enter";
        titleLabel.font = [UIFont boldSystemFontOfSize:16];
        titleLabel.numberOfLines = 2;
        titleLabel.textColor = [UIColor redColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [subView addSubview:titleLabel];
        
        UILabel *messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, 270, 50)];
        messageLabel.text = @"Enter Your Bike VIN";
        messageLabel.font = [UIFont systemFontOfSize:14];
        messageLabel.numberOfLines = 2;
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.textAlignment = NSTextAlignmentCenter;
        
        [subView addSubview:messageLabel];
        [VINAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [VINAlertView setValue:subView forKey:@"accessoryView"];
        
        UITextField *textField = [VINAlertView textFieldAtIndex:0];
        textField.text = @"";
        textField.keyboardType = UIKeyboardTypeDefault;
        [VINAlertView show];
    }
    
    
    
    [VINAlertView show];
    
    
    
    
}
#pragma mark - UIAlertViewDelegate

//Now below code will check if uitextfield value.
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    
    if( [inputText length] > 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    // [self.view endEditing:YES];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            
            if (![textField.text length]) {
                
                [[UIAlertView alertViewWithTitle:@""
                                         message:@"Please fill in Mobile Number." cancelButtonTitle:nil
                               otherButtonTitles:@[@"OK"]
                                       onDismiss:^(int buttonIndex)
                  {
                      [self addYourVINNumber];
                  } onCancel:^
                  {
                      [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                      [_CMHomeViewController showLanguageSelection];

                  }] show];
                
                
              }
            else
            {
                [UIAppDelegate.userDefault setObject:textField.text forKey:@"VINNUMBER"];
                 NSString *vinNimber =  [UIAppDelegate.userDefault objectForKey:@"VINNUMBER"];
                 [self performSelector:@selector(callVINWebService:) withObject:vinNimber afterDelay:0.5];
            }
        }
        else
        {
            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
            [_CMHomeViewController showLanguageSelection];
        }
    }
    
}
//Move to dealer selection page
-(void)moveForward:(NSDictionary *)result
{
    //save VIN deatils to user defaults
    //format mobile num
    NSString *unfilteredString =[result objectForKey:@"OwnerPhone"];
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *resultString = [[unfilteredString componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    NSLog (@"Result: %@", resultString);
    //save owner mobile num//[result objectForKey:@"OwnerPhone"]
    
    
    
    NSMutableDictionary *getVinDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *mutableDict = [result mutableCopy];
    for (NSString *key in [result allKeys]) {
        if ([result[key] isEqual:[NSNull null]]) {
            mutableDict[key] = @"";//or [NSNull null] or whatever value you want to change it to
        }
    }
    getVinDictionary = [mutableDict copy];
    
    NSLog (@"getVinDictionary: %@", getVinDictionary);
    
    [UIAppDelegate.userDefault setObject:[result objectForKey:@"OwnerPhone"] forKey:@"MOBILENO"];
    [UIAppDelegate.userDefault setObject:getVinDictionary forKey:@"VIN_Record"];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:getVinDictionary];
    [UIAppDelegate.userDefault setObject:data forKey:CycleDealerInfoKey];
 
    /*
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:@"10102" forKey:@"DealerId"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    [UIAppDelegate.userDefault setObject:data forKey:CycleDealerInfoKey];
    
    NSDictionary *userDic = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleDealerInfoKey]];
    NSString *dealerID = [userDic stringValueForKey:@"DealerId"];
    */
    
    //Show dealer selection page
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [_CMHomeViewController automaticallyEnterviewPOPupMobile];
 }
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [backView setFrame:CGRectMake(0, -150, 320, 568)];
   // [backScroll setContentOffset:CGPointMake(0, -150)];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [backView setFrame:CGRectMake(0, 0, 320, 568)];
    [textField resignFirstResponder];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
