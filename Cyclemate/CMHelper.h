//
//  CMHelper.h
//  Cyclemate
//
//  Created by Sourav on 27/09/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>

#import <Foundation/Foundation.h>

@interface CMHelper : NSObject<CLLocationManagerDelegate>{


    BOOL flagTrackingLocationStarts;

    CLLocationManager *locationManager;
    IRLocationStatus locationStatus;
    
    BOOL isSetLoation,isSetLocationToCurrentLocation,isAlertShown;

    BOOL isLocationAlertDisplaying;

}

@property(nonatomic)BOOL flagTrackingLocationStarts;
+(void)alertMessageWithText:(NSString *)messageText delegate:(id)sender tag:(int)alerttag;
+(NSString *)removeSpaceCharactersFromStart:(NSString *)string;
+(void)alertMessageWithText:(NSString *)messageText;

-(void)startFindingLocation:(id)sender;
@end
