//
//  CMMotorcycleView.h
//  Cyclemate
//
//  Created by Rajesh on 9/26/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPGTextField.h"
#import "TPKeyboardAvoidingScrollView.h"
@interface CMMotorcycleView : TPKeyboardAvoidingScrollView<MPGTextFieldDelegate,UITextFieldDelegate>
{
    
    @public
    IBOutlet UITextField *mcNameField;
    IBOutlet UITextField *mcYearField;
    IBOutlet UITextField *mcVINField;
    IBOutlet UITextField *mcOdometerField;
    
    IBOutlet UITextField *mcTireSizeField;
    IBOutlet UITextField *mcDateTireInField;
    IBOutlet UITextField *mcTirePressureField;
//    IBOutlet UITextField *mcDealerField;
    IBOutlet UITextField *mcDatePurchaseField;
    IBOutlet UITextField *mcDateOilChangeField;
    IBOutlet UITextField *mcDateServiceMainField;
   

    
    IBOutlet CMButton_DropDown *cycleBrandBtn;
    IBOutlet CMButton_DropDown *ccycleModelBtn;
    IBOutlet CMButton_DropDown *ccycleCategoryBtn;
    IBOutlet CMButton_DropDown *ccycleRideingBtn;
    
    IBOutlet CMButton_DropDown *ccycleDealerBtn;
    
    
/////for Prefances........
    
    IBOutlet UITextField *oilQtyField;
    IBOutlet UITextField *maintQtyField;
    
    IBOutlet CMButton_DropDown *resolveHealthBtn;
    IBOutlet CMButton_DropDown *primatyBikeBtn;
    
    
    IBOutlet UIButton *addHealthBtn;
    
    IBOutlet MPGTextField *cycleBrandField;
    IBOutlet MPGTextField *ccycleModelField;
    
    NSString * strBrandID;
    NSString * strModelID;

}

@property (weak, nonatomic) IBOutlet CMUIButton *takePhotoBtn;
@property (weak, nonatomic) IBOutlet CMUIButton *galleryBtn;

@property (weak, nonatomic) IBOutlet UIImageView *docImageView;
@property(nonatomic,retain)NSString *photoPath;
@property(nonatomic)BOOL isNewImage;

/////for Prefances........

-(IBAction)showresolveHealthPopUp:(id)sender;
-(IBAction)showprimatyBikePopUp:(id)sender;

-(IBAction)healthIndicaterClicked:(id)sender;
@end
