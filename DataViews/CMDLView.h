//
//  CMDLView.h
//  Cyclemate
//
//  Created by Rajesh on 9/27/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMDLView : UIView
@property (weak, nonatomic) IBOutlet CMUIButton *takePhotoBtn;
@property (weak, nonatomic) IBOutlet CMUIButton *galleryBtn;

@property (weak, nonatomic) IBOutlet UIImageView *docImageView;
@property(nonatomic,retain)NSString *photoPath;
@property(nonatomic)BOOL isNewImage;

@end
