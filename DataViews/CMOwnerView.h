//
//  CMOwnerView.h
//  Cyclemate
//
//  Created by Rajesh on 9/27/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMOwnerView : UIView
{
    
    @public
    IBOutlet UITextField *mcOwnernameField;
    IBOutlet UITextField *mcOwnerLastNameField;
    IBOutlet UITextField *addressField;
    IBOutlet UITextField *cityField;
    IBOutlet UITextField *stateField;
    IBOutlet UITextField *zipField;
    IBOutlet UITextField *mobileField;
    IBOutlet UITextField *altMobileField;
    IBOutlet UITextField *emailField;
  

}
@end
