//
//  CMMotorcycleView.m
//  Cyclemate
//
//  Created by Rajesh on 9/26/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMMotorcycleView.h"
#import "DropDownListView.h"


@interface CMMotorcycleView()<kDropDownListViewDelegate,UITextFieldDelegate,MPGTextFieldDelegate>{
    
    NSArray *arryList;
    NSArray *arryIDs;
    NSMutableArray *arryBrandList;
    NSMutableArray *arryModelList;
    NSArray *arryFliter;
    DropDownListView * Dropobj;
    CMButton_DropDown *selectedButton;
    NSString *strFindBrandName;
}
-(IBAction)showMotorcycleBrandPopUp:(id)sender;
-(IBAction)showMotorcycleModelPopUp:(id)sender;
-(IBAction)showMotorcycleCategoryPopUp:(id)sender;




@end

@implementation CMMotorcycleView
@synthesize photoPath;
@synthesize isNewImage;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization codew
        [self awakeFromNib];
    }
    return self;
}
- (void)awakeFromNib{
    
    // Initialization code
     [cycleBrandField setDelegate:self];
     [ccycleModelField setDelegate:self];
     [self showMotorcycleBrandPopUp];
    strFindBrandName = [[NSString alloc]init];
    NSArray *contactList = [NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:OwnerCycleListKey]];
//    if ([contactList count]==1) {
//        NSDictionary *dic = [contactList objectAtIndex:0];
//        [primatyBikeBtn setTitle:[dic stringValueForKey:@"Name"] forState:UIControlStateNormal];
//        [primatyBikeBtn setSelectedObjectID:[NSString stringWithFormat:@"%@",[dic stringValueForKey:@"CycleId"]]];
//        [primatyBikeBtn setEnabled:NO];
//
//    }else  if ([contactList count]>1){
//        [primatyBikeBtn setEnabled:YES];
//
//    
//    }
//    else{
//        [primatyBikeBtn setEnabled:NO];
//    }
  
    if ([contactList count]>=1){
        
        NSDictionary *dic = [contactList objectAtIndex:0];
        [primatyBikeBtn setTitle:[dic stringValueForKey:@"Name"] forState:UIControlStateNormal];
        [primatyBikeBtn setSelectedObjectID:[NSString stringWithFormat:@"%@",[dic stringValueForKey:@"CycleId"]]];
        [primatyBikeBtn setEnabled:YES];
        
        
    }
    else{
        [primatyBikeBtn setEnabled:NO];
    }

    
  

    
    photoPath = @"";
    
    [_takePhotoBtn setStatus:CMMotorcycleViewType];
    [_galleryBtn setStatus:CMMotorcycleViewType];
    
    
    [mcDateTireInField setTextFieldType:@"Date"];
    [mcDatePurchaseField setTextFieldType:@"Date"];
    [mcDateOilChangeField setTextFieldType:@"Date"];
    [mcDateServiceMainField setTextFieldType:@"Date"];
    
    
    [mcYearField setTextFieldType:@"Number"];
    [mcOdometerField setTextFieldType:@"Number"];
    [mcTireSizeField setTextFieldType:@"Number"];
    [mcTirePressureField setTextFieldType:@"Number"];
    
    [oilQtyField setTextFieldType:@"Number"];
    [maintQtyField setTextFieldType:@"Number"];
    [mcVINField setDelegate:self];
    
    //prepopulate fields with VIN details
    if ([[UIAppDelegate.userDefault objectForKey:@"VIN_Record"] isKindOfClass:[NSMutableDictionary class]] && !([UIAppDelegate.userDefault objectForKey:@"VIN_Record"]==NULL))
         {
             [self prePopulateTextFieldsWithVINDetails:[UIAppDelegate.userDefault objectForKey:@"VIN_Record"]];
         }
    
    
    //Number
  
   // arryList=@[@"Royal Enfield",@"Hero MotoCorp",@"Ideal Jawa",@"Australlia",@"LML",@"Mahindra & Mahindra Limited",@"Metropolitan II (CHF50P)",@"Express (NC50)",@"Elite E (SB50)",@"Mini Trail (Z50A)"];

}

#pragma mark
#pragma mark IBAction methods


-(IBAction)healthIndicaterClicked:(id)sender{
    
    [addHealthBtn setTag:![addHealthBtn tag]];
    if ([addHealthBtn tag])
        [addHealthBtn setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    else
        [addHealthBtn setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
-(IBAction)showresolveHealthPopUp:(id)sender{
    [self endEditing:YES];
    selectedButton = (CMButton_DropDown *)sender;
    
    
    arryList = [NSArray arrayWithObjects:@"Dealer",@"Myself", nil];
    arryIDs  = [NSArray arrayWithObjects:@"0",@"1", nil];
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Resolver" withOption:arryList xy:selectedButton.frame.origin size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
}

-(IBAction)showprimatyBikePopUp:(id)sender{
    [self endEditing:YES];
    selectedButton = (CMButton_DropDown *)sender;

    CGRect frameInRootView = [selectedButton convertRect:selectedButton.bounds toView:self];
    
    CGPoint POINTS =   self.contentOffset;
    POINTS.x =frameInRootView.origin.x;
    POINTS.y = frameInRootView.origin.y+frameInRootView.size.height-200;

    NSString *ownerId    = [[NSKeyedUnarchiver unarchiveObjectWithData:[UIAppDelegate.userDefault objectForKey:CycleValidationUUIDKey]] stringValueForKey:@"OwnerId"];

    
   
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,ownerId];
    
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getcategory %@ **",resultDict);
            
            if ([[resultDict objectForKey:@"GetCycleResult"] count]>1) {
                arryList = [[resultDict objectForKey:@"GetCycleResult"] valueForKey:@"Name"];
                arryIDs = [[resultDict objectForKey:@"GetCycleResult"] valueForKey:@"CycleId"];
                
                [Dropobj fadeOut];
                [self showPopUpWithTitle:@"Select Cycle" withOption:arryList xy:POINTS size:CGSizeMake(287, MIN(200, arryList.count*50+50))  isMultiple:NO];
                

            }
            else
                selectedButton.enabled = NO;
        }
    }];
}


-(IBAction)showDealerBtnPopUp:(id)sender{
    selectedButton = (CMButton_DropDown *)sender;

    [self endEditing:YES];
    selectedButton.enabled = NO;
    NSLog(@"showMotorcycleBrandPopUp");
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetDealer/DealerId=0",Service_URL];

    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [sender setEnabled:YES];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            arryList = [[resultDict objectForKey:@"GetDealerResult"] valueForKey:@"Name"];
            arryIDs = [[resultDict objectForKey:@"GetDealerResult"] valueForKey:@"DealerId"];
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Dealer" withOption:arryList xy:selectedButton.frame.origin size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
            
        }
    }];

    
}




-(IBAction)showMotorcycleBrandPopUp:(id)sender{
    NSLog(@"showMotorcycleBrandPopUp");
    selectedButton = (CMButton_DropDown *)sender;
    
    [self endEditing:YES];
    selectedButton.enabled = NO;

    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetBrand/BrandId=0",Service_URL];

    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [sender setEnabled:YES];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            arryList = [[resultDict objectForKey:@"GetBrandResult"] valueForKey:@"BrandName"];//[[NSArray arrayWithObject:@"Motorcycle Brand"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetBrandResult"] valueForKey:@"BrandName"]];
            arryIDs = [[resultDict objectForKey:@"GetBrandResult"] valueForKey:@"BrandId"];//[[NSArray arrayWithObject:@"0"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetBrandResult"] valueForKey:@"BrandId"]];
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Brand" withOption:arryList xy:CGPointMake(16, 0) size:CGSizeMake(287, 280) isMultiple:NO];
            
        }
    }];

   
}


-(void)showMotorcycleBrandPopUp{
    NSLog(@"showMotorcycleBrandPopUp--->%@",strBrandID);
    
//    [self endEditing:YES];
//    selectedButton.enabled = NO;
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetBrand/BrandId=0",Service_URL];
    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            arryBrandList = [[NSMutableArray alloc]init];
            arryBrandList = [resultDict objectForKey:@"GetBrandResult"];
            arryList = [[resultDict objectForKey:@"GetBrandResult"] valueForKey:@"BrandName"];//[[NSArray arrayWithObject:@"Motorcycle Brand"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetBrandResult"] valueForKey:@"BrandName"]];
            arryIDs = [[resultDict objectForKey:@"GetBrandResult"] valueForKey:@"BrandId"];//[[NSArray arrayWithObject:@"0"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetBrandResult"] valueForKey:@"BrandId"]];
 //           [Dropobj fadeOut];
 //           [self showPopUpWithTitle:@"Select Brand" withOption:arryList xy:CGPointMake(16, 0) size:CGSizeMake(287, 280) isMultiple:NO];
            if ([[UIAppDelegate.userDefault objectForKey:@"VIN_Record"] isKindOfClass:[NSMutableDictionary class]] && !([UIAppDelegate.userDefault objectForKey:@"VIN_Record"]==NULL))
            {
                for (int i = 0; i < [arryBrandList count]; i++)
                {
                    NSLog(@"%@", [[arryBrandList objectAtIndex:i] valueForKey:@"BrandName"]);
                    NSString *strCompareBrandName = [[arryBrandList objectAtIndex:i] valueForKey:@"BrandName"];
                    if ([strCompareBrandName isEqualToString:strFindBrandName] )
                    {
                        strBrandID =  [[arryBrandList objectAtIndex:i] valueForKey:@"BrandId"];
                        [self showMotorcycleModelPopUp];
                    }
                }
            }
        }
    }];
    
    
}
-(IBAction)showMotorcycleModelPopUp:(id)sender{
    
    
    if (![[cycleBrandBtn selectedObjectID] length]) {
        [CMHelper alertMessageWithText:@"First select brand."];
        return;
    }
    selectedButton = (CMButton_DropDown *)sender;
    [self endEditing:YES];
    selectedButton.enabled = NO;
    
    
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetModel/ModelId=0/BrandId=%@",Service_URL,[cycleBrandBtn selectedObjectID]];

    
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [sender setEnabled:YES];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getmodel %@ **",resultDict);
         
            arryList = [[resultDict objectForKey:@"GetModelResult"] valueForKey:@"ModelName"];//[[NSArray arrayWithObject:@"Motorcycle Model"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetModelResult"] valueForKey:@"ModelName"]];
            arryIDs = [[resultDict objectForKey:@"GetModelResult"] valueForKey:@"ModelId"];//[[NSArray arrayWithObject:@"0"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetModelResult"] valueForKey:@"ModelId"]];

            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Model" withOption:arryList xy:selectedButton.frame.origin size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
            
        }
    }];

}
-(void)showMotorcycleModelPopUp{
    
    
    if ([cycleBrandField.text length] == 0) {
        [CMHelper alertMessageWithText:@"First select brand."];
        return;
    }
   // [self endEditing:YES];
   // selectedButton.enabled = NO;
    
     NSLog(@"cycleBrandField.text --- >%@",cycleBrandField.text);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@",cycleBrandField.text];
    NSArray *b = [arryBrandList filteredArrayUsingPredicate:predicate];
    // NSLog(@"barryBrandList>b-->%@",arryBrandList);
    if ([b count] > 0)
    {
        strBrandID = [[b valueForKey:@"BrandId"]objectAtIndex:0];
        
        NSString *stringURL = [NSString stringWithFormat:@"%@/GetModel/ModelId=0/BrandId=%@",Service_URL,strBrandID];
        
        if ([[UIAppDelegate.userDefault objectForKey:@"VIN_Record"] isKindOfClass:[NSMutableDictionary class]] && !([UIAppDelegate.userDefault objectForKey:@"VIN_Record"]==NULL))
        {
            
        }
        else
        {
            ccycleModelField.text = @"";
            ccycleModelField.placeholder = @"Model";
        }
        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
            
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"*** getmodel %@ **",resultDict);
                arryModelList = [[NSMutableArray alloc]init];
                arryModelList = [resultDict objectForKey:@"GetModelResult"];
                arryList = [[resultDict objectForKey:@"GetModelResult"] valueForKey:@"ModelName"];//[[NSArray arrayWithObject:@"Motorcycle Model"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetModelResult"] valueForKey:@"ModelName"]];
                arryIDs = [[resultDict objectForKey:@"GetModelResult"] valueForKey:@"ModelId"];//[[NSArray arrayWithObject:@"0"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetModelResult"] valueForKey:@"ModelId"]];
                
                // [Dropobj fadeOut];
                // [self showPopUpWithTitle:@"Select Model" withOption:arryList xy:selectedButton.frame.origin size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
                
            }
        }];

    }
   
  
    
}

-(IBAction)showMotorcycleCategoryPopUp:(id)sender{
    
    selectedButton = (CMButton_DropDown *)sender;
   
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetCategory/CategoryId=0",Service_URL];

//    [self endEditing:YES];
//    selectedButton.enabled = NO;
    
    
[[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
    [sender setEnabled:YES];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getcategory %@ **",resultDict);
            arryList = [[resultDict objectForKey:@"GetCategoryResult"] valueForKey:@"CategoryName"];//[[NSArray arrayWithObject:@"Motorcycle Category"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetCategoryResult"] valueForKey:@"CategoryName"]];
            arryIDs = [[resultDict objectForKey:@"GetCategoryResult"] valueForKey:@"CategoryId"];//[[NSArray arrayWithObject:@"0"] arrayByAddingObjectsFromArray:[[resultDict objectForKey:@"GetCategoryResult"] valueForKey:@"CategoryId"]];

            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Category" withOption:arryList xy:selectedButton.frame.origin size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
            
        }
    }];
}

-(IBAction)showMotorcycleRidingStylePopUp:(id)sender{
    
    selectedButton = (CMButton_DropDown *)sender;
    [self endEditing:YES];
    selectedButton.enabled = NO;
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetRideType/RideTypeId=0",Service_URL];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [sender setEnabled:YES];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getcategory %@ **",resultDict);
            arryList = [[resultDict objectForKey:@"GetRideTypeResult"] valueForKey:@"Name"];
            arryIDs = [[resultDict objectForKey:@"GetRideTypeResult"] valueForKey:@"RideTypeId"];

            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Riding Style" withOption:arryList xy:selectedButton.frame.origin size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
            
        }
    }];
}


#pragma mark - Collapse Click Delegate

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    [selectedButton setTitle:[arryList objectAtIndex:anIndex] forState:UIControlStateNormal];
    
    [selectedButton setSelectedObjectID:[NSString stringWithFormat:@"%@",[arryIDs objectAtIndex:anIndex]]];

    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}

#pragma mark - TextField Delegate for


- (NSArray *)dataForPopoverInTextField:(MPGTextField *)textField
{
    if ([textField isEqual:cycleBrandField])
    {
         NSLog(@"cycleBrandField.text-- > %@",cycleBrandField.text);
        if ([cycleBrandField.text length] > 0) {
           
        }
        else
        {
            ccycleModelField.text = @"";
            ccycleModelField.placeholder = @"Model";
            [CMHelper alertMessageWithText:@"First select brand."];
        }
        return [arryBrandList valueForKey:@"BrandName"];
    }
   else if ([textField isEqual:ccycleModelField])
   {
            if ([cycleBrandField.text length] == 0) {
            ccycleModelField.text = @"";
            ccycleModelField.placeholder = @"Model";
            [CMHelper alertMessageWithText:@"First select brand."];
        }
       return [arryModelList valueForKey:@"ModelName"];
       // return arryList;
    }
   else
   {
       return nil;
   }
}

- (BOOL)textFieldShouldSelect:(MPGTextField *)textField
{
    if ([textField isEqual:cycleBrandField])
    {
        if ([cycleBrandField.text length] == 0) {
            ccycleModelField.text = @"";
            ccycleModelField.placeholder = @"Model";
            [CMHelper alertMessageWithText:@"First select brand."];
        }
        else
        {
         [self performSelector:@selector(showMotorcycleModelPopUp) withObject:self afterDelay:0.0];
        }
    }
    else if ([textField isEqual:ccycleModelField])
    {
        if (arryModelList.count > 0 || [ccycleModelField.text length] > 0)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",ccycleModelField.text];
            
            NSArray *b = [arryModelList filteredArrayUsingPredicate:predicate];
           
            if ([b count] > 0)
            {
             strModelID = [[b valueForKey:@"ModelId"]objectAtIndex:0];
            }
            else
            {
                strModelID=@"";
            }
        }
     }
    return YES;
}

- (void)textField:(MPGTextField *)textField didEndEditingWithSelection:(NSDictionary *)result
{
    

    if ([textField isEqual:cycleBrandField])
    {
      //   [self performSelector:@selector(showMotorcycleModelPopUp) withObject:self afterDelay:0.0];
    }
    else if([textField isEqual:ccycleModelField])
    {
        // [self performSelector:@selector(showMotorcycleModelPopUp) withObject:self afterDelay:0.0];
    }
    
}
//@Parvin: added code to fetch VIN details
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //call vin service
    if(textField==mcVINField)
    {
        [SVProgressHUD showWithStatus:@"Fetching VIN Details" maskType:SVProgressHUDMaskTypeGradient];
        [self callVINWebService:textField.text];
    }
    
    else if ([textField isEqual:ccycleModelField])
    {
        //if not models
        if (arryModelList.count > 0 || [ccycleModelField.text length] > 0)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",ccycleModelField.text];
            
            NSArray *b = [arryModelList filteredArrayUsingPredicate:predicate];
            
            if ([b count] > 0)
            {
                strModelID = [[b valueForKey:@"ModelId"]objectAtIndex:0];
            }
            else
            {
                [CMHelper alertMessageWithText:@"Please select from drop down."];
                ccycleModelField.text=@"";
            }
            
            
        }
    }
    return YES;
}
-(void)callVINWebService:(NSString *)VINStr
{
    NSLog(@"etered here");
    
    [self endEditing:YES];
    
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetVIN/VIN=%@",Service_URL,VINStr];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        
        NSLog(@"*** get vin file %@ **",resultDict);
        if ([resultDict objectForKey:@"GetVINResult"] != nil && [[resultDict objectForKey:@"GetVINResult"] count]>0)
        {
            NSMutableDictionary *VINDict=[[NSMutableDictionary alloc] init];
            VINDict=[[resultDict objectForKey:@"GetVINResult"] objectAtIndex:0];
            [SVProgressHUD dismiss];
            [self prePopulateTextFieldsWithVINDetails:VINDict];
            
            
        }
        else
        {
            //File not found
            [SVProgressHUD dismiss];
        }
    }];
}
-(void)prePopulateTextFieldsWithVINDetails:(NSDictionary *)VINDict
{
    //populating fields
    mcVINField.text=[VINDict objectForKey:@"VINNumber"];
    mcNameField.text=[VINDict objectForKey:@"ProductName"];
    //mcNameField=[resultDict objectForKey:@"DealerName"];
    cycleBrandField.text=[VINDict objectForKey:@"Brand"];
    ccycleModelField.text=[VINDict objectForKey:@"TrueModel"];
    mcYearField.text=[VINDict objectForKey:@"ModelYear"];
//    strModelID = [VINDict objectForKey:@"ModelID"];
//    strBrandID = [VINDict objectForKey:@"MakeID"];
    
    strFindBrandName = [VINDict objectForKey:@"Brand"];
    
//    for (int i = 0; i < [arryBrandList count]; i++)
//    {
//        NSLog(@"%@", [[arryBrandList objectAtIndex:i] valueForKey:@"BrandName"]);
//        NSString *strCompareBrandName = [[arryBrandList objectAtIndex:i] valueForKey:@"BrandName"];
//        if ([strCompareBrandName isEqualToString:strFindBrandName] )
//        {
//            strBrandID =  [[arryBrandList objectAtIndex:i] valueForKey:@"BrandName"];
//            [self showMotorcycleModelPopUp];
//        }
//    }

    NSString *recDate = [VINDict objectForKey:@"SoldDate"];
    if (recDate==(id) [NSNull null] || [recDate length]==0 || [recDate isEqualToString:@"(null)"]||[recDate isKindOfClass:[NSNull class]])
    {
        mcDatePurchaseField.text = @" ";
    }
    else
    {
        NSString *newString1 = [recDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
        NSArray *listItems = [newString1 componentsSeparatedByString:@"-"];
        NSString* dayString = [listItems objectAtIndex: 0];
        double timeStamp = [dayString doubleValue];
        NSTimeInterval timeInterval=timeStamp/1000;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"MM-dd-yyyy"];
        mcDatePurchaseField.text=[dateformatter stringFromDate:date];
    }
   
    
    NSString *lastServiceDate = [VINDict objectForKey:@"LastserviceDate"];
    NSLog(@"lastServiceDate ==>%@",lastServiceDate);
    if (lastServiceDate==(id) [NSNull null] || [lastServiceDate length]==0 || [lastServiceDate isEqualToString:@"(null)"]||[lastServiceDate isKindOfClass:[NSNull class]])
    {
        mcDateServiceMainField.text = @" ";
    }
    else
    {
        NSString *newString1 = [lastServiceDate stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
        NSLog(@"newString1 ==>%@",newString1);
        NSArray *listItems = [newString1 componentsSeparatedByString:@"+"];
        NSString* dayString = [listItems objectAtIndex: 0];
        double timeStamp = [dayString doubleValue];
        NSTimeInterval timeInterval=timeStamp/1000;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"MM-dd-yyyy"];
        mcDateServiceMainField.text=[dateformatter stringFromDate:date];
    }

    
    //mcDatePurchaseField.text=[VINDict objectForKey:@"RecordDate"];
}
@end
