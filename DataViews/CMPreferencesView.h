//
//  CMPreferencesView.h
//  Cyclemate
//
//  Created by Rajesh on 9/27/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DropDownListView.h"


@interface CMPreferencesView : UIView<kDropDownListViewDelegate>{
    DropDownListView * Dropobj;
    NSArray *arryList;
    NSArray *arryIDs;
    @public
    IBOutlet UITextField *oilQtyField;
    IBOutlet UITextField *maintQtyField;

    IBOutlet CMButton_DropDown *resolveHealthBtn;
    IBOutlet CMButton_DropDown *primatyBikeBtn;


    IBOutlet UIButton *addHealthBtn;
    
    CMButton_DropDown *selectedButton;
    

}
-(IBAction)showresolveHealthPopUp:(id)sender;
-(IBAction)showprimatyBikePopUp:(id)sender;

-(IBAction)healthIndicaterClicked:(id)sender;
@end
