//
//  CMHealthView.h
//  Cyclemate
//
//  Created by Rajesh on 9/27/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMHealthView : TPKeyboardAvoidingScrollView
{
    @public
    IBOutlet UIImageView *alertImageView;
    IBOutlet UILabel *healthLbl;
    IBOutlet UIView *stripView;
    IBOutlet UIImageView *alerTypeImg;
    
    IBOutlet UILabel *bikeNameLbl;
    IBOutlet UIButton *healthStatusBtn;

    
    IBOutlet UIActivityIndicatorView *inficatorView;

}
@end
