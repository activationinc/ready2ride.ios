//
//  CMSCRequestService.h
//  Cyclemate
//
//  Created by Rajesh on 9/30/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownListView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface CMSCRequestService : TPKeyboardAvoidingScrollView<kDropDownListViewDelegate>{

    DropDownListView * Dropobj;
    NSMutableArray  *arryList;
    NSArray  *arryIDs;
    @public
    
    IBOutlet UITextField *mcModelField;
    IBOutlet UITextField *mcBrandField;
    IBOutlet UITextField *mcYearField;
    IBOutlet UITextField *mcReqDateField;
    IBOutlet UITextField *mcReqTimeField;

    
    IBOutlet UITextField *mcdealerLocField;
    IBOutlet UITextField *mcVINField;
    IBOutlet UITextField *mclastNameField;
    IBOutlet UITextField *mcFirstNameField;
    IBOutlet UITextField *mcPhField;
    IBOutlet UITextField *mcAlPhField;
    IBOutlet UITextField *mcemailField;
    IBOutlet UITextField *mcMemoField;
    
    IBOutlet UITextField *mcCurrMilageField;

    IBOutlet UITextField *otherDesField;

    IBOutlet  UILabel *mcMemoLbl;
    IBOutlet  UIView *bottomView;

    IBOutlet UIButton *serviceCatBtn;
    
    IBOutlet CMButton_DropDown *dealerLocBtn;
    IBOutlet CMButton_DropDown *serviceCategoryBnt;


    
    
     CMButton_DropDown *selectedBtn;
    
    
    IBOutlet UIImageView *alertImageView;
    IBOutlet UILabel *healthLbl;
    IBOutlet UIImageView *alerTypeImg;
    IBOutlet UILabel *bikeNameLbl;

    IBOutlet UIActivityIndicatorView *inficatorView;
    
    IBOutlet UIButton *healthStatusBtn;


}
-(IBAction)showServiceCategoryPopUp:(id)sender;
-(IBAction)showDelerLocPopUp:(id)sender;

@property(nonatomic,retain)NSString *delerId;

@end
