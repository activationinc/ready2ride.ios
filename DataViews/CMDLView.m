//
//  CMDLView.m
//  Cyclemate
//
//  Created by Rajesh on 9/27/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMDLView.h"

@implementation CMDLView
@synthesize photoPath;
@synthesize isNewImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization codew
        [self awakeFromNib];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
    
    photoPath = @"";
    [_takePhotoBtn setStatus:CMDLViewType];
    [_galleryBtn setStatus:CMDLViewType];
    
    

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
