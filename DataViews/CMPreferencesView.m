//
//  CMPreferencesView.m
//  Cyclemate
//
//  Created by Rajesh on 9/27/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMPreferencesView.h"

@implementation CMPreferencesView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}
- (void)awakeFromNib
{
    
    //   [mcDateServiceMainField setTextFieldType:@"Date"];
    
    [oilQtyField setTextFieldType:@"Number"];
    [maintQtyField setTextFieldType:@"Number"];
    
}

-(IBAction)healthIndicaterClicked:(id)sender{

    [addHealthBtn setTag:![addHealthBtn tag]];
    if ([addHealthBtn tag])
        [addHealthBtn setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    else
        [addHealthBtn setBackgroundImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
    

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(IBAction)showresolveHealthPopUp:(id)sender{
    [self endEditing:YES];
    selectedButton = (CMButton_DropDown *)sender;

    
    arryList = [NSArray arrayWithObjects:@"Dealer",@"Myself", nil];
    arryIDs  = [NSArray arrayWithObjects:@"0",@"1", nil];
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Resovler" withOption:arryList xy:CGPointMake(16, 0) size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
}

-(IBAction)showprimatyBikePopUp:(id)sender{
    [self endEditing:YES];

        selectedButton = (CMButton_DropDown *)sender;
      NSString *stringURL = [NSString stringWithFormat:@"%@/GetCycle/CycleId=0/OwnerId=%@",Service_URL,@"1"];

    
        [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
            if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
                NSLog(@"*** getcategory %@ **",resultDict);
                arryList = [[resultDict objectForKey:@"GetCycleResult"] valueForKey:@"Name"];
                arryIDs = [[resultDict objectForKey:@"GetCycleResult"] valueForKey:@"CycleId"];
                
                [Dropobj fadeOut];
                [self showPopUpWithTitle:@"Select Cycle" withOption:arryList xy:CGPointMake(16, 0) size:CGSizeMake(287, MIN(200, arryList.count*50+50))  isMultiple:NO];
                
            }
        }];
}
    
    
#pragma mark - Collapse Click Delegate
    
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
        
        
        Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
        Dropobj.delegate = self;
        [Dropobj showInView:self animated:YES];
        
        /*----------------Set DropDown backGroundColor-----------------*/
        [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
        
    }
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
        /*----------------Get Selected Value[Single selection]-----------------*/
        [selectedButton setTitle:[arryList objectAtIndex:anIndex] forState:UIControlStateNormal];
        [selectedButton setSelectedObjectID:[NSString stringWithFormat:@"%@",[arryIDs objectAtIndex:anIndex]]];

}

- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
        
        /*----------------Get Selected Value[Multiple selection]-----------------*/
        
#if 0
        if (ArryData.count>0) {
            _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
            CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
            _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
        }
        else{
            _selectedMotorCyBtn.text=@"";
        }
#endif
    }
    - (void)DropDownListViewDidCancel{
        
    }
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
        UITouch *touch = [touches anyObject];
        
        if ([touch.view isKindOfClass:[UIView class]]) {
            [Dropobj fadeOut];
        }
    }


@end
