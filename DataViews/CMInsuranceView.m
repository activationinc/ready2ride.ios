//
//  CMInsuranceView.m
//  Cyclemate
//
//  Created by Rajesh on 9/27/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMInsuranceView.h"

@implementation CMInsuranceView
@synthesize photoPath;
@synthesize isNewImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization codew
        [self awakeFromNib];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
    photoPath = @"";
    
    
    
    [_takePhotoBtn setStatus:CMInsuranceViewType];
    [_galleryBtn setStatus:CMInsuranceViewType];
    
    [nsuranceExpiField setTextFieldType:@"Date"];
    
    [nsuranceCarPhField setTextFieldType:@"Number"];
    [nsuranceAccPhField setTextFieldType:@"Number"];


}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
