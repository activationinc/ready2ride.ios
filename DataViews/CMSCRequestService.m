//
//  CMSCRequestService.m
//  Cyclemate
//
//  Created by Rajesh on 9/30/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMSCRequestService.h"

@implementation CMSCRequestService
@synthesize delerId;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self awakeFromNib];
    }
    return self;
}
- (void)awakeFromNib
{
    [mcReqTimeField setTextFieldType:@"Time"];
    [mcReqDateField setTextFieldType:@"Date"];
    [mcCurrMilageField setTextFieldType:@"Number"];
    
    arryList = [NSMutableArray new];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(IBAction)showDelerLocPopUp:(id)sender
{
    [self endEditing:YES];

    selectedBtn = (UIButton*)sender;
    [selectedBtn setEnabled:NO];
    
    CGRect frameInRootView = [selectedBtn convertRect:selectedBtn.bounds toView:self];
    
    CGPoint POINTS =   self.contentOffset;
    POINTS.x =frameInRootView.origin.x;
    POINTS.y = frameInRootView.origin.y+frameInRootView.size.height;
    
    if (![self.delerId length]) {
        self.delerId = @"0";
    }
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetDealerLocation/DealerId=%@",Service_URL,DealerID];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [sender setEnabled:YES];
        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            
            if ([arryList count])  [arryList removeAllObjects];
           
            for (id obj in [resultDict objectForKey:@"GetDealerLocationResult"] ) {
                [arryList addObject:[NSString stringWithFormat:@"%@, %@",[obj stringValueForKey:@"DealerName"],[obj stringValueForKey:@"City"]]];
            }
            
           // arryList
            arryIDs = [[resultDict objectForKey:@"GetDealerLocationResult"] valueForKey:@"DealerLocationId"];
            [Dropobj fadeOut];
            [self showPopUpWithTitle:@"Select Dealer Location" withOption:arryList xy:POINTS size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
            
        }
    }];

}
-(IBAction)showServiceCategoryPopUp:(id)sender{
    [self endEditing:YES];
//NSArray  *
    selectedBtn = (CMButton_DropDown*)sender;
    [selectedBtn setEnabled:NO];

    
    CGPoint POINTS =   self.contentOffset;
    POINTS.x = 10;
    
    NSString *stringURL = [NSString stringWithFormat:@"%@/GetServiceCategory/ServiceCategoryId=0",Service_URL];
    
    [[CMNetworkManager sharedInstance] getResponse:stringURL requestHandler:^(NSDictionary *resultDict, NSError *error) {
        [sender setEnabled:YES];

        if (resultDict != nil && [resultDict isKindOfClass:[NSDictionary class]]) {
            NSLog(@"*** getbrands %@ **",resultDict);
            arryList = [[resultDict objectForKey:@"GetServiceCategoryResult"] valueForKey:@"Name"];
            arryIDs = [[resultDict objectForKey:@"GetServiceCategoryResult"] valueForKey:@"ServiceCategoryId"];
            [Dropobj fadeOut];
           [self showPopUpWithTitle:@"Select Service Category" withOption:arryList xy:POINTS size:CGSizeMake(287, MIN(280, arryList.count*50+50)) isMultiple:NO];
            
        }
    }];


}

#pragma mark - Collapse Click Delegate

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    [selectedBtn setTitle:[arryList objectAtIndex:anIndex] forState:UIControlStateNormal];
    
    
    [selectedBtn setSelectedObjectID:[NSString stringWithFormat:@"%@",[arryIDs objectAtIndex:anIndex]]];

    
    if ([[selectedBtn currentTitle] isEqualToString:@"Other"]) {
        
        if (otherDesField.hidden) {
            otherDesField.hidden = NO;
            int margin = 30;
            
           CGRect fremae =  mcMemoField.frame;
            fremae.origin.y+=margin;
            mcMemoField.frame = fremae;

           fremae = mcMemoLbl.frame;
            fremae.origin.y+=margin;
            mcMemoLbl.frame = fremae;
            
            
            fremae = bottomView.frame;
            fremae.origin.y+=margin;
            bottomView.frame = fremae;
            

            self.contentSize = CGSizeMake(320, 560+(IS_IPHONE_5?0:88)+margin);

        }
    }else{
    
        if (!otherDesField.hidden) {
            otherDesField.hidden = YES;
            int margin = -30;
            
            CGRect fremae =  mcMemoField.frame;
            fremae.origin.y+=margin;
            mcMemoField.frame = fremae;
            
            fremae = mcMemoLbl.frame;
            fremae.origin.y+=margin;
            mcMemoLbl.frame = fremae;
            
            fremae = bottomView.frame;
            fremae.origin.y+=margin;
            bottomView.frame = fremae;
            
            self.contentSize = CGSizeMake(320, 560+(IS_IPHONE_5?0:88));
            
        }
    }

}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    
#if 0
    if (ArryData.count>0) {
        _selectedMotorCyBtn.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_selectedMotorCyBtn];
        _selectedMotorCyBtn.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _selectedMotorCyBtn.text=@"";
    }
#endif
}

- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}


@end
