//
//  CMUIButton.m
//  Cyclemate
//
//  Created by Rajesh on 9/26/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMUIButton.h"

@implementation CMUIButton
@synthesize status;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [[self layer] setBorderWidth:4.0f];
        [[self layer] setCornerRadius:4.0f];
        [[self layer] setBorderColor:[UIColor blackColor].CGColor];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
    
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setCornerRadius:8.0f];
    [[self layer] setBorderColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f  blue:210.0f/255.0f  alpha:1.0].CGColor];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
