//
//  Utilities.m
//  ServeItUp
//

#import "Utilities.h"


@implementation Utilities
+ (BOOL)isEmailValid:(NSString*)mEmail
{
    BOOL flag = FALSE;
    if(![mEmail isEqualToString:@""])
    {
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        //Valid email address
        if ([emailTest evaluateWithObject:mEmail] == YES)
        {
            flag = TRUE;
        }
        else
        {
            flag = FALSE;
        }
    }
    else{
        flag = FALSE;
    }
    
    return flag;
}
+ (BOOL)MobileNumberValidate:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if (([numberTest evaluateWithObject:number] == YES))
        return TRUE;
    else
        return FALSE;
}
+ (BOOL) isNumeric:(NSString*)number
{
    NSString *emailRegex = @"[0-9]+";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:number];
    
}
@end