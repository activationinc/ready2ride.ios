//
//  NSDictionary+NSDictionary_JSONEncoding.m
//  MCWCoreFoundation
//
//  Created by Jeevanantham Balusamy on 3/3/14.
//  Copyright (c) 2014 MyCityWay. All rights reserved.
//

#import "NSDictionary+NSDictionary_JSONEncoding.h"
#import "NSString+Additions.h"

@implementation NSDictionary (NSDictionary_JSONEncoding)

- (NSString*)stringValueForKey:(id)aKey
{
    NSString *string = @"";
    id object = [self objectForKey:aKey];
    if ([object isKindOfClass:[NSString class]]) {
        string = [(NSString*)object stringByReplacingEscapeSpecialCharacters];
    }
    else if ([object isKindOfClass:[NSNumber class]]) {
        string = [NSString stringWithFormat:@"%@",object];
    }
    return string;
}

@end
