//
//  NSDictionary+NSDictionary_JSONEncoding.h
//  MCWCoreFoundation
//
//  Created by Jeevanantham Balusamy on 3/3/14.
//  Copyright (c) 2014 MyCityWay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionary_JSONEncoding)

- (NSString*)stringValueForKey:(id)aKey;

@end
