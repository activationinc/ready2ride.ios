//
//  CMTextField.m
//  Cyclemate
//
//  Created by Rajesh on 10/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "CMButton_DropDown.h"
#import <objc/runtime.h>

@implementation CMButton_DropDown

@dynamic selectedObjectID;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

NSString const *key = @"my.very.unique.key";

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self awakeFromNib];
       // [self setSelectedObjectID:@""];
    }
    return self;
}
-(void)awakeFromNib{
[self setSelectedObjectID:@"0"];
   // self.titleLabel.font = [UIFont systemFontOfSize:15];
    //self.titleLabel.textColor = [UIColor blackColor];
   // self.titleLabel.font = [UIFont systemFontOfSize:13];
    self.titleLabel.font   = [UIFont fontWithName:@"Arial" size:13];
    
    self.titleLabel.textColor  =  [UIColor colorWithRed:26.0f/255.0f green:26.0f/255.0f  blue:26.0f/255.0f  alpha:1];
    

}

- (void)setSelectedObjectID:(NSString *)selectedObjectID
{
    
    if (![selectedObjectID length])
        selectedObjectID = @"0";
    
   
    objc_setAssociatedObject(self, &key, selectedObjectID, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    

}

- (NSString *)selectedObjectID
{
    return objc_getAssociatedObject(self, &key);
}





@end
