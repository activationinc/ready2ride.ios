//
//  NSString+Additions.h
//  MCWCoreFoundation
//
//  Created by Jeevanantham Balusamy on 3/3/14.
//  Copyright (c) 2014 MyCityWay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)


/**
 *  String be decoding XML charcters.
 *  &amp -> &
 *  &apos; -> '
 *  &quot; -> \"
 *  &lt; -> <
 *  &gt; -> >
 *
 *  @return decoded string
 */
- (NSString *)stringByDecodingXMLEntities;

/**
 *  String by replacing the special charaters.
 *  &#44; -> ,
 *  &#34; -> \\\"
 *  &#92; -> \\
 *
 */
- (NSString *)stringByReplacingEscapeSpecialCharacters;

/**
 *  Remove all HTML tags.
 *
 *  @return Plain Text String.
 */
- (NSString *)flattenHTMLString;

/**
 *  Validate the email address format.
 *
 */
- (BOOL)isValidEmailAddress;

/**
 *  Validate the first name.
 *
 */
- (BOOL)isValidFirstName;

/**
 *  String encoding with URL escape characters, this can be used to compose URL with special chancters.
 *  & -> %26
 *  + -> %2B
 *  , -> %2C
 *  / -> %2F
 *  : -> %3A
 *  ; -> %3B
 *  = -> %3D
 *  ? -> %3F
 *  @ -> %40
 *  " " -> %20
 *  \t -> %09
 *  # -> %23
 *  < -> %3C
 *  > -> %3E
 *  \" -> %22
 *  \n -> %0A
 *
 *  @return encoded string
 */
- (NSString *)stringByAddingPercentEscapesUsingEncoding;

/**
 *  This method will return size for text with given font.
 *
 *  @param font   UIFont for that string will be displayed.
 *  @param width  default width is screen width.
 *  @param height default height is 99999.0
 *
 *  @return calculated size
 */
- (CGSize)sizeForStringWithFont:(UIFont*)font width:(NSNumber*)width height:(NSNumber*)height;

/**
 *  Number will be suffixed with Ordinal Indicators @"th", @"st", @"nd", @"rd", @"th", @"th", @"th", @"th", @"th", @"th"
 *
 *  @param number number to be suffixed with ordinal indicator i.e 10
 *
 *  @return NSString from given number with ordinal indicator i.e 10th
 */
+ (NSString *)stringFromNumberWithOrdinalIndicatorsSuffixed:(NSNumber*)number;

/**
 *  Return NSDate from the given date in NSString format
 *
 *  @param format date format like 'yyyyMMdd'T'HH:mm:ss'
 *
 *  @return NSDate for given date format
 */
- (NSDate *)dateFromStringWithFormat:(NSString*)format;

/**
 *  Return date in NSString instance
 *
 *  @param date   will convert into NSString object
 *  @param format date format like 'yyyyMMdd'T'HH:mm:ss'
 *
 *  @return NSString value of given date and format
 */
+ (NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString*)format;


/**
 *  Return currency formatted string.
 *
 */
- (NSString*)stringCurrencyFormatUSD;

/**
 *  Return currency formatted string.
 */
+ (NSString*)stringCurrencyFormatUSDWithNumber:(NSNumber*)number;
@end