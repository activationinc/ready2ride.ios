//
//  CMTextField.m
//  Cyclemate
//
//  Created by Rajesh on 10/10/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//


#import "UITextField+CMTextField_BK.h"
#import <objc/runtime.h>
#import "CMTextField.h"

@implementation UITextField(CMTextField_BK)
@dynamic textFieldType;

NSString const *myKey = @"textFieldType.unique.key";

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setTextFieldType:(NSString *)textFieldType
{
    objc_setAssociatedObject(self, &myKey, textFieldType, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    
}

- (NSString *)textFieldType
{
    return objc_getAssociatedObject(self, &myKey);
}


-(void)awakeFromNib{
    
    if (![self isKindOfClass:[ CMTextField class]]) {
        [self setBackground:[UIImage imageNamed:@"textFieldBk"]];

    }
   // self.font = [UIFont systemFontOfSize:13];
    self.font = [UIFont fontWithName:@"Arial" size:13];

  //  self.textColor = [UIColor colorWithRed:215.0f/255.0f green:215.0f/255.0f  blue:215.0f/255.0f  alpha:1];
    
    self.textColor  =  [UIColor colorWithRed:26.0f/255.0f green:26.0f/255.0f  blue:26.0f/255.0f  alpha:1];


  //  [self setFont:[UIFont systemFontOfSize:12]];
}

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 5 , 0 );
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 5 , 0 );
}

@end
