//
//  NSString+Additions.m
//  MCWCoreFoundation
//
//  Created by Jeevanantham Balusamy on 3/3/14.
//  Copyright (c) 2014 MyCityWay. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (NSString *)stringByDecodingXMLEntities {
    NSUInteger myLength = [self length];
    NSUInteger ampIndex = [self rangeOfString:@"&" options:NSLiteralSearch].location;
	
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return self;
    }
    // Make result string with some extra capacity.
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
	
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:self];
	
    [scanner setCharactersToBeSkipped:nil];
	
    NSCharacterSet *boundaryCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" \t\n\r;"];
	
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            goto finish;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
        else if ([scanner scanString:@"&lt;" intoString:NULL])
            [result appendString:@"<"];
        else if ([scanner scanString:@"&gt;" intoString:NULL])
            [result appendString:@">"];
        else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
			
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
			
            if (gotNumber) {
                [result appendFormat:@"%C", (unsigned short)charCode];
				
				[scanner scanString:@";" intoString:NULL];
            }
            else {
                NSString *unknownEntity = @"";
				
				[scanner scanUpToCharactersFromSet:boundaryCharacterSet intoString:&unknownEntity];
				
				
				[result appendFormat:@"&#%@%@", xForHex, unknownEntity];
                NSLog(@"Expected numeric character entity but got &#%@%@;", xForHex, unknownEntity);
				
            }
			
        }
        else {
			NSString *amp;
			
			[scanner scanString:@"&" intoString:&amp];      //an isolated & symbol
			[result appendString:amp];
			
        }
		
    }
    while (![scanner isAtEnd]);
	
finish:
    return result;
}


- (NSString *)stringByReplacingEscapeSpecialCharacters {  //[]{}#%^*+=_\|~<>€£¥•.,?!'-/:;()$&@"
	if(self && [self isKindOfClass:[NSString class]]) {
		NSMutableString *stringOutput = [[NSMutableString alloc] initWithData:[(NSString *)self dataUsingEncoding:NSUTF8StringEncoding] encoding:NSUTF8StringEncoding];
		[stringOutput replaceOccurrencesOfString:@"&#44;" withString:@"," options:NSLiteralSearch range:NSMakeRange(0, [stringOutput length])];
		[stringOutput replaceOccurrencesOfString:@"&#34;" withString:@"\\\"" options:NSLiteralSearch range:NSMakeRange(0, [stringOutput length])];
		[stringOutput replaceOccurrencesOfString:@"&#92;" withString:@"\\" options:NSLiteralSearch range:NSMakeRange(0, [stringOutput length])];
		
		return stringOutput;
	}
	return @"";
}

//for romove HTML Tag
- (NSString *)flattenHTMLString {
	
    NSScanner *theScanner;
    NSString *text = nil;
	
    NSString *htmlString = [NSString stringWithString:self];
    theScanner = [NSScanner scannerWithString:self];
	
    while ([theScanner isAtEnd] == NO) {
		
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ;
		
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
		
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        htmlString = [htmlString stringByReplacingOccurrencesOfString:
				[ NSString stringWithFormat:@"%@>", text]
											   withString:@" "];
		
    } // while //
    
	htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    htmlString = [htmlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return htmlString;
	
}

- (BOOL) isValidEmailAddress
{
    BOOL isValid = NO;
    if (self) {
        BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
        NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        isValid = [emailTest evaluateWithObject:self];
    }
    
    return isValid;
}

- (BOOL)isValidFirstName
{
	BOOL isValidName = NO;
    if (self) {
        NSString *stringRegex = @"^[a-zA-Z\\- ]+$";
        
        NSPredicate *predicateTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stringRegex];
        
        isValidName = [predicateTest evaluateWithObject:self];
    }
    
    return isValidName;
}


#pragma mark -
#pragma mark Encoding URL Method
- (NSString *)stringByAddingPercentEscapesUsingEncoding
{
    NSString *stringURL = [NSString stringWithString:self];
	stringURL = [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@";" withString:@"%3B"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"?" withString:@"%3F"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"@" withString:@"%40"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"\t" withString:@"%09"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"#" withString:@"%23"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"<" withString:@"%3C"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@">" withString:@"%3E"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"\"" withString:@"%22"];
	stringURL = [stringURL stringByReplacingOccurrencesOfString:@"\n" withString:@"%0A"];
	return stringURL;
}

+ (NSString *)stringFromNumberWithOrdinalIndicatorsSuffixed:(NSNumber*)number
{
	NSArray *arraySuffixes = [[NSArray alloc] initWithObjects:@"th", @"st", @"nd", @"rd", @"th", @"th", @"th", @"th", @"th", @"th", nil];
    // handle most cases by modding by ten
	NSString *stringResult = @"";
	stringResult = [arraySuffixes objectAtIndex:([number intValue] % 10)];
	if( [number intValue] % 100 >= 11 && (int)number % 100 <= 13 ) stringResult = @"th";  // handle 11-13
	if( [number intValue] == 0 ) return @"";         // handle zero
	return stringResult;
}



+ (NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString*)format
{
    NSString *dateString = nil;
    if (date && format) {
        NSDateFormatter* dateformater = [[NSDateFormatter alloc] init];
        [dateformater setDateFormat:format];
        dateString = [dateformater stringFromDate:date];
    }

    return dateString;
}


- (NSDate *)dateFromStringWithFormat:(NSString*)format {
    
    NSDate *newDate = nil;
    if (format) {
        NSDateFormatter* dateformater = [[NSDateFormatter alloc] init];
        [dateformater setDateFormat:format];
        newDate = [dateformater dateFromString:self];
    }

    return newDate;
}

- (CGSize)sizeForStringWithFont:(UIFont*)font width:(NSNumber*)width height:(NSNumber*)height;
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGSize constrain = CGSizeMake((width ? [width floatValue] : screenSize.width), (height ? [height floatValue] : 99999.0));
    CGSize textSize = [self sizeWithFont:font constrainedToSize:constrain];
    return textSize;
}

- (NSString*)stringCurrencyFormatUSD
{
    NSNumberFormatter * numberformat = [[NSNumberFormatter alloc] init];
    numberformat.numberStyle = NSNumberFormatterCurrencyStyle;
    numberformat.currencyCode = @"USD";
    [numberformat setCurrencySymbol:@"$"];
    NSString * amountformat = [numberformat stringFromNumber: [NSNumber numberWithFloat:[self doubleValue]]];
    
    return amountformat;
}

+ (NSString*)stringCurrencyFormatUSDWithNumber:(NSNumber*)number
{
    NSNumberFormatter * numberformat = [[NSNumberFormatter alloc] init];
    numberformat.numberStyle = NSNumberFormatterCurrencyStyle;
    numberformat.currencyCode = @"USD";
    [numberformat setCurrencySymbol:@"$"];
    NSString * amountformat = [numberformat stringFromNumber:number];
    
    return amountformat;
}

@end
