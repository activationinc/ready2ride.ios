//
//  CMSytemFontOverride.m
//  Cyclemate
//
//  Created by Rajesh on 12/18/14.
//  Copyright (c) 2014 com.7 Media. All rights reserved.
//

#import "UIFont+SytemFontOverride.h"

@implementation UIFont (SytemFontOverride)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"Arial-BoldMT" size:fontSize];
}

+ (UIFont *)systemFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"Arial" size:fontSize];
}

#pragma clang diagnostic pop

@end
