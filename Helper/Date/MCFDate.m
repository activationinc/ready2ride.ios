//
//  MCFDate.m


#import "MCFDate.h"

@implementation MCFDate


+(NSDate*)dateFromString:(NSString*)date
{
    NSDate *newDate = nil;
    if (date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
       // [dateFormatter setLocale:[NSLocale currentLocale]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        newDate = [dateFormatter dateFromString:date];
    }
    return newDate;
}

+(NSDate*)dateFromString:(NSString*)date withFormat:(NSString*)format
{
    NSDate *newDate = nil;
    if (date && format) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:format];
        newDate = [dateFormatter dateFromString:date];
    }
    return newDate;
}

+ (NSString*)dateChangeDateFormat:(NSString*)date toFormat:(NSString*)requiredFormat
{
    NSString *newDateStr = nil;
    if (date && requiredFormat) {
        NSDate *currentDate = [MCFDate dateFromString:date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:requiredFormat];
        newDateStr = [dateFormatter stringFromDate:currentDate];
    }
    return newDateStr;
}

+ (NSString*)dateChangeDateFormat:(NSString*)date toFormat:(NSString*)requiredFormat currentDateFormat:(NSString*)currentFormat
{
    NSString *newDateStr = nil;
    if (date && requiredFormat && currentFormat) {
        NSDate *currentDate = [MCFDate dateFromString:date withFormat:currentFormat];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:requiredFormat];
        newDateStr = [dateFormatter stringFromDate:currentDate];
    }
    return newDateStr;
}

+ (NSString*)dateStringFromDate:(NSDate*)date format:(NSString*)format
{
    NSString *newDateStr = nil;
    if (date && format) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:format];
        /**
         *  Manual setting Local 
         */
        [dateFormatter setLocale:[NSLocale currentLocale]];
        newDateStr = [dateFormatter stringFromDate:date];
    }
    return newDateStr;
}

+ (NSString *)timeDurationFromCurrentTime:(NSString *)date
{
    NSDate *stDt = [MCFDate dateFromString:date];
    if(!stDt)
    {
        return @"";
    }
        
    NSDate *endDt =  [NSDate date];
    unsigned int unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit;
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:stDt  toDate:endDt  options:0];
    int years = [comps year];
    int months = [comps month];
    int days = [comps day];
    int hours = [comps hour];
    int minutes = [comps minute];
    
    NSString *duration = @"";
    if (years > 0) {
        
        if (years == 1) {
            duration = [NSString stringWithFormat:@"%d year ago",years];
        }
        else
            duration = [NSString stringWithFormat:@"%d years ago",years];
    }
    else if (months > 0) {
        
        if (months == 1) {
            duration = [NSString stringWithFormat:@"%d month ago",months];
        }
        else
            duration = [NSString stringWithFormat:@"%d months ago",months];
    }
    else if (days > 0) {
        
        if (days == 1) {
            duration = [NSString stringWithFormat:@"%d day ago",days];
        }
        else
            duration = [NSString stringWithFormat:@"%d days ago",days];
    }
    else if (hours > 0) {
        if (hours == 1) {
            duration = [NSString stringWithFormat:@"%d hour ago",hours];
        }
        else
            duration = [NSString stringWithFormat:@"%d hours ago",hours];
    }
    else if(minutes > 0){
        if (minutes == 1) {
            duration = [NSString stringWithFormat:@"%d min ago",minutes];
        }
        else
            duration = [NSString stringWithFormat:@"%d mins ago",minutes];
    }
    else
        duration = [NSString stringWithFormat:@"just now"];
    
    return duration;
}

+ (NSString *)timeDurationWithDate:(NSString *)date currentDateTime:(NSString *)currentDateTime
{
    NSDate *stDt = [MCFDate dateFromString:date];
    NSDate *endDt =  [MCFDate dateFromString:currentDateTime];
    unsigned int unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit;
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:stDt  toDate:endDt  options:0];
    int years = [comps year];
    int months = [comps month];
    int days = [comps day];
    int hours = [comps hour];
    int minutes = [comps minute];
    
    NSString *duration = @"";
    if (years > 0) {
        
        if (years == 1) {
            duration = [NSString stringWithFormat:@"%d yr ago",years];
        }
        else
            duration = [NSString stringWithFormat:@"%d yrs ago",years];
    }
    else if (months > 0) {
        
        if (months == 1) {
            duration = [NSString stringWithFormat:@"%d month ago",months];
        }
        else
            duration = [NSString stringWithFormat:@"%d months ago",months];
    }
    else if (days > 0) {
        
        if (days == 1) {
            duration = [NSString stringWithFormat:@"%d day ago",days];
        }
        else
            duration = [NSString stringWithFormat:@"%d days ago",days];
    }
    else if (hours > 0) {
        if (hours == 1) {
            duration = [NSString stringWithFormat:@"%d hr ago",hours];
        }
        else
            duration = [NSString stringWithFormat:@"%d hrs ago",hours];
    }
    else if(minutes > 0){
        if (minutes == 1) {
            duration = [NSString stringWithFormat:@"%d min ago",minutes];
        }
        else
            duration = [NSString stringWithFormat:@"%d mins ago",minutes];
    }
    else
        duration = [NSString stringWithFormat:@"just now"];
    
    return duration;
}

@end
