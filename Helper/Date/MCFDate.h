//
//  MCFDate.h


#import <UIKit/UIKit.h>

@interface MCFDate : NSDate

/**
 *  Returns a date representation of a given string interpreted using the default date format yyyy-MM-dd HH:mm:ss.
 *
 *  @param date The string to parse.
 *
 *  @return A date representation of string interpreted using the default date format yyyy-MM-dd HH:mm:ss. If dateFromString: can not parse the string, returns nil.
 */
+ (NSDate*)dateFromString:(NSString*)date;


/**
 *  Returns a date representation of a given string interpreted using given format.
 *
 *  @param date   The string to parse.
 *  @param format The date format of the given date.
 *
 *  @return A date representation of string interpreted using the the given date format. If dateFromString:withFormat: can not parse the string, returns nil.
 */
+ (NSDate*)dateFromString:(NSString*)date withFormat:(NSString*)format;


/**
 *  Returns a string representation of a given string interpreted using the default date format yyyy-MM-dd HH:mm:ss. And requiredFormat will be used to change the given date string into given required date format.
 *
 *  @param date           The string to parse. Date format should be yyyy-MM-dd HH:mm:ss.
 *  @param requiredFormat required date format in string type.
 *
 *  @return A string representation of string interpreted using the default yyyy-MM-dd HH:mm:ss. If dateChangeDateFormat:requiredFormat: can not parse the string, returns nil.
 */
+ (NSString*)dateChangeDateFormat:(NSString*)date toFormat:(NSString*)requiredFormat;

/**
 *  Returns a string representation of a given string interpreted using the given currentFormat. And requiredFormat will be used to change the given date string into given required date format.
 *
 *  @param date           The string to parse. Date format should be in format of given currentFormat.
 *  @param requiredFormat required date format in string type.
 *  @param currentFormat  date string's current date format will be used to interpreted.
 *
 *  @return A string representation of string interpreted using the given currentFormat. If dateChangeDateFormat:requiredFormat:currentDateFormat: can not parse the string, returns nil.
 */
+ (NSString*)dateChangeDateFormat:(NSString*)date toFormat:(NSString*)requiredFormat currentDateFormat:(NSString*)currentFormat;

/**
 *  Returns a string representation of a given date interpreted using the given format.
 *
 *  @param date   The string to parse.
 *  @param format required date format in string type.
 *
 *  @return A string representation of string interpreted using the given format. If dateStringFromDate:format: can not parse the string, returns nil.
 */
+ (NSString*)dateStringFromDate:(NSDate*)date format:(NSString*)format;

+ (NSString *)timeDurationFromCurrentTime:(NSString *)date;
+ (NSString *)timeDurationWithDate:(NSString *)date currentDateTime:(NSString *)currentDateTime;
@end
