//
//  DABoxesLayer.m
//  DAAttributedStringUtilsExample
//


#import "DABoxesLayer.h"

@implementation DABoxesLayer

- (void)drawInContext:(CGContextRef)ctx
{
	for (NSArray* box in _boxes) {
		CGRect rect = [[box objectAtIndex:0] CGRectValue];
		UIColor* color = (UIColor*)[box objectAtIndex:1];
		CGContextSetFillColorWithColor(ctx, color.CGColor);
		CGContextFillRect(ctx, rect);
	}
}

- (void) setBoxes:(NSArray *)boxes
{
	_boxes = boxes;
	[self setNeedsDisplay];
}

@end
