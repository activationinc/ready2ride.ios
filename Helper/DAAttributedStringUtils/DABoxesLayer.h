//
//  DABoxesLayer.h
//  DAAttributedStringUtilsExample
//


#import <QuartzCore/QuartzCore.h>

@interface DABoxesLayer : CALayer

// boxes must be an array of arrays, where each element contains two elements, an NSValue containing a CGRect, and a UIColor
@property (strong,nonatomic) NSArray* boxes;

@end
